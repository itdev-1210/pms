import React  from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { userActions } from '../../_actions';

class Logout extends React.Component {
    constructor(props) {
        super(props);
        this.props.dispatch(userActions.logout());

    }
    render() {
            return <Redirect to='/admin/login' />
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedLogout = connect(mapStateToProps)(Logout);
export { connectedLogout as Logout };
