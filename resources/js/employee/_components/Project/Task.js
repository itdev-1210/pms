import React from 'react'
import {
    Label, Button, Input,
    Modal, ModalBody, ModalFooter, ModalHeader, FormGroup
} from 'reactstrap';
import { Checkbox } from 'semantic-ui-react'
import Axios from 'axios';
import { NoteItem } from '.';
import DatePicker from 'react-date-picker'
import moment from 'moment'
import renderHTML from 'react-render-html'

export class ProjectTask extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpenReminder: false,
            reminderDate: null,
            reminderNote: '',
            selectFlag: false,
        }
        this.startTask = this.startTask.bind(this)
        this.completeTask = this.completeTask.bind(this)
        this.addReminder = this.addReminder.bind(this)
        this.changeReminderDate = this.changeReminderDate.bind(this)
        this.saveReminder = this.saveReminder.bind(this)
        this.completeReminder = this.completeReminder.bind(this)
        this.selectUpdatedItem = this.selectUpdatedItem.bind(this)
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.updateFlag !== nextProps.updateFlag && nextProps.updateFlag === false) {
            if (this.state.selectFlag) {
                this.setState({ selectFlag: false });
            }
        }
    }

    startTask(task) {
        Axios.post('/ajax/startTask', task)
            .then(response => {
                console.log(response)
                this.props.reload()
            })
            .catch(err => {
                console.log(err.response || err)
            })
    }
    completeTask(task) {
        Axios.post('/ajax/completeTask', task)
            .then(response => {
                console.log(response)
                this.props.reload()
            })
            .catch(err => {
                console.log(err.response || err)
            })
    }
    addReminder() {
        this.setState({ isOpenReminder: true })
    }
    addNote() {
        if (this.noteRef) {
            this.noteRef.addNewNote()
        }
    }
    changeReminderDate(date) {
        this.setState({ reminderDate: date })
    }
    saveReminder() {
        let reminder_date = this.state.reminderDate ? this.state.reminderDate.getFullYear() + "-" + ("0" + (this.state.reminderDate.getMonth() + 1)).slice(-2) + "-" + ("0" + this.state.reminderDate.getDate()).slice(-2) : null
        Axios.post('/ajax/setreminder', {
            id: this.props.task.id,
            reminder_date: reminder_date,
            notes: this.state.reminderNote
        })
            .then(response => {
                this.setState({ isOpenReminder: false })
                this.props.reload()
            })
            .catch(err => {
                console.log(err.response || err)
            })
    }
    completeReminder(item) {
        Axios.post('/ajax/finishreminder', {
            id: item.id,
        })
            .then(response => {
                this.props.reload()
            })
            .catch(err => {
                console.log(err.response || err)
            })
    }

    urlParser(notes) {
        var res = notes.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
        if (res !== null) {
            var unique = res.filter(function(elem, index, self) {
                return index === self.indexOf(elem);
            })
            unique.forEach(item => {
                let newItem = `<a href="${item.includes('https://') ? item : (item.includes('http://') ? item : 'https://' + item) }" target="_blank"><b>${item}</b></a>`;
                notes = notes.replace(item, newItem);
            });
        }

        notes = notes.replace(/\n/g, "<br>");
        return notes;
    }

    selectUpdatedItem(task) {
        const { selectFlag } = this.state;
        this.setState({ selectFlag: !selectFlag }, () => {this.props.onSelectedTasks(!selectFlag, task)});
    }

    onSort(flag) {
        const {index, length } = this.props;
        if (flag === 'up' && index === 0)
            return;

        if (flag === 'down' && (index === length - 1))
            return;

        this.props.onSort(flag, this.props.index);
    }

    render() {
        const { task, index, length } = this.props;
        return (
            <div className="d-flex">
                {this.props.updateFlag === true
                    ? <Checkbox className="mx-2" onChange={() => this.selectUpdatedItem(task)} />
                    : <></>
                }
                <Label onClick={() => this.addReminder(task)} className="mx-2"><i className="fa fa-clock-o text-primary"></i></Label>
                <Label className="mx-1" onClick={() => this.addNote()}><i className="fa fa-pencil text-primary"></i></Label>
                {
                    task.is_started ? <Checkbox className="mx-2" checked={task.is_completed === 1} onChange={() => this.completeTask(task)} disabled={task.is_completed === 1} />
                        :
                        <Label onClick={() => this.startTask(task)} className="mx-2"><i className="fa fa-play text-primary"></i></Label>
                }
                <Label className="mx-1" onClick={() => this.onSort('up')}>
                    <i className={index === 0 ? 'fa fa-arrow-circle-up text-secondary' : 'fa fa-arrow-circle-up text-primary'} style={{fontSize: '17px'}}></i>
                </Label>
                <Label className="mx-1" onClick={() => this.onSort('down')}>
                    <i className={index === length - 1 ? 'fa fa-arrow-circle-down text-secondary' : 'fa fa-arrow-circle-down text-primary'} style={{fontSize: '17px'}}></i>
                </Label>
                <div className="flex-fill">
                    <h5>
                        {task.title}
                        {task.is_started == 1 ? <Label className="mx-1 start_task">{task.started_at ? moment(task.started_at).format("YYYY-MM-DD HH:mm") : null}</Label> : null}
                        {task.is_started == 1 && task.is_completed == 1 ? <Label className="mx-1 complete_task">{'| ' + moment(task.completed_at).format("YYYY-MM-DD HH:mm")}</Label> : null}
                    </h5>
                    {
                        task.reminders && task.reminders.map((item, index) =>
                            <div key={index}>
                                <Checkbox className="mx-2" checked={item.is_trigger === 1} onChange={() => this.completeReminder(item)} disabled={item.is_trigger === 1}
                                    label={`Reminder: `}
                                />
                                <span>{renderHTML(this.urlParser(item.notes))}</span>
                                <span className="mx-1 start_task">{moment(item.reminder_date).format("YYYY-MM-DD HH:mm")}</span>
                                {item.is_trigger ? <span className="mx-1 complete_task">{'| ' + moment(item.updated_at).format("YYYY-MM-DD HH:mm")}</span> : null}
                            </div>
                        )
                    }
                    <NoteItem type={task.node_type} task={task} reload={this.props.reload} ref={ref => this.noteRef = ref} />
                </div>
                <Modal isOpen={this.state.isOpenReminder} toggle={() => this.setState({ isOpenReminder: false })} >
                    <ModalHeader toggle={() => this.setState({ isOpenReminder: false })}>Choose Date and Note to add Reminder</ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label>Date: </Label>
                            <div>
                                <DatePicker name='date' id="date" onChange={this.changeReminderDate} value={this.state.reminderDate} />
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <Label>Note: </Label>
                            <Input type="textarea" value={this.state.reminderNote} onChange={(e) => this.setState({ reminderNote: e.target.value })} />
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.saveReminder} disabled={!this.state.reminderDate || this.state.reminderNote.length === 0}>Add Reminder</Button>{' '}
                        <Button color="secondary" onClick={() => this.setState({ isOpenReminder: false })}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>)
    }
}
