var app = require('express')();
var fs = require('fs');
require('dotenv').config()
var debug = require('debug')('GenTask:sockets');
var request = require('request');
var dotenv = require('dotenv').config();

var port = process.env.PORT || 3000;
var chat_save_url = process.env.APP_URL;

var http = require('http');

var server = http.createServer(app);
var mysql      = require('mysql');
var con = mysql.createConnection({
host     : process.env.DB_HOST,
user     : process.env.DB_USERNAME,
password : process.env.DB_PASSWORD,
database : process.env.DB_DATABASE
});

con.connect();


var io = require('socket.io')(server);

server.listen(port, ()=>{
    console.log('Socket server running')

});
app.get('/', (req, res)=>{
    res.send('<h1>Socket Server</h1>')
})
app.get('/test', (req, res)=>{
    res.send('<h1>Socket Server Testing</h1>')
})
var users = {};
io.on('connection', (socket) =>{
    console.log('New socket connected', socket.id)
    socket.on('disconnect', ()=>{
      
    })
    socket.on('new_message', message =>{
        console.log(message, socket.id)
        socket.broadcast.emit('new_message', message)
    })
     
})



