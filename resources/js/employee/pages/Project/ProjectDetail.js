import React from 'react'
import { Col, ListGroupItem, Modal, ModalBody, ModalFooter, ModalHeader, FormGroup, Label,  Button } from 'reactstrap';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import Axios from 'axios';
import { Dropdown } from 'semantic-ui-react'
import AddNoteItem from './AddNoteItem'
import { confirmAlert } from 'react-confirm-alert';
import moment from 'moment'
import renderHTML from 'react-render-html'

const partStyle = {
    paddingTop: '15px'
}

const taskStyle = {
    paddingTop: '5px'
}
export class ProjectDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            active_title: '',
            isOpenAddModal: false,
            selectable_employees: [],
            selected_members : [],
        }
        this.handleRightClick = this.handleRightClick.bind(this)
        this.handleAddMembers = this.handleAddMembers.bind(this)
        this.handleSelectMembers = this.handleSelectMembers.bind(this)
        this.moveFocusItem = this.moveFocusItem.bind(this)
        this.setOutline = this.setOutline.bind(this)
        this.setRefresh = this.setRefresh.bind(this)
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.focusKey !== nextProps.focusKey) {
            this.moveFocusItem(nextProps.focusKey)
        }
    }

    moveFocusItem(id) {
        if (this.refs[id])
            this.refs[id].scrollIntoView({
                behavior: 'smooth',
                block: 'start',
            });
    }

    renderTasks(checklist_name, tasks) {
        return <ul>
            {
                tasks && tasks.length && tasks.map((task, index) => {
                    return <li key={index} ref={'task_'+task.id} className="note-parent">

                        <div style={{display:'flex', alignItems:'center'}}>
                            {/* {task.assigned_employees.length > 0 ? <i className="fa fa-pencil" style={{cursor: 'pointer',paddingRight:'5px', color: '#2196F3'}}></i> : null} */}
                            <ContextMenuTrigger id={"some_unique_identifier" + this.props.part.id} collect={() => {
                                this.handleRightClick({
                                    node_type: 'task',
                                    node_id: task.id,
                                    title: this.props.part.part_name + '_' + checklist_name + '_' + task.task_name,
                                })
                            }}><strong>{renderHTML(this.urlParser(task.task_name))}</strong></ContextMenuTrigger>
                            <div className="pt-1 pb-1">
                                <div style={{display:'flex'}}>
                                    {task.assigned_employees.map((employee, index) =>{
                                        return  <div key={index} style={{display:'flex'}}>
                                            <span className="badge badge-primary text-capitalize ml-1"><h5><i className="fa fa-user">&nbsp;</i>{employee.full_name}</h5></span>
                                        </div>
                                    })}
                                </div>
                            </div>
                            <div className="pl-2">
                            {task.assigned_details.map((assigned, index) =>{
                                let result = []
                                if(assigned.is_started == 1){
                                    result.push(<span key={'start'+index} className="start_task">{moment(assigned.started_at).format("YYYY-MM-DD HH:mm")}</span>)
                                }
                                if(assigned.is_started == 1 && assigned.is_completed == 1){
                                    result.push(<span key={'complete'+index} >
                                        | <span className="complete_task">{moment(assigned.completed_at).format("YYYY-MM-DD HH:mm")}</span>
                                    </span>)
                                }
                                return result
                            })}
                            </div>
                        </div>
                        <AddNoteItem callId="manager" type="task" note={task} reload = {this.props.reload} customStyle={taskStyle}/>
                        <ul>
                            {this.renderTasks(checklist_name+'_'+task.task_name, task.children)}
                        </ul>
                    </li>
                })
            }
        </ul>
    }
    handleAddMembers(){
        if (this.state.selected_members.length === 0) {
            this.setState({isOpenAddModal: false})
            return;
        }
        let post_data = {
            project_id : this.props.part.project_id,
            node_type : this.state.active_node_type,
            node_id : this.state.active_node_id,
            members : this.state.selected_members,
            title: this.state.active_title,
            name: this.props.name,
        }
        this.setState({
            is_loading : true
        }, ()=>{
            Axios.post('/ajax/assignmembers', post_data)
            .then(() =>{
                this.setState({
                    is_loading : false,
                    selectable_employees : [],
                    selected_members: [],
                    active_node_id : null,
                    active_node_type : null,
                    active_title: '',
                    isOpenAddModal : false
                }, ()=>{
                })
                this.props.reload()
            })
        })
    }
    handleSelectMembers(e, data){
        this.setState({
            selected_members : data.value
        })
    }

    handleRightClick(node) {
        this.setState({
            is_loading: true,
            active_node_type: node.node_type,
            active_node_id: node.node_id,
            active_title: node.title
        }, () => {
            let post_data = {
                node_type: node.node_type,
                node_id: node.node_id,
                project_id: this.props.part.project_id,
            }
            Axios.post('/ajax/getAssignableEmployees', post_data)
                .then(response => {
                    this.setState({
                        selectable_employees: response.data,
                        is_loading: false,
                    })
                })
        })
    }

    urlParser(notes) {
        var res = notes.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
        if (res !== null) {
            var unique = res.filter(function(elem, index, self) {
                return index === self.indexOf(elem);
            })
            unique.forEach(item => {
                let newItem = `<a href="${item.includes('https://') ? item : (item.includes('http://') ? item : 'https://' + item) }" target="_blank"><b>${item}</b></a>`;
                notes = notes.replace(item, newItem);
            });
        }

        notes = notes.replace(/\n/g, "<br>");
        return notes;
    }

    setOutline() {
        let post_data = {
            node_type : this.state.active_node_type,
            node_id : this.state.active_node_id,
        }
        Axios.post('/ajax/setOutline', post_data)
        .then(() =>{
            this.setState({
                active_node_id : null,
                active_node_type : null,
                active_title: '',
            }, ()=>{
            })
            this.props.reload()
        })
    }

    setRefresh() {
        let post_data = {
            node_type : this.state.active_node_type,
            node_id : this.state.active_node_id,
        }
        Axios.post('/ajax/setRefresh', post_data)
            .then(() =>{
                this.setState({
                    active_node_id : null,
                    active_node_type : null,
                    active_title: '',
                }, ()=>{
                })
                this.props.reload()
            })
    }

    render() {
        const employeeOptions = this.state.selectable_employees.map((employee) => {
            return {
                key: employee.id,
                text: employee.full_name,
                value: employee.id,
                image: { avatar: true, src: employee.profile_image_src },
            }
        })

        return <Col xs="12">
            <Modal isOpen={this.state.isOpenAddModal} toggle={() => this.setState({ isOpenAddModal: false })} >
                <ModalHeader toggle={() => this.setState({ isOpenAddModal: false })}>Choose Employees to add</ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <Label>Employee</Label>
                        <Dropdown placeholder='Employees' search fluid multiple selection options={employeeOptions} onChange={this.handleSelectMembers} />
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.handleAddMembers} >Add Members</Button>{' '}
                    <Button color="secondary" onClick={() => this.setState({ isOpenAddModal: false })}>Cancel</Button>
                </ModalFooter>
            </Modal>
            <ContextMenu id={"some_unique_identifier" + this.props.part.id}>
                <MenuItem onClick={() =>this.setState({isOpenAddModal:true})}>
                    <ListGroupItem style={{cursor : 'pointer'}}><a >Add Members </a></ListGroupItem>
                </MenuItem>
                <MenuItem onClick={this.setOutline}>
                    <ListGroupItem style={{cursor : 'pointer'}}><a >Its Money </a></ListGroupItem>
                </MenuItem>
                <MenuItem onClick={this.setRefresh}>
                    <ListGroupItem style={{cursor : 'pointer'}}><a >Refresh </a></ListGroupItem>
                </MenuItem>
            </ContextMenu>
            <div style={{display:'flex', alignItems:'center'}}>
                {/* {this.props.part.assigned_employees.length > 0 ? <i className="fa fa-pencil" style={{cursor: 'pointer',paddingRight:'5px', color: '#2196F3'}}></i> : null} */}
                <ContextMenuTrigger id={"some_unique_identifier" + this.props.part.id}
                    collect={() => {
                        this.handleRightClick({
                            node_type: 'project_part',
                            node_id: this.props.part.id,
                            title: this.props.part.part_name,
                        })
                    }}
                >
                    <h4 className="font-weight-bold" id={"part_" + this.props.part.id}>Part {this.props.index + 1}: {this.props.part.part_name}</h4>
                </ContextMenuTrigger>
                <div style={{display:'flex'}}>
                    {this.props.part.assigned_employees.map((employee, index) =>{
                        return <div key={index} style={{display:'flex'}}>
                            <img src={employee.profile_image_src} className="rounded-circle ml-1" alt={employee.full_name} style={{width : '50px', height : '50px'}} />
                        </div>
                    })}
                </div>
                <div className="pl-2">
                {this.props.part.project_employee_assignment.map((assigned, index) =>{
                    let result = []
                    if(assigned.is_started == 1){
                        result.push(<span key={'start'+index} className="start_task">{moment(assigned.started_at).format("YYYY-MM-DD HH:mm")}</span>)
                    }
                    if(assigned.is_started == 1 && assigned.is_completed == 1){
                        result.push(<span key={'complete'+index} >
                            | <span className="complete_task">{moment(assigned.completed_at).format("YYYY-MM-DD HH:mm")}</span>
                        </span>)
                    }
                    return result
                })}
                </div>
            </div>
            <AddNoteItem callId="manager" type="project_part" note={this.props.part} reload = {this.props.reload} customStyle={partStyle}/>
            <ul style={{ listStyle: 'decimal' }}>
                {this.props.part.checklists.map((checklist, index) => {
                    return <li key={index} ref={'checklist_'+checklist.id}>
                        <div className="note-parent">
                            <div style={{display:'flex', alignItems:'center'}}>
                                {/* {checklist.assigned_employees.length > 0 ? <i className="fa fa-pencil" style={{cursor: 'pointer',paddingRight:'5px', color: '#2196F3'}}></i> : null} */}
                                <ContextMenuTrigger id={"some_unique_identifier" + this.props.part.id} collect={() => {
                                    this.handleRightClick({
                                        node_type: 'checklist',
                                        node_id: checklist.id,
                                        title: this.props.part.part_name + '_' + checklist.name,
                                    })
                                }}>
                                    <h5 className="font-weight-bold" >{checklist.name}</h5>
                                </ContextMenuTrigger>
                                <div>
                                    <div style={{display:'flex'}}>
                                        {checklist.assigned_employees.map((employee, index) =>{
                                            return <div key={index} style={{display:'flex'}}>
                                                <img src={employee.profile_image_src} className="rounded-circle ml-1" alt={employee.full_name} style={{width : '50px', height : '50px'}} />
                                            </div>
                                        })}
                                    </div>
                                </div>
                                <div className="pl-2">
                                {checklist.project_employee_assignment.map((assigned, index) =>{
                                    let result = []
                                    if(assigned.is_started == 1){
                                        result.push(<span key={'start'+index} className="start_task">{moment(assigned.started_at).format("YYYY-MM-DD HH:mm")}</span>)
                                    }
                                    if(assigned.is_started == 1 && assigned.is_completed == 1){
                                        result.push(<span key={'complete'+index} >
                                            | <span className="complete_task">{moment(assigned.completed_at).format("YYYY-MM-DD HH:mm")}</span>
                                        </span>)
                                    }
                                    return result
                                })}
                                </div>
                            </div>
                            <AddNoteItem callId="manager" type="checklist" note={checklist} reload = {this.props.reload} customStyle={partStyle}/>
                        </div>
                        {checklist.tasks.length && this.renderTasks(checklist.name, checklist.tasks)}

                    </li>
                })}
            </ul>
        </Col>
    }
}
