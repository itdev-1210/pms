<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectAssignment extends Model
{
    //
    protected $table = "project_assignment";

    public function manager_details()
    {
        return $this->hasOne('App\Models\Manager','id','manager_id');
    }

}
