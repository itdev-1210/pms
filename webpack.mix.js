const mix = require('laravel-mix');
const CompressionPlugin = require('compression-webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react('resources/js/master/index.js', 'public/js/admin')
mix.react('resources/js/manager/index.js', 'public/js/manager');
mix.react('resources/js/employee/index.js', 'public/js/employee');
mix.copy('resources/js/service-worker.js', 'public/js');

mix.webpackConfig({
    plugins: [
        new CompressionPlugin({
          filename: '[path].gz[query]',
          algorithm: 'gzip',
          test: /\.js$|\.css$|\.html$|\.svg$/,
          threshold: 10240,
          minRatio: 0.8,
        }),
      ],
});
