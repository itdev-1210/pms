<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use App\Models\Project;
use App\ProductLog;

use Config;

class GetKonnektiveTimer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get-konnektive-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Konnektive data and update database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now()->subHours(5);
        $end = $now->copy();
        $endDate = $end->subSecond()->format('m/d/Y');
        $endTime = substr($end->toTimeString(), 0, -3);
        $startTime = substr($now->subHours(24)->toTimeString(), 0, -3);
        $startDate = $now->format('m/d/Y');
        $log_date = $now->toDateString();

        $konnektive_project_data = array();
        $konnektive_json = json_decode($this->getKonnektiveData($startDate, $endDate, $startTime, $endTime, 1, 200));
        $total_results = $konnektive_json->message->totalResults;
        $page_count = (int)($total_results / 200) + 1;
        for ($page_no = 1; $page_no <= $page_count; $page_no++) {
            $konnektive_json = json_decode($this->getKonnektiveData($startDate, $endDate, $startTime, $endTime, $page_no, 200));
            $konnektive_data = $konnektive_json->message->data;
            foreach($konnektive_data as $data) {
                $items = $data->items;
                foreach($items as $item) {
                    $temp = [$item->product => strtolower(str_replace('-', '', $data->campaignName))];
                    array_push($konnektive_project_data, $temp);
                }
            }
        }

        $konnektive_projects = array();
        $temp = array();
        $constants = config('constants'); $keyLists = array_keys($constants);
        foreach ($konnektive_project_data as $data) {
            foreach($data as $key => $campaign) {
                $newKey = trim(str_replace(',', '', $key));
                $result = $this->search($constants, $newKey, $campaign, $keyLists);
                if (count($result)) {
                    $project_name = array_keys($result)[0];
                    if (array_key_exists($project_name, $konnektive_projects)) {
                        $konnektive_projects[$project_name] += (int)$result[$project_name];
                    } else {
                        $konnektive_projects[$project_name] = (int)$result[$project_name];
                    }
                }
            }
        }

        $projects = Project::where('created_by', 'MANAGER')->get();
        foreach($projects as $project) {
            $project_name = $project->project_name;
            if(array_key_exists($project_name, $konnektive_projects)) {
                $product_log = ProductLog::where('project_id', $project->id)->where('log_date', $log_date)->first();
                if (!$product_log) $product_log = new ProductLog();
                $product_log->project_id = $project->id;
                $product_log->log_date = $log_date;
                $product_log->total_count += $konnektive_projects[$project_name];
                $product_log->save();
            }
        }
    }

    function search($array, $key, $campaign, $keyLists)
    {
        $results = array();

        if (!is_array($array)) return $results;

        $index = -1;
        foreach($keyLists as $project_name) {
            if (strpos($campaign, (strtolower(str_replace('-', '', $project_name)))) !== false) {
                $index = $project_name;
                break;
            }
        }
        if ($index === -1) {
            foreach ($array as $arraykey => $subarray) {
                if (isset($subarray[$key])) {
                    $results = [$arraykey => $subarray[$key]];
                    return $results;
                } else if (strlen($key) >= 39) {
                    foreach($subarray as $subKey => $sub) {
                        if (strpos($subKey, $key) !== false) {
                            $results = [$arraykey => $subarray[$subKey]];
                            return $results;
                        }
                    }
                }
            }
        } else {
            $subarray = $array[$index];
            if (isset($subarray[$key])) {
                $results = [$index => $subarray[$key]];
                return $results;
            } else if (strlen($key) >= 39) {
                foreach($subarray as $subKey => $sub) {
                    if (strpos($subKey, $key) !== false) {
                        $results = [$index => $subarray[$subKey]];
                        return $results;
                    }
                }
            }
        }

        return $results;
    }

    /**
     * Get Konnektive Data
     */
    public function getKonnektiveData($startDate, $endDate, $startTime, $endTime, $page=1, $resultsPerPage=25)
    {
        $curl = curl_init();
        $url = "https://api.konnektive.com/transactions/query/?loginId=".env('KONNEKTIVE_LOGIN_ID').
            "&password=".env('KONNEKTIVE_PASSWORD')."&startDate=".$startDate.
            "&endDate=".$endDate."&startTime=".$startTime."&endTime=".$endTime."&page=".$page."&resultsPerPage=".$resultsPerPage.
            "&txnType=AUTHORIZE&responseType=SUCCESS";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
