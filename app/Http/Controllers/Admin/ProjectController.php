<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\CheckList;
use App\Models\Task;
use App\Models\ProjectPart;
use App\Models\ProjectAssignment;
use App\Models\Manager;
use App\Models\Notification;
use App\Models\ProjectEmployeeAssignment;
use App\Models\EmployeeChecklistNote;
use App\Models\Employee;
use App\TaskNotification;
use App\Reminder;
use App\AlarmNotification;
use App\RelationTask;
use App\Notifications\PushDemo;
use Auth;
use Illuminate\Support\Carbon;

use App\Http\Controllers\Resuables\ProjectTrait;

class ProjectController extends Controller
{
    use ProjectTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest_admin');
    }

    /* Get Master Checklist */
    public function getmasterchecklist(){
        Auth::shouldUse('admin');
        $project = Project::where('created_by', 'ADMIN')->first();
        if($project){
            return $project->get_details();
        }
        else{
            $project = new Project;
            $project->project_name = "Master Checklist";
            $project->manager_id = Auth::user()->id;
            $project->created_by = "ADMIN";
            $project->save();
            return $project->get_details();
        }
    }

    /* Get Employees to add  */
    public function getAssignableEmployees(Request $request)
    {
        $assignedIds = ProjectEmployeeAssignment::where('node_type', $request->node_type)->where('project_id', $request->project_id)->where('node_id', $request->node_id)->get()->pluck('employee_id');
        return Employee::whereNotIn('id', $assignedIds)->get();
    }

    /* Assign Members */
    public function assignmembers(Request $request)
    {
        Auth::shouldUse('admin');
        $week_number = Carbon::now()->format('W');
        $year = Carbon::now()->format('Y');
        if (isset($request->members) && count($request->members)) {
            foreach ($request->members as $member) {
                $tmp = TaskNotification::where('week_number', $week_number)
                    ->where('year', $year)
                    ->where('employee_id', $member)
                    ->where('project_id', $request->project_id)
                    ->first();

                $project_order = $tmp ? $tmp->project_order : TaskNotification::where('week_number', $week_number)
                    ->where('year', $year)
                    ->where('employee_id', $member)
                    ->max('project_order') + 1;

                $task_order = TaskNotification::where('week_number', $week_number)
                    ->where('year', $year)
                    ->where('project_id', $request->project_id)
                    ->where('employee_id', $member)
                    ->max('task_order');

                $assign = new ProjectEmployeeAssignment;
                $assign->node_id = $request->node_id;
                $assign->node_type = $request->node_type;
                $assign->project_id = $request->project_id;
                $assign->employee_id = $member;
                $assign->manager_id = Auth::user()->id;
                $assign->save();

                $task_notification = new TaskNotification;
                $task_notification->employee_id = $member;
                $task_notification->assign_id = Auth::user()->id;
                $task_notification->assigned_by = "ADMIN";
                $task_notification->node_id = $request->node_id;
                $task_notification->node_type = $request->node_type;
                $task_notification->project_id = $request->project_id;
                $task_notification->title = $request->title;
                $task_notification->project = $request->name;
                $task_notification->name = '';
                $task_notification->is_started = 0;
                $task_notification->is_completed = 0;
                $task_notification->is_show = 0;
                $task_notification->year = $year;
                $task_notification->week_number = $week_number;
                $task_notification->project_order = $project_order;
                $task_notification->task_order = $task_order + 1;
                $task_notification->save();

                $employee = Employee::findOrFail($member);
                $employee->notify(new PushDemo('New Project Assigned!', $request->name.'_'.$request->title));
            }
            if ($request->node_type == 'task') {
                $relationTasks = RelationTask::where('task_id', $request->node_id)->first();
                if ($relationTasks) {
                    $relationTaskIds = explode(',', $relationTasks->relation_tasks);
                    $task = Task::findOrFail($request->node_id);
                    $task->need_relation = 0;
                    $task->save();
                    $task_ids = ProjectEmployeeAssignment::where('node_type', 'task')->pluck('node_id')->toArray();
                    $tasks = Task::update('need_relation', 1)->whereIn('id', Task::whereIn('id', $relationTasksIds)
                        ->whereNotIn('id', $task_ids)->pluck('id'));
                }
            }
        }

        return $this->getAssignmentDetails(Project::find($request->project_id));
    }

    /* Remove Assign Member */
    public function removeAssignMember(Request $request) {
        $assign = ProjectEmployeeAssignment::where('node_id', $request->node_id)
            ->where('node_type', $request->node_type)
            ->where('project_id', $request->project_id)
            ->where('employee_id', $request->employee_id)->first();
        if ($assign) $assign->delete();

        $task_notification = TaskNotification::where('node_id', $request->node_id)
            ->where('node_type', $request->node_type)
            ->where('project_id', $request->project_id)
            ->where('employee_id', $request->employee_id)->first();
        if ($task_notification) $task_notification->delete();
        return $this->getAssignmentDetails(Project::find($request->project_id));
    }

    /* Load all projects */
    public function loadprojects(){
        $projects = Project::where('created_by', 'MANAGER')->orderBy('created_at', 'desc')->get()->toArray();
        $i = 0;
        foreach ($projects as $key => $project) {
            $projects[$key]['number'] = ++$i;
            # code...
        }
        return $projects;
    }
    /* Preview project details */
    public function get_preview_project_details($id){
        return $this->getPreviewProjectDetails(Project::find($id));
    }

    /* Get Project Details */
    public function get_project_details($id){

        return Project::find($id)->get_details();

    }

    /* Get Project Assignment Details */
    public function get_assignment_details($id){
        $project = Project::find($id)->toArray();
        $assignment_details = ProjectAssignment::with('manager_details')->where('project_id', $id)->get();
        // return ProjectAssignment::where('project_id', $id)->get()->pluck("manager_id");
        $managers = Manager::whereNotIn('id', ProjectAssignment::where('project_id', $id)->get()->pluck("manager_id"))->get();
        return array(
            'project_detail' => $project,
            'assignmet_details' => $assignment_details,
            'managers' => $managers
        );
    }
    /* Create New Project */
    public function save(Request $request){
        Auth::shouldUse('admin');
        $same_name_project = Project::where('project_name', $request->project_name)->first();
        if($same_name_project){
            return array(
                'status' => 'error',
                'message' => 'This name is exist already'
            );
        }
        $project = new Project;
        $project->project_name = $request->project_name;
        $project->manager_id = Auth::user()->id;
        $project->created_by = "ADMIN";
        $project->save();
        return array(
            'status' => 'success',
            'project_detail' => $project->toArray()
        );
    }

    /* Delete project */
    public function remove($id){
        $manager = Project::find($id);
        $manager->delete();
        TaskNotification::where('project_id', $id)->delete();
        Reminder::where('project_id', $id)->delete();
        return array(
            'status' => 'success',
            'message' => 'Removed successfully!'
        );
    }

    /* Update Project */
    public function update(Request $request, $id){
        $isAdmin = Project::find($id)->created_by == 'ADMIN' ? true : false;
        return Project::find($id)->update_by_request($request, $id , $isAdmin);

    }

    /* Project Assign to Managers */
    public function assignmember(Request $request){
        Auth::shouldUse('admin');
        $project = Project::find($request->project_id);
        $managers = $request->managers;
        if ($managers && count($managers)) {
            foreach ($managers as $manager) {


                $assign = new ProjectAssignment;
                $assign->project_id = $request->project_id;
                $assign->original_project_id = $project->duplicated_from;
                $assign->manager_id = $manager;
                $assign->save();



                $notification = new Notification;
                $notification->from_account_type = "MANAGER";
                $notification->to_account_type = "MANAGER";
                $notification->from_account_id = Auth::user()->id;
                $notification->to_account_id = $manager;
                $notification->title = "New project assigned!";
                $notification->message = 'Project: "' . $project->project_name . '"  from admin!';
                $notification->type = "project_assign";
                $notification->save();
            }
            return array(
                'status' => 'success',
                'message' => "Saved successfully!"
            );
        } else {
            return array(
                'status' => 'error',
                'message' => "Please choose managers to assign!"
            );
        }
    }

    public function assign(Request $request)
    {
        Auth::shouldUse('admin');
        $managers = $request->managers;
        $project = Project::find($request->project_id);
        $assignProject = $project->replicate();
        if (isset($request->project_name)) {
            $assignProject->project_name = $request->project_name;
        }
        if (isset($request->project_category)) {
            $assignProject->category = $request->project_category;
            if ($request->project_category == 'winners') $assignProject->limit_inventory = 5000;
        }

        $assignProject->created_by = "MANAGER";
        $assignProject->manager_id = Auth::user()->id;
        $assignProject->duplicated_from = $request->project_id;

        $assignProject->save();

        $parts = ProjectPart::with([
            'checkLists',
            'checkLists.tasks'
            ])->where('project_id', $request->project_id)
            ->orderBy('sort_number')->get()->toArray();

        $partCount = ProjectPart::max('id');
        $check_list_count = CheckList::max('id');
        $task_count = Task::max('id');
        $assignParts = array();$assign_checklists = array();$assign_tasks = array();
        foreach ($parts as $part_sort_key => $part) {
            $assignPart = $part;

            $assignPart['project_id'] = $assignProject->id;
            $assignPart['sort_number'] = $part_sort_key;
            $assignPart['duplicated_from'] = $part['id'];
            $assignPart['id'] = $partCount = $partCount + 1;
            unset($assignPart['check_lists']);
            $assignParts[] = $assignPart;

            $checklists = $part['check_lists'];
            foreach ($checklists as $checklist) {
                $assign_checklist = $checklist;
                $assign_checklist['id'] = $check_list_count = $check_list_count + 1;
                $assign_checklist['project_id'] = $assignProject->id;
                $assign_checklist['project_part_id'] = $assignPart['id'];
                $assign_checklist['duplicated_from'] = $checklist['id'];
                unset($assign_checklist['tasks']);
                $assign_checklists[] = $assign_checklist;

                /* Assign Tasks */
                $tasks = $checklist['tasks'];
                $task = array(
                    'check_list_id' => -1,
                    'checklist_id_by_manager' => null,
                    'created_at' => "",
                    'created_by' => "",
                    'duplicated_from' => 1,
                    'id' => 0,
                    'parent' => -1,
                    'parent_by_manager' => null,
                    'project_employee_assignment' => [],
                    'project_id' => -1,
                    'sort_number' => 0,
                    'sort_number_by_manager' => null,
                    'task_name' => "",
                    'task_name_by_manager' => null,
                    'updated_at' => '',
                );
                array_unshift($tasks, $task);
                $new = array();
                foreach ($tasks as $a){
                    $new[$a['parent']][] = $a;
                }
                $tree = $this->getTasks($new, array($tasks[0]));
                if (array_key_exists('children', $tree[0])) {
                    $result = $this->assignTasks($assignProject->id, $check_list_count, $tree[0]['children'], $task_count);
                    foreach($result as $r) {
                        $assign_tasks[] = $r;
                    }
                    $task_count += count($result);
                }
            }
        }

        ProjectPart::insert($assignParts);
        CheckList::insert($assign_checklists);
        Task::insert($assign_tasks);
        $managers[] = Auth::user()->id;
        if ($managers && count($managers)) {

            foreach ($managers as $manager) {


                $assign = new ProjectAssignment;
                $assign->project_id = $assignProject->id;
                $assign->original_project_id = $request->project_id;
                $assign->manager_id = $manager;
                $assign->save();



                $notification = new Notification;
                $notification->from_account_type = "MANAGER";
                $notification->to_account_type = "MANAGER";
                $notification->from_account_id = Auth::user()->id;
                $notification->to_account_id = $manager;
                $notification->title = "New project assigned!";
                $notification->message = 'Project: "' . $project->project_name . '"  from admin!';
                $notification->type = "project_assign";
                $notification->save();
            }

        }

        return array(
            'status' => 'success',
            'message' => "Saved successfully!"
        );
    }

    public function assignTasks($assign_project_id, $assign_check_list_id, $tasks, $task_count, $parent = 0)
    {
        $assign_tasks = array();
        $count = $task_count;
        foreach ($tasks as $task_sort_key => $task) {
            $assignTask = $task;
            $assignTask['id'] = $count = $count + 1;
            $assignTask['project_id'] = $assign_project_id;
            $assignTask['check_list_id'] = $assign_check_list_id;
            $assignTask['sort_number'] = $task_sort_key;
            $assignTask['parent'] = $parent;
            $assignTask['duplicated_from'] = $task['id'];
            unset($assignTask['title']);unset($assignTask['node_type']);
            unset($assignTask['node_id']);unset($assignTask['notes']);unset($assignTask['assigned_details']);
            unset($assignTask['is_duplicate']);
            if (array_key_exists('children', $assignTask)) {
                unset($assignTask['expanede']);
                unset($assignTask['children']);
            }
            $assign_tasks[] = $assignTask;

            if (array_key_exists('children', $task)) {
                $result =$this->assignTasks($assign_project_id, $assign_check_list_id, $task['children'], $count, $count);
                $count += count($result);
                foreach($result as $r) {
                    $assign_tasks[] = $r;
                }
            }
        }
        return $assign_tasks;
    }

    /* Remove Assignment */
    public function removeassign($id){
        Auth::shouldUse('admin');
        if($assign = ProjectAssignment::find($id)){
            $manager_id = $assign->manager_id;
            $project = Project::find($assign->project_id);
            $assign->delete();
            $notification = new Notification;
            $notification->from_account_type = "ADMIN";
            $notification->to_account_type = "MANAGER";
            $notification->from_account_id = Auth::user()->id;
            $notification->to_account_id = $manager_id;
            $notification->title = "Revoked from Admin";
            $notification->type = "project_assign";
            $notification->message = 'You revoked at Project: "'.$project->project_name.'"  from admin!';
            $notification->save();
            return  array(
                 'status' => 'success',
                'message' => "Revoked successfully!"
            );
        }
        return array(
            'status' => 'error',
            'message' => "Something went wrong!"
        );
    }
    public function newnote(Request $request){
        Auth::shouldUse('admin');
        $this->newnoteById($request, Auth::user()->id);
    }

    public function editnote(Request $request){
        Auth::shouldUse('admin');
        $this->editnoteById($request, Auth::user()->id);
    }

    public function getNotifications() {
        Auth::shouldUse('admin');
        $alarm_notifications = AlarmNotification::where('assign_id', Auth::user()->id)
            ->where('assigned_by', 'ADMIN')
            ->where('is_completed', 1)
            ->get()->toArray();

        return $alarm_notifications;
    }

    public function getReminders() {
        Auth::shouldUse('admin');
        $now = date('Y-m-d');

        $reminders = Reminder::where('assign_id', Auth::user()->id)
                ->where('is_trigger', 0)
                ->where('assigned_by', 'ADMIN')
                ->whereRaw("Date(reminder_date) <= '$now'")
                ->get()->toArray();

        return $reminders;
    }

    public function newAssignMembers(Request $request) {
        Auth::shouldUse('admin');
        $week_number = Carbon::now()->format('W');
        $year = Carbon::now()->format('Y');

        if (isset($request->members) && count($request->members)) {
            foreach ($request->members as $member) {
                $tmp = TaskNotification::where('week_number', $week_number)
                    ->where('year', $year)
                    ->where('employee_id', $member)
                    ->where('project_id', $request->project_id)
                    ->first();

                $project_order = $tmp ? $tmp->project_order : TaskNotification::where('week_number', $week_number)
                    ->where('year', $year)
                    ->where('employee_id', $member)
                    ->max('project_order') + 1;

                $task_order = TaskNotification::where('week_number', $week_number)
                    ->where('year', $year)
                    ->where('project_id', $request->project_id)
                    ->where('employee_id', $member)
                    ->max('task_order');

                $assign = new ProjectEmployeeAssignment;
                $assign->node_id = $request->node_id;
                $assign->node_type = $request->node_type;
                $assign->project_id = $request->project_id;
                $assign->employee_id = $member;
                $assign->manager_id = Auth::user()->id;
                $assign->save();

                $task_notification = new TaskNotification;
                $task_notification->employee_id = $member;
                $task_notification->assign_id = Auth::user()->id;
                $task_notification->assigned_by = "MANAGER";
                $task_notification->node_id = $request->node_id;
                $task_notification->node_type = $request->node_type;
                $task_notification->project_id = $request->project_id;
                $task_notification->title = $request->title;
                $task_notification->project = $request->name;
                $task_notification->name = '';
                $task_notification->is_started = 0;
                $task_notification->is_completed = 0;
                $task_notification->is_show = 0;
                $task_notification->year = $year;
                $task_notification->week_number = $week_number;
                $task_notification->project_order = $project_order;
                $task_notification->task_order = $task_order + 1;
                $task_notification->save();

                $employee = Employee::findOrFail($member);
                $employee->notify(new PushDemo('New Project Assigned!', $request->name.'_'.$request->title));
            }
            if ($request->node_type == 'task') {
                $relationTasks = RelationTask::where('task_id', $request->node_id)->first();
                if ($relationTasks) {
                    $relationTaskIds = explode(',', $relationTasks->relation_tasks);
                    $task = Task::findOrFail($request->node_id);
                    $task->need_relation = 0;
                    $task->save();
                    $task_ids = ProjectEmployeeAssignment::where('node_type', 'task')->pluck('node_id')->toArray();
                    $tasks = Task::update('need_relation', 1)->whereIn('id', Task::whereIn('id', $relationTasksIds)
                        ->whereNotIn('id', $task_ids)->pluck('id'));
                }
            }
        }
        return [
            'msg' => 'success'
        ];
    }
}
