<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Revolution\Google\Sheets\Sheets;
use App\Models\Project;

class GetInventoryDataTimer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get-inventory-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get google sheet data and update database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \Google_Client();
        $path = storage_path().'/key.json';
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.$path);
        $client->useApplicationDefaultCredentials();
        $client->setScopes([\Google_Service_Sheets::DRIVE, \Google_Service_Sheets::SPREADSHEETS]);

        $service = new \Google_Service_Sheets($client);

        $sheets = new Sheets();
        $sheets->setService($service);
        $rows = $sheets->spreadsheet(env("GOOGLE_SHEET_ID"))->sheet('Inventory Receivable - NEW')->all();

        $products = config('product');
        $incomeSum = array();
        foreach($rows as $row) {
            if (count($row) < 4) continue;

            $product = strtolower($row[2]);
            if (!array_key_exists($product, $products)) continue;

            $val = str_replace('+', ' ', str_replace('-', ' ', str_replace(',', '', $row[3])));
            $tempVals = array_sum(explode(' ', str_replace('/', ' ', $val)));
            if (count($row) > 43) {
                if ($row[41] !== '') {
                    $val = str_replace('+', ' ', str_replace('-', ' ', str_replace(',', '', $row[43])));
                    $tempVals -= array_sum(explode(' ', str_replace('/', ' ', $val)));
                }
            }
            if (array_key_exists(strtolower($products[$product]), $incomeSum)) {
                $incomeSum[strtolower($products[$product])] += (int)$tempVals;
            } else {
                $incomeSum[strtolower($products[$product])] = (int)$tempVals;
            }
        }

        $projects = Project::where('created_by', 'MANAGER')->get();
        foreach($projects as $project) {
            if(!array_key_exists(strtolower($project->project_name), $incomeSum)) continue;

            $project->income_inventory = $incomeSum[strtolower($project->project_name)];
            $project->save();
        }
    }
}
