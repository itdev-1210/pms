<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\GetGoogleSheetTimer::class,
        Commands\GetGoogleSheetTimer::class,
        Commands\GetKonnektiveTimer::class,
        Commands\CalcAverageTimer::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('get-google-sheet-data')->hourly();
        $schedule->command('get-konnektive-data')->daily();
        $schedule->command('get-inventory-data')->dailyAt('00:30');
        $schedule->command('calc-konnektive-average-data')->dailyAt('01:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
