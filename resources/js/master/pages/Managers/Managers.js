import React, { Fragment } from 'react'
import { connect } from 'react-redux';
import { AppSwitch } from '@coreui/react'
import { Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import ReactDatatable from '@ashvin27/react-datatable';
import axios from 'axios'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'
import Axios from 'axios';
import { NotificationManager } from 'react-notifications';
class Managers extends React.Component{
    constructor(props) {
        super(props);
        this.columns = [
            {
                key: "number",
                text: "No",
                className: "number",
                align: "left",
                sortable: true,
            },
            {
                key: "full_name",
                text: "Full Name",
                className: "full_name",
                align: "left",
                sortable: true
            },
            {
                key: "email",
                text: "Email",
                className: "email",
                sortable: true
            },
            {
                key: "is_approved",
                text: "Approved",
                className: "is_approved",
                sortable: false,
                cell: record => {
                    return <Fragment>
                        <AppSwitch className={'mx-1'}   label dataOn={'\u2713'} dataOff={'\u2715'} variant={'pill'} color={'primary'} checked = {record.is_approved} onChange={(e) => this.handleApprove(e, record.id, record.number - 1)} />
                    </Fragment>
                }
            },
            {
                key: "action",
                text: "Action",
                className: "action",
                width: 100,
                align: "left",
                sortable: false,
                cell: record => {
                    return (
                        <Fragment>
                            <button
                                className="btn btn-primary btn-sm"
                                onClick={() => this.editRecord(record.id)}
                                style={{marginRight: '5px'}}>
                                <i className="fa fa-edit"></i>
                            </button>
                            <button
                                className="btn btn-danger btn-sm"
                                onClick={() => this.deleteRecord(record.id)}>
                                <i className="fa fa-trash"></i>
                            </button>
                        </Fragment>
                    );
                }
            }
        ];
        this.config = {
            page_size: 10,
            length_menu: [ 10, 20, 50 ],
        }

        this.state = {
            records: [],
            managers: [],
            is_deleting : false
        }
        this.handleApprove = this.handleApprove.bind(this)
        this.handleRemoveManager = this.handleRemoveManager.bind(this)
        this.loadManagers = this.loadManagers.bind(this)
    }
    componentDidMount(){
        this.loadManagers()
    }
    loadManagers(){
        axios.get("/admin/ajax/managers")
        .then(response =>{
            this.setState({
                managers : response.data
            })
        })
    }
    editRecord(id){
        this.props.history.push(`/admin/manager/edit/${id}`)
    }
    handleRemoveManager(id){
        this.setState({
            is_deleting : true
        }, ()=>{
            Axios.post(`/admin/ajax/manager/remove/${id}`)
            .then(response =>{
                if(response.data.status == 'success'){
                    this.setState({
                        is_deleting : false
                    }, ()=>{
                        this.loadManagers()
                    })
                    NotificationManager.success("Removed manager successfully!", "Success", 3000)
                }
                else{
                    this.setState({
                        is_deleting : false
                    })
                    NotificationManager.error(response.data.message, "Error", 3000)
                }

            }).catch(() =>{
                this.setState({
                    is_deleting : false
                })
                NotificationManager.error("Failed to remove manager!", "Error", 3000)
            })
        })
        // alert(id)
    }
    deleteRecord(id){
        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this manager?',
            buttons: [
              {
                label: 'Yes',
                onClick: () => this.handleRemoveManager(id)
              },
              {
                label: 'No',
                onClick: () => {

                }
              }
            ]
          });
    }
    handleApprove(e, id, index){
        e.preventDefault()
        e.target.checked = !e.target.checked
        let managers = this.state.managers
        managers[index].is_approved = e.target.checked ? 1 : 0
        this.setState({
            managers : managers
        })
    }

    render(){

        return <div className="animated fadeIn">
                <LoadingOverlay
                        active={this.state.is_deleting}
                        spinner={<BounceLoader />}
                    >

                </LoadingOverlay>
                <Row>
                    <Col xs="12">
                        <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i><strong>Managers</strong>
                            <div className="card-header-actions">
                            <Link color="primary" className="btn-pill btn btn-primary" to="/admin/manager/add">
                                <i className="icon-plus"></i>&nbsp;New Manager
                            </Link>
                            </div>
                        </CardHeader>
                        <CardBody>
                        <ReactDatatable
                            config={this.config}
                            records={this.state.managers}
                            columns={this.columns}
                        />
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
              </div>
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedManagers = connect(mapStateToProps)(Managers);
export default connectedManagers;
