import React, {  } from 'react'
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
} from 'reactstrap';
import { NotificationManager } from 'react-notifications';
import Axios from 'axios';

import ChecklistItem from '../Project/ChecklistItem';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'

export default class MasterChecklist extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            project_id : null,
            is_draging: false,
            is_loading: false,
            project_detail: {},
            checklists: [],
            parts : [],
            removed_checklists : [],
            removed_tasks : [],
            removed_parts : []
        }
        this.handleAddChecklist = this.handleAddChecklist.bind(this)
        this.handleChangeChecklist = this.handleChangeChecklist.bind(this)
        this.handleChecklistUp = this.handleChecklistUp.bind(this)
        this.handleChecklistDown = this.handleChecklistDown.bind(this)
        this.handleChecklistRemove = this.handleChecklistRemove.bind(this)
        this.handleRemoveChecklist = this.handleRemoveChecklist.bind(this)
        this.handleAddTaskToChecklist = this.handleAddTaskToChecklist.bind(this)
        this.handleTaskChange = this.handleTaskChange.bind(this)
        this.handleChangeTaskTree = this.handleChangeTaskTree.bind(this)
        this.handleSave = this.handleSave.bind(this)
        this.handleAddNewPart = this.handleAddNewPart.bind(this)
        this.handlePartUp = this.handlePartUp.bind(this)
        this.handlePartDown = this.handlePartDown.bind(this)
        this.handleChangePartName = this.handleChangePartName.bind(this)
        this.handlePartRemove = this.handlePartRemove.bind(this)
        this.handlePartRemoveConfirm = this.handlePartRemoveConfirm.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleView = this.handleView.bind(this)
    }
    componentDidMount(){
        Axios.get('/admin/ajax/getmasterchecklist')
        .then(response => {
            this.setState({
                project_detail: response.data.project_detail,
                parts: response.data.parts,
            })
        })
    }
    handleAddChecklist(index) {

        let checklist_temp = {
            id : null,
            name: '',
            description: '',
            sort_number: this.state.checklists.length + 1,
            is_edit: true,
            tasks: [
                { title: "", expanded: true }
                // { title: 'Chicken', children: [{ title: 'Egg' }, { title: 'Egg' }], expanded: true },
                // { title: 'Chicken1', children: [{ title: 'Egg' }, { title: 'Egg' }] }
            ]
        }
        let parts = this.state.parts
        parts[index].checklists.push(checklist_temp)
        this.setState({
            parts: parts
        })
    }
    handleView(){
        this.props.history.push('/admin/project/view/' + this.state.project_detail.id)
    }
    handleAddNewPart() {

        let project_part_temp = {
            id : null,
            part_name: '',
            sort_number: this.state.parts.length + 1,
            checklists: [
            ]
        }
        let parts = this.state.parts
        parts.push(project_part_temp)
        this.setState({
            parts: parts
        })
    }
    onDragStart() {
        // Todo
    }
    handleChangeChecklist(event, index, partIndex) {
        let parts = this.state.parts
        parts[partIndex].checklists[index][event.target.name] = event.target.value
        this.setState({
            parts : parts
        })
    }
    handleChecklistUp(index, partIndex){
        let parts = this.state.parts
        parts[partIndex].checklists = this.array_move(parts[partIndex].checklists, index, index - 1)
        this.setState({
            parts : parts
        })
    }
    handleChecklistDown(index, partIndex){
        let parts = this.state.parts
        parts[partIndex].checklists = this.array_move(parts[partIndex].checklists, index, index + 1)
        this.setState({
            parts : parts
        })
    }
    handlePartUp(index){
        let parts = this.state.parts
        parts = this.array_move(parts, index, index - 1)
        this.setState({
            parts : parts
        })
    }
    handlePartDown(index){
        let parts = this.state.parts
        parts = this.array_move(parts, index, index + 1)
        this.setState({
            parts : parts
        })
    }
    handleAddTaskToChecklist(index){
        let checklists = this.state.checklists
        let tasks = checklists[index].tasks

        tasks.push({
            title: `` ,
          })
        // checklists[index].tasks = tasks
        this.setState({
            checklists : checklists
        }, ()=>{
            checklists[index].updated_at = new Date()
            this.setState({
                checklists : checklists
            })
        })

    }
    handleTaskChange(e){
    }
    array_move(arr, old_index, new_index) {
        if (new_index >= arr.length) {
            var k = new_index - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr; // for testing
    };
    handlePartRemoveConfirm(index){
        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this part?',
            buttons: [
              {
                label: 'Yes',
                onClick: () => this.handlePartRemove(index)
              },
              {
                label: 'No',
                onClick: () => {
                }
              }
            ]
          });
    }
    handlePartRemove(index){
        let parts = this.state.parts
        let removed_parts = this.state.removed_parts
        if(parts[index].id){
            removed_parts.push(parts[index].id)
        }
        parts.splice(index, 1)
        this.setState({
            parts : parts
        })
    }
    handleChecklistRemove(index, partIndex){
        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this checklist?',
            buttons: [
              {
                label: 'Yes',
                onClick: () => this.handleRemoveChecklist(index, partIndex)
              },
              {
                label: 'No',
                onClick: () => {
                }
              }
            ]
          });
    }
    handleRemoveChecklist(index, partIndex){
        let parts = this.state.parts

        let removed_checklists = this.state.removed_checklists
        if(parts[partIndex].checklists[index].id){
            removed_checklists.push(parts[partIndex].checklists[index].id)
        }

        parts[partIndex].checklists.splice(index, 1)

        this.setState({
            is_loading : true
        }, ()=>{

            this.setState({
                is_loading : false,
                parts : parts,
                removed_checklists : removed_checklists
            })
            NotificationManager.success("Removed manager successfully!", "Success", 3000)
        })
    }
    handleChangeTaskTree(treeData, index, partIndex){
        let parts = this.state.parts
        parts[partIndex].checklists[index].tasks = treeData
        this.setState({
            parts : parts
        })
    }
    handleSave(){

        let valid = true
        let errors = []
        this.state.parts.map((part, part_index) =>{
            if(part.part_name == ""){
                valid = false
                errors.push('Missing part name at Index:' + (part_index + 1) + '')
            }
            part.checklists.map((checklist, index) =>{
                if(checklist.name == ""){
                    valid = false
                    errors.push('Missing checklist name at Index:' + (index + 1) + '')
                }
                if(checklist.description == ""){
                    valid = false
                    errors.push('Missing checklist description at Index:' + (index + 1) + '')
                }
                checklist.tasks.map((task, taskIndex) =>{
                    if(task.title == ""){
                        valid = false
                        errors.push('Missing task tittle at Index:' + (index + 1) + '/taskIndex: ' + (taskIndex + 1))
                    }
                })
            })

        })

        if(valid == false){
            errors.map(error =>{
                NotificationManager.error(error, "Error", 5000)
            })
            return false
        }

        let post_data = {
            project_name : this.state.project_detail.project_name,
            parts : this.state.parts,
            removed_checklists : this.state.removed_checklists,
            removed_parts : this.state.removed_parts
        }
        Axios.post('/admin/ajax/project/update/' + this.state.project_detail.id, post_data)
        .then(response =>{
            this.setState({
                is_loading : false
            }, ()=>{
                if(response.data.status == 'success'){
                    NotificationManager.success(response.data.message, "Success", 3000)
                    this.props.history.push('/admin/project/view/' + this.state.project_detail.id)
                }
                else{
                    NotificationManager.error(response.data.message, "Error", 3000)
                }
            })
        })
    }


    handleChangePartName(event, index){
        let parts = this.state.parts
        parts[index].part_name = event.target.value
        this.setState({
            parts : parts
        })
    }
    handleChange(event){
        let project_detail = this.state.project_detail
        project_detail.project_name = event.target.value
        this.setState({
            project_detail : project_detail
        })
    }
    render() {
        return <div className="animated fadeIn">
             <LoadingOverlay
                        active={this.state.is_loading}
                        spinner={<BounceLoader />}
                    >

                </LoadingOverlay>
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="icon-user-follow"></i><strong>Edit Project</strong>
                            <div className="card-header-actions">

                            <Button className="mr-1" color="primary" onClick={this.handleAddNewPart} >
                                    <i className="fa fa-plus "></i>&nbsp;Add New Part
                            </Button>
                            <Button className="mr-1" color="success" onClick={this.handleSave} >
                                    <i className="fa fa-save "></i>&nbsp;Save
                            </Button>
                            <Button className="mr-1" color="success" onClick={this.handleView} >
                                    <i className="fa fa-eye "></i>&nbsp;View
                            </Button>

                                <a color="danger" className="btn-pill btn btn-danger text-white" onClick={() => this.props.history.goBack()}>
                                    <i className="icon-arrow-left"></i>&nbsp;Back
                            </a>
                            </div>
                        </CardHeader>
                        <CardBody>
                            <FormGroup row>
                                <Label sm="3" lg="2" htmlFor="project_name">Project Name</Label>
                                <Col sm="9" lg="10">
                                    <Input type="text" id="project_name" name="project_name" placeholder="Project Name" onChange={this.handleChange} value={this.state.project_detail && this.state.project_detail.project_name ? this.state.project_detail.project_name : ''} />
                                </Col>
                            </FormGroup>
                        </CardBody>

                <hr/>

                <CardBody>
                {
                    this.state.parts.map((part, index) =>{
                        return <Card key={index}>
                                    <CardHeader>
                                    <i className="icon-user-follow"></i><strong>{part.part_name ? "Part " + (index + 1) + ": "+  part.part_name : "New Part"}</strong>
                                    <div className="card-header-actions">
                                    {
                                    index != 0 && <Button color="primary" className="btn-pill" onClick={()=>this.handlePartUp(index)}>
                                    <i className="fa fa-arrow-up"></i>&nbsp;Up
                                </Button>
                                }
                                {
                                    this.state.parts.length - 1 != index &&  <Button color="primary" className="btn-pill"  onClick={()=>this.handlePartDown(index)}>
                                    <i className="fa fa-arrow-down"></i>&nbsp;Down
                                </Button>
                                }
                                    <Button   className="mr-1" color="primary" onClick={()=>this.handleAddChecklist(index)}>
                                    <i className="icon-plus"></i>&nbsp;Add New Checklist
                                    </Button>
                                    <Button className="mr-1" color="danger" onClick={()=>this.handlePartRemoveConfirm(index)} >
                                    <i className="fa fa-trash "></i>&nbsp;Remove
                            </Button>
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    <FormGroup row>
                                        <Label sm="3" lg="2" htmlFor="part_name">Project Name</Label>
                                        <Col sm="9" lg="10">
                                            <Input type="text"   name="part_name" placeholder="Project Part Name" onChange={(event)=>this.handleChangePartName(event, index)} value={part.part_name} />
                                        </Col>
                                    </FormGroup>


                                        <CardBody>
                                            {
                                                part.checklists.map((checklist, checklist_index) => {
                                                    return <div key={checklist_index}>
                                                        <ChecklistItem
                                                            checklist={checklist}
                                                            handleChange={(event, checkListIndex)=>this.handleChangeChecklist(event, checkListIndex, index)}
                                                            handleUp={(checkListIndex)=>this.handleChecklistUp(checkListIndex, index)}
                                                            handleDown={(checkListIndex)=>this.handleChecklistDown(checkListIndex, index)}
                                                            handleRemove={(checkListIndex)=>this.handleChecklistRemove(checkListIndex, index)}
                                                            // handleRemove = {this.handleChecklistRemove}
                                                            handleAddTask = {this.handleAddTaskToChecklist}
                                                            handleChangeTaskTree = {(treeData, checklistIndex)=>this.handleChangeTaskTree(treeData, checklistIndex, index)}
                                                            index={checklist_index}
                                                            length = {part.checklists.length} />
                                                    </div>
                                                })
                                            }
                                        </CardBody>

                                </CardBody>
                            </Card>
                    })
                }
                </CardBody>
                        {/**/}
                        <CardFooter>
                            <div className="card-header-actions">
                            <Button className="mr-1" color="primary" onClick={this.handleAddNewPart} >
                                    <i className="fa fa-plus "></i>&nbsp;Add New Part
                            </Button>
                            <Button className="mr-1" color="success" onClick={this.handleSave} >
                                    <i className="fa fa-save "></i>&nbsp;Save
                            </Button>
                            <Button className="mr-1" color="success" onClick={this.handleView} >
                                    <i className="fa fa-eye "></i>&nbsp;View
                            </Button>
                                <a color="danger" className="btn-pill btn btn-danger text-white" onClick={() => this.props.history.goBack()}>
                                    <i className="icon-arrow-left"></i>&nbsp;Back
                            </a>
                            </div>
                        </CardFooter>
                    </Card>
                </Col>
            </Row>
        </div>
    }
}
