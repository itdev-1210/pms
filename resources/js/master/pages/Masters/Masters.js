import React, { Fragment } from 'react'
import { connect } from 'react-redux';
import { Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import ReactDatatable from '@ashvin27/react-datatable';
import axios from 'axios'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'
import Axios from 'axios';
import { NotificationManager } from 'react-notifications';
class Masters extends React.Component{
    constructor(props) {
        super(props);
        this.columns = [
            {
                key: "number",
                text: "No",
                className: "number",
                align: "left",
                sortable: true,
            },
            {
                key: "name",
                text: "Name",
                className: "name",
                align: "left",
                sortable: true
            },
            {
                key: "email",
                text: "Email",
                className: "email",
                sortable: true
            },
            {
                key: "action",
                text: "Action",
                className: "action",
                width: 100,
                align: "left",
                sortable: false,
                cell: record => {
                    return (
                        <Fragment>
                            <button
                                className="btn btn-primary btn-sm"
                                onClick={() => this.editRecord(record.id)}
                                style={{marginRight: '5px'}}>
                                <i className="fa fa-edit"></i>
                            </button>
                            <button
                                className="btn btn-danger btn-sm"
                                onClick={() => this.deleteRecord(record.id)}>
                                <i className="fa fa-trash"></i>
                            </button>
                        </Fragment>
                    );
                }
            }
        ];
        this.config = {
            page_size: 10,
            length_menu: [ 10, 20, 50 ],
        }

        this.state = {
            records: [],
            masters: [],
            is_deleting : false
        }
        this.handleApprove = this.handleApprove.bind(this)
        this.handleRemoveMaster = this.handleRemoveMaster.bind(this)
        this.loadmasters = this.loadmasters.bind(this)
    }
    componentDidMount(){
        this.loadmasters()
    }
    loadmasters(){
        axios.get("/admin/ajax/masters")
        .then(response =>{
            this.setState({
                masters : response.data
            })
        })
    }
    editRecord(id){
        this.props.history.push(`/admin/master/edit/${id}`)
    }
    handleRemoveMaster(id){
        this.setState({
            is_deleting : true
        }, ()=>{
            Axios.post(`/admin/ajax/master/remove/${id}`)
            .then(response =>{
                if(response.data.status == 'success'){
                    this.setState({
                        is_deleting : false
                    }, ()=>{
                        this.loadmasters()
                    })
                    NotificationManager.success("Removed master successfully!", "Success", 3000)
                }
                else{
                    this.setState({
                        is_deleting : false
                    })
                    NotificationManager.error(response.data.message, "Error", 3000)
                }

            }).catch(() =>{
                this.setState({
                    is_deleting : false
                })
                NotificationManager.error("Failed to remove manager!", "Error", 3000)
            })
        })
        // alert(id)
    }
    deleteRecord(id){
        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this Master?',
            buttons: [
              {
                label: 'Yes',
                onClick: () => this.handleRemoveMaster(id)
              },
              {
                label: 'No',
                onClick: () => {

                }
              }
            ]
          });
    }
    handleApprove(e, id, index){
        e.preventDefault()
        e.target.checked = !e.target.checked
        let masters = this.state.masters
        masters[index].is_approved = e.target.checked ? 1 : 0
        this.setState({
            masters : masters
        })
    }

    render(){

        return <div className="animated fadeIn">
                <LoadingOverlay
                        active={this.state.is_deleting}
                        spinner={<BounceLoader />}
                    >

                </LoadingOverlay>
                <Row>
                    <Col xs="12">
                        <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i><strong>Masters</strong>
                            <div className="card-header-actions">
                            <Link color="primary" className="btn-pill btn btn-primary" to="/admin/master/add">
                                <i className="icon-plus"></i>&nbsp;New Master
                            </Link>
                            </div>
                        </CardHeader>
                        <CardBody>
                        <ReactDatatable
                            config={this.config}
                            records={this.state.masters}
                            columns={this.columns}
                        />
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
              </div>
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedMasters = connect(mapStateToProps)(Masters);
export default connectedMasters;
