import React from 'react'
import { Col } from 'reactstrap';
import { Button } from 'reactstrap';

export class AssignProject extends React.Component{
    constructor(props){
        super(props)
    }
    renderTasks(tasks, parent = 0){
        return <ul>
            {
                tasks.map((task, index) =>{
                    return  <div className="media m-1 p-3">
                    {/* <img src="http://pngimg.com/uploads/plus/plus_PNG47.png" alt="Add Employee" className="rounded-circle" style={{width: '60px'}} /> */}
                    <div className="media-body">
                      <h4>{task.task_name} <small><i></i></small></h4>
                      <Button color="danger" className="btn-pill"><i className="fa fa-plus"></i></Button>
                      {this.renderTasks(task.children)}
                    </div>
                  </div>
                    return <li key={index}>
                        {task.task_name}
                        
                        <ul>
                            {this.renderTasks(task.children)}
                        </ul>
                    </li>
                })
            }
        </ul> 
    }
    render(){
        return <Col xs="12">

            <h4 className="font-weight-bold" id={"part_" + this.props.part.id}>Part {this.props.index + 1}: {this.props.part.part_name}</h4>
            <ul style={{listStyle : 'decimal'}}>
            {this.props.part.checklists.map((checklist, index) =>{
                return  <div className="media m-1 p-3">
                {/* <img src="http://pngimg.com/uploads/plus/plus_PNG47.png" alt="Add Employee" className="rounded-circle" style={{width: '60px'}} /> */}
                <div className="media-body">
                  <h4>{checklist.name} <small><i></i></small></h4>
                  <Button color="danger" className="btn-pill"><i className="fa fa-plus"></i></Button>
                  {this.renderTasks(checklist.tasks)}
                </div>
              </div>
                return <li><h5 className="font-weight-bold" >{checklist.name}</h5>
                    {this.renderTasks(checklist.tasks)}
                
                </li>
            } )}
            </ul>
        </Col>
    }
}