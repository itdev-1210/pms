<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('project_name');
            $table->enum('project_status', ['draft', 'completed'])->default('draft');
            $table->integer('parent')->default(0);
            $table->enum('created_by', ['ADMIN', 'MANAGER'])->default('ADMIN');
            $table->timestamps();
            $table->string('project_name_by_manager')->nullable();
            $table->integer('manager_id')->default(0);
            $table->integer('duplicated_from')->default(0);
            $table->string('category', 255)->nullable();
            $table->boolean('is_notify')->default(false);
            $table->integer('inventory');
            $table->integer('limit_inventory')->default(0);
            $table->integer('sell_rate')->default(0);
            $table->string('sold_out_date')->nullable();
            $table->integer('lead_time')->default(90);
            $table->string('order_date')->nullable();
            $table->integer('order_amount')->default(0);
            $table->integer('income_inventory')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
