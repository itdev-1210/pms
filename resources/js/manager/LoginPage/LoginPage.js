import React, { Component }  from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { userActions } from '../_actions';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Alert } from 'reactstrap';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        // reset login status
        // this.props.dispatch(userActions.logout());

        this.state = {
            username: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        const { dispatch } = this.props;
        if (username && password) {
            dispatch(userActions.login(username, password));
        }
    }

    render() {
        const { loggingIn } = this.props;
        const { username, password, submitted } = this.state;
        return (
            <div className="app flex-row align-items-center">
            <NotificationContainer/>
            <Container>
                <Row className="justify-content-center">
                <Col md="8">
                    <CardGroup>
                    <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                        <CardBody className="text-center">
                        <div>
                            <h2>Project Manager Panel</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua.</p>
                        </div>
                        </CardBody>
                    </Card>
                    <Card className="p-4">
                        <CardBody>
                        <Form  name="form" onSubmit={this.handleSubmit}>
                            <h1>Login</h1>
                            <p className="text-muted">Sign In to your account</p>
                            <InputGroup className="mb-3">
                            <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                <i className="icon-user"></i>
                                </InputGroupText>
                            </InputGroupAddon>
                            <Input type="text" placeholder="Username" autoComplete="username"  name="username" value={username} onChange={this.handleChange} />

                            </InputGroup>
                            {submitted && !username &&
                                <Alert color="danger">
                                Username is required
                                </Alert>
                            }
                            <InputGroup className="mb-4">
                            <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                <i className="icon-lock"></i>
                                </InputGroupText>
                            </InputGroupAddon>
                            <Input type="password" placeholder="Password" autoComplete="current-password"  name="password" value={password} onChange={this.handleChange} />

                            </InputGroup>
                            {submitted && !password &&
                                <Alert color="danger">
                                Password is required
                                </Alert>
                            }
                            <Row>
                            <Col xs="6">
                                <Button color="ladda-button btn btn-primary btn-ladda" className="px-4" data-color = "red" data-style = "contract">Login</Button>

                            </Col>
                                <Col xs="6" className="text-right">
                                <Button color="ladda-button btn btn-primary btn-ladda" type="button" className="px-4" data-color = "red" data-style = "contract">SignUp</Button>
                            </Col>
                            <Col xs='12' className="mt-1 text-center">
                                <Link to="" className="text-center w-100 mt-1">Forgot Password?</Link>
                            </Col>
                            </Row>
                        </Form>
                        </CardBody>
                    </Card>

                    </CardGroup>
                </Col>
                </Row>
            </Container>
            </div>

        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage };
