<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Project extends Model
{
    //
    public $saved_tasks = [];

    public function project_tasks() {
        return $this->hasMany('App\Models\Task');
    }

    public function projectParts() {
        return $this->hasMany('App\Models\ProjectPart');
    }

    public function getManagerNameAttribute(){
        if($this->attributes['created_by'] == 'ADMIN'){
            return Admin::find($this->attributes["manager_id"]) ? Admin::find($this->attributes["manager_id"])->name : 'Administrator';
        }
        else{
            return Manager::find($this->attributes["manager_id"])->full_name;
        }
    }

    public function getManagerProfileImageSrcAttribute(){
        if($this->attributes['created_by'] == 'ADMIN'){
            return asset("/images/managers/default.png");
        }
        else{
            return asset("/storage/avatars/".$this->attributes['profile_image']);
        }
    }



    public function getParts(){
        return ProjectPart::where('project_id', $this->id)->orderBy('sort_number')->get();
    }

    public function get_details(){
        $parts = ProjectPart::with([
            'checkLists' => function($query) {
                $query->orderBy('sort_number');
            },
            'checkLists.tasks' => function($query) {
                $query->orderBy('sort_number');
            },
        ])
        ->where('project_id', $this->id)->orderBy('sort_number')->get()->toArray();
        foreach($parts as $part_key => $part){

            $checklists = $part['check_lists'];
            foreach($checklists as $key => $checklist){
                $tasks = $checklist['tasks'];
                unset($checklists[$key]['tasks']);
                $task = array(
                    'check_list_id' => -1,
                    'checklist_id_by_manager' => null,
                    'created_at' => "",
                    'created_by' => "",
                    'duplicated_from' => 1,
                    'employee_check_list_note' => [],
                    'id' => 0,
                    'parent' => -1,
                    'parent_by_manager' => null,
                    'project_employee_assignment' => [],
                    'project_id' => -1,
                    'sort_number' => 0,
                    'sort_number_by_manager' => null,
                    'task_name' => "",
                    'task_name_by_manager' => null,
                    'updated_at' => '',
                );

                array_unshift($tasks, $task);
                $new = array();
                foreach ($tasks as $a){
                    $new[$a['parent']][] = $a;
                }
                $tree = $this->getTasks($new, array($tasks[0]));
                $checklists[$key]['tasks'] = array_key_exists('children', $tree[0]) ? $tree[0]['children'] : [];
            }
            $parts[$part_key]['checklists'] = $checklists;
            unset($parts[$part_key]['check_lists']);
        }

        return array(
            'project_detail' => $project = Project::find($this->id)->toArray(),
            'parts' => $parts,
        );
    }

    /* Get Task Data */
    public function getTasks(&$list, $parent) {
        $tree = array();
        foreach ($parent as $k=>$l){
            $l['title'] = $l['task_name'];
            $l['node_type'] = "task";
            $l['node_id'] = $l['id'];
            $l['is_duplicate'] = false;
            if (array_key_exists('employee_check_list_note', $l)) {
                $l['notes'] = array_filter($l['employee_check_list_note'], function($value){return $value['node_type'] === 'task';});
            } else {
                $l['notes'] = [];
            }
            if (array_key_exists('project_employee_assignment', $l)) {
                $l['assigned_details'] = array_filter($l['project_employee_assignment'], function($value){return $value['node_type'] === 'task';});
            } else {
                $l['assigned_details'] = [];
            }
            if(isset($list[$l['id']])){
                $l['expanede'] = true;
                $l['children'] = $this->getTasks($list, $list[$l['id']]);
            }
            $tree[] = $l;
        }
        return $tree;
    }

    /* Update Project with part/checklists details */
    public function update_by_request($request, $id, $isAdmin = false){
        $project = Project::find($id);
        $project->project_name = $request->project_name;
        $project->category = $request->category;
        $project->limit_inventory = $request->limit_inventory;
        $project->lead_time = $request->lead_time;
        if ($project->category == 'winners' && $project->inventory < $project->limit_inventory)
            $project->is_notify = 1;
        else $project->is_notify = 0;
        $project->save();
        $assignedProjects = ProjectAssignment::where('original_project_id', $project->id)->groupBy('project_id')->get()->pluck('project_id');
        if($isAdmin == true){
            // DB::table('projects')->where('duplicated_from', $id)->update(
            //     ['project_name' => $request->project_name]
            // );
        }
        $parts = $request->parts;
        if(count($parts)){
            foreach($parts as $part_key => $part){
                $isNewPart = false;
                $isUpdatePart = false;
                if(isset($part['id']) && $part['id']){
                    $part_model = ProjectPart::find($part['id']);
                    if(!$part_model){
                        $part_model = new ProjectPart;
                        $isNewPart = true;
                    }
                }
                else{
                    $part_model = new ProjectPart;
                    $isNewPart = true;
                }

                $part_model->project_id = $project->id;
                $part_model->sort_number = $part_key;
                if($part_model->part_name != $part['part_name']){
                    $isUpdatePart = true;
                }
                $part_model->part_name = $part['part_name'];
                $part_model->save();

                if($isAdmin == true){
                    if($isNewPart == true){
                            foreach($assignedProjects as $assignedProject){
                                $assign_part_model = new ProjectPart;
                                $assign_part_model->project_id = $assignedProject;
                                $assign_part_model->sort_number = $part_key;
                                $assign_part_model->part_name = $part['part_name'];
                                $assign_part_model->duplicated_from = $part_model->id;
                                $assign_part_model->save();
                            }

                    }
                    if($isUpdatePart == true){
                        DB::table('project_parts')->where('duplicated_from', $part_model->id)->update(
                            ['part_name' => $part['part_name']]
                        );
                    }
                }

                $checklists =  $part['checklists'];
                if(count($checklists)){
                    foreach($checklists as $key => $checklist){
                        $isNewChecklist = false;
                        $isUpdateChecklistName = false;
                        $isUpdateChecklistDescription = false;
                        if(isset($checklist['id']) && $checklist['id']){
                            $checklisst_model = CheckList::find($checklist['id']);
                        }
                        else{
                            $checklisst_model = new CheckList;
                            $isNewChecklist = true;
                        }

                        if($checklisst_model->name != $checklist['name']){
                            $isUpdateChecklistName = true;
                        }
                        $checklisst_model->name = $checklist['name'];
                        if($checklisst_model->description != $checklist['description']){
                            $isUpdateChecklistDescription = true;
                        }
                        $checklisst_model->description = $checklist['description'];

                        $checklisst_model->project_id = $project->id;
                        $checklisst_model->project_part_id = $part_model->id;

                        $checklisst_model->sort_number = $key;

                        $checklisst_model->save();

                        if($isAdmin == true){
                            if($isNewChecklist == true){
                                    $assigned_parts = ProjectPart::where('duplicated_from', $part_model->id)->get();
                                    foreach($assigned_parts as $assigned_part){
                                        $assigned_checklisst_model = new CheckList;
                                        $assigned_checklisst_model->name = $checklist['name'];
                                        $assigned_checklisst_model->description = $checklist['description'];
                                        $assigned_checklisst_model->project_id = $assigned_part->project_id;
                                        $assigned_checklisst_model->project_part_id = $assigned_part->id;
                                        $assigned_checklisst_model->sort_number = $key;
                                        $assigned_checklisst_model->duplicated_from = $checklisst_model->id;
                                        $assigned_checklisst_model->save();
                                    }
                            }
                            else{
                                if($isUpdateChecklistDescription == true){
                                    DB::table('checklists')->where('duplicated_from', $checklisst_model->id)->update(
                                        [
                                            'description' => $checklist['description'],

                                            'sort_number' => $key,
                                        ]
                                    );
                                }
                                if($isUpdateChecklistName == true){
                                    DB::table('checklists')->where('duplicated_from', $checklisst_model->id)->update(
                                        [
                                            'name' => $checklist['name'],

                                            'sort_number' => $key,
                                        ]
                                    );
                                }

                            }
                        }

                        $tasks = $checklist['tasks'];
                        if(count($tasks)){
                            foreach($tasks as $t_key => $task_req){
                            $saved_tasks = $this->saveTasks($task_req, $project->id, $checklisst_model->id, 0, $t_key, $isAdmin);
                            }
                            $removeable_tasks = Task::where("project_id",$project->id)->where("check_list_id", $checklisst_model->id)->whereNotIn("id", $this->saved_tasks)->pluck('id')->toArray();
                            if ($removeable_tasks) {
                                Task::whereIn('duplicated_from', $removeable_tasks)->delete();
                                Task::whereIn('id', $removeable_tasks)->delete();
                            }
                        }
                    }
                }
           }
           $removed_checklists = $request->removed_checklists;
               if(count($removed_checklists)){
                   foreach($removed_checklists as $checklist){
                       CheckList::find($checklist)->delete();
                       CheckList::where('duplicated_from',$checklist)->delete();
                   }
               }
            $removed_parts = $request->removed_parts;
            if(count($removed_parts)){
                foreach($removed_parts as $part){
                    ProjectPart::find($part)->delete();
                    ProjectPart::where('duplicated_from',$part)->delete();
                }
            }
       }

        return array(
            'status' => 'success',
            'message' => "Saved project data successfully!",
        );

    }
     /* Save Task Table */
     public function saveTasks($task_req, $project_id, $check_list_id, $parent, $sort_number = 0, $isAdmin = false){
        $isNewTask = false;
        $isUpdateTask = false;
        if (isset($task_req['is_duplicate']) && $task_req['is_duplicate']) {
            $task_model = new Task;
            $isNewTask = true;
         } else {
            if(isset($task_req['id']) && $task_req['id']){
                $task_model = Task::find($task_req['id']);
            }
            else{
                $task_model = new Task;
                $isNewTask = true;
            }
         }
        $task_model->project_id = $project_id;
        $task_model->check_list_id = $check_list_id;
        if($task_model->task_name != $task_req['title'] || $task_model->category !== $task_req['category']){
            $isUpdateTask = true;
        }
        $task_model->task_name = $task_req['title'];
        $task_model->sort_number = $sort_number;
        $task_model->parent = $parent;
        if ($task_model->category !== $task_req['category'])
            $task_model->is_change_category = 1;
        $task_model->category = $task_req['category'];
        $task_model->save();

        if($isAdmin == true){
            if($isNewTask == true){
                if($parent == 0){
                    $assigned_checklists = CheckList::where('duplicated_from', $task_model->check_list_id)->get();
                    foreach($assigned_checklists as $key => $assigned_checklist){
                        $assign_task_model = new Task;
                        $assign_task_model->project_id = $assigned_checklist->project_id;
                        $assign_task_model->check_list_id = $assigned_checklist->id;
                        $assign_task_model->task_name = $task_req['title'];
                        $assign_task_model->category = $task_req['category'];
                        $assign_task_model->sort_number = $sort_number;
                        $assign_task_model->parent = 0;
                        $assign_task_model->duplicated_from = $task_model->id;
                        $assign_task_model->save();
                    }
                }
                else{
                    $assigned_tasks = Task::where('duplicated_from', $task_model->parent)->get();
                    foreach ($assigned_tasks as $assigned_task_parent){
                        $assign_task_model = new Task;
                        $assign_task_model->project_id = $assigned_task_parent->project_id;
                        $assign_task_model->check_list_id = $assigned_task_parent->check_list_id;
                        $assign_task_model->task_name = $task_req['title'];
                        $assign_task_model->category = $task_req['category'];
                        $assign_task_model->sort_number = $sort_number;
                        $assign_task_model->parent = $assigned_task_parent->id;
                        $assign_task_model->duplicated_from = $task_model->id;
                        $assign_task_model->save();
                    }
                }
            }
            else{
                if($isUpdateTask){
                    DB::table('tasks')->where('duplicated_from', $task_model->id)->update(
                        [
                            'task_name' => $task_req['title'],
                            'sort_number' => $sort_number,
                            // 'parent' => $key,
                        ]
                    );
                    DB::table('tasks')->where('duplicated_from', $task_model->id)->where('is_change_category', 0)->update(
                        [
                            'category' => $task_req['category']
                        ]
                    );
                }

            }
        }



        $saved[] = $task_model->id;
        $this->saved_tasks[] = $task_model->id;
        if (isset($task_req['is_duplicate']) && $task_req['is_duplicate']) {
            $max_id = Task::max('id');
            $duplicate_tasks = $this->getDuplicateTasks($max_id, $task_req, $project_id, $check_list_id, $task_model->id, $sort_number, $isAdmin);
            array_shift($duplicate_tasks);
            Task::insert($duplicate_tasks);
            return;
        } else {
            if (isset($task_req['children']) && count($task_req['children'])) {
                foreach ($task_req['children'] as $sort_key => $child_task) {
                    $this->saveTasks($child_task, $project_id, $check_list_id, $task_model->id, $sort_key, $isAdmin);
                }
            }
        }
    }

    public function getDuplicateTasks($max_id, $task_req, $project_id, $check_list_id, $parent, $sort_number = 0, $isAdmin = false){
        $duplicate_tasks = array();

        $task_model = array();
        $task_model['project_id'] = $project_id;
        $task_model['check_list_id'] = $check_list_id;
        $task_model['task_name'] = $task_req['title'];
        $task_model['sort_number'] = $sort_number;
        $task_model['parent'] = $parent;
        $task_model['id'] = $max_id;
        $duplicate_tasks[] = $task_model;
        $this->saved_tasks[] = $max_id;

        if (array_key_exists('children', $task_req)) {
            $parent = $max_id;
            foreach($task_req['children'] as $sort_key => $child_task) {
                $result = $this->getDuplicateTasks(++$max_id, $child_task, $project_id, $check_list_id, $parent, $sort_key, $isAdmin);
                foreach($result as $r) {
                    $duplicate_tasks[] = $r;
                }
                $max_id += count($result) - 1;
            }
        }

        return $duplicate_tasks;
    }
}
