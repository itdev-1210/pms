<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class AuthController extends Controller
{
    /**
     * Show the application Login Page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.index');
    }

    public function login(Request $request){

        $this->validate($request, [
            'username'   => 'required',
            'password' => 'required|min:3'
        ]);
        Auth::shouldUse('admin');

        if (Auth::attempt(['name' => $request->username, 'password' => $request->password], 1)) {
            return array(
                'status' => 'success',
                'user_info' => Auth::user()
            );
        }
        return array('status' => 'error');
    }

    public function logout(Request $request) {
        Auth::shouldUse('admin');
        Auth::logout();
    }
}
