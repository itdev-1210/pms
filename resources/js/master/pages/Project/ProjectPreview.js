import React, {  } from 'react'
import { connect } from 'react-redux';
import {
    Button,
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
} from 'reactstrap';
import Axios from 'axios';
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'
import TreeMenu from 'react-simple-tree-menu';
import 'react-simple-tree-menu/dist/main.css';
import { ProjectDetail } from './ProjectDetail';
import ReactPDF from '@react-pdf/renderer';

class ProjectPreview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            is_draging: false,
            is_loading: true,
            project_detail: {},
            parts: [],
            menu: [],
            removed_checklists : [],
            removed_tasks : []
        }
        this.genderatePdf = this.genderatePdf.bind(this)
        this.handleMenuClick = this.handleMenuClick.bind(this)
        this.reload = this.reload.bind(this)
        this.moveFocusItem = this.moveFocusItem.bind(this)
    }
    componentDidMount() {
        this.reload()
    }
    reload(){
        Axios.get('/admin/ajax/project/preview/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    project_detail: response.data.project_detail,
                    parts: response.data.parts,
                    menu: response.data.menu,
                    is_loading : false
                })
            })
    }
    genderatePdf(){
        ReactPDF.render(<CardBody>
            {/*  */}

          <h2 className="font-weight-bold text-center text-capitalize">{this.state.project_detail.project_name}</h2>
          <hr/>
          <Row>

          {
              this.state.parts.map((part, partIndex) =>{
                  return <ProjectDetail key={partIndex} index={partIndex} part = {part} reload={this.reload}/>
              })
          }
          </Row>
          </CardBody>   , `${__dirname}/${this.state.project_detail.project_name}.pdf`);

    }
    handleMenuClick(Item){
        if (Item.parent === '') {
            this.moveFocusItem(Item.key)
        } else {
            var keyArr = Item.key.split('/');
            this.setState({focusKey: keyArr[keyArr.length - 1]})
        }
    }

    moveFocusItem(id) {
        this.refs[id].scrollIntoView({
            behavior: 'smooth',
            block: 'start',
        });
    }

    render() {
        return <div className="animated fadeIn">
             <LoadingOverlay
                        active={this.state.is_loading}
                        spinner={<BounceLoader />}
                    >
                </LoadingOverlay>
            <Row>
                <Col  md="3">
                <TreeMenu data={this.state.menu} onClickItem={this.handleMenuClick}/>
                </Col>
                <Col   md="9">
                <Card>
                <CardHeader>
                            <i className="icon-user-follow"></i><strong>Preview Project</strong>
                            <div className="card-header-actions">
                                {/* <Button color="primary" className="btn-pill mr-1" onClick={this.genderatePdf} >
                                <i className="fa fa-file-pdf-o"></i>&nbsp;PDF Generate
                                </Button> */}
                                <Button color="success" className="btn-pill mr-1" onClick={()=>this.props.history.push("/admin/project/edit/" + this.props.match.params.id)} >
                                <i className="fa fa-pencil"></i>&nbsp;Edit
                                </Button>
                                <a color="danger" className="btn-pill btn btn-danger text-white" onClick={() => this.props.history.goBack()}>
                                        <i className="icon-arrow-left"></i>&nbsp;Back
                                </a>
                            </div>
                        </CardHeader>
                    <CardBody>
                        {/*  */}

                      <h2 className="font-weight-bold text-center text-capitalize">{this.state.project_detail.project_name}</h2>
                      <hr/>
                      <Row>
                      {
                          this.state.parts.map((part, partIndex) =>{
                              return <div ref={"part_"+part.id} key={partIndex} style={{width: '100%'}}>
                                <ProjectDetail name={this.state.project_detail.project_name} focusKey={this.state.focusKey} index={partIndex} part = {part} reload={this.reload}/>
                              </div>
                          })
                      }
                      </Row>
                      </CardBody>
                    </Card>
                </Col>
            </Row>
        </div>
    }
}




function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedProjectPreview = connect(mapStateToProps)(ProjectPreview);
export default connectedProjectPreview;
