import React from 'react'
import Axios from 'axios';
import { ProjectDetail } from '../../_components/Project';
import { Button, Col, Row, Label, Card, CardBody, Input, CardTitle} from 'reactstrap';
import DatePicker from 'react-date-picker'
import moment from 'moment'

export default class AllProjects extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            start_date: null,
            end_date: null,
            complete_date_start: null,
            complete_date_end: null,
            assigneddates: [],
            dup_data: [],
            taskType: "0",
            weekLists: [],
            updateFlag: false,
            selectedWeekNumber: null,
            selectedTasks: [],
            isArchivedWeeks: true,
        }
        this.handleSearch = this.handleSearch.bind(this)
        this.reload = this.reload.bind(this)
        this.changeStart = this.changeStart.bind(this)
        this.changeEnd = this.changeEnd.bind(this)
        this.onChangeType = this.onChangeType.bind(this)
        this.onChangeUpdateType = this.onChangeUpdateType.bind(this)
        this.onChangeWeekNumber = this.onChangeWeekNumber.bind(this)
        this.changeCompleteDateStart = this.changeCompleteDateStart.bind(this)
        this.changeCompleteDateEnd = this.changeCompleteDateEnd.bind(this)
        this.handleFilter = this.handleFilter.bind(this)
        this.handleUpdate = this.handleUpdate.bind(this)
        this.onSelectedTasks = this.onSelectedTasks.bind(this)

        this.onDragOverTask = this.onDragOverTask.bind(this);
        this.onDropTask = this.onDropTask.bind(this);
        this.onArchivedWeeks = this.onArchivedWeeks.bind(this);
    }
    componentDidMount() {
        this.reload()
    }
    reload() {
        Axios.get('/ajax/assigneddates')
        .then(response => {
            this.setState({
                currentMonthTasks: response.data.currentMonthTasks,
                tasks: response.data.tasks,
                assigneddates: this.state.isArchivedWeeks ? response.data.currentMonthTasks : response.data.tasks,
                dup_data: this.state.isArchivedWeeks ? response.data.currentMonthTasks : response.data.tasks,
                weekLists: response.data.weekLists
            }, ()=> this.handleFilter())

            if (response.data.weekLists.length > 0) {
                this.setState({ selectedWeekNumber: response.data.weekLists[0].week_number });
            }
        })
    }
    changeStart(date) {
        this.setState({ start_date: date })
    }

    changeEnd(date) {
        this.setState({ end_date: date })
    }

    changeCompleteDateStart(date) {
        this.setState({ complete_date_start: date }, ()=>{this.handleFilter()})
    }

    changeCompleteDateEnd(date) {
        this.setState({ complete_date_end: date }, ()=>{this.handleFilter()})
    }

    onChangeType(type) {
        this.setState({taskType: type.target.value}, ()=>{this.handleFilter()})
    }

    onChangeUpdateType(type) {
        this.setState(({
            updateFlag: type.target.value === '1',
            selectedTasks: []
        }));
    }

    onChangeWeekNumber(type) {
        this.setState({ selectedWeekNumber: type.target.value });
    }

    handleFilter() {
        let data_string = JSON.stringify(this.state.dup_data)
        let assigneddates = JSON.parse(data_string)
        let tmp_data = []
        if (this.state.taskType.toString() === '1') {
            tmp_data = assigneddates.filter((assign) => {
                let tmp_projects = [];
                Object.keys(assign.category).map(key => {
                    tmp_projects = assign.category[key].filter((project) => {
                        let tmp_tasks = project.tasks.filter((task) => { return task.is_completed === 1})
                        project.tasks = tmp_tasks
                        return tmp_tasks.length > 0
                    })

                    assign.category[key] = tmp_projects
                });
                return true
            })
        } else if (this.state.taskType.toString() === '3') {
            tmp_data = assigneddates.filter((assign) => {
                let tmp_projects = [];
                Object.keys(assign.category).map(key => {
                    tmp_projects = assign.category[key].filter((project) => {
                        let tmp_tasks = project.tasks.filter((task) => { return task.is_completed === 0})
                        project.tasks = tmp_tasks
                        return tmp_tasks.length > 0
                    })

                    assign.category[key] = tmp_projects
                });
                return true
            })
        } else if (this.state.taskType.toString() === '2') {
            tmp_data = assigneddates.filter((assign) => {
                let tmp_projects = [];
                Object.keys(assign.category).map(key => {
                    tmp_projects = assign.category[key].filter((project) => {
                        let tmp_tasks = project.tasks.filter((task) => {
                            if (this.state.complete_date_start && this.state.complete_date_end)
                                return task.is_completed === 1 && moment(this.state.complete_date_start).isSameOrBefore(moment(task.completed_at), 'date') && moment(this.state.complete_date_end).isSameOrAfter(moment(task.completed_at), 'date')
                            else if (this.state.complete_date_start && !this.state.complete_date_end)
                                return task.is_completed === 1 && moment(this.state.complete_date_start).isSameOrBefore(moment(task.completed_at), 'date')
                            else if (!this.state.complete_date_start && this.state.complete_date_end)
                                return task.is_completed === 1 && moment(this.state.complete_date_end).isSameOrAfter(moment(task.completed_at), 'date')
                            else
                                return task.is_completed === 1
                        })
                        project.tasks = tmp_tasks
                        return tmp_tasks.length > 0
                    })

                    assign.category[key] = tmp_projects
                });
                return true
            })
        } else {
            tmp_data = assigneddates
        }
        this.setState({assigneddates: tmp_data})
    }
    handleSearch(e) {
        e.preventDefault()
        const { start_date, end_date } = this.state
        let start_string = start_date ? start_date.getFullYear() + "-" + ("0" + (start_date.getMonth() + 1)).slice(-2) + "-" + ("0" + start_date.getDate()).slice(-2) : null;
        let end_string = end_date ? end_date.getFullYear() + "-" + ("0" + (end_date.getMonth() + 1)).slice(-2) + "-" + ("0" + end_date.getDate()).slice(-2) : null;
        Axios({
            method: 'post',
            url: '/ajax/assigneddates',
            data: {
                start_date: start_string,
                end_date: end_string,
            },
        })
            .then(res => {
                this.setState({
                    currentMonthTasks: res.data.currentMonthTasks,
                    tasks: res.data.tasks,
                    assigneddates: this.state.isArchivedWeeks ? res.data.currentMonthTasks : res.data.tasks
                })
            })
            .catch(() => {
            })
    }

    handleUpdate() {
        let post_data = {
            week_number: this.state.selectedWeekNumber,
            task_ids: this.state.selectedTasks
        };
        Axios.post('/multi_move_task', post_data)
            .then(response => {
                this.setState({
                    selectedTasks: [],
                    updateFlag: false
                }, () => this.reload());

                this.setState({ updateFlag: true });
            })
    }

    onSelectedTasks(flag, task) {
        let task_id = task.id;
        let selectedTasks = this.state.selectedTasks.slice(0);
        if (flag) {
            if (selectedTasks.indexOf(task_id) === -1)
                selectedTasks.push(task_id);
        } else {
            if (selectedTasks.indexOf(task_id) !== -1)
                selectedTasks.splice(selectedTasks.indexOf(task_id), 1);
        }

        this.setState({ selectedTasks: selectedTasks });
    }

    onDragOverTask(event) {
        event.preventDefault();
    }

    onDropTask(event, week_number) {
        let isTask = event.dataTransfer.getData("isTask");
        if (isTask) {
            let task = JSON.parse(event.dataTransfer.getData("task"));
            let project_id = event.dataTransfer.getData("project_id");
            if (week_number === task.week_number) {
                if (this.props.part.project_id === parseInt(project_id)) {
                    let task_ids = this.props.part.tasks.map(item => item.id);
                    let dropped_id = parseInt(event.currentTarget.getAttribute("task_id"));
                    if (task.id === dropped_id)
                        return;

                    task_ids.splice(task_ids.indexOf(dropped_id), 0, task_ids.splice(task_ids.indexOf(task.id), 1)[0]);
                    let post_data = {
                        ids: task_ids,
                        type: 1
                    };

                    Axios.post('/sort_task', post_data)
                        .then(response => {
                            this.reload();
                        })
                }
            } else {
                let post_data = {
                    week_number: week_number,
                    task_id: task.id
                };

                Axios.post('/move_task', post_data)
                    .then(response => {
                        this.reload();
                    })
            }
        }
    }

    onArchivedWeeks() {
        let isArchivedWeeks = !this.state.isArchivedWeeks;
        this.setState({
            assigneddates: isArchivedWeeks ? this.state.currentMonthTasks : this.state.tasks,
            dup_data: isArchivedWeeks ? this.state.currentMonthTasks : this.state.tasks,
            isArchivedWeeks: isArchivedWeeks
        });
    }

    render() {
        return (
            <div className="ml-0 flex-fill d-flex flex-column">
                <Card className="row">
                    <CardBody>
                        <Row>
                            <div className="ml-3">
                                <CardTitle>Filters</CardTitle>
                                <div>
                                    <span className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="from_date" className="mr-sm-2">From</Label>
                                        <DatePicker name='start_date' id="from_date" onChange={this.changeStart} value={this.state.start_date} />
                                    </span>
                                    <span className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="to_date" className="mr-sm-2">To</Label>
                                        <DatePicker name='to_date' id="to_date" onChange={this.changeEnd} value={this.state.end_date} />
                                    </span>
                                    <Button onClick={this.handleSearch}>Search</Button>
                                </div>
                            </div>
                            <div className="ml-3 mt-auto">
                                <b>Filters</b>
                                <Input type="select" name="select" id="exampleSelect" onChange={this.onChangeType}>
                                    <option value="0">View All Tasks</option>
                                    <option value="1">View Completed Tasks</option>
                                    <option value="2">View All Tasks By Date Completed</option>
                                    <option value="3">View Uncopmleted Tasks</option>
                                </Input>
                            </div>
                            <div className="ml-2 mt-auto">
                                <b>Update</b>
                                <Input type="select" name="select" id="updateSelect" onChange={this.onChangeUpdateType}>
                                    <option value="0"></option>
                                    <option value="1">Update Week Assigned</option>
                                </Input>
                            </div>
                            <div className="ml-2 mt-auto">
                                <Input type="select" name="select" id="updateWeek" onChange={this.onChangeWeekNumber}>
                                    {this.state.weekLists.map((data, index) => {
                                        return <option value={data.week_number} key={index}>{data.week_assigned}</option>
                                    })}
                                </Input>
                            </div>
                            <div className="ml-2 mt-auto">
                                <Button onClick={this.handleUpdate} disabled={this.state.selectedTasks.length === 0}>Update</Button>
                            </div>
                            {
                                this.state.taskType === '2' &&
                                <div className="ml-3 mt-auto">
                                    <Label for="complete_date_start" className="mr-sm-2">From</Label>
                                    <DatePicker name='complete_date_start' id="complete_date_start" onChange={this.changeCompleteDateStart} value={this.state.complete_date_start} />
                                    <Label for="complete_date_end" className="mr-sm-2">To</Label>
                                    <DatePicker name='complete_date_end' id="complete_date_end" onChange={this.changeCompleteDateEnd} value={this.state.complete_date_end} />
                                </div>
                            }
                        </Row>
                    </CardBody>
                </Card>

                <Card className='row mt-5 flex-fill d-flex flex-column'>
                    <Row className="flex-fill pr-4">
                        <Col md="3" className="border-right my-2 overflow-auto" style={{maxHeight: '60vh'}}>
                            <h4 className="ml-2">Week Assigned</h4>
                            <div className="list-group">
                                <a className="list-group-item-action py-2 pl-4" onClick={this.onArchivedWeeks} style={{cursor:'pointer'}}>
                                    <h5>Archived Weeks</h5>
                                </a>
                                {
                                    (this.state.assigneddates && this.state.assigneddates.length) ? this.state.assigneddates.map((date, index) => {
                                        return <a href={`#date-item-${index}`} className="list-group-item-action py-2 pl-4" key={index}>
                                            <div className="d-flex w-100 justify-content-between">
                                                {date.week_assigned}
                                            </div>
                                        </a>
                                    }) : null
                                }
                            </div>
                        </Col>
                        <Col md="9" className="my-2 flex-fill p-4 overflow-auto" style={{maxHeight: '60vh', scrollBehavior: "smooth"}}>

                            {
                                (this.state.assigneddates && this.state.assigneddates.length) ? this.state.assigneddates.map((date, index) => {
                                    return (
                                        <div key={index} className="mb-4"
                                             draggable
                                             onDragOver={(event)=>this.onDragOverTask(event)}
                                             onDrop={(event)=>this.onDropTask(event, date.week_number)}>
                                            <h2 id={`date-item-${index}`}>{date.week_assigned}</h2>
                                            {date.category.Setup.length ? <h3>Setup</h3> : null}
                                            {date.category.Setup.length ? date.category.Setup.map((part, partindex) =>
                                                <ProjectDetail
                                                    length={date.category.Setup.length}
                                                    projects={date.category.Setup}
                                                    userId={this.state.userId}
                                                    part={part}
                                                    key={partindex}
                                                    index={partindex}
                                                    reload={this.reload}
                                                    week_number={date.week_number}
                                                    updateFlag={this.state.updateFlag}
                                                    onSelectedTasks={(flag, task) => this.onSelectedTasks(flag, task)}
                                                />
                                            ) : null}
                                            {date.category.Launch.length ? <h3>Launch</h3> : null}
                                            {date.category.Launch.length ? date.category.Launch.map((part, partindex) =>
                                                <ProjectDetail
                                                    length={date.category.Launch.length}
                                                    projects={date.category.Launch}
                                                    userId={this.state.userId}
                                                    part={part}
                                                    key={partindex}
                                                    index={partindex}
                                                    reload={this.reload}
                                                    week_number={date.week_number}
                                                    updateFlag={this.state.updateFlag}
                                                    onSelectedTasks={(flag, task) => this.onSelectedTasks(flag, task)}
                                                />
                                            ) : null}
                                            {date.category.Refresh.length ? <h3>Refresh</h3> : null}
                                            {date.category.Refresh.length ? date.category.Refresh.map((part, partindex) =>
                                                <ProjectDetail
                                                    length={date.category.Refresh.length}
                                                    projects={date.category.Refresh}
                                                    userId={this.state.userId}
                                                    part={part}
                                                    key={partindex}
                                                    index={partindex}
                                                    reload={this.reload}
                                                    week_number={date.week_number}
                                                    updateFlag={this.state.updateFlag}
                                                    onSelectedTasks={(flag, task) => this.onSelectedTasks(flag, task)}
                                                />
                                            ) : null}
                                            {date.category.Optimize.length ? <h3>Optimize</h3> : null}
                                            {date.category.Optimize.length ? date.category.Optimize.map((part, partindex) =>
                                                <ProjectDetail
                                                    length={date.category.Optimize.length}
                                                    projects={date.category.Optimize}
                                                    userId={this.state.userId}
                                                    part={part}
                                                    key={partindex}
                                                    index={partindex}
                                                    reload={this.reload}
                                                    week_number={date.week_number}
                                                    updateFlag={this.state.updateFlag}
                                                    onSelectedTasks={(flag, task) => this.onSelectedTasks(flag, task)}
                                                />
                                            ) : null}
                                            {date.category.Miscellaneous.length ? <h3>Miscellaneous</h3> : null}
                                            {date.category.Miscellaneous.length ? date.category.Miscellaneous.map((part, partindex) =>
                                                <ProjectDetail
                                                    length={date.category.Miscellaneous.length}
                                                    projects={date.category.Miscellaneous}
                                                    userId={this.state.userId}
                                                    part={part}
                                                    key={partindex}
                                                    index={partindex}
                                                    reload={this.reload}
                                                    week_number={date.week_number}
                                                    updateFlag={this.state.updateFlag}
                                                    onSelectedTasks={(flag, task) => this.onSelectedTasks(flag, task)}
                                                />
                                            ) : null}
                                        </div>)
                                }): null
                            }
                        </Col>
                    </Row>
                </Card>
            </div>)
    }
}
