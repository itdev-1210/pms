<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckList extends Model
{
    //
    protected $table = "checklists";

    // protected $appends = ['assigned_employees'];
    // public function getAssignedEmployeesAttribute(){
    //     $result = array();
    //     $assigns = ProjectEmployeeAssignment::with('employee_details')->where('node_type', 'checklist')->where('node_id', $this->attributes['id'])->get();
    //     foreach($assigns as $assign){
    //         $result[] = $assign->employee_details;
    //     }
    //     return $result;
    // }

    public static function getAssignedIds($projectid, $partid, $employeeid, $date = null){
        $checklists = self::where('project_id', $projectid)->where('project_part_id', $partid)->get();
        $assigned = [];
        foreach($checklists as $checklist){
            $checklist_assign_query = ProjectEmployeeAssignment::where('node_type', 'checklist')->where('employee_id', $employeeid)->where('node_id', $checklist->id);
            if($date){
                $checklist_assign_query->whereRaw("Date(`created_at`) = '$date'");
            }
            if(($checklist_assign_query->count())){
                $assigned[] = $checklist->id;
            }
            else{
                if(count(Task::getAssignedIds($projectid, $checklist->id, $employeeid, $date))){
                    $assigned[] = $checklist->id;
                }
            }
        }
        return $assigned;
    }

    /**
     * Get the project part instance of this check list instance
     */
    public function projectPart()
    {
        return $this->belongsTo(ProjectPart::class);
    }

    /**
     * Get all the tasks of this CheckList
     */
    public function tasks() {
        return $this->hasMany(Task::class);
    }

    /**
     * Get all the projectEmployeeAssignment of this CheckList
     */
    public function projectEmployeeAssignment() {
        return $this->hasMany(ProjectEmployeeAssignment::class,'node_id','id');
    }

    /**
     * Get all the employee check list note of this task
     */
    public function employeeCheckListNote() {
        return $this->hasMany(EmployeeChecklistNote::class,'node_id','id');
    }
}
