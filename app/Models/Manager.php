<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use NotificationChannels\WebPush\HasPushSubscriptions;

class Manager  extends Authenticatable
{
    //
    use Notifiable, HasPushSubscriptions;

    protected $guard = 'writer';

    protected $fillable = [
        'name', 'email', 'password','first_name', 'last_name'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['full_name', 'profile_image_src'];

    public function getFullNameAttribute(){
        return $this->attributes['first_name']." ".$this->attributes['last_name'];
    }

    public function getProfileImageSrcAttribute(){
        if($this->attributes['profile_image'] == "" || $this->attributes['profile_image'] == "default.png"){
            return "https://ui-avatars.com/api/?name=" . $this->attributes['first_name']." ".$this->attributes['last_name'];
        }
        return asset("/storage/avatars/".$this->attributes['profile_image']);
    }
}
