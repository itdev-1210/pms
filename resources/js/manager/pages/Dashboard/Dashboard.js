import React, {Suspense} from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import * as router from 'react-router-dom';

import { userActions } from '../../_actions';
import navigation from '../../Layouts/Navigation'
const DefaultFooter = React.lazy(() => import('../../Layouts/DefaultFooter'));
const DefaultHeader = React.lazy(() => import('../../Layouts/DefaultHeader'));
import { Container } from 'reactstrap';
import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import renderHTML from 'react-render-html'

import {
    AppFooter,
    AppHeader,
    AppSidebar,
    AppSidebarFooter,
    AppSidebarHeader,
    AppSidebarMinimizer,
    AppBreadcrumb2 as AppBreadcrumb,
    AppSidebarNav2 as AppSidebarNav,
  } from '@coreui/react';
import routes from '../../routes';
import { LoginPage } from '../../LoginPage';
import Axios from "axios/index";

let avatar_style = {
    backgroundImage: ''
}

class Dashboard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            menus: navigation.items
        }
    }

    componentDidMount() {
        if (this.props.location.pathname !== "/login")
            this.fetchMenus()
    }

    fetchMenus() {
        Axios.get('/manager/ajax/getmenus')
            .then(({ data }) => {
                let allEmployeeMenus = [];
                data.forEach(temp => {
                    let retired = temp.menus.retired.map(item => ({
                        name: ' - ' + item.project_name,
                        url: `/manager/project/${temp.id}/${item.id}`
                    }))
                    let winners = temp.menus.winners.map(item => ({
                        name: ' - ' + item.project_name,
                        url: `/manager/project/${temp.id}/${item.id}`
                    }))
                    let testing = temp.menus.testing.map(item => ({
                        name: ' - ' + item.project_name,
                        url: `/manager/project/${temp.id}/${item.id}`
                    }))
                    const others = temp.menus.others.map(item => ({
                        name: item.project_name,
                        url: `/manager/project/${temp.id}/${item.id}`
                    }));

                    let user_profile = 'test';

                    let menus = {
                        name: renderHTML('<img class="avatar_image" src="' + temp.profile_image_src + '">' + temp.first_name + ' ' + temp.last_name),
                        children: [{
                            name: 'All Projects',
                            url: `/manager/projects/${temp.id}`
                        }, {
                            name: 'WINNERS',
                            children: winners
                        }, {
                            name: 'TESTING',
                            children: testing
                        }, {
                            name: 'RETIRED',
                            children: retired
                        },
                            ...others]
                    };

                    allEmployeeMenus.push(menus);
                });
                let tempNav = navigation.items.slice(0);
                if (data.length > 0) {
                   let employee_checklist = {
                       title: true,
                       name: 'Employee Management',
                       wrapper: {
                           element: '',
                           attributes: {}
                       },
                       class: ''
                   };

                   tempNav.splice(tempNav.length - 1, 0, employee_checklist);
                };
                tempNav.splice(tempNav.length - 1, 0, ...allEmployeeMenus);
                this.setState({ menus: tempNav });
            })
            .catch(err => {
                console.log(err.response ? err.response : err)
            })
    }

    handleDeleteUser(id) {
        return () => this.props.dispatch(userActions.delete(id));
    }
    loading (){
         return  <div className="animated fadeIn pt-1 text-center">Loading...</div>
    }
    render() {
        if (this.props.location.pathname === "/manager/login") {
            return <LoginPage />
        } else {
            return (
                <div className="app">
                <NotificationContainer/>
                  <AppHeader fixed>
                    <Suspense  fallback={this.loading()}>
                      <DefaultHeader onLogout={e=>this.signOut(e)}/>
                    </Suspense>
                  </AppHeader>
                  <div className="app-body">
                    <AppSidebar fixed display="lg">
                      <AppSidebarHeader />
                      {/* <AppSidebarForm /> */}
                      <Suspense>
                      <AppSidebarNav navConfig={{ items: this.state.menus }} {...this.props} router={router}/>
                      </Suspense>
                      <AppSidebarFooter />
                      <AppSidebarMinimizer />
                    </AppSidebar>
                    <main className="main">
                      <AppBreadcrumb appRoutes={routes} router={router}/>
                      <Container fluid>
                        <Suspense fallback={this.loading()}>
                          <Switch>
                            {routes.map((route, idx) => {
                              return route.component ? (
                                <Route
                                  key={idx}
                                  path={route.path}
                                  exact={route.exact}
                                  name={route.name}
                                  render={props => (
                                    <route.component fetchMenus={()=> {this.fetchMenus()}} {...props } />
                                  )} />
                              ) : (null);
                            })}
                            <Redirect from="/" to="/manager/dashboard" />
                          </Switch>
                        </Suspense>
                      </Container>
                    </main>

                  </div>
                  <AppFooter>
                    <Suspense fallback={this.loading()}>
                      <DefaultFooter />
                    </Suspense>
                  </AppFooter>
                </div>
            );
        }
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedDashboard = connect(mapStateToProps)(Dashboard);
export { connectedDashboard as Dashboard };
