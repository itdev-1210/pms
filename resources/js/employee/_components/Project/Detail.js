import React from 'react'
import { Label } from 'reactstrap';
import { Link } from 'react-router-dom';
import { ProjectTask } from '.';
import Axios from "axios";

export class ProjectDetail extends React.Component {
    constructor(props) {
        super(props)

        this.moveFocusItem = this.moveFocusItem.bind(this)

        this.onDragStartProject = this.onDragStartProject.bind(this);
        this.onDragOverProject = this.onDragOverProject.bind(this);
        this.onDropProject = this.onDropProject.bind(this);

        this.onDragStartTask = this.onDragStartTask.bind(this);
        this.onDragOverTask = this.onDragOverTask.bind(this);
        this.onDropTask = this.onDropTask.bind(this);
    }

    componentDidMount() {
        if (this.props.selectedTask)
            this.moveFocusItem(this.props.selectedTask);
    }

    moveFocusItem(id) {
        if (this.refs[id]) {
            this.refs[id].scrollIntoView({
                behavior: 'smooth',
                block: 'start',
            });
        }
    }

    onDragStartProject(event, week_number, project_id) {
        event.dataTransfer.setData("isProject", true);
        event.dataTransfer.setData("week_number", week_number);
        event.dataTransfer.setData("project_id", project_id);
    }

    onDragOverProject(event) {
        event.preventDefault();
    }

    onDropProject(event, cat) {
        let isTask = event.dataTransfer.getData("isTask");
        let isProject = event.dataTransfer.getData("isProject");
        if (!isTask && isProject) {
            let week_number = event.dataTransfer.getData("week_number");
            if (this.props.week_number === parseInt(week_number)) {
                let project_id = parseInt(event.dataTransfer.getData("project_id"));
                let dropped_id = parseInt(event.currentTarget.getAttribute("project_id"));
                if (project_id === dropped_id)
                    return;

                let projects = this.props.projects.map(item => item.project_id);
                projects.splice(projects.indexOf(dropped_id), 0, projects.splice(projects.indexOf(project_id), 1)[0]);
                let post_data = {
                    ids: projects,
                    type: 0
                };

                Axios.post('/sort_task', post_data)
                    .then(response => {
                        this.props.reload();
                    })
            }
        }
    }

    onDragStartTask(event, task, project_id) {
        event.dataTransfer.setData("isTask", true);
        event.dataTransfer.setData("task", task);
        event.dataTransfer.setData("project_id", project_id);
    }

    onDragOverTask(event) {
        event.preventDefault();
    }

    onDropTask(event, cat) {
        let isTask = event.dataTransfer.getData("isTask");
        if (isTask) {
            let task = JSON.parse(event.dataTransfer.getData("task"));
            let project_id = event.dataTransfer.getData("project_id");
            if (this.props.week_number === task.week_number) {
                if (this.props.part.project_id === parseInt(project_id)) {
                    let task_ids = this.props.part.tasks.map(item => item.id);
                    let dropped_id = parseInt(event.currentTarget.getAttribute("task_id"));
                    if (task.id === dropped_id)
                        return;

                    task_ids.splice(task_ids.indexOf(dropped_id), 0, task_ids.splice(task_ids.indexOf(task.id), 1)[0]);
                    let post_data = {
                        ids: task_ids,
                        type: 1
                    };

                    Axios.post('/sort_task', post_data)
                        .then(response => {
                            this.props.reload();
                        })
                }
            } else {
                let post_data = {
                    week_number: this.props.week_number,
                    task_id: task.id
                };

                Axios.post('/move_task', post_data)
                    .then(response => {
                        this.props.reload();
                    })
            }
        }
    }

    onSort(flag, index) {
        let task_ids = this.props.part.tasks.map(item => item.id);
        if (flag === 'up')
            task_ids.splice(index - 1, 0, task_ids.splice(index, 1)[0]);
        else
            task_ids.splice(index + 1, 0, task_ids.splice(index, 1)[0]);

        let post_data = {
            ids: task_ids,
            type: 1
        };

        Axios.post('/sort_task', post_data)
            .then(response => {
                this.props.reload();
            })
    }

    onProjectSort(flag, index) {
        if (flag === 'up' && index === 0)
            return;

        if (flag === 'down' && (index === this.props.length - 1))
            return;

        let project_ids = this.props.projects.map(item => item.project_id);
        if (flag === 'up')
            project_ids.splice(index - 1, 0, project_ids.splice(index, 1)[0]);
        else
            project_ids.splice(index + 1, 0, project_ids.splice(index, 1)[0]);

        let post_data = {
            ids: project_ids,
            type: 0
        };

        Axios.post('/sort_task', post_data)
            .then(response => {
                this.props.reload();
            })
    }

    render() {
        const { index, length } = this.props;
        return (
            <div className="mt-4"
                 draggable
                 onDragStart = {(event) => this.onDragStartProject(event, this.props.week_number, this.props.part.project_id)}
                 onDragOver={(event)=>this.onDragOverProject(event)}
                 onDrop={(event)=>this.onDropProject(event, "Done")}
                 project_id={this.props.part.project_id}
            >
                <h4 className="font-weight-bold pl-4" id={"part_" + this.props.part.project_id}>
                    <Label className="mx-1" onClick={() => this.onProjectSort('up', index)}>
                        <i className={index === 0 ? 'fa fa-arrow-circle-up text-secondary' : 'fa fa-arrow-circle-up text-primary'} style={{fontSize: '17px'}}></i>
                    </Label>
                    <Label className="mx-1" onClick={() => this.onProjectSort('down', index)}>
                        <i className={index === length - 1 ? 'fa fa-arrow-circle-down text-secondary' : 'fa fa-arrow-circle-down text-primary'} style={{fontSize: '17px'}}></i>
                    </Label>
                    <Link className="text-dark" to={`/projects/view/${this.props.part.project_id}`}>
                        {this.props.part.project_name}
                    </Link>
                </h4>
                {this.props.part.tasks.map((task, index) => {
                    return <div ref={task.title} key={index}
                                draggable
                                onDragStart = {(event) => this.onDragStartTask(event, JSON.stringify(task), this.props.part.project_id)}
                                onDragOver={(event)=>this.onDragOverTask(event)}
                                onDrop={(event)=>this.onDropTask(event, "Done")}
                                task_id={task.id}
                                project_id={this.props.part.project_id}
                    >
                            <ProjectTask
                                index={index}
                                length={this.props.part.tasks.length}
                                task={task}
                                reload={this.props.reload}
                                updateFlag={this.props.updateFlag}
                                onSelectedTasks={(flag, task) => this.props.onSelectedTasks(flag, task)}
                                onSort={(flag, index) => this.onSort(flag, index)}
                            />
                        </div>
                })}
            </div>
        )
    }
}
