# project management system

## Requirements
------------------
*   PHP  >= 7.1.3
*   BCMath PHP Extension
*   Ctype PHP Extension
*   JSON PHP Extension
*   Mbstring PHP Extension
*   OpenSSL PHP Extension
*   PDO PHP Extension
*   Tokenizer PHP Extension
*   XML PHP Extension
*   MySql

## Install System
------------------




### Extract Source code or Clon from git
### Apache2 Document Root Configuration
* Please set up document root of your server until public folder.

### Env Configuration
* Change database name and database name, password on .env file
* DB_DATABASE=project_management_system // your database name
* DB_USERNAME=root // your user name
* DB_PASSWORD=password // your password

### Install Composer and Node Modules
* If you installed composer on your server you can run "composer install" command on root directory of source code.
* If you did not install, you need to install composer first.
* If you installed node on your server you can run "npm install" command on root directory of source code.
* If you did not install, you need to install node on your server first.

### Install database
* run "php artisan migrate --seed" command on your root folder via ssh access or putty.

