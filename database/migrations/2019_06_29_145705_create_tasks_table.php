<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('task_name', 255);
            $table->integer('parent')->default(0);
            $table->timestamps();
            $table->integer('check_list_id');
            $table->integer('project_id');
            $table->string('task_name_by_manager')->nullable();
            $table->integer('parent_by_manager')->nullable();
            $table->integer('sort_number')->nullable();
            $table->integer('sort_number_by_manager')->nullable();
            $table->integer('checklist_id_by_manager')->nullable();
            $table->enum('created_by', ['ADMIN', 'MANAGER'])->default('ADMIN');
            $table->integer('duplicated_from')->default(0);
            $table->boolean('is_outline')->default(false);
            $table->boolean('refresh')->default(false);
            $table->string('category');
            $table->boolean('is_change_category')->default(false);
            $table->boolean('need_relation')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
