import React from 'react'
import Axios from 'axios';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export default class Home extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            projects : []
        }
    }
    componentDidMount() {
        Axios.get('/ajax/getchecklist')
            .then(response => {
                console.log(response.data)
                this.setState({
                    projects : response.data
                })
            })
    }
    render(){
        return <div className='row'>


            {
                this.state.projects.map((project, index) =>{
                    return <div className="card border-success mb-3 mt-1 col-md-3"  key={index}>
                    <div className="card-header bg-transparent border-success">
                    <Link to={'/project/' + project.id}> <h1 >{project.project_name}</h1></Link>
                    </div>
                    <div className="card-body text-success">
                      {/* <h5 className="card-title">{project.project_name}</h5> */}
                      {/* <p className="card-text">Assigned Parts : {project.assigned_parts}</p> */}
                      {/* <p className="card-text">Assigned Checklists : {project.assigned_checklists}</p> */}
                      <p className="card-text">Assigned Tasks : {project.assigned_tasks}</p>
                    </div>
                    <div className="card-footer bg-transparent border-success text-right">Assigned by {project.manager_name}</div>
                  </div>
                })
            }
        </div>
    }
}