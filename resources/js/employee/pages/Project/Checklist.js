import React from 'react'
class Checklist extends React.Component {
    render() {
      const { provided, innerRef, children } = this.props;
      return (
        <div {...provided.droppableProps} ref={innerRef}>
          {children}
        </div>
      );
    }
  }

  export default Checklist