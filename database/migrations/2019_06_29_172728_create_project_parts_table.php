<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_parts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('part_name');
            $table->integer('project_id');
            $table->timestamps();
            $table->integer('sort_number')->default(0);
            $table->string('part_name_by_manager')->nullable();
            $table->integer('sort_number_by_manager')->nullable();
            $table->enum('created_by', ['ADMIN', 'MANAGER'])->default('ADMIN');
            $table->integer('duplicated_from')->default(0);
            $table->boolean('is_outline')->default(false);
            $table->boolean('refresh')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_parts');
    }
}
