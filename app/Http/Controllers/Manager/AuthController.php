<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class AuthController extends Controller
{
    /**
     * Show the application Login Page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('manager.index');
    }

    public function login(Request $request){

        $this->validate($request, [
            'username'   => 'required',
            'password' => 'required|min:3'
        ]);
        Auth::shouldUse('manager');

        if (Auth::attempt(['name' => $request->username, 'password' => $request->password], 1)) {
            return array(
                'status' => 'success',
                'user_info' => Auth::user()
            );
        }
        return array('status' => 'error');
    }

    public function logout(Request $request) {
        Auth::shouldUse('manager');
        Auth::logout();
    }
}
