<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Storage;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Show the application Login Page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function login(Request $request){

        $this->validate($request, [
            'email'   => 'required',
            'password' => 'required|min:3'
        ]);
        // Auth::shouldUse('employee');?
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], 1)) {
            return array(
                'status' => 'success',
                'user_info' => Auth::user()
            );
        }
        return array('status' => 'error');
    }

    public function logout(Request $request) {
        Auth::logout();
    }
}
