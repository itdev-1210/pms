<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlarmNotification extends Model
{
    public function user_info(){
        return $this->hasOne('App\Models\Employee','id','employee_id');
    }
}
