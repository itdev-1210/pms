import React, {  } from 'react'
import { connect } from 'react-redux';
import {
    Button,
    Card,
    CardBody,
    CardHeader,
    Col,
    FormGroup,
    Label,
    Row,
    Modal, ModalBody, ModalFooter, ModalHeader,
} from 'reactstrap';
import Axios from 'axios';
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'
import 'react-simple-tree-menu/dist/main.css';
import ReactPDF from '@react-pdf/renderer';
import OrgChart from 'react-orgchart';
import 'react-orgchart/index.css';
import ElementPan  from "react-element-pan";
import { Dropdown } from 'semantic-ui-react'

class ProjectAssign extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            is_draging: false,
            is_loading: true,
            project_detail: {},
            parts: [],
            orgJson: [],
            removed_checklists : [],
            removed_tasks : [],
            isOpenAddModal : false,
            selectable_employees : [],
            selected_members : []
        }
        this.genderatePdf = this.genderatePdf.bind(this)
        this.handleMenuClick = this.handleMenuClick.bind(this)
        this.handleAddMembers = this.handleAddMembers.bind(this)
        this.handleOpenEmployeeChooseModal = this.handleOpenEmployeeChooseModal.bind(this)
        this.handleSelectMembers = this.handleSelectMembers.bind(this)

    }

    componentDidMount() {
        Axios.get('/manager/ajax/project/get_assignment_details/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    project_detail: response.data.project_detail,
                    orgJson: response.data.orgJson,
                    is_loading : false
                })
            })
    }
    genderatePdf(){
        ReactPDF.render(<CardBody>
            {/*  */}

          <h2 className="font-weight-bold text-center text-capitalize">{this.state.project_detail.project_name}</h2>
          <hr/>
          <Row>


          {
              this.state.parts.map((part, partIndex) =>{
                  return <ProjectDetail key={partIndex} index={partIndex} part = {part}/>
              })
          }
          </Row>
          </CardBody>   , `${__dirname}/${this.state.project_detail.project_name}.pdf`);

    }
    handleMenuClick(Item){
    }
    handleMouseWhileOnOrgChart(e){
        e.preventDefault();
    }
    handleOpenEmployeeChooseModal(node){

        this.setState({
            is_loading : true,
            active_node_type : node.node_type,
            active_node_id : node.node_id,
        }, ()=>{
            let post_data = {
                node_type : node.node_type,
                node_id : node.node_id,
                project_id : this.props.match.params.id,
            }
            Axios.post('/manager/ajax/getAssignableEmployees/', post_data)
            .then(response =>{
                this.setState({
                    selectable_employees : response.data,
                    is_loading : false,
                    isOpenAddModal : true
                })
            })
        })
    }
    handleAddMembers(){
        let post_data = {
            project_id : this.props.match.params.id,
            node_type : this.state.active_node_type,
            node_id : this.state.active_node_id,
            members : this.state.selected_members
        }
        this.setState({
            is_loading : true
        }, ()=>{
            Axios.post('/manager/ajax/assignmembers', post_data)
            .then(response =>{
                this.setState({
                    project_detail: response.data.project_detail,
                    orgJson: response.data.orgJson,
                    is_loading : false,
                    selectable_employees : [],
                    active_node_id : null,
                    active_node_type : null,
                    isOpenAddModal : false
                })
            })
        })
    }
    handleSelectMembers(e, data){
        this.setState({
            selected_members : data.value
        })
    }
    render() {
        const initechOrg = {
            label: this.state.project_detail.project_name,
            node_id: this.state.project_detail.id,
            node_type : 'project',
            children: [...this.state.orgJson]
          }
          const MyNodeComponent = ({node}) =>{
          return (
              <div className="border m-auto" >
              <h6 className="card-header">{node.label ? node.label : node.task_name}</h6>
              <div className="card-body">
                {node.node_type == 'checklist' && <p className="card-text">{node.description}</p>}
                <div>
                    {node.assigned_employees && node.assigned_employees.length != 0 && node.assigned_employees.map((assign) =>{
                        return <img src={assign.employee_details.profile_image_src} className="rounded-circle ml-1" alt={assign.employee_details.full_name} style={{width : '50px'}} />
                        // return <Image style={{maxWidth : '50px'}} src={assign.employee_details.profile_image_src} avatar key={index}></Image>
                    })}
                </div>
                {node.node_type != 'project' && <Button className="btn-pill" color="primary" onClick={()=>this.handleOpenEmployeeChooseModal(node)}>Add</Button>}
              </div>
            </div>
              );
        };
        const employeeOptions = this.state.selectable_employees.map((employee) =>{
            return  {
                key: employee.id,
                text: employee.full_name,
                value: employee.id,
                image: { avatar: true, src: employee.profile_image_src },
              }
        })
        return <div className="animated fadeIn">


             <LoadingOverlay
                        active={this.state.is_loading}
                        spinner={<BounceLoader />}
                    >
                </LoadingOverlay>

                <Modal isOpen={this.state.isOpenAddModal} toggle={()=>this.setState({isOpenAddModal : false})} >
                  <ModalHeader toggle={()=>this.setState({isOpenAddModal : false})}>Choose Employees to add</ModalHeader>
                  <ModalBody>
                    <FormGroup>
                        <Label>Employee</Label>
                        <Dropdown placeholder='Employees' search fluid multiple selection options={employeeOptions}  onChange={this.handleSelectMembers}/>
                    </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="primary" onClick={this.handleAddMembers} >Add Members</Button>{' '}
                    <Button color="secondary" onClick={()=>this.setState({isOpenAddModal : false})}>Cancel</Button>
                  </ModalFooter>
                </Modal>


            <Row>

                <Col   md="12">
                <Card>
                <CardHeader>
                            <i className="icon-user-follow"></i><strong> Project Assignment</strong>
                            <div className="card-header-actions">
                                {/* <Button color="primary" className="btn-pill mr-1" onClick={this.genderatePdf} >
                                <i className="fa fa-file-pdf-o"></i>&nbsp;PDF Generate
                                </Button> */}
                                <Button color="success" className="btn-pill mr-1" onClick={()=>this.props.history.push("/manager/project/edit/" + this.props.match.params.id)} >
                                <i className="fa fa-pencil"></i>&nbsp;Edit
                                </Button>
                                <a color="danger" className="btn-pill btn btn-danger text-white" onClick={() => this.props.history.goBack()}>
                                        <i className="icon-arrow-left"></i>&nbsp;Back
                                </a>
                            </div>
                        </CardHeader>
                    <CardBody>
                        {/*  */}

                      <h2 className="font-weight-bold text-center text-capitalize">{this.state.project_detail.project_name}</h2>
                      <hr/>
                      <Row>
                      <Col xs={12} >
                           <ElementPan
                            height = {window.innerHeight - 350}
                           >
                           {this.state.orgJson.length && <OrgChart tree={initechOrg} NodeComponent={MyNodeComponent} />}
                           </ElementPan>

                      </Col>
                      {/* {
                          this.state.parts.map((part, partIndex) =>{
                              return <AssignProject key={partIndex} index={partIndex} part = {part}/>
                          })
                      } */}
                      </Row>
                      </CardBody>
                    </Card>
                </Col>
            </Row>
        </div>
    }
}




function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedProjectAssign = connect(mapStateToProps)(ProjectAssign);
export default connectedProjectAssign;
