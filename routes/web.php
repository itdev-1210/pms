<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');

Route::get('/admin', 'Admin\IndexController@index');
Route::get('/admin/login', 'Admin\AuthController@index')->name('admin.login');
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function () {
    Route::post('/login', 'AuthController@login');
    Route::post('/logout', 'AuthController@logout');

    Route::get('/ajax/masters', 'MasterController@load');
    Route::get('/ajax/master/get/{id}', 'MasterController@get');
    Route::post('/ajax/master/register', 'MasterController@register');
    Route::post('/ajax/master/update', 'MasterController@update');
    Route::post('/ajax/master/remove/{id}', 'MasterController@remove');


    /* Project Managers Management Routes */
    Route::get("/ajax/managers", "ManagerController@managers");
    Route::post('/manager/register','ManagerController@register');
    Route::post('/manager/update','ManagerController@update');
    Route::post('/ajax/manager/remove/{id}','ManagerController@remove');
    Route::get("/ajax/managers/get/{id}", "ManagerController@get");

    /* Routes for project management */
    Route::post("/ajax/project/save", "ProjectController@save");
    Route::post("/ajax/project/remove/{id}", "ProjectController@remove");
    Route::post("/ajax/project/update/{id}", "ProjectController@update");
    Route::get("/ajax/projects", "ProjectController@loadprojects");
    Route::get("/ajax/project/get/{id}", "ProjectController@get_project_details");
    Route::get("/ajax/project/preview/{id}", "ProjectController@get_preview_project_details");
    Route::get("/ajax/project/get_assignment_details/{id}", "ProjectController@get_assignment_details");
    Route::post("/ajax/project/assign", "ProjectController@assign");
    Route::post("/ajax/project/assignmember", "ProjectController@assignmember");
    Route::post("/ajax/project/removeassign/{id}", "ProjectController@removeassign");
    Route::post("/ajax/getAssignableEmployees", "ProjectController@getAssignableEmployees");
    Route::post("/ajax/assignmembers", "ProjectController@assignmembers");
    Route::post("/ajax/remove_assignmembers", "ProjectController@removeAssignMember");
    Route::post('/ajax/newnote', 'ProjectController@newnote');
    Route::post('/ajax/editnote', 'ProjectController@editnote');
    Route::get('/ajax/getnotifications', 'ProjectController@getNotifications');
    Route::get('/ajax/getreminders', 'ProjectController@getReminders');
    /* Master Checklist Routes */
    Route::get('/ajax/getmasterchecklist', "ProjectController@getmasterchecklist");
    /* Route for Employee Management */
    Route::get('/ajax/employees', 'EmployeeController@loademployees');
    Route::get('/ajax/employee/get/{id}', 'EmployeeController@get');
    Route::post('/ajax/employee/register', 'EmployeeController@register');
    Route::post('/ajax/employee/update', 'EmployeeController@update');
    Route::post('/ajax/employee/remove/{id}', 'EmployeeController@remove');
    Route::post('/save-subscription', 'PushController@store');
    Route::post('/ajax/newAssign', 'ProjectController@newAssignMembers');

    /* Remain routes for admin panel! */
    Route::get('{all}', 'IndexController@index');
    Route::get('/{all}/{function}', 'IndexController@index');
    Route::get('/{all}/{function}/{param}', 'IndexController@index');

});

Route::get('/manager', 'Manager\IndexController@index');
Route::get('/manager/login', 'Manager\AuthController@index')->name('manager.login');
Route::group(['prefix' => 'manager', 'as' => 'manager.', 'namespace' => 'Manager'], function () {
    Route::post('/login', 'AuthController@login');
    Route::post('/logout', 'AuthController@logout');
    Route::get("/ajax/managers", "ProjectController@managers");
    /* Route for Employee Management */
    Route::get('/ajax/employees', 'EmployeeController@loademployees');
    Route::get('/ajax/employee/get/{id}', 'EmployeeController@get');
    Route::post('/ajax/employee/register', 'EmployeeController@register');
    Route::post('/ajax/employee/update', 'EmployeeController@update');
    Route::post('/ajax/employee/remove/{id}', 'EmployeeController@remove');
    /* Routes for project management */
    // Route::post("/ajax/project/update/{id}", "Proj/ajax/editnoteectController@update");
    Route::post("/ajax/project/save", "ProjectController@save");
    Route::post("/ajax/project/remove/{id}", "ProjectController@remove");
    Route::post("/ajax/project/update/{id}", "ProjectController@update");
    Route::get("/ajax/projects", "ProjectController@loadprojects");
    Route::get("/ajax/project/get/{id}", "ProjectController@get_project_details");
    Route::get("/ajax/project/preview/{id}", "ProjectController@get_preview_project_details");
    Route::get("/ajax/project/get_assignments/{id}", "ProjectController@get_assignments");
    Route::get("/ajax/project/get_assignment_details/{id}", "ProjectController@get_assignment_details");
    Route::post("/ajax/getAssignableEmployees", "ProjectController@getAssignableEmployees");
    Route::post("/ajax/assignmembers", "ProjectController@assignmembers");
    Route::post("/ajax/remove_assignmembers", "ProjectController@removeAssignMember");
    Route::post("/ajax/setOutline", "ProjectController@setOutline");
    Route::post("/ajax/setRefresh", "ProjectController@setRefresh");
    Route::post('/ajax/newnote', 'ProjectController@newnote');
    Route::post('/ajax/editnote', 'ProjectController@editnote');
    Route::get('/ajax/getnotifications', 'ProjectController@getNotifications');
    Route::get('/ajax/getreminders', 'ProjectController@getReminders');
    /* Master Checklist Routes */
    Route::get('/ajax/getmasterchecklist', "ProjectController@getmasterchecklist");

    Route::post("/ajax/project/assign", "ProjectController@assign");
    Route::post("/ajax/project/assignmember", "ProjectController@assignmember");
    Route::post("/ajax/project/removeassign/{id}", "ProjectController@removeassign");

    Route::post('/ajax/getchecklist', 'ProjectController@getEmployeeChecklist');
    Route::post('/ajax/getprojecechecklist', 'ProjectController@getEmployeeProjecetChecklist');
    Route::post('/ajax/assigneddates', 'ProjectController@employeeAssignedSearchdates');
    Route::get('/ajax/getmenus', 'ProjectController@getSideMenus');

    Route::post('/ajax/startTask', 'ProjectController@startTask');
    Route::post('/ajax/completeTask', 'ProjectController@completeTask');
    Route::post('/ajax/setreminder', 'ProjectController@setTrigger');
    Route::post('/save-subscription', 'PushController@store');
    Route::post('/ajax/newAssign', 'ProjectController@newAssignMembers');

    Route::post('profile', 'UserController@update_avatar');

    /* Remain routes for manager panel! */
    Route::get('{all}', 'IndexController@index');
    Route::get('/{all}/{function}', 'IndexController@index');
    Route::get('/{all}/{function}/{param}', 'IndexController@index');
});

Route::post('/ajax/getAvailableRealtionTasks', 'ProjectController@getAvailableRealtionTasks');
Route::post('/ajax/setRelationTasks', 'ProjectController@setRelationTasks');
Route::get('/ajax/getchecklist', 'ProjectController@getchecklist')->name('getchecklist');
Route::get('/ajax/getprojecechecklist/{id}', 'ProjectController@getprojecechecklist')->name('getprojecechecklist');
Route::get('/ajax/assigneddates', 'ProjectController@assigneddates')->name('assigneddates');
Route::post('/ajax/assigneddates', 'ProjectController@assignedSearchdates')->name('assignedsearchdates');
Route::post('/ajax/startTask', 'ProjectController@startTask')->name('startTask');
Route::post('/ajax/completeTask', 'ProjectController@completeTask')->name('completeTask');
Route::post('/ajax/newnote', 'ProjectController@newnote')->name('newnote');
Route::post('/ajax/editnote', 'ProjectController@editnote');
Route::post('/ajax/removenote', 'ProjectController@removenote');
Route::post('/ajax/removenotification', 'ProjectController@removeNotification');
Route::get('/ajax/getnotifications', 'ProjectController@getNotifications');
Route::get('/ajax/getmenus', 'ProjectController@getSideMenus');
Route::post('/ajax/setreminder', 'ProjectController@setTrigger');
Route::post('/ajax/finishreminder', 'ProjectController@finishTrigger');
Route::get('/ajax/getreminders', 'ProjectController@getReminders');
Route::get('/ajax/getLowerProjects', 'ProjectController@getLowerProject');
Route::get('/ajax/getOrderMoreProject', 'ProjectController@getOrderMoreProject');
Route::get('/login', 'Auth\LoginController@index');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');
Route::post('/save-subscription', 'PushController@store');
Route::get("/ajax/projects", "ProjectController@loadprojects");
Route::get("/ajax/project/preview/{id}", "ProjectController@get_preview_project_details");
Route::post("/ajax/getAssignableEmployees", "ProjectController@getAssignableEmployees");
Route::post("/ajax/assignmembers", "ProjectController@assignmembers");
Route::post("/ajax/setOutline", "ProjectController@setOutline");
Route::post("/ajax/setRefresh", "ProjectController@setRefresh");
Route::post('profile', 'UserController@update_avatar');
Route::post('move_task', 'ProjectController@moveTask');
Route::post('multi_move_task', 'ProjectController@multiMoveTask');
Route::post('sort_task', 'ProjectController@sortTask');
Route::post('/ajax/getAjaxCategoryTasks', 'ProjectController@getCategoryTasks');
Route::get('/ajax/getRefreshTasks', 'ProjectController@getRefreshTasks');

Route::get('{all}', 'HomeController@index');
Route::get('/{all}/{function}', 'HomeController@index');
Route::get('/{all}/{function}/{param}', 'HomeController@index');
