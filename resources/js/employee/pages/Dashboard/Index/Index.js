import React from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Col, Row } from 'reactstrap';
import './styles.scss'
import Axios from 'axios';
import Moment from 'react-moment';
import moment from 'moment'

class Index extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            datas: [],
            reminders: [],
            lowerProjects: [],
            orderMoreProjects: [],
        }

        this.onReminder = this.onReminder.bind(this);
    }

    componentWillMount() {
        this.getDatas();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.user !== nextProps.user) {
            this.getDatas();
        }
    }

    getDatas() {
        Axios.get('/ajax/getnotifications')
            .then(({data}) => {
                this.setState({datas: data})
            })
            .catch(err => {
                console.log('Get Notifications:', err)
            })

        Axios.get('/ajax/getreminders')
            .then(({data}) => {
                this.setState({reminders: data})
            })
            .catch(err => {
                console.log('Get Reminders', err)
            })

        Axios.get('/ajax/getLowerProjects')
            .then(({data}) => {
                this.setState({lowerProjects: data})
            })
            .catch(err => {
                console.log('Get LowerProjects', err)
            })
        Axios.get('/ajax/getOrderMoreProject')
            .then(({data}) => {
                this.setState({orderMoreProjects: data})
            })
            .catch(err => {
                console.log('Get OrderMoreProject', err)
            })
    }

    onReminder(data) {
        this.props.history.push({
            pathname: `/project/${data.project_id}`,
            state: {
                title: data.title
            }
        })
    }

    render(){
        return (
            <Row>
                <Col sm="12" md="6">
                    <div className="dashboard-box" style={{height: '400px', overflowY: 'auto'}}>
                      <div className="box-title">Notifications</div>
                      <div className="box-line"></div>
                      <div className="box-content">
                          {this.state.datas.map((data, index) => <div className="box-one-content avatar-content" key={index}>
                            <img src={data.user_info.profile_image_src} className="reminder-avatar" />
                            <div className="content-title">{data.project + '_' + data.title}</div>
                            <div className="content-time">
                                <Moment fromNow>{moment.utc(data.created_at).local()}</Moment>
                            </div>
                          </div>)}
                      </div>
                    </div>
                </Col>

                <Col sm="12" md="6">
                    <div className="dashboard-box" style={{height: '400px', overflowY: 'auto'}}>
                      <div className="box-title">Reminders</div>
                      <div className="box-line"></div>
                      <div className="box-content">
                          {this.state.reminders.map((data, index) => <div className="box-one-content avatar-content" key={index}>
                            <img src={data.user_info.profile_image_src} className="reminder-avatar" />
                            <div className="content-title">
                                <div onClick={() => this.onReminder(data)} className="reminder-title">
                                  {data.project + '_' + data.title}
                                </div>
                            </div>
                            <div className="content-time">
                                <Moment fromNow>{moment.utc(data.created_at).local()}</Moment>
                            </div>
                          </div>)}
                      </div>
                    </div>
                </Col>

                <Col sm="12" md="6" className="mt-lg-3 mb-lg-3">
                    <div className="dashboard-box" style={{height: '400px', overflowY: 'auto'}}>
                        <div className="box-title">Low Inventory</div>
                        <div className="box-line"></div>
                        <div className="box-content">
                            <div className="box-one-content lower-projects">
                                <div className="content-title header">Project Name</div>
                                <div className="content-category header">Project Category</div>
                                <div className="content-inventory header">Inventory</div>
                            </div>
                            {this.state.lowerProjects.map((data, index) => <div className="box-one-content lower-projects" key={index}>
                                <div className="content-title">
                                    {data.project_name}
                                </div>
                                <div className="content-category">
                                    {data.category}
                                </div>
                                <div className="content-inventory">
                                    {data.inventory}
                                </div>
                            </div>)}
                        </div>
                    </div>
                </Col>

                <Col sm="12" md="6" className="mt-lg-3 mb-lg-3">
                    <div className="dashboard-box" style={{height: '400px', overflowY: 'auto'}}>
                        <div className="box-title">Order More Inventory</div>
                        <div className="box-line"></div>
                        <div className="box-content">
                            <div className="box-one-content lower-projects">
                                <div className="content-title header">Project Name</div>
                                <div className="content-category header">Order Date</div>
                                <div className="content-inventory header">Order Amount</div>
                            </div>
                            {this.state.orderMoreProjects.map((data, index) => <div className="box-one-content lower-projects" key={index}>
                                <div className="content-title">
                                    {data.project_name}
                                </div>
                                <div className="content-category">
                                    {data.order_date}
                                </div>
                                <div className="content-inventory">
                                    {data.order_amount}
                                </div>
                            </div>)}
                        </div>
                    </div>
                </Col>
            </Row>
        )
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    const { users, authentication } = state;
    const { user } = authentication;

    return {
        users,
        user,
        loggingIn
    };
}

const connectedIndex = connect(mapStateToProps)(Index);
export default connectedIndex;
