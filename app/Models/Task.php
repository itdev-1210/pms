<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    // protected $appends = ['assigned_employees'];


    // public function getAssignedEmployeesAttribute(){
    //     $result = array();
    //     $assigns = ProjectEmployeeAssignment::with('employee_details')->where('node_type', 'task')->where('node_id', $this->attributes['id'])->get();
    //     foreach($assigns as $assign){
    //         $result[] = $assign->employee_details;
    //     }
    //     return $result;
    // }

    public static function getAssignedIds($projectid, $checklistid, $employeeid, $date = null, $parent = 0){
        $tasks = self::where('check_list_id', $checklistid)->where('parent', $parent)->get();
        $assigned = [];
        foreach($tasks as $task){
            $task_assigned_query = ProjectEmployeeAssignment::where('node_type', 'task')->where('employee_id', $employeeid)->where('node_id', $task->id);
            if($date){
                $task_assigned_query->whereRaw("Date(`created_at`) = '$date'");
            }
            if(($task_assigned_query->count())){
                $assigned[] = $task->id;
            }
            else{
                if(count(self::getAssignedIds($projectid, $checklistid, $employeeid,$date, $task->id))){
                    $assigned[] = $task->id;
                }
            }
        }
        return $assigned;
    }

    /**
     * Get the check list instance of this task instance
     */
    public function checkList()
    {
        return $this->belongsTo(CheckList::class);
    }

    /**
     * Get all the employee check list note of this task
     */
    public function employeeCheckListNote() {
        return $this->hasMany(EmployeeChecklistNote::class,'node_id','id');
    }

    /**
     * Get all the project employee assignment of this task
     */
    public function projectEmployeeAssignment() {
        return $this->hasMany(ProjectEmployeeAssignment::class,'node_id','id');
    }

    public function relationTasks() {
        return $this->hasOne('App\RelationTask', 'task_id', 'id');
    }
}
