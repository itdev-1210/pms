import React from 'react';
// const DashboardIndex = React.lazy(() => import('./pages/Dashboard/Index/Index'));
import  DashboardIndex from './pages/Dashboard/Index/Index'
import { Logout } from './pages/Logout/Logout';
import connectedCreateProject from './pages/Project/CreateProject';
import connectedEditProject from './pages/Project/EditProject';
import connectedProjectList from './pages/Project/ProjectList';
import connectedProjectPreview from './pages/Project/ProjectPreview';
import connectedAssignmentProject from './pages/Project/ProjectAssignment';
import connectedNewEmployee from './pages/Employees/NewEmployee';
import connectedEditEmployee from './pages/Employees/EditEmployee';
import connectedEmployees from './pages/Employees/Employees';
import AllProjects from './pages/Project/AllProjects';
import ProjectChecklist from "./pages/Project/ProjectChecklist";
import connectedSetup from "./pages/Task/Setup";
import connectedLaunch from "./pages/Task/Launch";
import connectedRefresh from "./pages/Task/Refresh";
import connectedOptimize from "./pages/Task/Optimize";

const routes = [
  { path: '/manager', exact: true, name: 'Manager' },
  { path: '/manager/dashboard', name: 'Dashboard', component: DashboardIndex },
  { path: '/manager/users', name: 'Dashboard', component: DashboardIndex },
  { path: '/manager/employee', name: 'Employees',exact: true, component: connectedEmployees },
  { path: '/manager/employee/add', name: 'New Employee',exact: true, component: connectedNewEmployee },
  { path: '/manager/employee/edit/:id', name: 'Edit Employee',exact: true, component: connectedEditEmployee },
  { path: '/manager/project', name: 'All Projects',exact: true, component: connectedProjectList },
  { path: '/manager/project/create', name: 'Create New Project',exact: true, component: connectedCreateProject },
  { path: '/manager/project/edit/:id', name: 'Edit Project',exact: true, component: connectedEditProject },
  { path: '/manager/project/view/:id', name: 'Edit Project',exact: true, component: connectedProjectPreview },
  { path: '/manager/project/assign/:id', name: 'Assign Project',exact: true, component: connectedAssignmentProject },
  { path: '/manager/projects/:userId', name: 'All Projects', exact: true, component: AllProjects },
  { path: '/manager/project/:userId/:id', name: 'Project', component: ProjectChecklist },
  { path: '/manager/task/setup', name: 'Setup', component: connectedSetup },
  { path: '/manager/task/launch', name: 'Launch', component: connectedLaunch },
  { path: '/manager/task/refresh', name: 'Refresh', component: connectedRefresh },
  { path: '/manager/task/optimize', name: 'Optimize', component: connectedOptimize },
  { path: '/manager/logout', name: 'Logout', component: Logout },
];
export default routes;
