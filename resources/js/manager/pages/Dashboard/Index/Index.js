import React from 'react'
import { connect } from 'react-redux';
import { Col, Row } from 'reactstrap';
import './styles.scss'
import Axios from 'axios';
import Moment from 'react-moment';
import moment from 'moment'
import { Checkbox } from 'semantic-ui-react'

class Index extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            datas: [],
            reminders: [],
            lowerProjects: [],
            orderMoreProjects: [],
            allCheck: false,
            isChecked: false,
            checkValue: []
        }
        this.removeNotification = this.removeNotification.bind(this)
        this.reload = this.reload.bind(this)
        this.onReminder = this.onReminder.bind(this)
        this.allCheck = this.allCheck.bind(this)
        this.checkedValue = this.checkedValue.bind(this)
        this.beforeRemove = this.beforeRemove.bind(this)
    }

    componentWillMount() {
        this.reload()
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.user !== nextProps.user) {
            this.reload();
        }
    }

    reload() {
        Axios.get('/manager/ajax/getnotifications')
        .then(({data}) => {
            let checkValue = []
            data.map((notify, index) => {
                checkValue[index] = false
            })
            this.setState({datas: data, checkValue: checkValue, allCheck: false, isChecked: false})
        })
        .catch(err => {
            console.log('Get Notifications:', err)
        })

        Axios.get('/manager/ajax/getreminders')
        .then(({data}) => {
            this.setState({reminders: data})
        })
        .catch(err => {
            console.log('Get Reminders:', err)
        })

        Axios.get('/ajax/getLowerProjects')
            .then(({data}) => {
                this.setState({lowerProjects: data})
            })
            .catch(err => {
                console.log('Get LowerProjects', err)
            })

        Axios.get('/ajax/getOrderMoreProject')
            .then(({data}) => {
                this.setState({orderMoreProjects: data})
            })
            .catch(err => {
                console.log('Get OrderMoreProject', err)
            })
    }

    beforeRemove() {
        if (!this.state.isChecked) return;
        const {datas, checkValue} = this.state
        let ids = []
        for(let i = 0; i < datas.length; i++) {
            if (checkValue[i]) ids.push(datas[i].id)
        }
        this.removeNotification(ids)
    }

    removeNotification(ids) {
        Axios.post('/ajax/removenotification', {ids: ids})
        .then(response => {
            this.reload()
        })
        .catch(err => {
            console.log('Remove Notification:', err)
        })
    }

    onReminder(data) {
        this.props.history.push({
            pathname: `/manager/project/${data.employee_id}/${data.project_id}`,
            state: {
                title: data.title
            }
        })
    }

    allCheck() {
        let checkValue = this.state.checkValue.map(value=>this.state.allCheck ? false : true)
        this.setState({
            allCheck: !this.state.allCheck,
            checkValue: checkValue,
            isChecked: this.state.datas.length ? !this.state.allCheck : false
        })
    }

    checkedValue(index) {
        let checkValue = Object.assign([], this.state.checkValue)
        checkValue[index] = !checkValue[index]

        let countTrue = checkValue.filter((obj) => obj === true).length;
        this.setState({
            checkValue: checkValue,
            isChecked: countTrue > 0 ? true : false,
            allCheck: countTrue === this.state.datas.length ? true : false
        })
    }

    render(){
        const disableStyle = {
            backgroundColor: '#9a9a9a'
        }
        const {allCheck, isChecked, checkValue} = this.state
        return (
            <Row>
                <Col sm="12" md="6">
                    <div className="dashboard-box" style={{height: '400px', overflowY: 'auto'}}>
                        <div className="box-header">
                            <div className="box-check">
                                <Checkbox className="mx-2" checked={allCheck} onChange={this.allCheck}/>
                                <div className="box-title">Notifications</div>
                            </div>
                            <div className="remove-notify" style={!isChecked ? disableStyle : {}} onClick={this.beforeRemove}>Remove</div>
                        </div>
                        <div className="box-line"></div>
                        <div className="box-content">
                            {this.state.datas.map((data, index) => <div className="box-one-content avatar-content" key={index}>
                                <Checkbox className="mx-2" checked={checkValue[index]} onChange={()=>(this.checkedValue(index))}/>
                                <img src={data.user_info ? data.user_info.profile_image_src : "https://ui-avatars.com/api/?name=" + data.name} className="reminder-avatar" />
                                <div className="content-title">{data.name + (data.is_completed ? ' completed ' : ' started ') + '"' + data.project + '_' + data.title + '"'}</div>
                                <div className="content-time"><Moment fromNow>{moment.utc(data.created_at).local()}</Moment></div>
                                <div className="content-btn" onClick={()=>this.removeNotification([data.id])}><span>&#215;</span></div>
                            </div>)}
                        </div>
                    </div>
                </Col>

                <Col sm="12" md="6">
                    <div className="dashboard-box" style={{height: '400px', overflowY: 'auto'}}>
                      <div className="box-title">Reminders</div>
                      <div className="box-line"></div>
                      <div className="box-content">
                          {this.state.reminders.map((data, index) => <div className="box-one-content avatar-content" key={index}>
                            <img src={data.user_info.profile_image_src} className="reminder-avatar" />
                            <div className="content-title">
                                <div onClick={() => this.onReminder(data)} className="reminder-title">
                                    {data.project + '_' + data.title}
                                </div>
                            </div>
                            <div className="content-time"><Moment fromNow>{moment.utc(data.created_at).local()}</Moment></div>
                          </div>)}
                      </div>
                    </div>
                </Col>

                <Col sm="12" md="6" className="mt-lg-3 mb-lg-3">
                    <div className="dashboard-box" style={{height: '400px', overflowY: 'auto'}}>
                        <div className="box-title">Low Inventory</div>
                        <div className="box-line"></div>
                        <div className="box-content">
                            <div className="box-one-content lower-projects">
                                <div className="content-title header">Project Name</div>
                                <div className="content-category header">Project Category</div>
                                <div className="content-inventory header">Inventory</div>
                            </div>
                            {this.state.lowerProjects.map((data, index) => <div className="box-one-content lower-projects" key={index}>
                                <div className="content-title">
                                    {data.project_name}
                                </div>
                                <div className="content-category">
                                    {data.category}
                                </div>
                                <div className="content-inventory">
                                    {data.inventory}
                                </div>
                            </div>)}
                        </div>
                    </div>
                </Col>

                <Col sm="12" md="6" className="mt-lg-3 mb-lg-3">
                    <div className="dashboard-box" style={{height: '400px', overflowY: 'auto'}}>
                        <div className="box-title">Order More Inventory</div>
                        <div className="box-line"></div>
                        <div className="box-content">
                            <div className="box-one-content lower-projects">
                                <div className="content-title header">Project Name</div>
                                <div className="content-category header">Order Date</div>
                                <div className="content-inventory header">Order Amount</div>
                            </div>
                            {this.state.orderMoreProjects.map((data, index) => <div className="box-one-content lower-projects" key={index}>
                                <div className="content-title">
                                    {data.project_name}
                                </div>
                                <div className="content-category">
                                    {data.order_date}
                                </div>
                                <div className="content-inventory">
                                    {data.order_amount}
                                </div>
                            </div>)}
                        </div>
                    </div>
                </Col>
            </Row>
        )
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    const { users, authentication } = state;
    const { user } = authentication;

    return {
        users,
        user,
        loggingIn
    };
}

const connectedIndex = connect(mapStateToProps)(Index);
export default connectedIndex;
