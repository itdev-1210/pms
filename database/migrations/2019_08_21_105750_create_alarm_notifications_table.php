<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlarmNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alarm_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('assign_id');
            $table->enum('assigned_by', ['ADMIN', 'MANAGER'])->default('MANAGER');
            $table->string('title');
            $table->string('project');
            $table->string('name');
            $table->integer('employee_id');
            $table->integer('is_completed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alarm_notifications');
    }
}
