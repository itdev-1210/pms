import React, {Suspense} from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import * as router from 'react-router-dom';

import { userActions } from '../../_actions';
import navigation from '../../Layouts/Navigation'
const DefaultAside = React.lazy(() => import('../../Layouts/DefaultAside'));
const DefaultFooter = React.lazy(() => import('../../Layouts/DefaultFooter'));
const DefaultHeader = React.lazy(() => import('../../Layouts/DefaultHeader'));
import { Container } from 'reactstrap';
import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import {
    AppAside,
    AppFooter,
    AppHeader,
    AppSidebar,
    AppSidebarFooter,
    AppSidebarForm,
    AppSidebarHeader,
    AppSidebarMinimizer,
    AppBreadcrumb2 as AppBreadcrumb,
    AppSidebarNav2 as AppSidebarNav,
  } from '@coreui/react';
import routes from '../../routes';
import { LoginPage } from '../../LoginPage';

class Dashboard extends React.Component {
    componentDidMount() {
    }

    handleDeleteUser(id) {
        return () => this.props.dispatch(userActions.delete(id));
    }
    loading (){
         return  <div className="animated fadeIn pt-1 text-center">Loading...</div>
    }
    render() {
        if (this.props.location.pathname === "/admin/login") {
            return <LoginPage />
        } else {
            return (
                <div className="app">
                <NotificationContainer/>
                <AppHeader fixed>
                    <Suspense  fallback={this.loading()}>
                    <DefaultHeader onLogout={e=>this.signOut(e)}/>
                    </Suspense>
                </AppHeader>
                <div className="app-body">
                    <AppSidebar fixed display="lg">
                    <AppSidebarHeader />
                    <AppSidebarForm />
                    <Suspense>
                    <AppSidebarNav navConfig={navigation} {...this.props} router={router}/>
                    </Suspense>
                    <AppSidebarFooter />
                    <AppSidebarMinimizer />
                    </AppSidebar>
                    <main className="main">
                    <AppBreadcrumb appRoutes={routes} router={router}/>
                    <Container fluid>
                        <Suspense fallback={this.loading()}>
                        <Switch>
                            {routes.map((route, idx) => {
                            return route.component ? (
                                <Route
                                key={idx}
                                path={route.path}
                                exact={route.exact}
                                name={route.name}
                                render={props => (
                                    <route.component {...props} />
                                )} />
                            ) : (null);
                            })}
                            <Redirect from="/" to="/admin/dashboard" />
                        </Switch>
                        </Suspense>
                    </Container>
                    </main>
                    <AppAside fixed>
                    <Suspense fallback={this.loading()}>
                        <DefaultAside />
                    </Suspense>
                    </AppAside>
                </div>
                <AppFooter>
                    <Suspense fallback={this.loading()}>
                    <DefaultFooter />
                    </Suspense>
                </AppFooter>
                </div>
            );
        }
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedDashboard = connect(mapStateToProps)(Dashboard);
export { connectedDashboard as Dashboard };
