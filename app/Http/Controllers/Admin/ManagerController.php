<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Manager;
class ManagerController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest_admin');
    }

    /* Get all managers */
    public function managers(){
        $managers = Manager::all()->toArray();
        $i = 0;
        foreach ($managers as $key => $manager) {
            $managers[$key]['number'] = ++$i;
            # code...
        }
        return $managers;
    }
    /* Get Manager Detail */
    public function get($id){
        return Manager::find($id)->toArray();
    }

    /* Register new manager */
    public function register(Request $request){
        /* If there is manager who have same email */
        $isSameEmailManager = Manager::where('email', $request->email)->first();
        if($isSameEmailManager){
            return array('status' => 'error', 'message' => "Email already has been taken!");
        }
        /* If there is manager who have same username */
        $isSameNamelManager = Manager::where('email', $request->name)->first();
        if($isSameNamelManager){
            return array('status' => 'error', 'message' => "Username already has been taken!");
        }


        /* Register new manager */
        $manager = new Manager;
        $manager->first_name = $request->first_name;
        $manager->last_name = $request->last_name;
        $manager->name = $request->username;
        $manager->email = $request->email;
        $manager->password = bcrypt($request->password);
        $manager->profile_image = 'default.png';
        $manager->is_approved = 1;
        $manager->save();
        return array(
            'status' => 'success',
            'data' => $manager
        );
    }

    /* Update Manager Detail */
    public function update(Request $request){
        $manager = Manager::find($request->id);
        $manager->first_name = $request->first_name;
        $manager->last_name = $request->last_name;
        $manager->email = $request->email;
        $manager->name = $request->name;
        $manager->save();
        return array(
            'status' => 'success',
            'message' => 'Updated successfully!'
        );
    }

    /* Remove manager */
    public function remove($id){
        $manager = Manager::find($id);
        $manager->delete();
        return array(
            'status' => 'success',
            'message' => 'Removed successfully!'
        );
    }
}
