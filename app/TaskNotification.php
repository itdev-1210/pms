<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class TaskNotification extends Model
{
    public function getStartedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone('America/Los_Angeles')
            ->toDateTimeString()
        ;
    }
    public function getCompletedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone('America/Los_Angeles')
            ->toDateTimeString()
        ;
    }
    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone('America/Los_Angeles')
            ->toDateTimeString()
        ;
    }
    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone('America/Los_Angeles')
            ->toDateTimeString()
        ;
    }
    /**
     * Get all the employee check list note of this task
     */
    public function employeeCheckListNote() {
        return $this->hasMany('App\Models\EmployeeChecklistNote', 'node_id', 'node_id', 'node_type', 'node_type');
    }

    /**
     * Get all the reminders of this task
     */
    public function reminders() {
        return $this->hasMany('App\Reminder', 'node_id', 'node_id', 'node_type', 'node_type');
    }

    public function user_info(){
        return $this->hasOne('App\Models\Manager','id','assign_id');
    }

    public function tasks() {
        return $this->belongsTo('App\Models\Task','node_id','id');
    }
}
