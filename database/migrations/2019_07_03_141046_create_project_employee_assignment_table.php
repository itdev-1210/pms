<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectEmployeeAssignmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_employee_assignment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('node_id')->default(0);
            $table->string('node_type')->default(0);
            $table->integer('project_id')->default(0);
            $table->integer('employee_id')->default(0);
            $table->integer('manager_id')->default(0);
            $table->enum('assigned_by', ['ADMIN', 'MANAGER'])->default('MANAGER');
            $table->timestamps();
            $table->dateTimeTz('started_at')->nullable();
            $table->dateTimeTz('completed_at')->nullable();
            $table->integer('is_started')->default(0);
            $table->integer('is_completed')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_employee_assignment');
    }
}
