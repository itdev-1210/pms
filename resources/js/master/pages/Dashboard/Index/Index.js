import React from 'react'
import { connect } from 'react-redux';
import { Col, Row } from 'reactstrap';
import './styles.scss'
import Axios from 'axios';
import Moment from 'react-moment';
import moment from 'moment'
import { Checkbox } from 'semantic-ui-react'

class Index extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            datas: [],
            reminders: [],
            allCheck: false,
            isChecked: false,
            checkValue: []
        }
        this.removeNotification = this.removeNotification.bind(this)
        this.reload = this.reload.bind(this)
        this.allCheck = this.allCheck.bind(this)
        this.checkedValue = this.checkedValue.bind(this)
        this.beforeRemove = this.beforeRemove.bind(this)
    }

    componentWillMount() {
        this.reload()
    }

    reload() {
        Axios.get('/admin/ajax/getnotifications')
        .then(({data}) => {
            let checkValue = []
            data.map((notify, index) => {
                checkValue[index] = false
            })
            this.setState({datas: data, checkValue: checkValue, allCheck: false, isChecked: false})
        })
        .catch(err => {
            console.log('Get Notifications:', err)
        })

        Axios.get('/admin/ajax/getreminders')
        .then(({data}) => {
            this.setState({reminders: data})
        })
        .catch(err => {
            console.log('Get Reminders:', err)
        })
    }

    beforeRemove() {
        if (!this.state.isChecked) return;
        const {datas, checkValue} = this.state
        let ids = []
        for(let i = 0; i < datas.length; i++) {
            if (checkValue[i]) ids.push(datas[i].id)
        }
        this.removeNotification(ids)
    }

    removeNotification(ids) {
        Axios.post('/ajax/removenotification', {ids: ids})
        .then(response => {
            this.reload()
        })
        .catch(err => {
            console.log('Remove Notification:', err)
        })
    }

    allCheck() {
        let checkValue = this.state.checkValue.map(value=>this.state.allCheck ? false : true)
        this.setState({
            allCheck: !this.state.allCheck,
            checkValue: checkValue,
            isChecked: this.state.datas.length ? !this.state.allCheck : false
        })
    }

    checkedValue(index) {
        let checkValue = Object.assign([], this.state.checkValue)
        checkValue[index] = !checkValue[index]

        let countTrue = checkValue.filter((obj) => obj === true).length;
        this.setState({
            checkValue: checkValue,
            isChecked: countTrue > 0 ? true : false,
            allCheck: countTrue === this.state.datas.length ? true : false
        })
    }

    render(){
        const disableStyle = {
            backgroundColor: '#9a9a9a'
        }
        const {allCheck, isChecked, checkValue} = this.state
        return (
            <Row>
                <Col sm="12" md="6">
                    <div className="dashboard-box">
                        <div className="box-header">
                            <div className="box-check">
                                <Checkbox className="mx-2" checked={allCheck} onChange={this.allCheck}/>
                                <div className="box-title">Notifications</div>
                            </div>
                            <div className="remove-notify" style={!isChecked ? disableStyle : {}} onClick={this.beforeRemove}>Remove</div>
                        </div>
                        <div className="box-line"></div>
                        <div className="box-content">
                            {this.state.datas.map((data, index) => <div className="box-one-content" key={index}>
                                <Checkbox className="mx-2" checked={checkValue[index]} onChange={()=>(this.checkedValue(index))}/>
                                <div className={data.is_completed ? "complete_task" : "start_task"}>{"TASK"}</div>
                                <div className="content-title">{data.name + (data.is_completed ? ' completed ' : ' started ') + '"' + data.project + '_' + data.title + '"'}</div>
                                <div className="content-time"><Moment fromNow>{moment.utc(data.created_at).local()}</Moment></div>
                                <div className="content-btn" onClick={()=>this.removeNotification(data.id)}><span>&#215;</span></div>
                            </div>)}
                        </div>
                    </div>
                </Col>

                <Col sm="12" md="6">
                    <div className="dashboard-box">
                      <div className="box-title">Reminders</div>
                      <div className="box-line"></div>
                      <div className="box-content">
                          {this.state.reminders.map((data, index) => <div className="box-one-content" key={index}>
                            <div className="reminder">{"ATTN"}</div>
                            <div className="content-title">{data.project + '_' + data.title}</div>
                            <div className="content-time"><Moment fromNow>{moment.utc(data.created_at).local()}</Moment></div>
                          </div>)}
                      </div>
                    </div>
                </Col>
            </Row>
        )
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedIndex = connect(mapStateToProps)(Index);
export default connectedIndex;
