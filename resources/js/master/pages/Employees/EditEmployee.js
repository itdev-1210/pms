import React, {  } from 'react'
import { connect } from 'react-redux';

import {
    Button,
    Card,
    CardBody,
    CardHeader,
    Col,
    Form,
    FormGroup,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Row,
  } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios'
import {NotificationManager} from 'react-notifications';
import Axios from 'axios';

class EditEmployee extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            employee_detail : {}
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }
    componentDidMount(){
        let employeeId = this.props.match.params.id
        Axios.get(`/admin/ajax/employee/get/${employeeId}`)
        .then( response => {
            return response.data
        })
        .then( employee =>{
            this.setState({
                employee_detail : employee
            })
        })
    }
    handleChange(e){
        // e.preventDefault()
        let employee_detail = this.state.employee_detail
        employee_detail[e.target.name] = e.target.value
        this.setState({
            employee_detail : employee_detail
        })
    }
    handleSubmit(e){
        e.preventDefault()

        axios.post('/admin/ajax/employee/update', this.state.employee_detail)
        .then(response =>{
            let data = response.data
            if(data.status == "success"){
                NotificationManager.success(data.message,'Success', 3000);
                this.props.history.goBack()
            }
            else{
                NotificationManager.warning(data.message,'Warning', 3000);
            }
        }).catch(error =>{
            // NotificationManager.error( 'Server connection error!','Error', 3000);
        })
    }
    render(){
        return <div className="animated fadeIn">
                <Row>
                    <Col xs="12">
                        <Card>
                        <CardHeader>
                            <i className="icon-user-follow"></i><strong>Edit Employee</strong>
                            <div className="card-header-actions">
                            <Link color="danger" className="btn-pill btn btn-danger" to="/admin/employee">
                                <i className="icon-arrow-left"></i>&nbsp;Back
                            </Link>
                            </div>
                        </CardHeader>
                        <CardBody>
                        <Form  method="post" onSubmit={this.handleSubmit}>
                            <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" id="first_name" name="first_name" placeholder="First Name" required onChange={this.handleChange} value = {this.state.employee_detail ? this.state.employee_detail.first_name : ""} />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" id="last_name" name="last_name" placeholder="Last Name" required onChange={this.handleChange} value = {this.state.employee_detail ? this.state.employee_detail.last_name : ""} />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" id="username" name="username" placeholder="Username" required onChange={this.handleChange} value = {this.state.employee_detail ? this.state.employee_detail.name : ""} />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-envelope"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="email" id="email" name="email" placeholder="Email"required onChange={this.handleChange} value = {this.state.employee_detail ? this.state.employee_detail.email : ""} />
                                </InputGroup>
                            </FormGroup>
                            {/* <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-asterisk"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="password" id="password" name="password" placeholder="Password" required autoComplete={false} />
                                </InputGroup>
                            </FormGroup> */}
                            <FormGroup className="form-actions">
                                <Button type="submit" size="sm" color="success">Submit</Button>
                            </FormGroup>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
              </div>
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedEditEmployee = connect(mapStateToProps)(EditEmployee);
export default connectedEditEmployee;
