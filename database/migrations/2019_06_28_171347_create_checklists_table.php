<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255);
            $table->text('description');
            $table->integer('sort_number')->default(0);
            $table->integer('project_id');
            $table->timestamps();
            $table->integer('project_part_id')->default(0);
            $table->string('name_by_manager')->nullable();
            $table->string('description_by_manager')->nullable();
            $table->integer('sort_number_by_manager')->nullable();
            $table->integer('project_part_id_by_manager')->nullable();
            $table->enum('created_by', ['ADMIN', 'MANAGER'])->default('ADMIN');
            $table->integer('duplicated_from')->default(0);
            $table->boolean('is_outline')->default(false);
            $table->boolean('refresh')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checklists');
    }
}
