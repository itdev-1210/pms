import React from 'react'
import Axios from 'axios';
import { ProjectDetail } from './ProjectChecklist/ProjectDetail';
import { Button, Form, FormGroup, Label } from 'reactstrap';
import DatePicker from 'react-date-picker'

export default class ChecklistByDate extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            start_date: null,
            end_date: null,
            assigneddates : []
        }
        this.handleSearch = this.handleSearch.bind(this)
        this.reload = this.reload.bind(this)
        this.changeStart = this.changeStart.bind(this)
        this.changeEnd = this.changeEnd.bind(this)
    }
    componentDidMount(){
        Axios.get('/ajax/assigneddates')
        .then(response =>{
            this.setState({
                assigneddates : response.data
            })
        })
    }
    reload(){
        Axios.get('/ajax/assigneddates')
        .then(response =>{
            this.setState({
                assigneddates : response.data
            })
        })
    }
    changeStart(date) {
        this.setState({start_date: date})
    }

    changeEnd(date) {
        this.setState({end_date: date})
    }

    handleSearch(e){
        e.preventDefault()
        const {start_date, end_date} = this.state
        let start_string = start_date ? start_date.getFullYear() + "-" + ("0" + (start_date.getMonth() + 1)).slice(-2) + "-" + ("0" + start_date.getDate()).slice(-2) : null;
        let end_string = end_date ? end_date.getFullYear() + "-" + ("0" + (end_date.getMonth() + 1)).slice(-2) + "-" + ("0" + end_date.getDate()).slice(-2) : null;
        Axios({
            method: 'post',
            url: '/ajax/assigneddates',
            data: {
                start_date: start_string,
                end_date: end_string,
            },
        })
        .then(res => {
            this.setState({
                assigneddates : res.data
            })
        })
        .catch(() => {
        })
    }
    render(){
        return <div>
            <h1 className='text-center'>Checklist by date</h1>
            <div className="row">
                <Form inline className="mx-auto " onSubmit={this.handleSearch}>
                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="from_date" className="mr-sm-2">From</Label>
                        <DatePicker name='start_date' id="from_date" onChange={this.changeStart} value={this.state.start_date}/>
                    </FormGroup>
                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="to_date" className="mr-sm-2">To</Label>
                        <DatePicker name='to_date' id="to_date" onChange={this.changeEnd} value={this.state.end_date}/>
                    </FormGroup>
                    <Button>Search</Button>
                </Form>
            </div>

            <hr/>

            <div className='row mt-5'>
                <div className='col-md-3'>
                    <div className="list-group">
                        {
                            this.state.assigneddates && this.state.assigneddates.length && this.state.assigneddates.map((date, index) =>{
                                return <a href="#" className="list-group-item list-group-item-action flex-column align-items-start" key={index}>
                                <div className="d-flex w-100 justify-content-between">
                                  <h5 className="mb-1"><i className="fa fa-calendar"></i>{date.assigned_date}</h5>
                                  {/* <small>3 days ago</small> */}
                                </div>
                                <div className="mb-1"><h4>{date.project_details ? date.project_details.project_name : ''}</h4></div>
                                <small><i className="fa fa-user"></i>Assigned by <strong className='text-capitalize'>{date.manager_details ? date.manager_details.full_name : 'Manager'}</strong></small>
                              </a>
                            })
                        }
                    </div>
                </div>
                <div className='col-md-9'  >
                    {
                        this.state.assigneddates && this.state.assigneddates.length && this.state.assigneddates.map((date, index) =>{

                            return <div key={index}>
                                <h2 className=''><i className="fa fa-calendar"></i>{date.assigned_date}</h2>
                                <h3>Project Name: {date.project_details ? date.project_details.project_name : ''}</h3>
                                {  date.checklists.map((part, partindex) =>{
                                    return <ProjectDetail part = {part} key={partindex} index={partindex} reload={this.reload}/>
                                })}
                            </div>
                        })
                    }

                </div>
            </div>
        </div>
    }
}
