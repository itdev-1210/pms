import React, { Component }  from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { userActions } from '../../_actions';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Alert } from 'reactstrap';
import {NotificationContainer, NotificationManager} from 'react-notifications';

class Logout extends React.Component {
    constructor(props) {
        super(props);
        this.props.dispatch(userActions.logout());

    }
    render() {
            return <Redirect to='/manager/login' />
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedLogout = connect(mapStateToProps)(Logout);
export { connectedLogout as Logout };
