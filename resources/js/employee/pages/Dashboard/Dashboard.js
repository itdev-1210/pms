import React, { Suspense } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import * as router from 'react-router-dom';

import { userActions } from '../../_actions';
import navigation from '../../Layouts/Navigation'
const DefaultFooter = React.lazy(() => import('../../Layouts/DefaultFooter'));
const DefaultHeader = React.lazy(() => import('../../Layouts/DefaultHeader'));
import { Container } from 'reactstrap';
import { NotificationContainer } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import {
    AppFooter,
    AppHeader,
    AppSidebar,
    AppSidebarFooter,
    AppSidebarHeader,
    AppSidebarMinimizer,
    AppBreadcrumb2 as AppBreadcrumb,
    AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
import routes from '../../routes';
import Axios from 'axios';
import { LoginPage } from '../../LoginPage';
class Dashboard extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            menus: [
                {
                    name: 'Dashboard',
                    url: '/dashboard',
                    icon: 'icon-speedometer',
                    badge: {
                        variant: 'info',
                        text: 'NEW',
                    },
                },
                {
                    name: 'Projects',
                    icon: 'fa fa-user-secret',
                    children: []
                },
                {
                    name: 'Logout',
                    url: '/logout',
                    icon: 'cui-account-logout'
                },
            ]
        }
    }

    fetchMenus() {
        Axios.get('/ajax/getmenus')
            .then(({ data }) => {
                const retired = data.retired.map(item => ({
                    name: ' - ' + item.project_name,
                    url: `/project/${item.id}`
                }))
                const winners = data.winners.map(item => ({
                    name: ' - ' + item.project_name,
                    url: `/project/${item.id}`
                }))
                const testing = data.testing.map(item => ({
                    name: ' - ' + item.project_name,
                    url: `/project/${item.id}`
                }))
                const others = data.others.map(item => ({
                    name: item.project_name,
                    url: `/project/${item.id}`
                }))
                this.setState({
                    menus: [
                        {
                            name: 'Dashboard',
                            url: '/dashboard',
                            icon: 'icon-speedometer',
                            badge: {
                                variant: 'info',
                                text: 'NEW',
                            },
                        },
                        {
                            name: 'All Projects',
                            icon: 'fa fa-user-secret',
                            url: '/allProjects'
                        },
                        {
                            name: 'Projects',
                            icon: 'fa fa-user-secret',
                            children: [{
                                name: 'All Projects',
                                url: '/projects'
                            }, {
                                name: 'WINNERS',
                                children: winners
                            }, {
                                name: 'TESTING',
                                children: testing
                            }, {
                                name: 'RETIRED',
                                children: retired
                            },
                            ...others]
                        },
                        {
                            name: 'Logout',
                            url: '/logout',
                            icon: 'cui-account-logout'
                        },
                    ]
                });
            })
            .catch(err => {
                console.log(err.response ? err.response : err)
            })
    }

    componentDidMount() {
        if (this.props.location.pathname !== "/login")
            this.fetchMenus()
    }

    componentWillReceiveProps (nextProps) {
        if (this.props !== nextProps && nextProps.location.pathname === "/dashboard")
            this.fetchMenus()
    }

    handleDeleteUser(id) {
        return () => this.props.dispatch(userActions.delete(id));
    }
    loading() {
        return <div className="animated fadeIn pt-1 text-center">Loading...</div>
    }
    render() {
        if (this.props.location.pathname === "/login") {
            return <LoginPage />
        } else  {
            return (
                <div className="app employee">
                    <NotificationContainer />
                    <AppHeader fixed>
                        <Suspense fallback={this.loading()}>
                            <DefaultHeader onLogout={e => this.signOut(e)} />
                        </Suspense>
                    </AppHeader>
                    <div className="app-body">
                        <AppSidebar fixed display="lg">
                            <AppSidebarHeader />
                            {/* <AppSidebarForm /> */}
                            <Suspense>
                                <AppSidebarNav navConfig={{ items: this.state.menus }} {...this.props} router={router} />
                            </Suspense>
                            <AppSidebarFooter />
                            <AppSidebarMinimizer />
                        </AppSidebar>
                        <main className="main">
                            <AppBreadcrumb appRoutes={routes} router={router} />
                            <Container fluid className="flex-fill">
                                <Suspense fallback={this.loading()}>
                                    <Switch>
                                        {routes.map((route, idx) => {
                                            return route.component ? (
                                                <Route
                                                    key={idx}
                                                    path={route.path}
                                                    exact={route.exact}
                                                    name={route.name}
                                                    render={props => (
                                                        <route.component {...props} />
                                                    )} />
                                            ) : (null);
                                        })}
                                        <Redirect from="/" to="/dashboard" />
                                    </Switch>
                                </Suspense>
                            </Container>
                        </main>

                    </div>
                    <AppFooter>
                        <Suspense fallback={this.loading()}>
                            <DefaultFooter />
                        </Suspense>
                    </AppFooter>
                </div>
            );
        }
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedDashboard = connect(mapStateToProps)(Dashboard);
export { connectedDashboard as Dashboard };
