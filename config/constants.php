<?php

return [

    // This File contains many constants
    /* Don't Remove/Edit below code */
    "SafeSound" => [ // Alarm1,2,5,10,3,4,6,7,8,20  and +
        // 22 Alarm1
        // 200 Alarm1+
        // 762 Alarm1+
        // 789 Alarm1++
        "Free Gift: Safesound Alarm" => 1,
        "1 FREE Safesound Alarm" => 1,
        "1 FREE SafeSound Alarm" => 1,
        "1 Promo Deal Safesound Alarm" => 1,
        "1 Promo Deal SafetyGuard Alarm" => 1,
        "Free Gift: SafetyGuard Alarm" => 1,

        // 25 Alarm10
        "Best Deal: Buy 3 Get 7 FREE" => 10,
        "Best Deal: Buy 3 Get 7 FREE + $10 OFF" => 10,
        "Best Deal: Buy 3 Get 7 FREE + $13 OFF" => 10,
        "Best Deal: Buy 3 Get 7 FREE + $20 OFF" => 10,
        "Best Deal: Buy 3 Get 7 FREE + $25 OFF" => 10,
        "Best Deal: Buy 3 Get 7 FREE + €16 OFF" => 10,
        "Best Deal: Buy 3 Get 7 FREE + €8 OFF" => 10,
        "Best Deal: Buy 3 Get 7 FREE + £15 OFF" => 10,
        "Best Deal: Buy 3 Get 7 FREE + £8 OFF" => 10,
        "Buy 3 Get 7 FREE + PROMO $10 OFF" => 10,

        // 203 Alarm10+
        "10 Promo Deal Safesound Alarms" => 10,

        // 23 Alarm2
        "Buy 1 Get 1 FREE (Normally $80)" => 2,
        "Buy 1 Get 1 FREE (Normally £60)" => 2,
        "Buy 1 Get 1 FREE (Normally €64)" => 2,
        "Buy 1 Get 1 FREE (Normally 68€)" => 2,
        "Buy 1 Get 1 FREE (Normally AU$100)" => 2,
        "Buy 1 Get 1 FREE (Normally CA$100)" => 2,
        "Buy 1 Get 1 FREE + $10 OFF" => 2,
        "Buy 1 Get 1 FREE + $13 OFF" => 2,
        "Buy 1 Get 1 FREE + $20 OFF" => 2,
        "Buy 1 Get 1 FREE + $25 OFF" => 2,
        "Buy 1 Get 1 FREE + €16 OFF" => 2,
        "Buy 1 Get 1 FREE + €8 OFF" => 2,
        "Buy 1 Get 1 FREE + £15 OFF" => 2,
        "Buy 1 Get 1 FREE + £8 OFF" => 2,
        "Buy 1 Get 1 FREE + PROMO $10 OFF" => 2,

        // 201 Alarm2+
        "2 Promo Deal Safesound Alarms" => 2,
        "2 Promo Deal SafetyGuard Alarms" => 2,

        // 96 Alarm20
        "Buy 5 Get 15 FREE + PROMO $10 OFF" => 20,
        "Community Deal: Buy 5 Get 15 FREE" => 20,
        "Community Deal: Buy 5 Get 15 FREE + $10 OFF" => 20,
        "Community Deal: Buy 5 Get 15 FREE + $13 OFF" => 20,
        "Community Deal: Buy 5 Get 15 FREE + $20 OFF" => 20,
        "Community Deal: Buy 5 Get 15 FREE + $25 OFF" => 20,
        "Community Deal: Buy 5 Get 15 FREE + £15 OFF" => 20,
        "Community Deal: Buy 5 Get 15 FREE + £8 OFF" => 20,
        "Community Deal: Buy 5 Get 15 FREE + €16 OFF" => 5,
        "Community Deal: Buy 5 Get 15 FREE + €8 OFF" => 5,

        // 176 Alarm3+
        "3 Promo Deal Safesound Alarms" => 3,
        "3 Promo Deal SafetyGuard Alarms" => 3,

        // 177 Alarm4+
        "4 Promo Deal Safesound Alarms" => 4,
        "4 Promo Deal SafetyGuard Alarms" => 4,

        // 24 Alarm5
        "Buy 2 Get 3 FREE + PROMO $10 OFF" => 5,
        "Popular Deal: Buy 2 Get 3 FREE" => 5,
        "Popular Deal: Buy 2 Get 3 FREE + $10 OFF" => 5,
        "Popular Deal: Buy 2 Get 3 FREE + $13 OFF" => 5,
        "Popular Deal: Buy 2 Get 3 FREE + $20 OFF" => 5,
        "Popular Deal: Buy 2 Get 3 FREE + $25 OFF" => 5,
        "Popular Deal: Buy 2 Get 3 FREE + €16 OFF" => 5,
        "Popular Deal: Buy 2 Get 3 FREE + €8 OFF" => 5,
        "Popular Deal: Buy 2 Get 3 FREE + £15 OFF" => 5,
        "Popular Deal: Buy 2 Get 3 FREE + £8 OFF" => 5,

        // 202 Alarm5+
        "5 Promo Deal Safesound Alarms" => 5,

        // 178 Alarm6+
        "6 Promo Deal Safesound Alarms" => 6,
        "6 Promo Deal SafetyGuard Alarms" => 6,

        // 179 Alarm7+
        "7 Promo Deal Safesound Alarms" => 7,

        // 180 Alarm8+
        "8 Promo Deal Safesound Alarms" => 8,

        // 181 Alarm9+
        "9 Promo Deal Safesound Alarms" => 9,
    ],
    "BabySmooth" => [ // BabySmooth1,2,3,4,6,8 and +
        // 414 BabySmooth1
        "50% + £8 OFF: 1 BabySmooth Epilator" => 1,
        "50% + €8 OFF:: 1 BabySmooth Epilator" => 1,
        "50% + $10 OFF: 1 BabySmooth Epilator" => 1,
        "50% + AU$13 OFF:: 1 BabySmooth Epilator" => 1,
        "50% + CA$13 OFF:: 1 BabySmooth Epilator" => 1,
        "50% Discount: 1 BabySmooth Epilator" => 1,

        // 474 BabySmooth1+
        "1x Promo Deal BabySmooth" => 1,

        // 415 BabySmooth2
        "53% + £8 OFF: 2 BabySmooth Epilators" => 2,
        "53% + €8 OFF: 2 BabySmooth Epilators" => 2,
        "53% + $10 OFF: 2 BabySmooth Epilators" => 2,
        "53% + AU$13 OFF: 2 BabySmooth Epilators" => 2,
        "53% + CA$13 OFF: 2 BabySmooth Epilators" => 2,
        "53% Discount: 2 BabySmooth Epilators" => 2,

        // 475 BabySmooth2+
        "2x Promo Deal BabySmooth" => 2,

        // 416 BabySmooth3
        "56% + £8 OFF: 3 BabySmooth Epilators" => 3,
        "56% + €8 OFF: 3 BabySmooth Epilators" => 3,
        "56% + $10 OFF: 3 BabySmooth Epilators" => 3,
        "56% + AU$13 OFF: 3 BabySmooth Epilators" => 3,
        "56% + CA$13 OFF: 3 BabySmooth Epilators" => 3,
        "56% Discount: 3 BabySmooth Epilators" => 3,

        // 476 BabySmooth3+
        "3x Promo Deal BabySmooth" => 3,

        // 417 BabySmooth4
        "60% + £8 OFF: 4 BabySmooth Epilators" => 4,
        "60% + €8 OFF: 4 BabySmooth Epilators" => 4,
        "60% + $10 OFF: 4 BabySmooth Epilators" => 4,
        "60% + AU$13 OFF: 4 BabySmooth Epilators" => 4,
        "60% + CA$13 OFF: 4 BabySmooth Epilators" => 4,
        "60% Discount: 4 BabySmooth Epilators" => 4,

        // 477 BabySmooth4+
        "4x Promo Deal BabySmooth" => 4,

        // 484 BabySmooth6
        // "484" => 6,

        // 485 BabySmooth8
        "2 Everva Sleep Creams" => 8,
    ],
    "SteadyMat" => [ // BathMat1,2,3,4,6,10 and +
        // 542 BathMat1
        "1 Anti-Slip SteadyMat" => 1,
        "1 Anti-Slip SteadyMat + $5 OFF!" => 1,

        // 571 BathMat1+
        "1x Promo Deal SteadyMat" => 1,

        // 578 BathMat10
        // "578" => 10,

        // 543 BathMat2
        "2 Anti-Slip SteadyMats" => 2,
        "2 Anti-Slip SteadyMats + $5 OFF!" => 2,

        // 572 BathMat2+
        "2x Promo Deal SteadyMat" => 2,

        // 544 BathMat3
        "3 Anti-Slip SteadyMats" => 3,
        "3 Anti-Slip SteadyMats + $5 OFF!" => 3,

        // 573 BathMat3+
        "3x Promo Deal SteadyMat" => 3,

        // 545 BathMat4
        "4 Anti-Slip SteadyMats" => 4,
        "4 Anti-Slip SteadyMats + $5 OFF!" => 4,

        // 574 BathMat4+
        "4x Promo Deal SteadyMat" => 4,

        // 1608 BathMat5
        // "1608" => 5,

        // 577 BathMat6
        // "577" => 6,
    ],
    "FreeLight" => [ // CeilingLight1,2,3,4,5,10,20 and 1,2,3,4+
        // 679 CeilingLight1
        "1 Wireless FreeLight - 50% + $10 OFF!" => 1,
        "1 Wireless FreeLight - 50% + $5 OFF!" => 1,
        "1 Wireless FreeLight - 50% + CA$5 OFF!" => 1,
        "1 Wireless FreeLight - 50% OFF!" => 1,

        // 708 CeilingLight1+
        "1x Promo Deal FreeLight" => 1,

        // 707 CeilingLight10
        "10 Wireless FreeLights - 75% + $5 OFF!" => 10,
        "10 Wireless FreeLights - 75% + CA$5 OFF!" => 10,
        "10 Wireless FreeLights - 75% OFF!" => 10,

        // 680 CeilingLight2
        "2 Wireless FreeLights - 56% + $10 OFF!" => 2,
        "2 Wireless FreeLights - 56% + $5 OFF!" => 2,
        "2 Wireless FreeLights - 56% + CA$5 OFF!" => 2,
        "2 Wireless FreeLights - 56% OFF!" => 2,

        // 709 CeilingLight2+
        "2x Promo Deal FreeLights" => 2,

        // 759 CeilingLight20
        "20 Wireless FreeLights - 80% OFF!" => 20,

        // 681 CeilingLight1
        "3 Wireless FreeLights - 58% + $5 OFF!" => 3,
        "3 Wireless FreeLights - 58% + CA$5 OFF!" => 3,
        "3 Wireless FreeLights - 58% OFF!" => 3,
        "3 Wireless FreeLights - 60% + $10 OFF!" => 3,
        "3 Wireless FreeLights - 60% OFF!" => 3,

        // 710 CeilingLight3
        "3x Promo Deal FreeLights" => 3,

        // 682 CeilingLight4
        "4 Wireless FreeLights - 60% + $5 OFF!" => 4,
        "4 Wireless FreeLights - 60% OFF!" => 4,
        "4 Wireless FreeLights - 64% + $10 OFF!" => 4,
        "4 Wireless FreeLights - 64% OFF!" => 4,

        // 711 CeilingLight4+
        "4x Promo Deal FreeLights" => 4,

        // 758 CeilingLight5
        "5 Wireless FreeLights - 70% + $10 OFF!" => 5,
        "5 Wireless FreeLights - 70% OFF!" => 5,
    ],
    "DrainStix" => [ // DrainStix1,2,3,4,5,10,20 and +
        // 329 DrainStix1
        "1 Pack Of DrainStix" => 1,
        "1 Pack of DrainStix (Bonus 20% OFF!)" => 1,

        // 333 DrainStix1+
        "1x Promo Deal DrainStix" => 1,

        // 348 DrainStix10
        "10 Packs of DrainStix (46% OFF+Bonus 20% OFF!" => 10,
        "10 Packs of DrainStix (Extra 46% OFF)" => 10,

        // 330 DrainStix2
        "2 Packs of DrainStix (5% OFF+Bonus 20% OFF!)" => 2,
        "2 Packs Of DrainStix (Extra 5% OFF)" => 2,

        // 334 DrainStix2+
        "2x Promo Deal DrainStix" => 2,

        // 349 DrainStix20
        // "349" => 20,

        // 331 DrainStix3
        "3 Packs of DrainStix (10% OFF+Bonus 20% OFF!)" => 3,
        "3 Packs Of DrainStix (Extra 10% OFF)" => 3,

        // 335 DrainStix3+
        "3x Promo Deal DrainStix" => 3,

        // 332 DrainStix4
        "4 Packs of DrainStix (15% OFF+Bonus 20% OFF!)" => 4,
        "4 Packs Of DrainStix (Extra 15% OFF)" => 4,

        // 336 DrainStix4+
        "4x Promo Deal DrainStix" => 4,

        // 666 DrainStix4+MAINPest1
        // "666" => 4,

        // 347 DrainStix5
        "5 Packs of DrainStix (20% OFF+Bonus 20% OFF!)" => 5,
        "5 Packs Of DrainStix (Extra 20% OFF)" => 5,

        // 337 DrainStix5+
        "5x Promo Deal DrainStix" => 5,

        // 665 DrainStix5+MAINPest1
        // "665" => 5,
    ],
    "Dr Sock" => [ // DrSock1,3,5,7,9,11,14,2,10,20
        // 57 DrSock1
        "1 DrSock Soother" => 1,
        "1 FREE Dr Sock Soother" => 1,
        "Buy 1 Soothers - 50% + $5 OFF!" => 1,
        "Free Gift: Dr Sock" => 1,

        // 392 DrSock1+
        // "1 FREE Dr Sock Soother" => 1,
        "1 Promo Deal Dr Sock" => 1,

        // 765 DrSock1+
        // "765" => 1,

        // 198 DrSock10
        "Buy 5 Get 5 FREE" => 10,
        "Buy 5 Get 5 FREE + $5 OFF" => 10,
        "Buy 5 Get 5 FREE + €4 OFF" => 10,
        "Buy 5 Get 5 FREE + £4 OFF" => 10,
        "Buy 5 Get 5 FREE + AU$7 OFF" => 10,

        // 65 DrSock11
        // "65" => 11,

        // 1257 DrSock13
        // "1257" => 13,

        // 66 DrSock14
        // "66" => 14,

        // 58 DrSock2
        "2 DrSock Soothers" => 2,

        // 393
        "2 Promo Deal Dr Socks" => 2,

        // 199 DrSock20
        "Buy 8 Get 12 FREE" => 20,
        "Buy 8 Get 12 FREE + $5 OFF" => 20,
        "Buy 8 Get 12 FREE + 4 € OFF" => 20,
        "Buy 8 Get 12 FREE + €4 OFF" => 20,
        "Buy 8 Get 12 FREE + AU$7 OFF" => 20,

        // 59 DrSock3
        "3 DrSock Soothers" => 3,
        "Buy 2 Get 1 FREE" => 3,
        "Buy 2 Get 1 FREE + $5 OFF" => 3,
        "Buy 2 Get 1 FREE + 4 € OFF" => 3,
        "Buy 2 Get 1 FREE + €4 OFF" => 3,
        "Buy 2 Get 1 FREE + £4 OFF" => 3,
        "Buy 2 Get 1 FREE + AU$7 OFF" => 3,
        "Buy 2 Get 1 FREE + CA$7 OFF" => 3,
        "Buy 2 Soothers - GET ONE FREE! - 51% OFF" => 3,

        // 394 DrSock3+
        "3 Promo Deal Dr Socks" => 3,

        // 60 DrSock4
        "4 DrSock Soothers" => 4,

        // 395 DrSock4+
        "4 Promo Deal Dr Socks" => 4,

        // 61 DrSock5
        "Buy 3 Get 2 FREE" => 5,
        "Buy 3 Get 2 FREE + $5 OFF" => 5,
        "Buy 3 Get 2 FREE + 4 € OFF" => 5,
        "Buy 3 Get 2 FREE + €4 OFF" => 5,
        "Buy 3 Get 2 FREE + £4 OFF" => 5,
        "Buy 3 Get 2 FREE + AU$7 OFF" => 5,
        "Buy 3 Get 2 FREE + CA$7 OFF" => 5,
        "Buy 3 Soothers - GET TWO FREE! - 52% OFF" => 5,

        // 396 DrSock5+
        "5 Promo Deal Dr Socks" => 5,

        // 62 DrSock6
        // "62" => 6,

        // 63 DrSock7
        "Buy 4 Soothers - GET THREE FREE! - 53% OFF" => 7,

        // 64 DrSock9
        // "64" => 9,
    ],
    "EcoPowerPlate" => [ // EcoPowerPlate1,2,3,4,5,10,20 and 1,2,3,4,5,6,7,8,9,10+
        // 667 EcoPowerPlate1
        // "667" => 1,

        // 794 EcoPowerPlate1+
        "1x Promo Duplex EcoPowerPlate" => 1,
        "1x Promo Deal EcoPowerPlate" => 1,

        // 689 EcoPowerPlate10
        "Buy 5 EcoPowerPlates Get 5 FREE!" => 10,
        "Buy 5 Get 5 FREE + $5 OFF!" => 10,
        "10 EcoPowerPlates - 59% OFF!" => 10,
        "10 EcoPowerPlates - 59% + $5 OFF!" => 10,
        "Buy 5 Get 5 FREE + CA$6 OFF!" => 10,
        "Buy 5 Duplex EcoPowerPlates Get 5 FREE!" => 10,
        "Buy 5 Duplex Get 5 FREE + $5 OFF!" => 10,

        // 803 EcoPowerPlate10+
        "10x Promo Deal EcoPowerPlates" => 10,

        // 668 EcoPowerPlate2
        // "668" => 2,

        // 795 EcoPowerPlate2+
        "2x Promo Duplex EcoPowerPlates" => 2,
        "2x Promo Deal EcoPowerPlates" => 2,

        // 690 EcoPowerPlate20
        "20 EcoPowerPlates - 61% OFF!" => 20,
        "20 EcoPowerPlates - 61% + $5 OFF!" => 20,
        "Buy 8 Duplex EcoPowerPlates Get 12 FREE" => 20,
        "Buy 8 Duplex Get 12 FREE + $5 OFF!" => 20,
        "Buy 8 EcoPowerPlates Get 12 FREE!" => 20,
        "Buy 8 Get 12 FREE + $5 OFF!" => 20,
        "Buy 8 Get 12 FREE + CA$6 OFF!" => 20,

        // 669 EcoPowerPlate3
        "3 EcoPowerPlates - 50% OFF!" => 3,
        "3 EcoPowerPlates - 50% + $5 OFF!" => 3,
        "Buy 2 Duplex EcoPowerPlates Get 1 FREE!" => 3,
        "Buy 2 Duplex Get 1 FREE + $5 OFF!" => 3,
        "Buy 2 EcoPowerPlates Get 1 FREE!" => 3,
        "Buy 2 Get 1 FREE + $5 OFF!" => 3,
        "Buy 2 Get 1 FREE + CA$6 OFF!" => 3,

        // 796 EcoPowerPlate3+
        "3x Promo Duplex EcoPowerPlates" => 3,
        "3x Promo Deal EcoPowerPlates" => 3,

        // 670 EcoPowerPlate4
        // "670" => 4,

        // 797 EcoPowerPlate4+
        "4x Promo Duplex EcoPowerPlates" => 4,
        "4x Promo Deal EcoPowerPlates" => 4,

        // 688 EcoPowerPlate5
        "5 EcoPowerPlates - 57% OFF!" => 5,
        "5 EcoPowerPlates - 57% + $5 OFF!" => 5,
        "Buy 3 Duplex EcoPowerPlates Get 2 FREE!" => 5,
        "Buy 3 EcoPowerPlates Get 2 FREE!" => 5,
        "Buy 3 Duplex Get 2 FREE + $5 OFF!" => 5,
        "Buy 3 Get 2 FREE + $5 OFF!" => 5,
        "Buy 3 Get 2 FREE + CA$6 OFF!" => 5,

        // 798 EcoPowerPlate5+
        "5x Promo Duplex EcoPowerPlates" => 5,
        "5x Promo Deal EcoPowerPlates" => 5,

        // 799 EcoPowerPlate6
        "6x Promo Duplex EcoPowerPlates" => 6,
        "6x Promo Deal EcoPowerPlates" => 6,

        // 800 EcoPowerPlate7
        "7x Promo Duplex EcoPowerPlates" => 7,
        "7x Promo Deal EcoPowerPlates" => 7,

        // 801 EcoPowerPlate8
        "8x Promo Duplex EcoPowerPlates" => 8,
        "8x Promo Deal EcoPowerPlates" => 8,

        // 802 EcoPowerPlate9
        "9x Promo Duplex EcoPowerPlates" => 9,
        "9x Promo Deal EcoPowerPlates" => 9,
    ],
    "Hemp Cream" => [ // Everva1,2,3,4,5,7 and +
        // 1391 Everva1
        "1 Everva Hemp Cream - 50% + $5 OFF!" => 1,
        "1 Everva Hemp Cream - 50% OFF!" => 1,

        // 1454 Everva1+
        "1 Promo Deal Everva Hemp Cream" => 1,

        // 1392 Everva2
        "2 Everva Hemp Creams - 53% + $5 OFF!" => 2,
        "2 Everva Hemp Creams - 54% + $5 OFF!" => 2,
        "2 Everva Hemp Creams - 54% OFF!" => 2,

        // 1455 Everva2+
        "2 Promo Deal Everva Hemp Creams" => 2,

        // 1393 Everva3
        "3 Everva Hemp Creams - 58% OFF!" => 3,

        // 1456 Everva3+
        "3 Promo Deal Everva Hemp Creams" => 3,

        // 1394 Everva4
        "4 Everva Hemp Creams - 60% + $5 OFF!" => 4,
        "4 Everva Hemp Creams - 60% OFF!" => 4,

        // 1457 Everva4+
        "4 Promo Deal Everva Hemp Creams" => 4,

        // 1458 Everva5
        "Buy 3 Everva Hemp Creams Get 2 FREE!" => 5,
        "Buy 3 Everva Creams Get 2 FREE + $5 OFF!" => 5,

        // 1459 Everva7
        "Buy 4 Everva Hemp Creams Get 3 FREE!" => 7,
        "Buy 4 Everva Creams Get 3 FREE + $5 OFF!" => 7,
    ],
    "HomeLife LED" => [ // LEDbar1,2,3,5,10,20 and +
        // 462 LEDBar1
        "1 HomeLife LED Bar" => 1,
        "1 HomeLife LED Bar (Bonus 15% OFF!)" => 1,

        // 491 LEDBar1+
        "1x Promo HomeLife LED Bar" => 1,

        // 465 LEDBar10
        "10 Pack HomeLife LED Deal! (Bonus 15% OFF!)" => 10,
        "10 Pack HomeLife LED Deal! Save $50!" => 10,

        // 495 LEDBar10+
        "10x Promo HomeLife LED Bars" => 10,

        // 492 LEDBar2
        "2x Promo HomeLife LED Bars" => 2,

        // 490 LEDBar20
        "20 Pack HomeLife LED Deal! (Bonus 15% OFF!)" => 20,
        "20 Pack HomeLife LED Deal! Save $200!" => 20,

        // 496 LEDBar20+
        "20x Promo HomeLife LED Bars" => 20,

        // 463 LEDBar3
        "3 HomeLife LED Bars" => 3,
        "3 Pack HomeLife LED Deal! (Bonus 15% OFF!)" => 3,
        "3 Pack HomeLife LED Deal! Save $10!" => 3,

        // 493 LEDBar3+
        "3x Promo HomeLife LED Bars" => 3,

        // 464 LEDBar5
        "5 Pack HomeLife LED Deal! (Bonus 15% OFF!)" => 5,
        "5 Pack HomeLife LED Deal! Save $20!" => 5,

        // 494 LEDBar5+
        "5x Promo HomeLife LED Bars" => 5,
    ],
    // LuminaCool1,3,5,6,7,8,10,20 and + and A
    // LuminaWarm1,3,5,10,20 and + and C
    "LuminaBright" => [
        // 692 LuminaCool1
        "1 LuminaBright Cool White 50% + $10 OFF!" => 1,
        "1 LuminaBright Cool White 50% + CA$13 OFF!" => 1,
        "1 LuminaBright Cool White 50% OFF!" => 1,
        "1 OutdoorBright Cool White 50% + $10 OFF!" => 1,
        "1 OutdoorBright Cool White 50% OFF!" => 1,

        // 720 LuminaCool1+
        "1x Promo Deal Cool White" => 1,

        // 1040 LuminaCool1+
        // "1x Promo Deal Cool White" => 1,

        // 697 LuminaCool10
        "10 LuminaBright Cool White 65% + $10 OFF!" => 10,
        "10 LuminaBright Cool White 65% OFF!" => 10,
        "10 OutdoorBright Cool White 65% + $10 OFF!" => 10,
        "10 OutdoorBright Cool White 65% OFF!" => 10,

        // 725 LuminaCool10+
        "10x Promo Deal Cool White" => 10,

        // 1049 LuminaCool10+
        // "1049" => 10,

        // 740 LuminaCool2
        "2x Promo Deal Cool White" => 2,

        // 1041 LuminaCool2+
        // "1041" => 2,

        // 698 LuminaCool20
        "20 LuminaBright Cool White 75% OFF!" => 20,
        "20 OutdoorBright Cool White 75% + $10 OF" => 20,
        "20 OutdoorBright Cool White 75% OFF!" => 20,

        // 726 LuminaCool20+
        "20x Promo Deal Cool White" => 20,

        // 1050 LuminaCool20+
        // "1050" => 20,

        // 695 LuminaCool3
        "3 LuminaBright Cool White 55% + $10 OFF!" => 3,
        "3 LuminaBright Cool White 55% + CA$13 OF" => 3,
        "3 LuminaBright Cool White 55% OFF!" => 3,
        "3 OutdoorBright Cool White 55% + $10 OFF" => 3,
        "3 OutdoorBright Cool White 55% OFF!" => 3,

        // 723 LuminaCool3
        "3x Promo Deal Cool White" => 3,

        // 1042 LuminaCool3+
        // "3x Promo Deal Cool White" => 3,

        // 741 LuminaCool4
        "4x Promo Deal Cool White" => 4,

        // 1043 LuminaCool4+
        // "1043" => 4,

        // 696 LuminaCool5
        "5 LuminaBright Cool White 60% + $10 OFF!" => 5,
        "5 LuminaBright Cool White 60% OFF!" => 5,
        "5 OutdoorBright Cool White 60% + $10 OFF!" => 5,
        "5 OutdoorBright Cool White 60% OFF!" => 5,

        // 724 LuminaCool5+
        "5x Promo Deal Cool White" => 5,

        // 1044 LuminaCool5+
        // "1044" => 5,

        // 742 LuminaCool6+
        "6x Promo Deal Cool White" => 6,

        // 1045 LuminaCool6+
        // "1045" => 6,

        // 743 LuminaCool7+
        "7x Promo Deal Cool White" => 7,

        // 1046 LuminaCool7+
        // "1046" => 7,

        // 744 LuminaCool8+
        "8x Promo Deal Cool White" => 8,

        // 744 LuminaCool8+
        // "1047" => 8,

        // 745 LuminaCool9+
        "9x Promo Deal Cool White" => 9,

        // 1048 LuminaCool9+
        // "1048" => 9,

        // 694 LuminaWarm1
        "1 LuminaBright Warm White 50% OFF!" => 1,
        "1 OutdoorBright Warm White 50% + $10 OFF" => 1,
        "1 OutdoorBright Warm White 50% OFF!" => 1,

        // 722 LuminaWarm1+
        "1x Promo Deal Warm White" => 1,

        // 1051 LuminaWarm1+
        // "1x Promo Deal Warm White" => 1,

        // 705 LuminaWarm10
        "10 LuminaBright Warm White 65% OFF!" => 10,
        "10 OutdoorBright Warm White 65% + $10 OF" => 10,
        "10 OutdoorBright Warm White 65% OFF!" => 10,

        // 733 LuminaWarm10+
        "10x Promo Deal Warm White" => 10,

        // 1060 LuminaWarm10+
        // "1060" => 10,

        // 752 LuminaWarm2+
        "2x Promo Deal Warm White" => 2,

        // 1052 LuminaWarm2+
        // "1052" => 2,

        // 706 LuminaWarm20
        "20 OutdoorBright Warm White 75% OFF!" => 20,

        // 734 LuminaWarm20+
        "20x Promo Deal Warm White" => 20,

        // 1061 LuminaWarm20+
        // "1061" => 20,

        // 703 LuminaWarm3
        "3 LuminaBright Warm White 55% + $10 OFF!" => 3,
        "3 LuminaBright Warm White 55% OFF!" => 3,
        "3 OutdoorBright Warm White 55% OFF!" => 3,

        // 731 LuminaWarm3+
        "3x Promo Deal Warm White" => 3,

        // 1053 LuminaWarm3+
        // "1053" => 3,

        // 753 LuminaWarm4+
        "4x Promo Deal Warm White" => 4,

        // 1054 LuminaWarm4+
        // "1054" => 4,

        // 704 LuminaWarm5
        "5 LuminaBright Warm White 60% + $10 OFF!" => 5,
        "5 LuminaBright Warm White 60% OFF!" => 5,
        "5 OutdoorBright Warm White 60% + $10 OFF" => 5,
        "5 OutdoorBright Warm White 60% OFF!" => 5,

        // 732 LuminaWarm5+
        "5x Promo Deal Warm White" => 5,

        // 1055 LuminaWarm5+
        // "1055" => 5,

        // 754 LuminaWarm6+
        "6x Promo Deal Warm White" => 6,

        // 1056 LuminaWarm6+
        // "1056" => 6,

        // 755 LuminaWarm7+
        "7x Promo Deal Warm White" => 7,

        // 1057 LuminaWarm7+
        // "1057" => 7,

        // 756 LuminaWarm8+
        "8x Promo Deal Warm White" => 8,

        // 1058 LuminaWarm8+
        // "1058" => 8,

        // 757 LuminaWarm9+
        "9x Promo Deal Warm White" => 9,

        // 1059 LuminaWarm9+
        // "1059" => 9,
    ],
    "Pest Reject" => [ // AUMAINPest1,2,3,4,5,10  and +  ++
        // 81 AUMAINPest1
        "1 FREE Pest Reject" => 1,
        "50% OFF + AU$13 OFF: 1 Unit (Reg. AU$106)" => 1,
        "50% OFF: 1 Pest Reject (Reg. AU$106)" => 1,
        "Free Gift: Pest Reject" => 1,

        // 432 AUMAINPest1+
        // "1 FREE Pest Reject" => 1,
        "1 Promotional Deal Pest Reject" => 1,

        // 437 AUMAINPest1++
        // "Free Gift: Pest Reject" => 1,

        // 86 AUMAINPest10
        "75% OFF + AU$13 OFF: 10 Units (Reg. AU$1060)" => 10,
        "75% OFF: 10 Pest Rejects (Reg. AU$1060)" => 10,

        // 82 AUMAINPest2
        "56% OFF + AU$13 OFF: 2 Units (Reg. AU$212)" => 2,
        "56% OFF: 2 Pest Reject (Reg. AU$212)" => 2,

        // 433 AUMAINPest2+
        "2 Promotional Deal Pest Reject" => 2,

        // 83 AUMAINPest3
        "60% OFF + AU$13 OFF: 3 Units (Reg. AU$318)" => 3,
        "60% OFF: 3 Pest Reject (Reg. AU$318)" => 3,

        // 434 AUMAINPest3+
        "3 Promotional Deal Pest Reject" => 3,

        // 84 AUMAINPest4
        "64% OFF + AU$13 OFF: 4 Units (Reg. AU$424)" => 4,
        "64% OFF: 4 Pest Rejects (Reg. AU$424)" => 4,

        // 435 AUMAINPest4+
        "4 Promotional Deal Pest Reject" => 4,

        // 85 AUMAINPest5
        "70% OFF + AU$13 OFF: 5 Units (Reg. AU$530)	" => 5,
        "70% OFF: 5 Pest Rejects (Reg. AU$530)" => 5,

        // 436 AUMAINPest5+
        "5 Promotional Deal Pest Reject" => 5,

        // 666 AUMAINPest1
        // "666" => 1,

        // 665 AUMAINPest1
        // "665" => 1,

        // 75 EUMAINPest1
        "50% OFF + R$40 OFF: 1 Unit (Normally R$300)" => 1,
        "50% OFF: 1 Pest Reject (Normally R$300)" => 1,

        // 426 EUMAINPest1+
        // "426" => 1,

        // 431 EUMAINPest1++
        // "431" => 1,

        // 80 EUMAINPest10
        "75% OFF + R$40 OFF: 10 Units (Normally R$3000" => 10,
        "75% OFF: 10 Pest Rejects (Normally R$3000)" => 10,

        // 76 EUMAINPest2
        "56% OFF + R$40 OFF: 2 Units (Normally R$600)" => 2,
        "56% OFF: 2 Pest Rejects (Normally R$600)" => 2,

        // 427 EUMAINPest2+
        // "427" => 2,

        // 77 EUMAINPest3
        "60% OFF + R$40 OFF: 3 Units (Normally R$900)" => 3,
        "60% OFF: 3 Pest Rejects (Normally R$900)" => 3,

        // 428 EUMAINPest3+
        // "428" => 3,

        // 78 EUMAINPest4
        "64% OFF + R$40 OFF: 4 Units (Normally R$1200)" => 4,
        "64% OFF: 4 Pest Rejects (Normally R$1200)" => 4,

        // 429 EUMAINPest4+
        // "429" => 4,

        // 79 EUMAINPest5
        "70% OFF + R$40 OFF: 5 Units (Normally R$1500)" => 5,
        "70% OFF: 5 Pest Rejects (Normally R$1500)" => 5,

        // 430 EUMAINPest5+
        // "430" => 5,

        // 4 MAINPest1
        // "1 FREE Pest Reject" => 1,
        "1 Ultrasonic Pest Reject" => 1,
        "50% + PROMO $10 OFF: 1 Unit (Normally $80)" => 1,
        "50% OFF + $10 OFF: 1 Unit (Normally $80)" => 1,
        "50% OFF + CA$13 OFF: 1 Unit (Reg. CA$106)" => 1,
        "50% OFF: 1 Pest Reject (Normally $80)" => 1,
        "50% OFF: 1 Pest Reject (Reg. CA$106)" => 1,
        // "Free Gift: Pest Reject" => 1,

        // 218 MAINPest1+
        // "1 FREE Pest Reject" => 1,
        // "1 Promotional Deal Pest Reject" => 1,

        // 768 MAINPest1+
        // "1 FREE Pest Reject" => 1,

        // 222 MAINPest1++
        // "Free Gift: Pest Reject" => 1,

        // 9 MAINPest10
        "75% + PROMO $10 OFF: 10 Units (Normally $800)" => 10,
        "75% OFF + $10 OFF: 10 Units (Normally $800)" => 10,
        "75% OFF: 10 Pest Rejects (Normally $800)" => 10,

        // 5 MAINPest2
        "56% + PROMO $10 OFF: 2 Units (Normally $160)" => 2,
        "56% OFF + $10 OFF: 2 Units (Normally $160)" => 2,
        "56% OFF + CA$13 OFF: 2 Units (Reg. CA$212)" => 2,
        "56% OFF: 2 Pest Rejects (Normally $160)" => 2,
        "56% OFF: 2 Pest Rejects (Reg. CA$212)" => 2,

        // 219 MAINPest2+
        // "2 Promotional Deal Pest Rejects" => 2,

        // 6 MAINPest3
        "3 Ultrasonic Pest Rejects" => 3,
        "60% + PROMO $10 OFF: 3 Units (Normally $240)" => 3,
        "60% OFF + $10 OFF: 3 Units (Normally $240)" => 3,
        "60% OFF + CA$13 OFF: 3 Units (Reg. CA$318)" => 3,
        "60% OFF: 3 Pest Rejects (Normally $240)" => 3,
        "60% OFF: 3 Pest Rejects (Reg. CA$318)" => 3,

        // 220 MAINPest3+
        // "3 Promotional Deal Pest Rejects" => 3,

        // 7 MAINPest4
        "3 Promotional Deal Pest Rejects" => 4,
        "64% + PROMO $10 OFF: 4 Units (Normally $320)" => 4,
        "64% OFF + $10 OFF: 4 Units (Normally $320)" => 4,
        "64% OFF + CA$13 OFF: 4 Units (Reg. CA$424)" => 4,
        "64% OFF: 4 Pest Rejects (Normally $320)" => 4,
        "64% OFF: 4 Pest Rejects (Reg. CA$424)" => 4,

        // 8 MAINPest5
        "3 Promotional Deal Pest Rejects" => 5,
        "70% + PROMO $10 OFF: 5 Units (Normally $400)" => 5,
        "70% OFF + $10 OFF: 5 Units (Normally $400)" => 5,
        "70% OFF + CA$13 OFF: 5 Units (Reg. CA$530)" => 5,
        "70% OFF: 5 Pest Rejects (Normally $400)" => 5,
        "70% OFF: 5 Pest Rejects (Reg. CA$530)" => 5,

        // 221 MAINPest5+
        // "5 Promotional Deal Pest Rejects" => 5,

        // 438 UKMAINPest1
        // "1 FREE Pest Reject" => 1,
        "50% OFF + £7 OFF: 1 Unit (Reg. £60)" => 1,
        "50% OFF: 1 Pest Reject (Reg. £60)" => 1,
        "Free Gift: Pest Reject" => 1,

        // 444 UKMAINPest1+
        // "1 FREE Pest Reject" => 1,
        // "1 Promotional Deal Pest Reject" => 1,

        // 449 UKMAINPest1++
        // "Free Gift: Pest Reject" => 1,

        // 443 UKMAINPest10
        "75% OFF + £7 OFF: 10 Units (Reg. £600)" => 10,
        "75% OFF: 10 Pest Rejects (Reg. £600)" => 10,

        // 439 UKMAINPest2
        "56% OFF + £7 OFF: 2 Units (Reg. £120)" => 2,
        "56% OFF: 2 Pest Rejects (Reg. £120)" => 2,

        // 445 UKMAINPest2+
        // "445" => 2,

        // 440 UKMAINPest3
        "60% OFF + £7 OFF: 3 Units (Reg. £180)" => 3,
        "60% OFF: 3 Pest Rejects (Reg. £180)" => 3,

        // 446 UKMAINPest3+
        // "446" => 3,

        // 441 UKMAINPest4
        "64% OFF + £7 OFF: 4 Units (Reg. £240)" => 4,
        "64% OFF: 4 Pest Rejects (Reg. £240)" => 4,

        // 447 UKMAINPest4+
        // "447" => 4,

        // 442 UKMAINPest5
        "70% OFF + £7 OFF: 5 Units (Reg. £300)" => 5,
        "70% OFF: 5 Pest Rejects (Reg. £300)" => 5,

        // 448 UKMAINPest5+
        // "448" => 5,
    ],
    "MirrorAid" => [ // MirrorAid1,2,3,4,5 and +
        // 1292 MirrorAid1
        "1 Pair Of MirrorAid Films - 50% + $10 OFF" => 1,
        "1 Pair Of MirrorAid Films - 50% OFF" => 1,

        // 1304 MirrorAid1+
        "1 MirrorAid Anti-fog Film" => 1,
        "1 MirrorAid Film" => 1,
        "1 Promo MirrorAid" => 1,

        // 1293 MirrorAid2
        "2 Pairs Of MirrorAid Films - 56% + $10 OFF" => 2,
        "2 Pairs Of MirrorAid Films - 56% OFF" => 2,

        // 1305 MirrorAid2+
        "2 Promo MirrorAids" => 2,

        // 1294 MirrorAid3
        "3 Pairs Of MirrorAid Films - 58% + $10 OFF" => 3,
        "3 Pairs Of MirrorAid Films - 58% OFF" => 3,

        // 1306 MirrorAid3+
        "3 MirrorAid Anti-fog Films" => 3,
        "3 MirrorAid Films" => 3,
        "3 Promo MirrorAids" => 3,

        // 1295 MirrorAid4
        "4 Pairs Of MirrorAid Films - 60% + $10 OFF" => 4,
        "4 Pairs Of MirrorAid Films - 60% OFF" => 4,

        // 1307 MirrorAid4+
        "4 Promo MirrorAids" => 4,

        // 1308 MirrorAid5
        "5 Promo MirrorAids" => 5,
    ],
    "NanoMagic Cloth" => [ // NanoMagic1,2,3,4,5,6,7,8,9,10 and +
        // 1314 NanoMagic1

        // 1309 NanoMagic1+
        "1 Promo NanoMagic" => 1,
        "1 NanoMagic Car Cloth" => 1,

        // 1291 NanoMagic10
        "Buy 4 NanoMagic Cloths Get 6 FREE" => 10,
        "Buy 4 NanoMagic Cloths Get 6 FREE + $5 OFF" => 10,
        "4 NanoMagic Car Cloths - 60% OFF" => 10,
        "4 NanoMagic Car Cloth - 60% + $5 OFF" => 10,

        // 1288 NanoMagic2
        "Buy 1 NanoMagic Cloth Get 1 FREE" => 2,
        "Buy 1 NanoMagic Cloth Get 1 FREE + $5 OFF" => 2,
        "1 NanoMagic Car Cloth - 50% OFF" => 2,
        "1 NanoMagic Car Cloth - 50% + $5 OFF" => 2,

        // 1310 NanoMagic2+
        "2 Promo NanoMagics" => 2,

        // 1315 NanoMagic3

        // 1311 NanoMagic3+
        "3 Promo NanoMagics" => 3,
        "3 NanoMagic Car Cloths" => 3,

        // 1316 NanoMagic4

        // 1312 NanoMagic4+
        "4 Promo NanoMagics" => 4,

        // 1289 NanoMagic5
        "2 NanoMagic Car Cloths - 54% OFF" => 5,
        "2 NanoMagic Car Cloth - 54% + $5 OFF" => 5,
        "Buy 2 NanoMagic Cloths Get 3 FREE" => 5,
        "Buy 2 NanoMagic Cloths Get 3 FREE + $5 " => 5,

        // 1313 NanoMagic5+
        "5 Promo NanoMagics" => 5,

        // 1290 NanoMagic7
        "Buy 3 NanoMagic Cloths Get 4 FREE" => 7,
        "Buy 3 NanoMagic Cloths Get 4 FREE + $5 " => 7,
        "3 NanoMagic Car Cloths - 56% OFF" => 7,
        "3 NanoMagic Car Cloth - 56% + $5 OFF" => 7,
    ],
    "ClearSight" => [ // NIGHTGlass1,2,3,4,5,6,7,8,9,10   and +
        // 18 NIGHTGlass1
        "1 FREE ClearSight" => 1,

        // 44 NIGHTGlass1
        "1 ClearSight HD Driving Glasses 50% OFF" => 1,
        "1 HD Driving Glasses 50% OFF + $10 OFF" => 1,
        "1 HD Driving Glasses 50% OFF + $13 OFF" => 1,
        "1 HD Driving Glasses 50% OFF + PROMO $10 OFF" => 1,

        // 374 NIGHTGlass1+
        "1 ClearSight Driving Glasses" => 1,
        "1x Promo Deal Night Glasses" => 1,
        "Free Driving Glasses" => 1,
        "Free Gift: Driving Glasses" => 1,

        // 763 NIGHTGlass1+
        // "763" => 1,

        // 380 NIGHTGlass10
        // "380" => 10,

        // 45 NIGHTGlass2
        "2 ClearSight HD Driving Glasses 52% OFF" => 2,
        "2 HD Driving Glasses 52% OFF + $10 OFF" => 2,
        "2 HD Driving Glasses 52% OFF + $13 OFF" => 2,
        "2 HD Driving Glasses 52% OFF + PROMO $10 OFF" => 2,

        // 375 NIGHTGlass2+
        "2 ClearSight Driving Glasses" => 2,
        "2x Promo Deal Night Glasses" => 2,

        // 46 NIGHTGlass3+
        "3 ClearSight HD Driving Glasses 55% OFF" => 3,
        "3 HD Driving Glasses 55% OFF + $10 OFF" => 3,
        "3 HD Driving Glasses 55% OFF + $13 OFF" => 3,
        "3 HD Driving Glasses 55% OFF + PROMO $10 OFF" => 3,

        // 376 NIGHTGlass3+
        "3 ClearSight Driving Glasses" => 3,
        "3x Promo Deal Night Glasses" => 3,

        // 1237 NIGHTGlass4
        // "1237" => 4,

        // 377 NIGHTGlass4+
        "4 ClearSight Driving Glasses" => 4,
        "4x Promo Deal Night Glasses" => 4,

        // 379 NIGHTGlass5
        // "379" => 5,

        // 1238 NIGHTGlass5
        // "1238" => 5,

        // 378 NIGHTGlass5+
        "5 ClearSight Driving Glasses" => 5,
        "5x Promo Deal Night Glasses" => 5,
    ],
    "Odyssey Blanket" => [ // OdysseyA1,2,3,4 and +
        // 845 OdysseyA1
        "1 Odyssey Blanket - 50% + $10 OFF!" => 1,
        "1 Odyssey Blanket - 50% OFF!" => 1,

        // 851 OdysseyA1+
        "1 Promo Odyssey Blanket" => 1,

        // 846 OdysseyA2
        "2 Odyssey Blankets - 55% OFF!" => 2,
        "2 Odyssey Blankets - 55% + $10 OFF!" => 2,

        // 852 OdysseyA2+
        "2 Promo Odyssey Blankets" => 2,

        // 847 OdysseyA3
        "3 Odyssey Blankets - 58% + $10 OFF!" => 3,
        "3 Odyssey Blankets - 58% OFF!" => 3,

        // 853 OdysseyA3+
        "3 Promo Odyssey Blankets" => 3,

        // 848 OdysseyA4
        "4 Odyssey Blankets - 60% + $10 OFF!" => 4,
        "4 Odyssey Blankets - 60% OFF!" => 4,

        // 854 OdysseyA4+
        "4 Promo Odyssey Blankets" => 4,
    ],
    "ZeroClean Oven Liners" => [ // OvenLiner1,2,3,4,5,10 and 1,2,3,4+
        // 609 OvenLiner1
        "Buy 1 ZeroClean 3-Pack 50% OFF!" => 1,
        "Buy 1 ZeroClean 3-Pack 50% + $5 OFF" => 1,
        "1 ZeroClean 3-Pack - 50% OFF!" => 1,
        "1 ZeroClean 3-Pack - 50% + $5 OFF!" => 1,

        // 652 OvenLiner1+
        "1x Promo Deal ZeroClean 3-Pack" => 1,

        // 660 OvenLiner10
        "Buy 5 ZeroClean 3-Packs Get 5 FREE!" => 10,
        "Buy 5 ZeroClean Packs Get 5 FREE + $5 OFF" => 10,

        // 610 OvenLiner2

        // 653 OvenLiner2+
        "2x Promo Deal ZeroClean 3-Packs" => 2,

        // 611 OvenLiner3
        "Buy 2 ZeroClean 3-Packs Get 1 FREE!" => 3,
        "Buy 2 ZeroClean Packs Get 1 FREE + $5 OFF" => 3,
        "2 ZeroClean 3-Packs - 57% OFF!" => 3,
        "2 ZeroClean 3-Packs - 57% + $5 OFF!" => 3,

        // 654 OvenLiner3+
        "3x Promo Deal ZeroClean 3-Packs" => 3,

        // 612 OvenLiner4

        // 655 OvenLiner4+
        "4x Promo Deal ZeroClean 3-Packs" => 4,

        // 659 OvenLiner5
        "Buy 3 ZeroClean 3-Packs Get 2 FREE!" => 5,
        "Buy 3 ZeroClean Packs Get 2 FREE + $5 OFF" => 5,
        "3 ZeroClean 3-Packs - 59% OFF!" => 5,
        "3 ZeroClean 3-Packs - 59% + $5 OFF!" => 5,
    ],
    "Pet Gentle" => [ // PetGentle1,2,3,4,5,10   and +
        // 120 PetGentle1
        "50% + $10 OFF PROMO: 1 PetGentle" => 1,

        "50% Discount: 1 PetGentle (Normally $80)" => 1,
        "50% + $10 OFF: 1 PetGentle (Normally $80)" => 1,
        "50% Discount: 1 PetGentle (Normally £66)" => 1,
        "50% Discount + £8 OFF: 1 PetGentle" => 1,
        "50% Discount: 1 PetGentle (Normally AU$118)" => 1,
        "50% Discount + AU$13 OFF: 1 PetGentle" => 1,
        "50% Discount: 1 PetGentle (Normally CA$106)" => 1,
        "50% Discount + CA$13 OFF: 1 PetGentle" => 1,

        "1 Unit of PetGentle (50% OFF!)" => 1,
        "1 PetGentle + Extra AU$13 OFF" => 1,
        "1 PetGentle + Extra CA$13 OFF" => 1,
        "1 PetGentle + Extra €8 OFF" => 1,
        "1 PetGentle + Extra £8 OFF" => 1,

        "50% Discount: 1 PetAttention (Normally $80)" => 1,
        "50% + $10 OFF: 1 PetAttention (Normally $80)" => 1,
        "50% Discount: 1 PetGentle" => 1,
        "50% + £8 OFF: 1 PetGentle" => 1,
        "50% + AU$13 OFF: 1 PetGentle" => 1,
        "50% + CA$13 OFF: 1 PetGentle" => 1,
        // 265 PetGentle1

        // 360 PetGentle1+
        "1x Promo Deal PetAttention" => 1,
        "1x Promo Deal PetGentle" => 1,

        // 125 PetGentle10

        // 121 PetGentle2
        "56% + $10 OFF PROMO: 2 PetGentle" => 2,

        "56% Discount: 2 PetGentle (Normally $160)" => 2,
        "56% + $10 OFF: 2 PetGentle (Normally $160)" => 2,
        "56% Discount: 2 PetGentle (Normally £132)" => 2,
        "56% Discount + £8 OFF: 2 PetGentle" => 2,
        "56% Discount: 2 PetGentle (Normally AU$236)" => 2,
        "56% Discount + AU$13 OFF: 2 PetGentle" => 2,
        "56% Discount: 2 PetGentle (Normally CA$212)" => 2,
        "56% Discount + CA$13 OFF: 2 PetGentle" => 2,

        "2 Units of PetGentle (Extra Discount!)" => 2,
        "2 PetGentle Extra Discount + AU$13 OFF" => 2,
        "2 PetGentle Extra Discount + CA$13 OFF" => 2,
        "2 PetGentle Extra Discount + €8 OFF" => 2,
        "2 PetGentle Extra Discount + £8 OFF" => 2,

        "56% Discount: 2 PetAttention (Normally $160)" => 2,
        "56% + $10 OFF: 2 PetAttention (Normally $160)" => 2,
        "56% Discount: 2 PetGentle" => 2,
        "56% + £8 OFF: 2 PetGentle" => 2,
        "56% + AU$13 OFF: 2 PetGentle" => 2,
        "56% + CA$13 OFF: 2 PetGentle" => 2,
        // 266 PetGentle2

        // 361 PetGentle2+
        "2x Promo Deal PetGentle" => 2,

        // 122 PetGentle3
        "60% + $10 OFF PROMO: 3 PetGentle" => 3,

        "60% Discount: 3 PetGentle (Normally $240)" => 3,
        "60% + $10 OFF: 3 PetGentle (Normally $240)" => 3,
        "60% Discount: 3 PetGentle (Normally £198)" => 3,
        "60% Discount + £8 OFF: 3 PetGentle" => 3,
        "60% Discount: 3 PetGentle (Normally AU$354)" => 3,
        "60% Discount + AU$13 OFF: 3 PetGentle" => 3,
        "60% Discount: 3 PetGentle (Normally CA$318)" => 3,
        "60% Discount + CA$13 OFF: 3 PetGentle" => 3,

        "3 Units of PetGentle (Extra Discount!)" => 3,
        "3 PetGentle Extra Discount + AU$13 OFF" => 3,
        "3 PetGentle Extra Discount + CA$13 OFF" => 3,
        "3 PetGentle Extra Discount + €8 OFF" => 3,
        "3 PetGentle Extra Discount + £8 OFF" => 3,

        "60% Discount: 3 PetAttention (Normally $240)" => 3,
        "60% + $10 OFF: 3 PetAttention (Normally $240)" => 3,
        "60% Discount: 2 PetGentle" => 3,
        "60% + £8 OFF: 2 PetGentle" => 3,
        "60% + AU$13 OFF: 2 PetGentle" => 3,
        "60% + CA$13 OFF: 2 PetGentle" => 3,

        // 267 PetGentle3

        // 362 PetGentle3+
        "3x Promo Deal PetGentle" => 3,

        // 123 PetGentle4
        "64% + $10 OFF PROMO: 4 PetGentle" => 4,

        "64% Discount: 4 PetGentle (Normally $320)" => 4,
        "64% + $10 OFF: 4 PetGentle (Normally $320)" => 4,
        "64% Discount: 4 PetGentle (Normally £264)" => 4,
        "64% Discount + £8 OFF: 4 PetGentle" => 4,
        "64% Discount: 4 PetGentle (Normally AU$472)" => 4,
        "64% Discount + AU$13 OFF: 4 PetGentle" => 4,
        "64% Discount: 4 PetGentle (Normally CA$424)" => 4,
        "64% Discount + CA$13 OFF: 4 PetGentle" => 4,

        "4 Units of PetGentle (Extra Discount!)" => 4,
        "4 PetGentle Extra Discount + AU$13 OFF" => 4,
        "4 PetGentle Extra Discount + CA$13 OFF" => 4,
        "4 PetGentle Extra Discount + €8 OFF" => 4,
        "4 PetGentle Extra Discount + £8 OFF" => 4,

        "64% Discount: 4 PetAttention (Normally $320)" => 4,
        "64% + $10 OFF: 4 PetAttention (Normally $320)" => 4,
        "64% Discount: 4 PetGentle" => 4,
        "64% + £8 OFF: 4 PetGentle" => 4,
        "64% + AU$13 OFF: 4 PetGentle" => 4,
        "64% + CA$13 OFF: 4 PetGentle" => 4,

        // 268 PetGentle4

        // 363 PetGentle4+
        "4x Promo Deal PetGentle" => 4,

        // 124 PetGentle5
        "70% + $10 OFF PROMO: 5 PetGentle" => 5,

        "70% Discount: 5 PetGentle (Normally $400)" => 5,
        "70% + $10 OFF: 5 PetGentle (Normally $400)" => 5,
        "70% Discount: 5 PetGentle (Normally £330)" => 5,
        "70% Discount + £8 OFF: 5 PetGentle" => 5,
        "70% Discount: 5 PetGentle (Normally AU$590)" => 5,
        "70% Discount + AU$13 OFF: 5 PetGentle" => 5,
        "70% Discount: 5 PetGentle (Normally CA$530)" => 5,
        "70% Discount + CA$13 OFF: 5 PetGentle" => 5,

        "5 Units of PetGentle (Extra Discount!)" => 5,
        "5 PetGentle Extra Discount + AU$13 OFF" => 5,
        "5 PetGentle Extra Discount + CA$13 OFF" => 5,
        "5 PetGentle Extra Discount + €8 OFF" => 5,
        "5 PetGentle Extra Discount + £8 OFF" => 5,

        "70% Discount: 5 PetAttention (Normally $400)" => 5,
        "70% + $10 OFF: 5 PetAttention (Normally $400)" => 5,
        "70% Discount: 5 PetGentle" => 5,
        "70% + £8 OFF: 5 PetGentle" => 5,
        "70% + AU$13 OFF: 5 PetGentle" => 5,
        "70% + CA$13 OFF: 5 PetGentle" => 5,

        // 269 PetGentle5

        // 364 PetGentle5+
        "4x Promo Deal AquaTheory" => 5,
    ],
    "Dental Probiotics" => [ // ProbFresh1,2,3,4,+ / RebillProbFresh1,2,3,4
        // 1276 ProbFresh1
        "1 Bottle of Pro-B Fresh - 50% OFF" => 1,
        "1 Bottle of Pro-B Fresh - 50% + $5 OFF" => 1,
        "1 Bottle of Pro Dental Biotics - 50% OFF" => 1,
        "1 Bottle of Pro Dental Biotics - 50% + $5 OFF" => 1,

        // 1572 ProbFresh1+
        "1 Promo Deal Pro-B Fresh" => 1,
        "1 Promo Deal Pro Dental Biotics" => 1,

        // 1277 ProbFresh2
        "2 Bottles of Pro-B Fresh - 56% OFF" => 2,
        "2 Bottles of Pro-B Fresh - 56% + $5 OFF" => 2,
        "2 Bottles of Pro Dental Biotics - 56% OFF" => 2,
        "2 Bottles of Pro Dental Biotics - 56% + $5 OF" => 2,

        // 1573 ProbFresh2+
        "2 Promo Deal Pro-B Fresh" => 2,
        "2 Promo Deal Pro Dental Biotics" => 2,

        // 1278 ProbFresh3
        "3 Bottles of Pro-B Fresh - 58% OFF" => 3,
        "3 Bottles of Pro-B Fresh - 58% + $5 OFF" => 3,
        "3 Bottles of Pro Dental Biotics - 58% OFF" => 3,
        "3 Bottles of Pro Dental Biotics - 58% + $5 OF" => 3,

        // 1574 ProbFresh3+
        "3 Promo Deal Pro-B Fresh" => 3,
        "3 Promo Deal Pro Dental Biotics" => 3,

        // 1279 ProbFresh4
        "4 Bottles of Pro-B Fresh - 60% OFF" => 4,
        "4 Bottles of Pro-B Fresh - 60% + $5 OFF" => 4,
        "4 Bottles of Pro Dental Biotics - 60% OFF" => 4,
        "4 Bottles of Pro Dental Biotics - 60% + $5 OF" => 4,

        // 1575 ProbFresh4+
        "4 Promo Deal Pro-B Fresh" => 4,
        "4 Promo Deal Pro Dental Biotics" => 4,

        // 1576 ProBFreshRebill1
        "1 Pro-B Fresh (Shipped Every Month)" => 1,

        // 1577 ProBFreshRebill2
        "2 Pro-B Fresh (Shipped Every Month)" => 2,

        // 1578 ProBFreshRebill3
        "3 Pro-B Fresh (Shipped Every Month)" => 3,

        // 1579 ProBFreshRebill4
        "4 Pro-B Fresh (Shipped Every Month)" => 4,
    ],
    "NatureFresh" => [ // Purifier1,2,3,5,10,20   and +  ++  PurifierCA1,2,3,5,10,20 and PurifierAU1,2,3,5,10,20 and PurifierIE1,2,3,5,10,20
        // 145 Purifier1
        "1 NatureFresh" => 1,
        "1 NatureFresh Air Purifier" => 1,

        // 189 Purifier1+
        // 761 Purifier1+
        // 223 Purifier1++
        // 238 PurifierAU1+
        // 228 PurifierCA1+
        // 275 PurifierEU1+
        // 258 PurifierIE1+
        // 248 PurifierUK1+
        "FREE Car Purifier" => 1,
        "1 FREE NatureFresh" => 1,
        "Free Gift: NatureFresh" => 1,
        "Free Gift: Naturefresh" => 1,
        "1 Promo Deal Purifier" => 1,

        // 149 Purifier10
        // 236 PurifierAU10
        // 226 PurifierCA10
        // 273 PurifierEU10
        // 256 PurifierIE10
        // 246 PurifierUK10
        "Buy 5 Get 5 FREE" => 10,
        "Buy 5 Get 5 FREE + $10 OFF" => 10,
        "Buy 5 Get 5 FREE + $15 OFF" => 10,
        "Buy 5 Get 5 FREE + $13 OFF" => 10,
        "Buy 5 Get 5 FREE + $20 OFF" => 10,
        "Buy 5 Get 5 FREE + €8 OFF" => 10,
        "Buy 5 Get 5 FREE + €12 OFF" => 10,
        "Buy 5 Get 2 FREE + £7 OFF" => 10,
        "Buy 5 Get 2 FREE + £10 OFF" => 10,
        "Buy 5 Get 5 FREE + PROMO $10 OFF" => 10,
        "Buy 5 Get 5 FREE + PROMO $15 OFF" => 10,

        // 193 Purifier10+
        // 242 PurifierAU10+
        // 232 PurifierCA10+
        // 279 PurifierEU10+
        // 262 PurifierIE10+
        // 252 PurifierUK10+
        "10 Promo Deal Purifiers" => 10,

        // 158 Purifier2

        // 190 Purifier2+
        // 239 PurifierAU2+
        // 229 PurifierCA2+
        // 276 PurifierEU2+
        // 259 PurifierIE2+
        // 249 PurifierUK2+
        "2 Promo Deal Purifiers" => 2,

        // 150 Purifier20
        // 237 PurifierAU20
        // 227 PurifierCA20
        // 274 PurifierEU20
        // 257 PurifierIE20
        // 247 PurifierUK20
        "Buy 8 Get 12 FREE" => 20,
        "Buy 8 Get 12 FREE + $10 OFF" => 20,
        "Buy 8 Get 12 FREE + $15 OFF" => 20,
        "Buy 8 Get 12 FREE + $13 OFF" => 20,
        "Buy 8 Get 12 FREE + $20 OFF" => 20,
        "Buy 8 Get 12 FREE + €8 OFF" => 20,
        "Buy 8 Get 12 FREE + €12 OFF" => 20,
        "Buy 8 Get 12 FREE + £7 OFF" => 20,
        "Buy 8 Get 12 FREE + £10 OFF" => 20,
        "Buy 8 Get 12 FREE + PROMO $10 OFF" => 20,
        "Buy 8 Get 12 FREE + PROMO $15 OFF" => 20,

        // 194 Purifier20+
        // 243 PurifierAU20+
        // 233 PurifierCA20+
        // 280 PurifierEU20+
        // 262 PurifierIE20+
        // 253 PurifierUK20+
        "20 Promo Deal Purifiers" => 20,

        // 144 Purifier3
        // 234 PurifierAU3
        // 224 PurifierCA3
        // 271 PurifierEU3
        // 254 PurifierIE3
        // 244 PurifierUK3
        "3 NatureFresh" => 3,
        "3 NatureFresh Air Purifiers" => 3,
        "Buy 2 Get 1 FREE" => 3,
        "Buy 2 Get 1 FREE + $10 OFF" => 3,
        "Buy 2 Get 1 FREE + $15 OFF" => 3,
        "Buy 2 Get 1 FREE + $13 OFF" => 3,
        "Buy 2 Get 1 FREE + $20 OFF" => 3,
        "Buy 2 Get 1 FREE + €8 OFF" => 3,
        "Buy 2 Get 1 FREE + €12 OFF" => 3,
        "Buy 2 Get 1 FREE + £7 OFF" => 3,
        "Buy 2 Get 1 FREE + £10 OFF" => 3,
        "Buy 2 Get 1 FREE + PROMO $10 OFF" => 3,
        "Buy 2 Get 1 FREE + PROMO $15 OFF" => 3,

        // 191 Purifier3+
        // 240 PurifierAU3+
        // 230 PurifierCA3+
        // 277 PurifierEU3+
        // 260 PurifierIE3+
        // 250 PurifierUK3+
        "3 Promo Deal Purifiers" => 3,

        // 1566 Purifier4+

        // 148 Purifier5
        // 235 PurifierAU5
        // 225 PurifierCA5
        // 272 PurifierEU5
        // 255 PurifierIE5
        // 245 PurifierUK5
        "Buy 3 Get 2 FREE" => 5,
        "Buy 3 Get 2 FREE + $10 OFF" => 5,
        "Buy 3 Get 2 FREE + $15 OFF" => 5,
        "Buy 3 Get 2 FREE + $13 OFF" => 5,
        "Buy 3 Get 2 FREE + $20 OFF" => 5,
        "Buy 3 Get 2 FREE + €8 OFF" => 5,
        "Buy 3 Get 2 FREE + €12 OFF" => 5,
        "Buy 3 Get 2 FREE + £7 OFF" => 5,
        "Buy 3 Get 2 FREE + £10 OFF" => 5,
        "Buy 3 Get 2 FREE + PROMO $10 OFF" => 5,
        "Buy 3 Get 2 FREE + PROMO $15 OFF" => 5,

        // 192 Purifier5+
        // 241 PurifierAU5+
        // 231 PurifierCA5+
        // 278 PurifierEU5+
        // 261 PurifierIE5+
        // 251 PurifierUK5+
        "5 Promo Deal Purifiers" => 5,
    ],
    "Q-Grips" => [ // QGrips1,2,3,4,6,10 and 1,2,3,4+
        // 675 QGrips1
        "1 FREE Q-Grips" => 1,

        // 713 QGrips1+
        "1x Promo Deal Q-Grips" => 1,

        // 719 QGrips10
        "Buy 5 Q-Grips Get 5 FREE!" => 10,
        "Buy 5 Q-Grips Get 5 FREE + $10 OFF!" => 10,
        "Buy 5 Q-Grips Get 5 FREE + CA$13 OFF!" => 10,
        "Buy 5 Q-Grips Get 5 FREE + AU$13 OFF!" => 10,
        "Buy 5 Q-Grips Get 5 FREE + €8 OFF!" => 10,
        "Buy 5 Q-Grips Get 5 FREE + £8 OFF!" => 10,

        // 676 QGrips2
        "Buy 1 Q-Grips Get 1 FREE!" => 2,
        "Buy 1 Q-Grips Get 1 FREE + $10 OFF!" => 2,
        "Buy 1 Q-Grips Get 1 FREE + CA$13 OFF!" => 2,
        "Buy 1 Q-Grips Get 1 FREE + AU$13 OFF!" => 2,
        "Buy 1 Q-Grips Get 1 FREE + €8 OFF!" => 2,
        "Buy 1 Q-Grips Get 1 FREE + £8 OFF!" => 2,

        // 714 QGrips2+
        "2x Promo Deal Q-Grips" => 2,

        // 677 QGrips3

        // 715 QGrips3+
        "3x Promo Deal Q-Grips" => 3,

        // 678 QGrips4
        "Buy 2 Q-Grips Get 2 FREE!" => 4,
        "Buy 2 Q-Grips Get 2 FREE + $10 OFF!" => 4,
        "Buy 2 Q-Grips Get 2 FREE + CA$13 OFF!" => 4,
        "Buy 2 Q-Grips Get 2 FREE + AU$13 OFF!" => 4,
        "Buy 2 Q-Grips Get 2 FREE + €8 OFF!" => 4,
        "Buy 2 Q-Grips Get 2 FREE + £8 OFF!" => 4,

        // 716 QGrips4+
        "4x Promo Deal Q-Grips" => 4,

        // 718 QGrips6
        "Buy 3 Q-Grips Get 3 FREE!" => 6,
        "Buy 3 Q-Grips Get 3 FREE + $10 OFF!" => 6,
        "Buy 3 Q-Grips Get 3 FREE + CA$13 OFF!" => 6,
        "Buy 3 Q-Grips Get 3 FREE + AU$13 OFF!" => 6,
        "Buy 3 Q-Grips Get 3 FREE + €8 OFF!" => 6,
        "Buy 3 Q-Grips Get 3 FREE + £8 OFF!" => 6,
    ],
    "Senzu Knife Sharpener" => [ // SenzuSharpener1,2,3,4,5 and +
        // 402 SenzuSharpener1
        "50% Discount: 1 Senzu Sharpener" => 1,
        "50% + $10 OFF: 1 Senzu Sharpener" => 1,
        "50% + CA$13 OFF: 1 Senzu Sharpener" => 1,
        "50% + AU$13 OFF: 1 Senzu Sharpener" => 1,
        "50% + €8 OFF: 1 Senzu Sharpener" => 1,
        "50% + £8 OFF: 1 Senzu Sharpener" => 1,

        // 408 SenzuSharpener1+
        "1x Promo Deal Senzu Sharpener" => 1,
        "Free Gift: Senzu Knife Sharpener" => 1,

        // 403 SenzuSharpener2
        "56% Discount: 2 Senzu Sharpeners" => 2,
        "56% + $10 OFF: 2 Senzu Sharpeners" => 2,
        "56% + CA$13 OFF: 2 Senzu Sharpeners" => 2,
        "56% + AU$13 OFF: 2 Senzu Sharpeners" => 2,
        "56% + €8 OFF: 2 Senzu Sharpeners" => 2,
        "56% + £8 OFF: 2 Senzu Sharpeners" => 2,

        // 409 SenzuSharpener2+
        "2x Promo Deal Senzu Sharpeners" => 2,

        // 404 SenzuSharpener3
        "58% Discount: 3 Senzu Sharpeners" => 3,
        "58% + $10 OFF: 3 Senzu Sharpeners" => 3,
        "58% + CA$13 OFF: 3 Senzu Sharpeners" => 3,
        "58% + AU$13 OFF: 3 Senzu Sharpeners" => 3,
        "58% + €8 OFF: 3 Senzu Sharpeners" => 3,
        "58% + £8 OFF: 3 Senzu Sharpeners" => 3,

        // 410 SenzuSharpener3+
        "3x Promo Deal Senzu Sharpeners" => 3,

        // 405 SenzuSharpener4
        "60% Discount: 4 Senzu Sharpeners" => 4,
        "60% + $10 OFF: 4 Senzu Sharpeners" => 4,
        "60% + CA$13 OFF: 4 Senzu Sharpeners" => 4,
        "60% + AU$13 OFF: 4 Senzu Sharpeners" => 4,
        "60% + €8 OFF: 4 Senzu Sharpeners" => 4,
        "60% + £8 OFF: 4 Senzu Sharpeners" => 4,

        // 411 SenzuSharpener4+
        "4x Promo Deal Senzu Sharpeners" => 4,

        // 412 SenzuSharpener5+
    ],
    "AquaTheory" => [ // ShowerFilter1,2,3,4,5 and +
        // 356 ShowerFilter1
        "50% Discount: 1 AquaTheory Shower Filter" => 1,
        "50% + $10 OFF: 1 AquaTheory Shower Filter" => 1,
        "1 Unit of AquaTheory (50% OFF!)" => 1,
        "1 AquaTheory + Extra AU$13 OFF" => 1,
        "1 AquaTheory + Extra CA$13 OFF" => 1,
        "1 AquaTheory + Extra €8 OFF" => 1,
        "1 AquaTheory + Extra £8 OFF" => 1,

        // 366 ShowerFilter1+
        "1x Promo Deal AquaTheory" => 1,

        // 357 ShowerFilter2
        "56% Discount: 2 AquaTheory Shower Filters" => 2,
        "56% + $10 OFF: 2 AquaTheory Shower Filters" => 2,
        "2 Units of AquaTheory (Extra Discount!)" => 2,
        "2 AquaTheory Extra Discount + AU$13 OFF" => 2,
        "2 AquaTheory Extra Discount + CA$13 OFF" => 2,
        "2 AquaTheory Extra Discount + €8 OFF" => 2,
        "2 AquaTheory Extra Discount + £8 OFF" => 2,

        // 367 ShowerFilter2+
        "2x Promo Deal AquaTheory" => 2,

        // 358 ShowerFilter3
        "58% Discount: 3 AquaTheory Shower Filters" => 3,
        "58% + $10 OFF: 3 AquaTheory Shower Filters" => 3,
        "3 Units of AquaTheory (Extra Discount!)" => 3,
        "3 AquaTheory Extra Discount + AU$13 OFF" => 3,
        "3 AquaTheory Extra Discount + CA$13 OFF" => 3,
        "3 AquaTheory Extra Discount + €8 OFF" => 3,
        "3 AquaTheory Extra Discount + £8 OFF" => 3,

        // 368 ShowerFilter3+
        "3x Promo Deal AquaTheory" => 3,

        // 359 ShowerFilter4
        "60% Discount: 4 AquaTheory Shower Filters" => 4,
        "60% + $10 OFF: 4 AquaTheory Shower Filters" => 4,
        "4 Units of AquaTheory (Extra Discount!)" => 4,
        "4 AquaTheory Extra Discount + AU$13 OFF" => 4,
        "4 AquaTheory Extra Discount + CA$13 OFF" => 4,
        "4 AquaTheory Extra Discount + €8 OFF" => 4,
        "4 AquaTheory Extra Discount + £8 OFF" => 4,

        // 369 ShowerFilter4+
        "4x Promo Deal AquaTheory" => 4,

        // 1607 ShowerFilter5
        "1607" => 5,
    ],
    "SpeedClean Foam Cleaner" => [ // SpeedClean1,2,3,4,5,10,20 and +
        // 579 SpeeedClean1
        "1 SpeedClean Foam Cleaner" => 1,
        "1 SpeedClean Foam Cleaner + $10 OFF!" => 1,

        // 1362 SpeeedClean1+
        "1x Promo Deal SpeedClean" => 1,

        // 619 SpeeedClean10
        "Buy 3 SpeedCleans Get 7 FREE!" => 10,
        "Buy 3 SpeedCleans Get 7 FREE + $10 OFF!" => 10,

        // 580 SpeeedClean2
        "2 SpeedClean Foam Cleaners" => 2,
        "2 SpeedClean Foam Cleaners + $10 OFF!" => 2,
        "Buy 1 SpeedClean Get 1 FREE!" => 2,
        "Buy 1 SpeedClean Get 1 FREE + $10 OFF!" => 2,

        // 1363 SpeeedClean2+
        "2x Promo Deal SpeedClean" => 2,

        // 620 SpeeedClean20
        "Buy 5 SpeedCleans Get 15 FREE!" => 20,
        "Buy 5 SpeedCleans Get 15 FREE + $10 OFF!" => 20,

        // 581 SpeeedClean3
        "3 SpeedClean Foam Cleaners" => 3,
        "3 SpeedClean Foam Cleaners + $10 OFF!" => 3,

        // 1364 SpeeedClean3+
        "3x Promo Deal SpeedClean" => 3,

        // 582 SpeeedClean4
        "4 SpeedClean Foam Cleaners" => 4,
        "4 SpeedClean Foam Cleaners + $10 OFF!" => 4,

        // 1365 SpeeedClean4+
        "4x Promo Deal SpeedClean" => 4,

        // 618 SpeeedClean5
        "Buy 2 SpeedCleans Get 3 FREE!" => 5,
        "Buy 2 SpeedCleans Get 3 FREE + $10 OFF!" => 5,

        // 1366 SpeeedClean5+
        "5x Promo Deal SpeedClean" => 5,
    ],
    "StampGuard" => [ // StampGuard1,2,3,4,5,10   and +
        // 133 StampGuard1
        "50% OFF: 1 StampGuard (Normally $60)" => 1,
        "50% + $10 OFF: 1 StampGuard (Normally $60)" => 1,
        "1 FREE StampGuard" => 1,
        "1 StampGuard Roller" => 1,
        "Free Gift: StampGuard" => 1,

        // 172 StampGuard1

        // 384 StampGuard1+
        "1 Promotional Deal StampGuard" => 1,

        // 764 StampGuard1+

        // 138 StampGuard10
        "65% OFF: 10 StampGuard (Normally $600)" => 10,
        "65% + $10 OFF: 10 StampGuard (Normally $600)" => 10,
        "Best Deal: Buy 3 Get 7 FREE" => 10,
        "Best Deal: Buy 3 Get 7 FREE + $10 OFF" => 10,
        "Best Deal: Buy 3 Get 7 FREE + CA$13 OFF" => 10,
        "Best Deal: Buy 3 Get 7 FREE + AU$13 OFF" => 10,
        "Best Deal: Buy 3 Get 7 FREE + £8 OFF" => 10,

        // 175 StampGuard10

        // 387 StampGuard10+
        "10 Promotional Deal StampGuards" => 10,

        // 134 StampGuard2
        "54% OFF: 2 StampGuard (Normally $120)" => 2,
        "54% + $10 OFF: 2 StampGuard (Normally $120)" => 2,
        "Buy 1 Get 1 FREE (Normally $80)" => 2,
        "Buy 1 Get 1 FREE + $10 OFF" => 2,
        "Buy 1 Get 1 FREE (Normally CA$107)" => 2,
        "Buy 1 Get 1 FREE + CA$13 OFF" => 2,
        "Buy 1 Get 1 FREE (Normally $100)" => 2,
        "Buy 1 Get 1 FREE + AU$13 OFF" => 2,
        "Buy 1 Get 1 FREE (Normally £64)" => 2,
        "Buy 1 Get 1 FREE + £8 OFF" => 2,
        "2 StampGuard Rollers" => 2,

        // 173 StampGuard2

        // 385 StampGuard2+
        "2 Promotional Deal StampGuards" => 2,

        // 159 StampGuard20
        "Community Deal: Buy 5 Get 15 FREE" => 20,
        "Community Deal: Buy 5 Get 15 FREE + $10 OFF" => 20,
        "Community Deal: Buy 5 Get 15 FREE + CA$13 OF" => 20,
        "Community Deal: Buy 5 Get 15 FREE + AU$13 OF" => 20,
        "Community Deal: Buy 5 Get 15 FREE + £8 OFF" => 20,

        // 135 StampGuard3
        "57% OFF: 3 StampGuard (Normally $180)" => 3,
        "57% + $10 OFF: 3 StampGuard (Normally $180)" => 3,

        // 136 StampGuard4
        "60% OFF: 4 StampGuard (Normally $240)" => 4,
        "60% + $10 OFF: 4 StampGuard (Normally $240)" => 4,

        // 137 StampGuard5
        "63% OFF: 5 StampGuard (Normally $300)" => 5,
        "63% + $10 OFF: 5 StampGuard (Normally $300)" => 5,
        "Popular Deal: Buy 2 Get 3 FREE" => 5,
        "Popular Deal: Buy 2 Get 3 FREE + $10 OFF" => 5,
        "Popular Deal: Buy 2 Get 3 FREE + CA$13 OFF" => 5,
        "Popular Deal: Buy 2 Get 3 FREE + AU$13 OFF" => 5,
        "Popular Deal: Buy 2 Get 3 FREE + £8 OFF" => 5,

        // 174 StampGuard5

        // 386 StampGuard5+
        "5 Promotional Deal StampGuards" => 5,
    ],
    "ThermoGrill" => [ // ThermoGrill1,2,3,4,5,10 and +
        // 398 ThermoGrill1
        "50% Discount: 1 ThermoGrill" => 1,
        "50% + $10 OFF: 1 ThermoGrill" => 1,
        "50% + CA$13 OFF: 1 ThermoGrill" => 1,
        "50% + AU$13 OFF: 1 ThermoGrill" => 1,
        "50% + €8 OFF: 1 ThermoGrill" => 1,
        "50% + £8 OFF: 1 ThermoGrill" => 1,
        "1 FREE ThermoGrill" => 1,

        // 450 ThermoGrill1+
        "1x Promo Deal ThermoGrill" => 1,

        // 461 ThermoGrill10

        // 399 ThermoGrill2
        "56% Discount: 2 ThermoGrills" => 2,
        "56% + $10 OFF: 2 ThermoGrills" => 2,
        "56% + CA$13 OFF: 2 ThermoGrills" => 2,
        "56% + AU$13 OFF: 2 ThermoGrills" => 2,
        "56% + €8 OFF: 2 ThermoGrills" => 2,
        "56% + £8 OFF: 2 ThermoGrills" => 2,

        // 451 ThermoGrill2+
        "2x Promo Deal ThermoGrill" => 2,

        // 400 ThermoGrill3
        "58% Discount: 3 ThermoGrills" => 3,
        "58% + $10 OFF: 3 ThermoGrills" => 3,
        "58% + CA$13 OFF: 3 ThermoGrills" => 3,
        "58% + AU$13 OFF: 3 ThermoGrills" => 3,
        "58% + €8 OFF: 3 ThermoGrills" => 3,
        "58% + £8 OFF: 3 ThermoGrills" => 3,

        // 452 ThermoGrill3+
        "3x Promo Deal ThermoGrill" => 3,

        // 401 ThermoGrill4
        "60% Discount: 4 ThermoGrills" => 4,
        "60% + $10 OFF: 4 ThermoGrills" => 4,
        "60% + CA$13 OFF: 4 ThermoGrills" => 4,
        "60% + AU$13 OFF: 4 ThermoGrills" => 4,
        "60% + €8 OFF: 4 ThermoGrills" => 4,
        "60% + £8 OFF: 4 ThermoGrills" => 4,

        // 453 ThermoGrill4+
        "4x Promo Deal ThermoGrill" => 4,

        // 460 ThermoGrill5
    ],
    "Toilet Cleaner" => [ // ToiletBowlSparkle1,2,3,4 and +
        // 1280 ToiletBowlSparkle1
        "1 Pack of BowlSparkle - 50% OFF" => 1,
        "1 Pack of BowlSparkle - 50% + $5 OFF" => 1,

        // 1610 ToiletBowlSparkle1+
        "1 Pack of BowlSparkle Tablets" => 1,
        "1 Promo Deal BowlSparkle" => 1,

        // 1281 ToiletBowlSparkle2
        "2 Packs of BowlSparkle - 56% OFF" => 2,
        "2 Packs of BowlSparkle - 56% + $5 OFF" => 2,

        // 1611 ToiletBowlSparkle2+
        "2 Packs of BowlSparkle Tablets" => 2,
        "2 Promo Deal BowlSparkles" => 2,

        // 1282 ToiletBowlSparkle3
        "3 Packs of BowlSparkle - 58% OFF" => 3,
        "3 Packs of BowlSparkle - 58% + $5 OFF" => 3,

        // 1612 ToiletBowlSparkle3+
        "3 Packs of BowlSparkle Tablets" => 3,
        "3 Promo Deal BowlSparkles" => 3,

        // 1283 ToiletBowlSparkle4
        "4 Packs of BowlSparkle - 60% OFF" => 4,
        "4 Packs of BowlSparkle - 60% + $5 OFF" => 4,

        // 1613 ToiletBowlSparkle4+
        "4 Packs of BowlSparkle Tablets" => 4,
        "4 Promo Deal BowlSparkles" => 4,
    ],
    "Posture Corrector" => [ // TrueFit1,2,3,4 and +
        // 1284 TrueFit1
        "1 TrueFit Posture Corrector - 50% OFF" => 1,
        "1 TrueFit Posture Corrector - 50% + $5 OFF" => 1,
        "1 TrueFit Posture Corrector - 50% + CA$7 OFF" => 1,

        // 1321 TrueFit2
        "1 Promo Deal TrueFit" => 1,

        // 1285 TrueFit2
        "2 TrueFit Posture Correctors - 56% OFF" => 2,
        "2 TrueFit Posture Correctors - 56% + $5 OFF" => 2,
        "2 TrueFit Posture Correctors - 56% + CA$7 OFF" => 2,

        // 1322 TrueFit2+
        "2 Promo Deal TrueFits" => 2,

        // 1286 TrueFit3
        "3 TrueFit Posture Correctors - 58% OFF" => 3,
        "3 TrueFit Posture Correctors - 58% + $5 OFF" => 3,
        "3 TrueFit Posture Correctors - 58% + CA$7 OFF" => 3,

        // 1323 TrueFit3+
        "3 Promo Deal TrueFits" => 3,

        // 1287 TrueFit4
        "4 TrueFit Posture Correctors - 60% OFF" => 4,
        "4 TrueFit Posture Correctors - 60% + $5 OFF" => 4,
        "4 TrueFit Posture Correctors - 60% + CA$7 OFF" => 4,

        // 1324 TrueFit4+
        "4 Promo Deal TrueFits" => 4,
    ],
    "WalletShield" => [ // WalletShield1,2,3,4,5,10 and +
        // 308 WalletShield1
        "1 WalletShield Card (50% OFF)" => 1,
        "1 WalletShield Card (50% + $5 OFF)" => 1,
        "1 WalletShield Card" => 1,
        "1 WalletShield Card + CA$7 OFF" => 1,
        "1 WalletShield Card + AUD$7 OFF" => 1,
        "1 WalletShield Card + €4 OFF" => 1,
        "1 WalletShield Card + £4 OFF" => 1,
        "Free Gift: WalletShield" => 1,
        "1 FREE WalletShield" => 1,
        "1 FREE WalletShield Protector" => 1,
        "1 FREE Wallet Shield" => 1,
        "1 Free WalletShield" => 1,

        // 338 WalletShield1+
        "1x Promo Card Protector" => 1,

        // 345 WalletShield10
        "Community Deal: 10 Cards (70% OFF)" => 10,
        "Community Deal: 10 Cards (70% + $5 OFF)" => 10,
        "Community Deal: 10 WalletShield Cards" => 10,
        "Community Deal: 10 Cards + CA$7 OFF" => 10,
        "Community Deal: 10 Cards + AUD$7 OFF" => 10,
        "Community Deal: 10 Cards + €4 OFF" => 10,
        "Community Deal: 10 Cards + £4 OFF" => 10,

        // 541 WalletShield10+
        "10x Promo Card Protectors" => 10,

        // 309 WalletShield2
        "2 WalletShield Cards (56% OFF)" => 2,
        "2 WalletShield Cards (56% + $5 OFF)" => 2,
        "2 WalletShield Cards" => 2,
        "2 WalletShield Cards + CA$7 OFF" => 2,
        "2 WalletShield Cards + AUD$7 OFF" => 2,
        "2 WalletShield Cards + €4 OFF" => 2,
        "2 WalletShield Cards + £4 OFF" => 2,

        // 339 WalletShield2+
        "2x Promo Card Protectors" => 2,

        // 346 WalletShield20

        // 310 WalletShield3
        "3 WalletShield Cards (58% OFF)" => 3,
        "3 WalletShield Cards (58% + $5 OFF)" => 3,
        "3 WalletShield Cards" => 3,
        "3 WalletShield Cards + CA$7 OFF" => 3,
        "3 WalletShield Cards + AUD$7 OFF" => 3,
        "3 WalletShield Cards + €4 OFF" => 3,
        "3 WalletShield Cards + £4 OFF" => 3,

        // 345 WalletShield3+
        "3x Promo Card Protectors" => 3,

        // 311 WalletShield4
        "4 WalletShield Cards (60% OFF)" => 4,
        "4 WalletShield Cards (60% + $5 OFF)" => 4,
        "4 WalletShield Cards" => 4,
        "4 WalletShield Cards + CA$7 OFF" => 4,
        "4 WalletShield Cards + AUD$7 OFF" => 4,
        "4 WalletShield Cards + €4 OFF" => 4,
        "4 WalletShield Cards + £4 OFF" => 4,

        // 341 WalletShield4+
        "4x Promo Card Protectors" => 4,

        // 344 WalletShield5
        "5 WalletShield Cards (62% OFF)" => 5,
        "5 WalletShield Cards (62% + $5 OFF)" => 5,
        "5 WalletShield Cards" => 5,
        "5 WalletShield Cards + CA$7 OFF" => 5,
        "5 WalletShield Cards + AUD$7 OFF" => 5,
        "5 WalletShield Cards + €4 OFF" => 5,
        "5 WalletShield Cards + £4 OFF" => 5,

        // 342 WalletShield5+
        "5x Promo Card Protectors" => 5,
    ],
    "FinallyFresh Washing Machine Cleaner" => [ // WashCleaner1,2,3,4,5,6,8,10 and +
        // 517 WashCleaner1
        "1 Pack Of FinallyFresh - 6 Tablets" => 1,
        "1 Pack of FinallyFresh (Bonus 20% OFF)" => 1,

        // 537 WashCleaner1+
        "1x Promo Deal FinallyFresh" => 1,

        // 1479 WashCleaner10
        "Buy 5 Packs Of FinallyFresh Get 5 FREE!" => 10,
        "Buy 5 Packs Get 5 FREE! (Bonus 15% OFF)" => 10,

        // 518 WashCleaner2
        "2 Packs Of FinallyFresh (Extra 5% OFF)" => 2,
        "2 Packs of FinallyFresh (5% + Bonus 20% OFF)" => 2,

        // 538 WashCleaner2+
        "2x Promo Deal FinallyFresh" => 2,

        // 519 WashCleaner3
        "3 Packs Of FinallyFresh (Extra 10% OFF)" => 3,
        "3 Packs of FinallyFresh (10% + Bonus 20% OFF)" => 3,

        // 539 WashCleaner3+
        "3x Promo Deal FinallyFresh" => 3,

        // 520 WashCleaner4
        "4 Packs Of FinallyFresh (Extra 15% OFF)" => 4,
        "4 Packs of FinallyFresh (15% + Bonus 20% OFF)" => 4,

        // 540 WashCleaner4+
        "4x Promo Deal FinallyFresh" => 4,

        // 1476 WashCleaner5
        "Buy 3 Packs Of FinallyFresh Get 2 FREE!" => 5,
        "Buy 3 Packs Get 2 FREE! (Bonus 15% OFF)" => 5,

        // 1477 WashCleaner6
        "Buy 4 Packs Of FinallyFresh Get 2 FREE!" => 6,
        "Buy 4 Packs Get 2 FREE! (Bonus 15% OFF)" => 6,

        // 1478 WashCleaner8
        "Buy 5 Packs Of FinallyFresh Get 3 FREE!" => 8,
        "Buy 5 Packs Get 3 FREE! (Bonus 15% OFF)" => 8,
    ],
    "WifiBlast" => [ // (AU/EU/UK)WifiBlast1,2,3,4 and +
        // 886 AUWifiBlast1
        // 909 UKWifiBlast1
        // 901 EUWifiBlast1
        // 546 WifiBlast1
        "1 WifiBlast Extender" => 1,
        "1 WifiBlast Extender + $10 OFF!" => 1,
        "1 WifiBlast Extender + AU$13 OFF!" => 1,
        "1 WifiBlast Extender + £8 OFF!" => 1,
        "1 WifiBlast Extender + €8 OFF!" => 1,
        "1 WifiBlast Extender + CA$13 OFF!" => 1,
        "1 WifiBlast Extender - 50% OFF" => 1,
        "1 WiFiFlare Extender" => 1,
        "1 WiFiFlare Extender + $10 OFF!" => 1,
        "1 WiFiFlare Extender + CA$13 OFF!" => 1,
        "1 WiFiBurst Extender" => 1,
        "1 WiFiBurst Extender + $10 OFF!" => 1,
        "1 WiFiBurst Extender + AU$13 OFF!" => 1,
        "1 WiFiBurst Extender + CA$13 OFF!" => 1,
        "1 WiFiBurst Extender + €8 OFF!" => 1,
        "1 WiFiBurst Extender + £8 OFF!" => 1,

        // 890 AUWifiBlast1+
        // 905 UKWifiBlast1+
        // 916 EUWifiBlast1+
        // 564 WifiBlast1+
        "1x Promo Deal WiFiBlast" => 1,
        "1x Promo Deal WiFiFlare" => 1,
        "1x Promo Deal WiFiBurst" => 1,

        // 887 AUWifiBlast2
        // 910 UKWifiBlast2
        // 902 EUWifiBlast2
        // 547 WifiBlast2
        "2 WifiBlast Extenders" => 2,
        "2 WifiBlast Extenders + $10 OFF!" => 2,
        "2 WifiBlast Extenders + CA$13 OFF!" => 2,
        "2 WifiBlast Extenders + £8 OFF!" => 2,
        "2 WifiBlast Extenders + €8 OFF!" => 2,
        "2 WiFiFlare Extenders" => 2,
        "2 WiFiFlare Extenders + $10 OFF!" => 2,
        "2 WiFiFlare Extenders + CA$13 OFF!" => 2,
        "2 WiFiBurst Extenders" => 2,
        "2 WiFiBurst Extenders + $10 OFF!" => 2,
        "2 WiFiBurst Extenders + AU$13 OFF!" => 2,
        "2 WiFiBurst Extenders + CA$13 OFF!" => 2,
        "2 WiFiBurst Extenders + €8 OFF!" => 2,
        "2 WiFiBurst Extenders + £8 OFF!" => 2,

        // 891 AUWifiBlast2+
        // 915 UKWifiBlast2+
        // 906 EUWifiBlast2+
        // 565 WifiBlast2+
        "2x Promo Deal WiFiBlast" => 2,
        "2x Promo Deal WiFiFlare" => 2,
        "2x Promo Deal WiFiBurst" => 2,

        // 888 AUWifiBlast3
        // 903 EUWifiBlast3
        // 911 UKWifiBlast3
        // 548 WifiBlast3
        "3 WifiBlast Extenders" => 3,
        "3 WifiBlast Extenders + $10 OFF!" => 3,
        "3 WifiBlast Extenders + AU$13 OFF!" => 3,
        "3 WifiBlast Extenders + £8 OFF!" => 3,
        "3 WifiBlast Extenders + €8 OFF!" => 3,
        "3 WifiBlast Extenders + CA$13 OFF!" => 3,
        "3 WiFiFlare Extenders" => 3,
        "3 WiFiFlare Extenders + $10 OFF!" => 3,
        "3 WiFiFlare Extenders + CA$13 OFF!" => 3,
        "3 WiFiBurst Extenders" => 3,
        "3 WiFiBurst Extenders + $10 OFF!" => 3,
        "3 WiFiBurst Extenders + AU$13 OFF!" => 3,
        "3 WiFiBurst Extenders + CA$13 OFF!" => 3,
        "3 WiFiBurst Extenders + €8 OFF!" => 3,
        "3 WiFiBurst Extenders + £8 OFF!" => 3,

        // 892 AUWifiBlast3+
        // 907 EUWifiBlast3+
        // 914 UKWifiBlast3+
        // 566 WifiBlast3+
        "892" => 3,
        "3x Promo Deal WiFiBlast" => 3,
        "3x Promo Deal WiFiBurst" => 3,

        // 889 AUWifiBlast4
        // 904 EUWifiBlast4
        // 912 UKWifiBlast4
        // 549 WifiBlast4
        "4 WifiBlast Extenders" => 4,
        "4 WifiBlast Extenders + $10 OFF!" => 4,
        "4 WifiBlast Extenders + AU$13 OFF!" => 4,
        "4 WifiBlast Extenders + £8 OFF!" => 4,
        "4 WifiBlast Extenders + €8 OFF!" => 4,
        "4 WifiBlast Extenders + CA$13 OFF!" => 4,
        "4 WiFiFlare Extenders" => 4,
        "4 WiFiFlare Extenders + $10 OFF!" => 4,
        "4 WiFiFlare Extenders + CA$13 OFF!" => 4,
        "4 WiFiBurst Extenders" => 4,
        "4 WiFiBurst Extenders + $10 OFF!" => 4,
        "4 WiFiBurst Extenders + AU$13 OFF!" => 4,
        "4 WiFiBurst Extenders + CA$13 OFF!" => 4,
        "4 WiFiBurst Extenders + €8 OFF!" => 4,
        "4 WiFiBurst Extenders + £8 OFF!" => 4,

        // 893 AUWifiBlast4+
        // 908 EUWifiBlast4+
        // 913 UKWifiBlast4+
        // 567 WifiBlast4+
        "4x Promo Deal WiFiBlast" => 4,
        "4x Promo Deal WiFiBurst" => 4,

        // 1567
        "5 WiFiBlast Extenders" => 5,
    ],
    "Cloud 1 Shoes" => [ // ShoesMen(Women)(8,9,19,11,12,13)White(Black)1,2,3 and +
        // Men
        // 1114 ShoesMen8Black1
        // 1117 ShoesMen9Black1
        // 1120 ShoesMen10Black1
        // 1123 ShoesMen11Black1
        // 1126 ShoesMen12Black1
        // 1129 ShoesMen13Black1
        "1 Black Cloud1 Shoes (Mens Size 8)" => 1,
        "1 Black Cloud1 Shoes (Mens Size 9)" => 1,
        "1 Black Cloud1 Shoes (Mens Size 10)" => 1,
        "1 Black Cloud1 Shoes (Mens Size 11)" => 1,
        "1 Black Cloud1 Shoes (Mens Size 12)" => 1,
        "1 Black Cloud1 Shoes (Mens Size 13)" => 1,
        "1 Black Cloud1 Shoes (Mens Size 8) - $10 OFF" => 1,
        "1 Black Cloud1 Shoes (Mens Size 9) - $10 OFF" => 1,
        "1 Black Cloud1 Shoes (Mens Size 10) - $10 OFF" => 1,
        "1 Black Cloud1 Shoes (Mens Size 11) - $10 OFF" => 1,
        "1 Black Cloud1 Shoes (Mens Size 12) - $10 OFF" => 1,
        "1 Black Cloud1 Shoes (Mens Size 13) - $10 OFF" => 1,

        // 1620 ShoesMen8Black1+
        // 1621 ShoesMen9Black1+
        // 1622 ShoesMen10Black1+
        // 1623 ShoesMen11Black1+
        // 1624 ShoesMen12Black1+
        // 1625 ShoesMen13Black1+
        "Extra Pair of Black Cloud1s (Mens Size 8)" => 1,
        "Extra Pair of Black Cloud1s (Mens Size 9)" => 1,
        "Extra Pair of Black Cloud1s (Mens Size 10)" => 1,
        "Extra Pair of Black Cloud1s (Mens Size 11)" => 1,
        "Extra Pair of Black Cloud1s (Mens Size 12)" => 1,
        "Extra Pair of Black Cloud1s (Mens Size 13)" => 1,

        // 1115 ShoesMen8Black2
        // 1118 ShoesMen9Black2
        // 1121 ShoesMen10Black2
        // 1124 ShoesMen11Black2
        // 1127 ShoesMen12Black2
        // 1130 ShoesMen13Black2
        "2 Black Cloud1 Shoes (Mens Size 8)" => 2,
        "2 Black Cloud1 Shoes (Mens Size 9)" => 2,
        "2 Black Cloud1 Shoes (Mens Size 10)" => 2,
        "2 Black Cloud1 Shoes (Mens Size 11)" => 2,
        "2 Black Cloud1 Shoes (Mens Size 12)" => 2,
        "2 Black Cloud1 Shoes (Mens Size 13)" => 2,
        "2 Black Cloud1 Shoes (Mens Size 8) - $10 OFF" => 2,
        "2 Black Cloud1 Shoes (Mens Size 9) - $10 OFF" => 2,
        "2 Black Cloud1 Shoes (Mens Size 10) - $10 OFF" => 2,
        "2 Black Cloud1 Shoes (Mens Size 11) - $10 OFF" => 2,
        "2 Black Cloud1 Shoes (Mens Size 12) - $10 OFF" => 2,
        "2 Black Cloud1 Shoes (Mens Size 13) - $10 OFF" => 2,

        // 1116 ShoesMen8Black3
        // 1119 ShoesMen9Black3
        // 1122 ShoesMen10Black3
        // 1125 ShoesMen11Black3
        // 1129 ShoesMen12Black3
        // 1131 ShoesMen13Black3
        "3 Black Cloud1 Shoes (Mens Size 8)" => 3,
        "3 Black Cloud1 Shoes (Mens Size 9)" => 3,
        "3 Black Cloud1 Shoes (Mens Size 10)" => 3,
        "3 Black Cloud1 Shoes (Mens Size 11)" => 3,
        "3 Black Cloud1 Shoes (Mens Size 12)" => 3,
        "3 Black Cloud1 Shoes (Mens Size 13)" => 3,
        "3 Black Cloud1 Shoes (Mens Size 8) - $10 OFF" => 3,
        "3 Black Cloud1 Shoes (Mens Size 9) - $10 OFF" => 3,
        "3 Black Cloud1 Shoes (Mens Size 10) - $10 OFF" => 3,
        "3 Black Cloud1 Shoes (Mens Size 11) - $10 OFF" => 3,
        "3 Black Cloud1 Shoes (Mens Size 12) - $10 OFF" => 3,
        "3 Black Cloud1 Shoes (Mens Size 13) - $10 OFF" => 3,

        // 1096 ShoesMen8White1
        // 1099 ShoesMen9White1
        // 1102 ShoesMen10White1
        // 1105 ShoesMen11White1
        // 1108 ShoesMen12White1
        // 1111 ShoesMen13White1
        "1 White Cloud1 Shoes (Mens Size 8)" => 1,
        "1 White Cloud1 Shoes (Mens Size 9)" => 1,
        "1 White Cloud1 Shoes (Mens Size 10)" => 1,
        "1 White Cloud1 Shoes (Mens Size 11)" => 1,
        "1 White Cloud1 Shoes (Mens Size 12)" => 1,
        "1 White Cloud1 Shoes (Mens Size 13)" => 1,
        "1 White Cloud1 Shoes (Mens Size 8) - $10 OFF" => 1,
        "1 White Cloud1 Shoes (Mens Size 9) - $10 OFF" => 1,
        "1 White Cloud1 Shoes (Mens Size 10) - $10 OFF" => 1,
        "1 White Cloud1 Shoes (Mens Size 11) - $10 OFF" => 1,
        "1 White Cloud1 Shoes (Mens Size 12) - $10 OFF" => 1,
        "1 White Cloud1 Shoes (Mens Size 13) - $10 OFF" => 1,

        // 1614 ShoesMen8White1+
        // 1615 ShoesMen9White1+
        // 1616 ShoesMen10White1+
        // 1617 ShoesMen11White1+
        // 1618 ShoesMen12White1+
        // 1619 ShoesMen13White1+
        "Extra Pair of White Cloud1s (Mens Size 8)" => 1,
        "Extra Pair of White Cloud1s (Mens Size 9)" => 1,
        "Extra Pair of White Cloud1s (Mens Size 10)" => 1,
        "Extra Pair of White Cloud1s (Mens Size 11)" => 1,
        "Extra Pair of White Cloud1s (Mens Size 12)" => 1,
        "Extra Pair of White Cloud1s (Mens Size 13)" => 1,

        // 1097 ShoesMen8White2
        // 1100 ShoesMen9White2
        // 1103 ShoesMen10White2
        // 1106 ShoesMen11White2
        // 1109 ShoesMen12White2
        // 1112 ShoesMen13White2
        "2 White Cloud1 Shoes (Mens Size 8)" => 2,
        "2 White Cloud1 Shoes (Mens Size 9)" => 2,
        "2 White Cloud1 Shoes (Mens Size 10)" => 2,
        "2 White Cloud1 Shoes (Mens Size 11)" => 2,
        "2 White Cloud1 Shoes (Mens Size 12)" => 2,
        "2 White Cloud1 Shoes (Mens Size 13)" => 2,
        "2 White Cloud1 Shoes (Mens Size 8) - $10 OFF" => 2,
        "2 White Cloud1 Shoes (Mens Size 9) - $10 OFF" => 2,
        "2 White Cloud1 Shoes (Mens Size 10) - $10 OFF" => 2,
        "2 White Cloud1 Shoes (Mens Size 11) - $10 OFF" => 2,
        "2 White Cloud1 Shoes (Mens Size 12) - $10 OFF" => 2,
        "2 White Cloud1 Shoes (Mens Size 13) - $10 OFF" => 2,

        // 1098 ShoesMen8White3
        // 1101 ShoesMen9White3
        // 1104 ShoesMen10White3
        // 1107 ShoesMen11White3
        // 1110 ShoesMen12White3
        // 1113 ShoesMen13White3
        "3 White Cloud1 Shoes (Mens Size 8)" => 3,
        "3 White Cloud1 Shoes (Mens Size 9)" => 3,
        "3 White Cloud1 Shoes (Mens Size 10)" => 3,
        "3 White Cloud1 Shoes (Mens Size 11)" => 3,
        "3 White Cloud1 Shoes (Mens Size 12)" => 3,
        "3 White Cloud1 Shoes (Mens Size 13)" => 3,
        "3 White Cloud1 Shoes (Mens Size 8) - $10 OFF" => 3,
        "3 White Cloud1 Shoes (Mens Size 9) - $10 OFF" => 3,
        "3 White Cloud1 Shoes (Mens Size 10) - $10 OFF" => 3,
        "3 White Cloud1 Shoes (Mens Size 11) - $10 OFF" => 3,
        "3 White Cloud1 Shoes (Mens Size 12) - $10 OFF" => 3,
        "3 White Cloud1 Shoes (Mens Size 13) - $10 OFF" => 3,

        // Women
        // 1168 ShoesWomen5Black1
        // 1171 ShoesWomen6Black1
        // 1174 ShoesWomen7Black1
        // 1177 ShoesWomen8Black1
        // 1180 ShoesWomen9Black1
        // 1183 ShoesWomen10Black1
        "1 Black Cloud1 Shoes (Womens Size 5)" => 1,
        "1 Black Cloud1 Shoes (Womens Size 6)" => 1,
        "1 Black Cloud1 Shoes (Womens Size 7)" => 1,
        "1 Black Cloud1 Shoes (Womens Size 8)" => 1,
        "1 Black Cloud1 Shoes (Womens Size 9)" => 1,
        "1 Black Cloud1 Shoes (Womens Size 10)" => 1,
        "1 Black Cloud1s (Womens Size 5) - $10 OFF" => 1,
        "1 Black Cloud1s (Womens Size 6) - $10 OFF" => 1,
        "1 Black Cloud1s (Womens Size 7) - $10 OFF" => 1,
        "1 Black Cloud1s (Womens Size 8) - $10 OFF" => 1,
        "1 Black Cloud1s (Womens Size 9) - $10 OFF" => 1,
        "1 Black Cloud1s (Womens Size 10) - $10 OFF" => 1,

        // 1632 ShoesWomen5Black1+
        // 1633 ShoesWomen6Black1+
        // 1634 ShoesWomen7Black1+
        // 1635 ShoesWomen8Black1+
        // 1636 ShoesWomen9Black1+
        // 1637 ShoesWomen10Black1+
        "Extra Pair of Black Cloud1s (Womens Size 5)" => 1,
        "Extra Pair of Black Cloud1s (Womens Size 6)" => 1,
        "Extra Pair of Black Cloud1s (Womens Size 7)" => 1,
        "Extra Pair of Black Cloud1s (Womens Size 8)" => 1,
        "Extra Pair of Black Cloud1s (Womens Size 9)" => 1,
        "Extra Pair of Black Cloud1s (Womens Size 10)" => 1,

        // 1169 ShoesWomen5Black2
        // 1172 ShoesWomen6Black2
        // 1177 ShoesWomen7Black2
        // 1178 ShoesWomen8Black2
        // 1181 ShoesWomen9Black2
        // 1184 ShoesWomen10Black2
        "2 Black Cloud1 Shoes (Womens Size 5)" => 2,
        "2 Black Cloud1 Shoes (Womens Size 6)" => 2,
        "2 Black Cloud1 Shoes (Womens Size 7)" => 2,
        "2 Black Cloud1 Shoes (Womens Size 8)" => 2,
        "2 Black Cloud1 Shoes (Womens Size 9)" => 2,
        "2 Black Cloud1 Shoes (Womens Size 10)" => 2,
        "2 Black Cloud1s (Womens Size 5) - $10 OFF" => 2,
        "2 Black Cloud1s (Womens Size 6) - $10 OFF" => 2,
        "2 Black Cloud1s (Womens Size 7) - $10 OFF" => 2,
        "2 Black Cloud1s (Womens Size 8) - $10 OFF" => 2,
        "2 Black Cloud1s (Womens Size 9) - $10 OFF" => 2,
        "2 Black Cloud1s (Womens Size 10) - $10 OFF" => 2,


        // 1170 ShoesWomen5Black3
        // 1173 ShoesWomen6Black3
        // 1178 ShoesWomen7Black3
        // 1179 ShoesWomen8Black3
        // 1180 ShoesWomen9Black3
        // 1185 ShoesWomen10Black3
        "3 Black Cloud1 Shoes (Womens Size 5)" => 3,
        "3 Black Cloud1 Shoes (Womens Size 6)" => 3,
        "3 Black Cloud1 Shoes (Womens Size 7)" => 3,
        "3 Black Cloud1 Shoes (Womens Size 8)" => 3,
        "3 Black Cloud1 Shoes (Womens Size 9)" => 3,
        "3 Black Cloud1 Shoes (Womens Size 10)" => 3,
        "3 Black Cloud1s (Womens Size 5) - $10 OFF" => 3,
        "3 Black Cloud1s (Womens Size 6) - $10 OFF" => 3,
        "3 Black Cloud1s (Womens Size 7) - $10 OFF" => 3,
        "3 Black Cloud1s (Womens Size 8) - $10 OFF" => 3,
        "3 Black Cloud1s (Womens Size 9) - $10 OFF" => 3,
        "3 Black Cloud1s (Womens Size 10) - $10 OFF" => 3,

        // 1150 ShoesWomen5White1
        // 1153 ShoesWomen6White1
        // 1156 ShoesWomen7White1
        // 1159 ShoesWomen8White1
        // 1162 ShoesWomen9White1
        // 1165 ShoesWomen10White1
        "1 White Cloud1 Shoes (Womens Size 5)" => 1,
        "1 White Cloud1 Shoes (Womens Size 6)" => 1,
        "1 White Cloud1 Shoes (Womens Size 7)" => 1,
        "1 White Cloud1 Shoes (Womens Size 8)" => 1,
        "1 White Cloud1 Shoes (Womens Size 9)" => 1,
        "1 White Cloud1 Shoes (Womens Size 10)" => 1,
        "1 White Cloud1s (Womens Size 5) - $5 OFF" => 1,
        "1 White Cloud1s (Womens Size 6) - $5 OFF" => 1,
        "1 White Cloud1s (Womens Size 7) - $5 OFF" => 1,
        "1 White Cloud1s (Womens Size 8) - $5 OFF" => 1,
        "1 White Cloud1s (Womens Size 9) - $5 OFF" => 1,
        "1 White Cloud1s (Womens Size 10) - $5 OFF" => 1,

        // 1626 ShoesWomen5White1+
        // 1627 ShoesWomen6White1+
        // 1628 ShoesWomen7White1+
        // 1629 ShoesWomen8White1+
        // 1630 ShoesWomen9White1+
        // 1631 ShoesWomen10White1+
        "Extra Pair of White Cloud1s (Womens Size 5)" => 1,
        "Extra Pair of White Cloud1s (Womens Size 6)" => 1,
        "Extra Pair of White Cloud1s (Womens Size 7)" => 1,
        "Extra Pair of White Cloud1s (Womens Size 8)" => 1,
        "Extra Pair of White Cloud1s (Womens Size 9)" => 1,
        "Extra Pair of White Cloud1s (Womens Size 10)" => 1,

        // 1151 ShoesWomen5White2
        // 1154 ShoesWomen6White2
        // 1157 ShoesWomen7White2
        // 1160 ShoesWomen8White2
        // 1163 ShoesWomen9White2
        // 1166 ShoesWomen10White2
        "2 White Cloud1 Shoes (Womens Size 5)" => 2,
        "2 White Cloud1 Shoes (Womens Size 6)" => 2,
        "2 White Cloud1 Shoes (Womens Size 7)" => 2,
        "2 White Cloud1 Shoes (Womens Size 8)" => 2,
        "2 White Cloud1 Shoes (Womens Size 9)" => 2,
        "2 White Cloud1 Shoes (Womens Size 10)" => 2,
        "2 White Cloud1s (Womens Size 5) - $10 OFF" => 2,
        "2 White Cloud1s (Womens Size 6) - $10 OFF" => 2,
        "2 White Cloud1s (Womens Size 7) - $10 OFF" => 2,
        "2 White Cloud1s (Womens Size 8) - $10 OFF" => 2,
        "2 White Cloud1s (Womens Size 9) - $10 OFF" => 2,
        "2 White Cloud1s (Womens Size 10) - $10 OFF" => 2,

        // 1152 ShoesWomen5White3
        // 1155 ShoesWomen6White3
        // 1158 ShoesWomen7White3
        // 1161 ShoesWomen8White3
        // 1164 ShoesWomen9White3
        // 1167 ShoesWomen10White3
        "3 White Cloud1 Shoes (Womens Size 5)" => 3,
        "3 White Cloud1 Shoes (Womens Size 6)" => 3,
        "3 White Cloud1 Shoes (Womens Size 7)" => 3,
        "3 White Cloud1 Shoes (Womens Size 8)" => 3,
        "3 White Cloud1 Shoes (Womens Size 9)" => 3,
        "3 White Cloud1 Shoes (Womens Size 10)" => 3,
        "3 White Cloud1s (Womens Size 5) - $10 OFF" => 3,
        "3 White Cloud1s (Womens Size 6) - $10 OFF" => 3,
        "3 White Cloud1s (Womens Size 7) - $10 OFF" => 3,
        "3 White Cloud1s (Womens Size 8) - $10 OFF" => 3,
        "3 White Cloud1s (Womens Size 9) - $10 OFF" => 3,
        "3 White Cloud1s (Womens Size 10) - $10 OFF" => 3,
    ],
];
