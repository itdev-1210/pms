<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css" />


    </head>
    <body class="app app sidebar-show  footer-fixed">
        <div id="root"></div>
        <script src="{{ asset("/js/employee/index.js") }}"></script>
        @auth
        <script>
            var _registration = null;
            function registerServiceWorker() {
              return navigator.serviceWorker.register('js/service-worker.js')
              .then(function(registration) {
                console.log('Service worker successfully registered.');
                _registration = registration;
                return registration;
              })
              .catch(function(err) {
                console.error('Unable to register service worker.', err);
              });
            }

            function askPermission() {
              return new Promise(function(resolve, reject) {
                const permissionResult = Notification.requestPermission(function(result) {
                  resolve(result);
                });

                if (permissionResult) {
                  permissionResult.then(resolve, reject);
                }
              })
              .then(function(permissionResult) {
                if (permissionResult !== 'granted') {
                  throw new Error('We weren\'t granted permission.');
                }
                else{
                  subscribeUserToPush();
                }
              });
            }

            function urlBase64ToUint8Array(base64String) {
              const padding = '='.repeat((4 - base64String.length % 4) % 4);
              const base64 = (base64String + padding)
                .replace(/\-/g, '+')
                .replace(/_/g, '/');

              const rawData = window.atob(base64);
              const outputArray = new Uint8Array(rawData.length);

              for (let i = 0; i < rawData.length; ++i) {
                outputArray[i] = rawData.charCodeAt(i);
              }
              return outputArray;
            }

            function getSWRegistration(){
              var promise = new Promise(function(resolve, reject) {
              // do a thing, possibly async, then…

              if (_registration != null) {
                resolve(_registration);
              }
              else {
                reject(Error("It broke"));
              }
              });
              return promise;
            }

            function subscribeUserToPush() {
              getSWRegistration()
              .then(function(registration) {
                console.log(registration);
                const subscribeOptions = {
                  userVisibleOnly: true,
                  applicationServerKey: urlBase64ToUint8Array(
                    "{{env('VAPID_PUBLIC_KEY')}}"
                  )
                };

                return registration.pushManager.subscribe(subscribeOptions);
              })
              .then(function(pushSubscription) {
                console.log('Received PushSubscription: ', JSON.stringify(pushSubscription));
                sendSubscriptionToBackEnd(pushSubscription);
                return pushSubscription;
              });
            }

            function sendSubscriptionToBackEnd(subscription) {
                const token = document.querySelector('meta[name=csrf-token]').getAttribute('content');
              return fetch('/save-subscription', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'X-CSRF-Token': token
                },
                body: JSON.stringify(subscription)
              })
              .then(function(response) {
                if (!response.ok) {
                  throw new Error('Bad status code from server.');
                }

                return response.json();
              })
              .then(function(responseData) {
                if (!(responseData && responseData.success)) {
                  throw new Error('Bad response from server.');
                }
              });
            }

            registerServiceWorker();
            askPermission();
        </script>
        @endauth
    </body>
</html>
