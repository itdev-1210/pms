<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee;
class EmployeeController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest_manager');
    }

    /* Get Manager Detail */
     public function get($id){
        return Employee::find($id)->toArray();
    }
    /* Load Employees */
    public function loademployees(){
        $employees = Employee::all()->toArray();
        $i = 0;
        foreach ($employees as $key => $employee) {
            $employees[$key]['number'] = ++$i;
            # code...
        }
        return $employees;
    }
    /* Register new manager */
    public function register(Request $request){
        /* If there is manager who have same email */
        $isSameEmailManager = Employee::where('email', $request->email)->first();
        if($isSameEmailManager){
            return array('status' => 'error', 'message' => "Email already has been taken!");
        }
        /* If there is manager who have same username */
        $isSameNamelManager = Employee::where('email', $request->name)->first();
        if($isSameNamelManager){
            return array('status' => 'error', 'message' => "Username already has been taken!");
        }


        /* Register new manager */
        $employee = new Employee;
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->name = $request->username;
        $employee->email = $request->email;
        $employee->password = bcrypt($request->password);
        $employee->profile_image = 'default.png';
        $employee->is_approved = 1;
        $employee->save();
        return array(
            'status' => 'success',
            'data' => $employee
        );
    }

    /* Update Employee Detail */
    public function update(Request $request){
        $employee = Employee::find($request->id);
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->email = $request->email;
        $employee->name = $request->name;
        $employee->save();
        return array(
            'status' => 'success',
            'message' => 'Updated successfully!'
        );
    }

    /* Remove Employee */
    public function remove($id){
        $employee = Employee::find($id);
        $employee->delete();
        return array(
            'status' => 'success',
            'message' => 'Removed successfully!'
        );
    }
}
