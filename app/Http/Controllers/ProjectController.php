<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\ProjectPart;
use App\Models\ProjectEmployeeAssignment;
use App\Models\EmployeeChecklistNote;
use App\TaskNotification;
use App\Reminder;
use App\AlarmNotification;
use Auth;
use App\Http\Controllers\Resuables\ProjectTrait;
use App\Notifications\PushDemo;
use App\Models\Admin;
use App\Models\Employee;
use App\Models\Manager;
use App\Models\Task;
use App\RelationTask;

use DB;

use Illuminate\Support\Carbon;

class ProjectController extends Controller
{
    use ProjectTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    const PROJECT_SORT = 0;
    const TASK_SORT = 1;

    public function __construct()
    {
        $this->middleware('guest')->except(
            'removenote',
            'finishTrigger',
            'moveTask',
            'removeNotification',
            'multiMoveTask',
            'sortTask',
            'getLowerProject',
            'getOrderMoreProject',
            'getCategoryTasks',
            'getRefreshTasks',
            'getAvailableRealtionTasks',
            'setRelationTasks');
    }

    public function getAvailableRealtionTasks(Request $request) {
        $task = Task::with(['relationTasks', 'checklist'])->findOrFail($request->id);
        $relationTasks = [];
        if ($task->relationTasks)
            $relationTasks = explode(',', $task->relationTasks->relation_tasks);

        $relationTasks[] = $task->id;
        $tasks = Task::where('project_id', $task->project_id)
            ->where('check_list_id', $task->check_list_id)
            ->get()->toArray();

        $tmp_task = array(
            'check_list_id' => -1,
            'checklist_id_by_manager' => null,
            'created_at' => "",
            'created_by' => "",
            'duplicated_from' => 1,
            'id' => 0,
            'parent' => -1,
            'parent_by_manager' => null,
            'project_id' => -1,
            'sort_number' => 0,
            'sort_number_by_manager' => null,
            'task_name' => "",
            'task_name_by_manager' => null,
            'updated_at' => '',
        );

        array_unshift($tasks, $tmp_task);
        $new = array();
        foreach ($tasks as $a){
            $new[$a['parent']][] = $a;
        }
        $tree = $this->getTasks($new, array($tasks[0]));

        $res_tasks = [];
        $prefix = $task->checklist->name;
        if (array_key_exists('children', $tree[0])) {
            $tmp_tasks = $this->getFullTasksName($tree[0]['children'], $relationTasks, $prefix);
            $res_tasks = array_merge($res_tasks, $tmp_tasks);
        }

        return $res_tasks;
    }

    public function setRelationTasks(Request $request) {
        $relationTasks = RelationTask::where('task_id', $request->id)->first();
        if (!$relationTasks) {
            $relationTasks = new RelationTask;
        }
        $relationTasks->task_id = $request->id;
        $relationTasks->relation_tasks = $relationTasks->relation_tasks . ',' . $request->task_ids;
        $relationTasks->save();
    }

    public function getchecklist(){
        return $this->getchecklistByEmployeeId(Auth::user()->id);
    }

    /* Load all projects */
    public function loadprojects()
    {
        $assignedProjects = Project::whereIn('id', ProjectEmployeeAssignment::where('employee_id', Auth::user()->id)
                ->groupBy('project_id')
                ->get()
                ->pluck('project_id')
            )->orderBy('is_notify', 'desc')->
            orderBy('created_at', 'desc')->get()->toArray();
        $i = 0;
        foreach ($assignedProjects as $key => $project) {
            $assignedProjects[$key]['number'] = ++$i;
            # code...
        }
        return $assignedProjects;
    }

    /* Preview project details */
    public function get_preview_project_details($id)
    {
        return $this->getPreviewProjectDetails(Project::find($id));
    }

    /* Get Employees to add  */
    public function getAssignableEmployees(Request $request)
    {
        $assignedIds = ProjectEmployeeAssignment::where('node_type', $request->node_type)->where('project_id', $request->project_id)->where('node_id', $request->node_id)->get()->pluck('employee_id');
        return Employee::whereNotIn('id', $assignedIds)->where('id', Auth::user()->id)->get();
    }

    /* Assign Members */
    public function assignmembers(Request $request)
    {
        $project = Project::findOrFail($request->project_id);
        $week_number = Carbon::now()->format('W');
        $year = Carbon::now()->format('Y');
        if (isset($request->members) && count($request->members)) {
            foreach ($request->members as $member) {
                $tmp = TaskNotification::where('week_number', $week_number)
                    ->where('year', $year)
                    ->where('employee_id', $member)
                    ->where('project_id', $request->project_id)
                    ->first();

                $project_order = $tmp ? $tmp->project_order : TaskNotification::where('week_number', $week_number)
                    ->where('year', $year)
                    ->where('employee_id', $member)
                    ->max('project_order') + 1;

                $task_order = TaskNotification::where('week_number', $week_number)
                    ->where('year', $year)
                    ->where('project_id', $request->project_id)
                    ->where('employee_id', $member)
                    ->max('task_order');

                $assign = new ProjectEmployeeAssignment;
                $assign->node_id = $request->node_id;
                $assign->node_type = $request->node_type;
                $assign->project_id = $request->project_id;
                $assign->employee_id = $member;
                $assign->manager_id = $project->manager_id;
                $assign->save();

                $task_notification = new TaskNotification;
                $task_notification->employee_id = $member;
                $task_notification->assign_id = $project->manager_id;
                $task_notification->assigned_by = $project->created_by;
                $task_notification->node_id = $request->node_id;
                $task_notification->node_type = $request->node_type;
                $task_notification->project_id = $request->project_id;
                $task_notification->title = $request->title;
                $task_notification->project = $request->name;
                $task_notification->name = '';
                $task_notification->is_started = 0;
                $task_notification->is_completed = 0;
                $task_notification->is_show = 0;
                $task_notification->year = $year;
                $task_notification->week_number = $week_number;
                $task_notification->project_order = $project_order;
                $task_notification->task_order = $task_order + 1;
                $task_notification->save();
            }
        }

        return $this->getAssignmentDetails(Project::find($request->project_id));
    }

    public function getprojecechecklist($id){
        return $this->getprojecechecklistByEmployeeId($id, Auth::user()->id);
    }

    public function assigneddates(){
        return $this->assigneddatesByEmployeeId(Auth::user()->id);
    }

    public function assignedSearchdates(Request $request) {
        return $this->assignedSearchdatesByEmployeeId($request, Auth::user()->id);
    }

    public function startTask(Request $request){
        $this->startTaskByEmployeeId($request, Auth::user());
    }

    public function completeTask(Request $request){
        $this->completeTaskByEmployeeId($request, Auth::user());
    }

    public function newnote(Request $request){
        $this->newnoteById($request, Auth::user()->id);
    }

    public function editnote(Request $request){
        $this->editnoteById($request, Auth::user()->id);
    }

    public function removenote(Request $request){
        $note = EmployeeChecklistNote::find($request->note_id);
        if ($note) $note->delete();
    }

    public function removeNotification(Request $request) {
        AlarmNotification::whereIn('id', $request->ids)->delete();
    }

    public function getNotifications() {
        $task_notifications = TaskNotification::where('employee_id', Auth::user()->id)
            ->where('is_completed', 0)
            ->get()->toArray();

        foreach($task_notifications as $key => $task_notification) {
            if ($task_notification['assigned_by'] == 'MANAGER') {
                $task_notifications[$key]['user_info'] = Manager::findOrFail($task_notification['assign_id']);
            } else {
                $task_notifications[$key]['user_info'] = Admin::findOrFail($task_notification['assign_id']);
            }
        }

        return $task_notifications;
    }

    public function setTrigger(Request $request) {
        $this->setTriggerByEmployeeId($request, Auth::user()->id);
    }

    public function finishTrigger(Request $request) {
        $reminder = Reminder::find($request->id);
        if ($reminder) {
            $reminder->is_trigger = 1;
            $reminder->save();
        }

        if ($reminder->assigned_by == 'MANAGER') {
            $manager = Manager::findOrFail($reminder->assign_id);
            $manager->notify(new PushDemo($reminder->name.' complete trigger', $reminder->project.'_'.$reminder->title.'(at '.$reminder->updated_at.')'));
        } else {
            $admin = Admin::findOrFail($reminder->assign_id);
            $admin->notify(new PushDemo($reminder->name.' complete trigger', $reminder->project.'_'.$reminder->title.'(at '.$reminder->updated_at.')'));
        }
        $employee = Employee::findOrFail($reminder->employee_id);
        $employee->notify(new PushDemo($reminder->name.' complete trigger', $reminder->project.'_'.$reminder->title.'(at '.$reminder->updated_at.')'));
    }

    public function getReminders() {
        $now = date('Y-m-d');
        $reminders = Reminder::with([
                    'user_info'
                ])
                ->where('employee_id', Auth::user()->id)
                ->where('is_trigger', 0)
                ->whereRaw("Date(reminder_date) <= '$now'")
                ->get()->toArray();

        return $reminders;
    }

    public function getSideMenus() {
        return $this->getSideMenusByEmployeeId(Auth::user()->id);
    }

    public function moveTask(Request $request) {
        $task_notification = TaskNotification::findOrFail($request->task_id);
        $task_notification->week_number = $request->week_number;
        $task_notification->save();
    }

    public function multiMoveTask(Request $request) {
        foreach($request->task_ids as $task_id) {
            $task_notification = TaskNotification::findOrFail($task_id);
            $task_notification->week_number = $request->week_number;
            $task_notification->save();
        }
    }

    public function sortTask(Request $request) {
        foreach($request->ids as $key => $id) {
            if ($request->type === self::PROJECT_SORT) {
                $task_notifications = TaskNotification::where('project_id', $id)->get();
                if ($task_notifications[0]->project_order !== $key+1) {
                    foreach($task_notifications as $task_notification) {
                        $task_notification->project_order = $key+1;
                        $task_notification->save();
                    }
                }
            } else {
                $task_notification = TaskNotification::findOrFail($id);
                if ($task_notification->task_order !== $key+1) {
                    $task_notification->task_order = $key+1;
                    $task_notification->save();
                }
            }
        }
    }

    public function setOutline(Request $request) {
        $this->setOutLineofTask($request->node_type, $request->node_id);
    }

    public function setRefresh(Request $request) {
        $this->setRefreshfTask($request->node_type, $request->node_id);
    }

    public function getLowerProject() {
        $projects = Project::where('created_by', 'MANAGER')->where('is_notify', 1)->get()->toArray();
        return $projects;
    }

    public function getOrderMoreProject() {
        $now = Carbon::now()->format('m/d/Y');
        $projects = Project::where('created_by', 'MANAGER')
            ->where('order_date', $now)
            ->where('category', 'winners')->get()->toArray();
        return $projects;
    }

    public function getCategoryTasks(Request $request) {
        return $this->getAjaxCategoryTasks($request->type);
    }

    public function getRefreshTasks(Request $request) {
        return $this->getAjaxRefreshTasks();
    }
}
