<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class ProjectEmployeeAssignment extends Model
{
    //
    protected $table = 'project_employee_assignment';

    public function getStartedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone('America/Los_Angeles')
            ->toDateTimeString()
        ;
    }
    public function getCompletedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone('America/Los_Angeles')
            ->toDateTimeString()
        ;
    }

    public function employee_details()
    {
        return $this->hasOne('App\Models\Employee','id','employee_id');
    }
    public function manager_details(){
            return $this->hasOne('App\Models\Manager','id','manager_id');
    }
    public function project_details(){
        return $this->hasOne('App\Models\Project','id','project_id');
    }
}


