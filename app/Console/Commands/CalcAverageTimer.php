<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use App\Models\Project;
use App\ProductLog;

class CalcAverageTimer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calc-konnektive-average-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calc Konnektive Average Sell Rate and Save to Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $end_date = Carbon::now()->subHours(5); $start_date = Carbon::now()->subDays(7);
        $day_list = $this->getDayList($start_date, $end_date);
        $projects = Project::where('created_by', 'MANAGER')->get();
        foreach($projects as $project) {
            $sell_rate = ProductLog::where('project_id', $project->id)->whereIn('log_date', $day_list)->avg('total_count');
            $after_days = $sell_rate==0 ? $project->inventory : ceil($project->inventory / $sell_rate);
            $project->sold_out_date = Carbon::now()->addDays($after_days)->format('m/d/Y') . ' (' . $after_days . ' days remaining)';
            $after_days = $sell_rate==0 ? $project->inventory + $project->income_inventory : ceil(($project->inventory+$project->income_inventory) / $sell_rate);
            $project->order_date = Carbon::now()->addDays($after_days - $project->lead_time > 0 ? $after_days - $project->lead_time : 0)->format('m/d/Y');
            $project->order_amount = $sell_rate * $project->lead_time;
            $project->sell_rate = $sell_rate;
            $project->save();
        }
    }

    public function getDayList($start_date, $end_date) {
        $date_pivot = $start_date->copy();
        $days = [];
        while ($date_pivot->lte($end_date)) {
            $days[] = $date_pivot->toDateString();
            $date_pivot->addDay();
        }

        return collect($days);
    }
}
