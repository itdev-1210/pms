import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Card, CardBody, CardHeader, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import axios from 'axios'
import { userActions } from '../_actions';

class AvatarModal extends Component {

    constructor(props){
        super(props)
        this.state = {
            fileURL: null,
            file: null,
            submitStatus: true
        };

        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    componentWillMount() {
        const { avatarSrc } = this.props;
        this.setState({
            fileURL: avatarSrc,
            file: null,
            submitStatus: true
        });
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.avatarSrc !== nextProps.avatarSrc) {
            this.setState({ fileURL: nextProps.avatarSrc });
        }
    }

    handleChange(event) {
        this.setState({
            fileURL: URL.createObjectURL(event.target.files[0]),
            file: event.target.files[0],
            submitStatus: false
        })
    };

    onSubmit() {
        const { dispatch } = this.props;
        dispatch(userActions.changeAvatar(this.state.file));

        this.onCancel();
    };

    onCancel() {
        this.setState({
            fileURL: null,
            file: null,
            submitStatus: true
        });

        this.props.toggleAvatar();
    }

    render() {
        return (
            <Modal isOpen={this.props.avatarModal} toggle={this.onCancel}
                   className={'modal-primary ' + this.props.className}>
                <ModalHeader toggle={this.props.toggleAvatar}>Avatar</ModalHeader>
                <ModalBody>
                    <input type="file" onChange={this.handleChange} id="avatar" name="avatar" accept="image/x-png,image/gif,image/jpeg" />
                    <img src={this.state.fileURL} className="preview-avatar" />
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.onSubmit} disabled={this.state.submitStatus}>Submit</Button>{' '}
                    <Button color="secondary" onClick={this.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;

    return {
        user,
        users,
        avatarSrc: user.profile_image_src
    };
}

export default connect(mapStateToProps)(AvatarModal);
