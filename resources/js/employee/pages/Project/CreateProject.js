import React, {  } from 'react'
import { connect } from 'react-redux';

import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
} from 'reactstrap';
import axios from 'axios'
import { NotificationManager } from 'react-notifications';
import Axios from 'axios';
import { Dropdown } from 'semantic-ui-react'

class CreateProject extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            project_name : '',
            project_category : '',
            selected_members : [],
            managers : [],
            project_detail : {}
        }
        this.handleSave = this.handleSave.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.createProject = this.createProject.bind(this)
        this.handleNext = this.handleNext.bind(this)
        this.loadManagers = this.loadManagers.bind(this)
        this.handleSelectMembers = this.handleSelectMembers.bind(this)
    }
    componentDidMount(){
        let self = this
        Axios.get('/manager/ajax/getmasterchecklist')
        .then(response => {
            this.setState({
                project_detail: response.data.project_detail,

            }, ()=>{
                self.loadManagers()
            })
        })

    }
    loadManagers(){
        axios.get("/manager/ajax/managers")
        .then(response =>{
            this.setState({
                managers : response.data
            })
        })
    }
    handleChange(e) {
       this.setState({
           [e.target.name] : e.target.value
       })
    }
    handleSave(){
        if(this.state.project_name == ""){
            NotificationManager.error("Please input project name!", "Error", 3000);
            return false
        }
        else{
            this.createProject()
        }
    }
    createProject(){

        let post_data = {
            project_name : this.state.project_name,
            project_category : this.state.project_category,
            managers : this.state.selected_members,
            project_id : this.state.project_detail.id
        }
        axios.post('/manager/ajax/project/assign', post_data)
        .then(response =>{
            console.log(response.data)
            if(response.data.status == 'success'){
                NotificationManager.success(response.data.message, "Success", 3000)

                this.props.history.push('/manager/project')
            }
            else{
                NotificationManager.error(response.data.message, "Error", 3000)
            }
        })
        // Axios.post('/manager/ajax/project/save', post_data)
        // .then(response => {
        //     if(response.data.status == 'success') {
        //         NotificationManager.success("You created new project successfully!", "Success", 3000)
        //         if(willNext){
        //             this.props.history.push(`/manager/project/edit/${response.data.project_detail.id}`)
        //         }

        //     }
        //     else{
        //         NotificationManager.error(response.data.message, "Error", 3000)
        //     }
        // })
    }
    handleNext(){
        if(this.state.project_name == ""){
            NotificationManager.error("Please input project name!", "Error", 3000);
            return false
        }
        else{
            this.createProject()
        }
    }
    handleSelectMembers(e, data){
        console.log(e, data.value)
        this.setState({
            selected_members : data.value
        })
    }
    render() {
        let managerOptions = this.state.managers.map((manager) =>{
            return  {
                key: manager.id,
                text: manager.full_name,
                value: manager.id,
                image: { avatar: true, src: manager.profile_image_src },
              }
        })
        return <div className="animated fadeIn">
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="icon-user-follow"></i><strong>Create New Project</strong>
                            <div className="card-header-actions">
                                <a color="danger" className="btn-pill btn btn-danger text-white" onClick={() => this.props.history.goBack()}>
                                    <i className="icon-arrow-left"></i>&nbsp;Back
                            </a>
                            </div>
                        </CardHeader>
                        <CardBody>

                                <FormGroup row>
                                    <Label sm="3" lg="2" htmlFor="project_name">Project Name</Label>
                                    <Col sm="9" lg="10">
                                    <Input type="text" id="project_name" name="project_name" placeholder="Project Name" onChange={this.handleChange} />
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm="3" lg="2" htmlFor="project_category">Project Category</Label>
                                    <Col sm="9" lg="10">
                                    <Input type="select" name="project_category" id="project_category"  onChange={this.handleChange}>
                                        <option value="">No selected</option>
                                        <option value="winners">Winners</option>
                                        <option value="testing">Testing</option>
                                        <option value="retired">Retired</option>
                                    </Input>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm="3" lg="2" htmlFor="project_name">Project Managers</Label>
                                    <Col sm="9" lg="10">
                                    <Dropdown placeholder='Managers' search fluid multiple selection options={managerOptions}  onChange={this.handleSelectMembers}/>
                                    </Col>
                                </FormGroup>

                        </CardBody>
                        <CardFooter>
                            <Button type="submit" size="sm" className="mr-1" color="primary" onClick={this.handleSave}><i className="fa fa-save"></i> Save</Button>
                            {/* <Button type="reset" size="sm" className="mr-1" color="success" onClick={this.handleNext}><i className="icon-arrow-right"></i> Next</Button> */}
                        </CardFooter>
                    </Card>
                </Col>
            </Row>
        </div>
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedCreateProject = connect(mapStateToProps)(CreateProject);
export default connectedCreateProject;
