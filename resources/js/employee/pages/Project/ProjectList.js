import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';
import { AppSwitch } from '@coreui/react'
import { Button, Breadcrumb, BreadcrumbItem, Input, Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import ReactDatatable from '@ashvin27/react-datatable';
import axios from 'axios'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'
import Axios from 'axios';
import { NotificationManager } from 'react-notifications';
class ProjectList extends React.Component{
    constructor(props) {
        super(props);
        this.columns = [
            {
                key: "number",
                text: "No",
                className: "number",
                align: "left",
                sortable: true,
                cell: record => {
                    return (
                        <div className={record.is_notify === 0 ? "" : "notify"}>{record.number}</div>
                    );
                }
            },
            {
                key: "project_name",
                text: "Project Name",
                className: "project_name",
                align: "left",
                sortable: true,
                cell: record => {
                    return (
                        <div className={record.is_notify === 0 ? "" : "notify"}>{record.project_name}</div>
                    );
                }
            },
            {
                key: "category",
                text: "Project Category",
                className: "category",
                align: "left",
                sortable: true,
                cell: record => {
                    return (
                        <div className={record.is_notify === 0 ? "" : "notify"}>{record.category}</div>
                    );
                }
            },
            {
                key: "inventory",
                text: "Current Inventory",
                className: "inventory",
                align: "left",
                sortable: true,
                cell: record => {
                    return (
                        <div className={record.is_notify === 0 ? "" : "notify"}>{record.inventory}</div>
                    );
                }
            },
            {
                key: "income_inventory",
                text: "Incoming Inventory",
                className: "inventory",
                align: "left",
                sortable: true,
                cell: record => {
                    return (
                        <div className={record.is_notify === 0 ? "" : "notify"}>{record.income_inventory}</div>
                    );
                }
            },
            {
                key: "sell_rate",
                text: "Sell Rate",
                className: "sell_rate",
                align: "left",
                sortable: true,
                cell: record => {
                    return (
                        <div className={record.is_notify === 0 ? "" : "notify"}>{record.sell_rate}</div>
                    );
                }
            },
            {
                key: "sold_out_date",
                text: "Sold Out Date",
                className: "sold_out_date",
                align: "left",
                sortable: true,
                cell: record => {
                    return (
                        <div className={record.is_notify === 0 ? "" : "notify"}>{record.sold_out_date}</div>
                    );
                }
            },
            {
                key: "lead_time",
                text: "Lead Time",
                className: "lead_time",
                align: "left",
                sortable: true,
                cell: record => {
                    return (
                        <div className={record.is_notify === 0 ? "" : "notify"}>{record.lead_time}</div>
                    );
                }
            },
            {
                key: "order_date",
                text: "Order Date",
                className: "order_date",
                align: "left",
                sortable: true,
                cell: record => {
                    return (
                        <div className={record.is_notify === 0 ? "" : "notify"}>{record.order_date}</div>
                    );
                }
            },
            {
                key: "order_amount",
                text: "Order Amount",
                className: "order_amount",
                align: "left",
                sortable: true,
                cell: record => {
                    return (
                        <div className={record.is_notify === 0 ? "" : "notify"}>{record.order_amount}</div>
                    );
                }
            },
            {
                key: "action",
                text: "Action",
                className: "action",
                width: 100,
                align: "left",
                sortable: false,
                cell: record => {
                    return (
                        <Fragment>
                            <div className="action">
                                 <a
                                    className="btn btn-success btn-sm m-1"
                                    href={`/projects/view/`+record.id}
                                    target="_blank"
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-eye"></i>
                                </a>
                            </div>
                        </Fragment>
                    );
                }
            }
        ];
        this.config = {
            page_size: 10,
            length_menu: [ 10, 20, 50 ],
        }

        this.state = {
            records: [],
            projects: [],
            filterProjects: [],
            is_deleting : false,
            project_category: "default"
        }
        this.loadprojects = this.loadprojects.bind(this)
        this.onChangeType = this.onChangeType.bind(this)
    }
    componentDidMount(){
        this.loadprojects()
    }
    loadprojects(){
        axios.get("/ajax/projects")
        .then(response =>{
            this.setState({ projects: response.data }, () => {this.handleFilter()});
        })
    }
    previewRecord(id){
        this.props.history.push(`/projects/view/${id}`)
    }

    onChangeType(type) {
        this.setState({project_category: type.target.value}, ()=>{this.handleFilter()})
    }

    handleFilter() {
        const { projects } = this.state;
        let temp = [];
        if (this.state.project_category === 'default') {
            temp = projects.filter(item => item.category != 'retired')
        } else {
            temp = projects.filter((item, index) => item.category === this.state.project_category)
        }

        temp.forEach((item, index) => item.number = index + 1);
        temp.forEach((item, index) => item.inventory = String(item.inventory));

        this.setState({ filterProjects: temp });
    }

    render(){

        return <div className="animated fadeIn">
                <LoadingOverlay
                        active={this.state.is_deleting}
                        spinner={<BounceLoader />}
                    >

                </LoadingOverlay>
                <Row>
                    <Col xs="12">
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"></i><strong>projects</strong>
                            </CardHeader>
                            <CardBody>
                                <div className="project-filter">
                                    <Input type="select" name="select" id="exampleSelect" onChange={this.onChangeType}>
                                        <option value="default">Default</option>
                                        <option value="winners">Winners</option>
                                        <option value="testing">Testing</option>
                                        <option value="retired">Retired</option>
                                    </Input>
                                </div>
                                <ReactDatatable
                                    className="table table-bordered table-striped notify-table"
                                    config={this.config}
                                    records={this.state.filterProjects}
                                    columns={this.columns}
                                />
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
              </div>
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedProjectList = connect(mapStateToProps)(ProjectList);
export default connectedProjectList;
