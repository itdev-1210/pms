export default {
    items: [
        {
            name: 'Dashboard',
            url: '/manager/dashboard',
            icon: 'icon-speedometer',
            badge: {
              variant: 'info',
              text: 'NEW',
            },
        },
        {
          title: true,
          name: 'Employee Management',
          wrapper: {            // optional wrapper object
            element: '',        // required valid HTML5 element tag
            attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
          },
          class: ''             // optional class names space delimited list for title item ex: "text-center"
        },
        {
          name: 'New Employee',
          url: '/manager/employee/add',
          icon: 'fa fa-user-secret'
        },
        {
          name: 'All Employees',
          url: '/manager/employee',
          icon: 'fa fa-user-secret'
        },
        {
          title: true,
          name: 'Project Management',
          wrapper: {            // optional wrapper object
            element: '',        // required valid HTML5 element tag
            attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
          },
          class: ''             // optional class names space delimited list for title item ex: "text-center"
        },
        {
          name: 'New Project',
          url: '/manager/project/create',
          icon: 'fa fa-user-secret'
        },
        {
          name: 'All Projects',
          url: '/manager/project',
          icon: 'fa fa-user-secret'
        },
        {
            title: true,
            name: 'Task Management',
            wrapper: {            // optional wrapper object
                element: '',        // required valid HTML5 element tag
                attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
            },
            class: ''             // optional class names space delimited list for title item ex: "text-center"
        },
        {
            name: 'Setup',
            url: '/manager/task/setup',
            icon: ''
        },
        {
            name: 'Launch',
            url: '/manager/task/launch',
            icon: ''
        },
        {
            name: 'Refresh',
            url: '/manager/task/refresh',
            icon: ''
        },
        {
            name: 'Optimize',
            url: '/manager/task/optimize',
            icon: ''
        },
        {
          name: 'Logout',
          url: '/manager/logout',
          icon: 'cui-account-logout'
        },
    ]
}

var navigation =  {
    items: [
      {
        title: true,
        name: 'Theme',
        wrapper: {            // optional wrapper object
          element: '',        // required valid HTML5 element tag
          attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
        },
        class: ''             // optional class names space delimited list for title item ex: "text-center"
      },
      {
        name: 'Colors',
        url: '/manager/theme/colors',
        icon: 'icon-drop',
      },
      {
        name: 'Typography',
        url: '/manager/theme/typography',
        icon: 'icon-pencil',
      },
      {
        title: true,
        name: 'Components',
        wrapper: {
          element: '',
          attributes: {},
        },
      },
      {
        name: 'Base',
        url: '/manager/base',
        icon: 'icon-puzzle',
        children: [
          {
            name: 'Breadcrumbs',
            url: '/manager/base/breadcrumbs',
            icon: 'icon-puzzle',
          },
          {
            name: 'Cards',
            url: '/manager/base/cards',
            icon: 'icon-puzzle',
          },
          {
            name: 'Carousels',
            url: '/manager/base/carousels',
            icon: 'icon-puzzle',
          },
          {
            name: 'Collapses',
            url: '/manager/base/collapses',
            icon: 'icon-puzzle',
          },
          {
            name: 'Dropdowns',
            url: '/manager/base/dropdowns',
            icon: 'icon-puzzle',
          },
          {
            name: 'Forms',
            url: '/manager/base/forms',
            icon: 'icon-puzzle',
          },
          {
            name: 'Jumbotrons',
            url: '/manager/base/jumbotrons',
            icon: 'icon-puzzle',
          },
          {
            name: 'List groups',
            url: '/manager/base/list-groups',
            icon: 'icon-puzzle',
          },
          {
            name: 'Navs',
            url: '/manager/base/navs',
            icon: 'icon-puzzle',
          },
          {
            name: 'Paginations',
            url: '/manager/base/paginations',
            icon: 'icon-puzzle',
          },
          {
            name: 'Popovers',
            url: '/manager/base/popovers',
            icon: 'icon-puzzle',
          },
          {
            name: 'Progress Bar',
            url: '/manager/base/progress-bar',
            icon: 'icon-puzzle',
          },
          {
            name: 'Switches',
            url: '/manager/base/switches',
            icon: 'icon-puzzle',
          },
          {
            name: 'Tables',
            url: '/manager/base/tables',
            icon: 'icon-puzzle',
          },
          {
            name: 'Tabs',
            url: '/manager/base/tabs',
            icon: 'icon-puzzle',
          },
          {
            name: 'Tooltips',
            url: '/manager/base/tooltips',
            icon: 'icon-puzzle',
          },
        ],
      },
      {
        name: 'Buttons',
        url: '/manager/buttons',
        icon: 'icon-cursor',
        children: [
          {
            name: 'Buttons',
            url: '/manager/buttons/buttons',
            icon: 'icon-cursor',
          },
          {
            name: 'Button dropdowns',
            url: '/manager/buttons/button-dropdowns',
            icon: 'icon-cursor',
          },
          {
            name: 'Button groups',
            url: '/manager/buttons/button-groups',
            icon: 'icon-cursor',
          },
          {
            name: 'Brand Buttons',
            url: '/manager/buttons/brand-buttons',
            icon: 'icon-cursor',
          },
        ],
      },
      {
        name: 'Charts',
        url: '/manager/charts',
        icon: 'icon-pie-chart',
      },
      {
        name: 'Icons',
        url: '/manager/icons',
        icon: 'icon-star',
        children: [
          {
            name: 'CoreUI Icons',
            url: '/manager/icons/coreui-icons',
            icon: 'icon-star',
            badge: {
              variant: 'info',
              text: 'NEW',
            },
          },
          {
            name: 'Flags',
            url: '/manager/icons/flags',
            icon: 'icon-star',
          },
          {
            name: 'Font Awesome',
            url: '/manager/icons/font-awesome',
            icon: 'icon-star',
            badge: {
              variant: 'secondary',
              text: '4.7',
            },
          },
          {
            name: 'Simple Line Icons',
            url: '/manager/icons/simple-line-icons',
            icon: 'icon-star',
          },
        ],
      },
      {
        name: 'Notifications',
        url: '/manager/notifications',
        icon: 'icon-bell',
        children: [
          {
            name: 'Alerts',
            url: '/manager/notifications/alerts',
            icon: 'icon-bell',
          },
          {
            name: 'Badges',
            url: '/manager/notifications/badges',
            icon: 'icon-bell',
          },
          {
            name: 'Modals',
            url: '/manager/notifications/modals',
            icon: 'icon-bell',
          },
        ],
      },
      {
        name: 'Widgets',
        url: '/manager/widgets',
        icon: 'icon-calculator',
        badge: {
          variant: 'info',
          text: 'NEW',
        },
      },
      {
        divider: true,
      },
      {
        title: true,
        name: 'Extras',
      },
      {
        name: 'Pages',
        url: '/manager/pages',
        icon: 'icon-star',
        children: [
          {
            name: 'Login',
            url: '/manager/login',
            icon: 'icon-star',
          },
          {
            name: 'Register',
            url: '/manager/register',
            icon: 'icon-star',
          },
          {
            name: 'Error 404',
            url: '/manager/404',
            icon: 'icon-star',
          },
          {
            name: 'Error 500',
            url: '/manager/500',
            icon: 'icon-star',
          },
        ],
      },
      {
        name: 'Disabled',
        url: '/manager/dashboard',
        icon: 'icon-ban',
        attributes: { disabled: true },
      },
      {
        name: 'Download CoreUI',
        url: 'https://coreui.io/react/',
        icon: 'icon-cloud-download',
        class: 'mt-auto',
        variant: 'success',
        attributes: { target: '_blank', rel: "noopener" },
      },
      {
        name: 'Try CoreUI PRO',
        url: 'https://coreui.io/pro/react/',
        icon: 'icon-layers',
        variant: 'danger',
        attributes: { target: '_blank', rel: "noopener" },
      },
    ],
  };
