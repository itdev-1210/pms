<?php

namespace App\Http\Controllers\Resuables;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\ProjectPart;
use App\Models\ProjectEmployeeAssignment;
use App\Models\EmployeeChecklistNote;
use App\TaskNotification;
use App\AlarmNotification;
use App\Models\Admin;
use App\Models\Employee;
use App\Models\Manager;
use App\Reminder;
use App\Notifications\PushDemo;
use App\Models\CheckList;
use App\Models\Task;

use Illuminate\Support\Carbon;
use DB;

trait ProjectTrait
{
    private function getchecklistByEmployeeId($employee_id){
        $assignedProjects = Project::whereIn('id', ProjectEmployeeAssignment::where('employee_id', $employee_id)->groupBy('project_id')->get()->pluck('project_id'))->get()->toArray();
        foreach($assignedProjects as $key => $project){
            $assignedProjects[$key]['assigned_parts'] = ProjectEmployeeAssignment::where('employee_id', $employee_id)->where('project_id', $project['id'])->where('node_type', 'project_part')->count();
            $assignedProjects[$key]['assigned_checklists'] = ProjectEmployeeAssignment::where('employee_id', $employee_id)->where('project_id', $project['id'])->where('node_type', 'checklist')->count();
            $assignedProjects[$key]['assigned_tasks'] = ProjectEmployeeAssignment::where('employee_id', $employee_id)->where('project_id', $project['id'])->where('node_type', 'task')->count();
        }
        return $assignedProjects;
    }

    private function getprojecechecklistByEmployeeId($id, $employee_id){
        ini_set("memory_limit", "-1");
        $tasks = TaskNotification::select(DB::raw('week_number, year'))
                    ->where('employee_id', $employee_id)
                    ->where('project_id', $id)
                    ->groupBy('year')
                    ->groupBy('week_number')
                    ->get()->toArray();
        $date = Carbon::now();

        $lastWeekNumber = $lastYear = 0;
        $currentLists = $this->getWeeks();
        $weekLists = array(); $currentMonthTasks = array(); $currentWeekLists = array();
        $misWeekLists = array(); $misTasks = array(); $misCurrentWeekLists = array(); $misCurrentMonthTasks = array();

        foreach($tasks as $key => $task){
            $date->setISODate($task['year'], $task['week_number']);
            $tasks[$key]['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
            $tasks[$key]['category'] = [];
            $tasks[$key]['category']['Setup'] = array();
            $tasks[$key]['category']['Launch'] = array();
            $tasks[$key]['category']['Refresh'] = array();
            $tasks[$key]['category']['Optimize'] = array();
            $tasks[$key]['category']['Miscellaneous'] = array();
            $tmps = $this->getProjects($task['year'], $task['week_number'], $employee_id, 'Setup');

            foreach($tmps as $tmp) {
                foreach($tmp['tasks'] as $tmp_task) {
                    if ($tmp_task['node_type'] !== 'task') {
                        $tasks[$key]['category']['Refresh'][] = $tmp;
                    } else if($tmp_task['tasks']['category'] == null) {
                        $tasks[$key]['category']['Miscellaneous'][] = $tmp;
                    } else {
                        $tasks[$key]['category'][$tmp_task['tasks']['category']][] = $tmp;
                    }
                }
            }

            $weekLists[$key]['year'] = $task['year'];
            $weekLists[$key]['week_number'] = $task['week_number'];
            $weekLists[$key]['week_assigned'] = $tasks[$key]['week_assigned'];

            if ($lastWeekNumber !== 0 && $lastWeekNumber + 1 !== $task['week_number']) {
                for($i = $lastWeekNumber + 1; $i < $task['week_number']; $i++) {
                    $misTask = array();
                    $misTask['week_number'] = $i;
                    $misTask['year'] = $lastYear;
                    $date->setISODate($lastYear, $i);
                    $misTask['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
                    $misTask['category'] = [];
                    $misTask['category']['Setup'] = [];$misTask['category']['Launch'] = [];$misTask['category']['Refresh'] = [];$misTask['category']['Optimize'] = [];$misTask['category']['Miscellaneous'] = [];

                    $misWeekList = array();
                    $misWeekList['year'] = $lastYear;
                    $misWeekList['week_number'] = $i;
                    $misWeekList['week_assigned'] = $misTask['week_assigned'];

                    $misWeekLists[] = $misWeekList;
                    $misTasks[] = $misTask;
                    if ($i >= $currentLists[0] && $i <= $currentLists[count($currentLists)-1]) {
                        $misCurrentMonthTask = array();
                        $misCurrentMonthTask['year'] = $lastYear;
                        $misCurrentMonthTask['week_number'] = $i;
                        $date->setISODate($lastYear, $i);
                        $misCurrentMonthTask['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
                        $misCurrentMonthTask['category'] = [];
                        $misCurrentMonthTask['category']['Setup'] = [];$misCurrentMonthTask['category']['Launch'] = [];$misCurrentMonthTask['category']['Refresh'] = [];$misCurrentMonthTask['category']['Optimize'] = [];$misCurrentMonthTask['category']['Miscellaneous'] = [];

                        $misCurrentWeekList = array();
                        $misCurrentWeekList['year'] = $lastYear;
                        $misCurrentWeekList['week_number'] = $i;
                        $misCurrentWeekList['week_assigned'] = $misCurrentMonthTask['week_assigned'];

                        $misCurrentWeekLists[] = $misCurrentWeekList;
                        $misCurrentMonthTasks[] = $misCurrentMonthTask;
                    }
                }
            }
            if ($task['week_number'] >= $currentLists[0] && $task['week_number'] <= $currentLists[count($currentLists)-1]) {
                $currentMonthTask = array();
                $currentMonthTask['year'] = $lastYear;
                $currentMonthTask['week_number'] = $task['week_number'];
                $currentMonthTask['week_assigned'] = $tasks[$key]['week_assigned'];
                $currentMonthTask['category'] = $tasks[$key]['category'];
                $currentMonthTasks[] = $currentMonthTask;

                $currentWeekList = array();
                $currentWeekList['year'] = $task['year'];
                $currentWeekList['week_number'] = $task['week_number'];
                $currentWeekList['week_assigned'] = $tasks[$key]['week_assigned'];
                $currentWeekLists[] = $currentWeekList;

            }
            $lastWeekNumber = $task['week_number'];
            $lastYear = $task['year'];
        }
        $tasks = array_merge($tasks, $misTasks);
        $datas = $this->getWeekLists($lastYear, $lastWeekNumber,
                count($tasks), $tasks,
                array_merge($weekLists, $misWeekLists),
                array_merge($currentMonthTasks, $misCurrentMonthTasks),
                array_merge($currentWeekLists, $misCurrentWeekLists));

        return array(
            'tasks' => $datas['tasks'],
            'weekLists' => $datas['weekLists'],
            'currentMonthTasks' => $datas['currentMonthTasks'],
            'currentWeekLists' => $datas['currentWeekLists'],
        );
    }

    private function assigneddatesByEmployeeId($employee_id){
        ini_set("memory_limit", "-1");
        $tasks = TaskNotification::select(DB::raw('week_number, year'))
                    ->where('employee_id', $employee_id)
                    ->groupBy('year')
                    ->groupBy('week_number')
                    ->get()->toArray();
        $date = Carbon::now();

        $lastWeekNumber = $lastYear = 0;
        $currentLists = $this->getWeeks();
        $weekLists = array(); $currentMonthTasks = array(); $currentWeekLists = array();
        $misWeekLists = array(); $misTasks = array(); $misCurrentWeekLists = array(); $misCurrentMonthTasks = array();
        foreach($tasks as $key => $task){
            $date->setISODate($task['year'], $task['week_number']);
            $tasks[$key]['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
            $tasks[$key]['category'] = [];
            $tasks[$key]['category']['Setup'] = array();
            $tasks[$key]['category']['Launch'] = array();
            $tasks[$key]['category']['Refresh'] = array();
            $tasks[$key]['category']['Optimize'] = array();
            $tasks[$key]['category']['Miscellaneous'] = array();
            $tmps = $this->getProjects($task['year'], $task['week_number'], $employee_id, 'Setup');

            foreach($tmps as $tmp) {
                foreach($tmp['tasks'] as $tmp_task) {
                    if($tmp_task['tasks']['category'] == null) {
                        $tasks[$key]['category']['Miscellaneous'][] = $tmp;
                    } else {
                        $tasks[$key]['category'][$tmp_task['tasks']['category']][] = $tmp;
                    }
                }
            }

            $weekLists[$key]['year'] = $task['year'];
            $weekLists[$key]['week_number'] = $task['week_number'];
            $weekLists[$key]['week_assigned'] = $tasks[$key]['week_assigned'];

            if ($lastWeekNumber !== 0 && $lastWeekNumber + 1 !== $task['week_number']) {
                for($i = $lastWeekNumber + 1; $i < $task['week_number']; $i++) {
                    $misTask = array();
                    $misTask['week_number'] = $i;
                    $misTask['year'] = $lastYear;
                    $date->setISODate($lastYear, $i);
                    $misTask['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
                    $misTask['category'] = [];
                    $misTask['category']['Setup'] = [];$misTask['category']['Launch'] = [];$misTask['category']['Refresh'] = [];$misTask['category']['Optimize'] = [];$misTask['category']['Miscellaneous'] = [];

                    $misWeekList = array();
                    $misWeekList['year'] = $lastYear;
                    $misWeekList['week_number'] = $i;
                    $misWeekList['week_assigned'] = $misTask['week_assigned'];

                    $misWeekLists[] = $misWeekList;
                    $misTasks[] = $misTask;
                    if ($i >= $currentLists[0] && $i <= $currentLists[count($currentLists)-1]) {
                        $misCurrentMonthTask = array();
                        $misCurrentMonthTask['year'] = $lastYear;
                        $misCurrentMonthTask['week_number'] = $i;
                        $date->setISODate($lastYear, $i);
                        $misCurrentMonthTask['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
                        $misCurrentMonthTask['category'] = [];
                        $misCurrentMonthTask['category']['Setup'] = [];$misCurrentMonthTask['category']['Launch'] = [];$misCurrentMonthTask['category']['Refresh'] = [];$misCurrentMonthTask['category']['Optimize'] = [];$misCurrentMonthTask['category']['Miscellaneous'] = [];

                        $misCurrentWeekList = array();
                        $misCurrentWeekList['year'] = $lastYear;
                        $misCurrentWeekList['week_number'] = $i;
                        $misCurrentWeekList['week_assigned'] = $misCurrentMonthTask['week_assigned'];

                        $misCurrentWeekLists[] = $misCurrentWeekList;
                        $misCurrentMonthTasks[] = $misCurrentMonthTask;
                    }
                }
            }
            if ($task['week_number'] >= $currentLists[0] && $task['week_number'] <= $currentLists[count($currentLists)-1]) {
                $currentMonthTask = array();
                $currentMonthTask['year'] = $lastYear;
                $currentMonthTask['week_number'] = $task['week_number'];
                $currentMonthTask['week_assigned'] = $tasks[$key]['week_assigned'];
                $currentMonthTask['category'] = $tasks[$key]['category'];
                $currentMonthTasks[] = $currentMonthTask;

                $currentWeekList = array();
                $currentWeekList['year'] = $task['year'];
                $currentWeekList['week_number'] = $task['week_number'];
                $currentWeekList['week_assigned'] = $tasks[$key]['week_assigned'];
                $currentWeekLists[] = $currentWeekList;

            }

            $lastWeekNumber = $task['week_number'];
            $lastYear = $task['year'];
        }
        $tasks = array_merge($tasks, $misTasks);
        $datas = $this->getWeekLists($lastYear, $lastWeekNumber,
                count($tasks), $tasks,
                array_merge($weekLists, $misWeekLists),
                array_merge($currentMonthTasks, $misCurrentMonthTasks),
                array_merge($currentWeekLists, $misCurrentWeekLists));

        return array(
            'tasks' => $datas['tasks'],
            'weekLists' => $datas['weekLists'],
            'currentMonthTasks' => $datas['currentMonthTasks'],
            'currentWeekLists' => $datas['currentWeekLists'],
        );
    }

    private function assignedSearchdatesByEmployeeId(Request $request, $employee_id) {

        ini_set("memory_limit", "-1");
        $from_date = $request->input('start_date');
        $to_date = $request->input('end_date');
        $date = Carbon::now();

        if (is_null($from_date) && is_null($to_date)) {
            $tasks = TaskNotification::select(DB::raw('week_number, year'))
                ->where('employee_id', $employee_id)
                ->groupBy('year')
                ->groupBy('week_number')
                ->get()->toArray();
        }

        if (!is_null($from_date) && is_null($to_date)) {
            $year = Carbon::parse($from_date)->format('Y');
            $week = Carbon::parse($from_date)->format('W');

            $tasks = TaskNotification::select(DB::raw('week_number, year'))
                ->where('employee_id',$employee_id)
                ->where('year', '>', $year)
                ->orWhere(function($query) use($year, $week) {
                    $query->where('year', $year)
                        ->where('week_number', '>=', $week);
                })
                ->groupBy('year')
                ->groupBy('week_number')
                ->get()->toArray();
        }

        if (is_null($from_date) && !is_null($to_date)) {
            $year = Carbon::parse($to_date)->format('Y');
            $week = Carbon::parse($to_date)->format('W');

            $tasks = TaskNotification::select(DB::raw('week_number, year'))
                ->where('employee_id', $employee_id)
                ->where('year', '<', $year)
                ->orWhere(function($query) use($year, $week) {
                    $query->where('year', $year)
                        ->where('week_number', '<=', $week);
                })
                ->groupBy('year')
                ->groupBy('week_number')
                ->get()->toArray();
        }

        if (!is_null($from_date) && !is_null($to_date)) {
            $start_year = Carbon::parse($from_date)->format('Y');
            $start_week = Carbon::parse($from_date)->format('W');

            $end_year = Carbon::parse($to_date)->format('Y');
            $end_week = Carbon::parse($to_date)->format('W');

            if ($start_year !== $end_year) {
                $tasks = TaskNotification::select(DB::raw('week_number, year'))
                ->where('employee_id', $employee_id)
                ->where('year', '<', $end_year)
                ->where('year', '>', $start_year)
                ->orWhere(function($query) use($end_year, $end_week) {
                    $query->where('year', $end_year)
                        ->where('week_number', '<=', $end_week);
                })
                ->orWhere(function($query) use($start_year, $start_week) {
                    $query->where('year', $start_year)
                        ->where('week_number', '>=', $start_week);
                })
                ->groupBy('year')
                ->groupBy('week_number')
                ->get()->toArray();
            } else {
                $tasks = TaskNotification::select(DB::raw('week_number, year'))
                ->where('employee_id', $employee_id)
                ->where('year', $end_year)
                ->where('week_number', '<=', $end_week)
                ->where('week_number', '>=', $start_week)
                ->groupBy('year')
                ->groupBy('week_number')
                ->get()->toArray();
            }
        }

        $lastWeekNumber = $lastYear = 0;
        $currentLists = $this->getWeeks();
        $weekLists = array(); $currentMonthTasks = array(); $currentWeekLists = array();
        $misWeekLists = array(); $misTasks = array(); $misCurrentWeekLists = array(); $misCurrentMonthTasks = array();
        foreach($tasks as $key => $task){
            $date->setISODate($task['year'], $task['week_number']);
            $tasks[$key]['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
            $tasks[$key]['category'] = [];
            $tasks[$key]['category']['Setup'] = array();
            $tasks[$key]['category']['Launch'] = array();
            $tasks[$key]['category']['Refresh'] = array();
            $tasks[$key]['category']['Optimize'] = array();
            $tasks[$key]['category']['Miscellaneous'] = array();
            $tmps = $this->getProjects($task['year'], $task['week_number'], $employee_id, 'Setup');

            foreach($tmps as $tmp) {
                foreach($tmp['tasks'] as $tmp_task) {
                    if($tmp_task['tasks']['category'] == null) {
                        $tasks[$key]['category']['Miscellaneous'][] = $tmp;
                    } else {
                        $tasks[$key]['category'][$tmp_task['tasks']['category']][] = $tmp;
                    }
                }
            }
            $weekLists[$key]['year'] = $task['year'];
            $weekLists[$key]['week_number'] = $task['week_number'];
            $weekLists[$key]['week_assigned'] = $tasks[$key]['week_assigned'];

            if ($lastWeekNumber !== 0 && $lastWeekNumber + 1 !== $task['week_number']) {
                for($i = $lastWeekNumber + 1; $i < $task['week_number']; $i++) {
                    $misTask = array();
                    $misTask['week_number'] = $i;
                    $misTask['year'] = $lastYear;
                    $date->setISODate($lastYear, $i);
                    $misTask['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
                    $misTask['category'] = [];
                    $misTask['category']['Setup'] = [];$misTask['category']['Launch'] = [];$misTask['category']['Refresh'] = [];$misTask['category']['Optimize'] = [];$misTask['category']['Miscellaneous'] = [];

                    $misWeekList = array();
                    $misWeekList['year'] = $lastYear;
                    $misWeekList['week_number'] = $i;
                    $misWeekList['week_assigned'] = $misTask['week_assigned'];

                    $misWeekLists[] = $misWeekList;
                    $misTasks[] = $misTask;
                    if ($i >= $currentLists[0] && $i <= $currentLists[count($currentLists)-1]) {
                        $misCurrentMonthTask = array();
                        $misCurrentMonthTask['year'] = $lastYear;
                        $misCurrentMonthTask['week_number'] = $i;
                        $date->setISODate($lastYear, $i);
                        $misCurrentMonthTask['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
                        $misCurrentMonthTask['category'] = [];
                        $misCurrentMonthTask['category']['Setup'] = [];$misCurrentMonthTask['category']['Launch'] = [];$misCurrentMonthTask['category']['Refresh'] = [];$misCurrentMonthTask['category']['Optimize'] = [];$misCurrentMonthTask['category']['Miscellaneous'] = [];

                        $misCurrentWeekList = array();
                        $misCurrentWeekList['year'] = $lastYear;
                        $misCurrentWeekList['week_number'] = $i;
                        $misCurrentWeekList['week_assigned'] = $misCurrentMonthTask['week_assigned'];

                        $misCurrentWeekLists[] = $misCurrentWeekList;
                        $misCurrentMonthTasks[] = $misCurrentMonthTask;
                    }
                }
            }
            if ($task['week_number'] >= $currentLists[0] && $task['week_number'] <= $currentLists[count($currentLists)-1]) {
                $currentMonthTask = array();
                $currentMonthTask['year'] = $lastYear;
                $currentMonthTask['week_number'] = $task['week_number'];
                $currentMonthTask['week_assigned'] = $tasks[$key]['week_assigned'];
                $currentMonthTask['category'] = $tasks[$key]['category'];
                $currentMonthTasks[] = $currentMonthTask;

                $currentWeekList = array();
                $currentWeekList['year'] = $task['year'];
                $currentWeekList['week_number'] = $task['week_number'];
                $currentWeekList['week_assigned'] = $tasks[$key]['week_assigned'];
                $currentWeekLists[] = $currentWeekList;
            }

            $lastWeekNumber = $task['week_number'];
            $lastYear = $task['year'];
        }

        $tasks = array_merge($tasks, $misTasks);
        $datas = $this->getWeekLists($lastYear, $lastWeekNumber,
                count($tasks), $tasks,
                array_merge($weekLists, $misWeekLists),
                array_merge($currentMonthTasks, $misCurrentMonthTasks),
                array_merge($currentWeekLists, $misCurrentWeekLists));

        return array(
            'tasks' => $datas['tasks'],
            'weekLists' => $datas['weekLists'],
            'currentMonthTasks' => $datas['currentMonthTasks'],
            'currentWeekLists' => $datas['currentWeekLists'],
        );
    }

    private function getSideMenusByEmployeeId($employee_id) {
        $ids = ProjectEmployeeAssignment::where('employee_id', $employee_id)
                    ->groupBy('project_id')
                    ->get()->pluck('project_id');

        $winners = Project::whereIn('id', $ids)
                ->where('category', 'winners')
                ->get()->toArray();

        $testing = Project::whereIn('id', $ids)
                ->where('category', 'testing')
                ->get()->toArray();

        $retired = Project::whereIn('id', $ids)
                ->where('category', 'retired')
                ->get()->toArray();
        $other = Project::whereIn('id', $ids)
                ->where('category', NULL)
                ->get()->toArray();
        return array(
            'winners' => $winners,
            'testing' => $testing,
            'retired' => $retired,
            'others' => $other
        );

    }

    private function startTaskByEmployeeId($request, $user) {
        $assigned = ProjectEmployeeAssignment::where('node_type', $request->node_type)
                    ->where('node_id', $request->node_id)
                    ->where('project_id', $request->project_id)
                    ->where('employee_id', $user->id)->first();
        $assigned->is_started = 1;
        $assigned->started_at = date('Y-m-d H:i:s');
        $assigned->save();

        $assigns = ProjectEmployeeAssignment::where('node_type', $request->node_type)
                    ->where('node_id', $request->node_id)
                    ->where('project_id', $request->project_id)
                    ->where('employee_id', '!=', $user->id)->pluck('id')->toArray();
        if($assigns) ProjectEmployeeAssignment::whereIn('id', $assigns)->delete();

        /* Task Notification */
        $task_notification = TaskNotification::where('node_type', $request->node_type)
                    ->where('node_id', $request->node_id)
                    ->where('project_id', $request->project_id)
                    ->where('employee_id', $user->id)->first();
        $task_notification->is_started = 1;
        $task_notification->name = $user->first_name . ' ' .$user->last_name;
        $task_notification->started_at = date('Y-m-d H:i:s');
        $task_notification->save();

        $taskNotifications = TaskNotification::where('node_type', $request->node_type)
                    ->where('node_id', $request->node_id)
                    ->where('project_id', $request->project_id)
                    ->where('employee_id', '!=', $user->id)->pluck('id')->toArray();
        if ($taskNotifications) TaskNotification::whereIn('id', $taskNotifications)->delete();

        /* Alarm Notification */
        $alarm_notification = new AlarmNotification;
        $alarm_notification->is_completed = 0;
        $alarm_notification->assign_id = $task_notification->assign_id;
        $alarm_notification->assigned_by = $task_notification->assigned_by;
        $alarm_notification->name = $user->first_name . ' ' .$user->last_name;
        $alarm_notification->project = $task_notification->project;
        $alarm_notification->title = $task_notification->title;
        $alarm_notification->employee_id = $user->id;
        $alarm_notification->save();

        if ($task_notification->assigned_by == 'MANAGER') {
            $manager = Manager::findOrFail($alarm_notification->assign_id);
            $manager->notify(new PushDemo($alarm_notification->name.' started task', $alarm_notification->project.'_'.$alarm_notification->title));
        } else {
            $admin = Admin::findOrFail($alarm_notification->assign_id);
            $admin->notify(new PushDemo($alarm_notification->name.' started task', $alarm_notification->project.'_'.$alarm_notification->title));
        }
    }

    private function completeTaskByEmployeeId($request, $user) {
        $assigned = ProjectEmployeeAssignment::where('node_type', $request->node_type)
                    ->where('node_id', $request->node_id)
                    ->where('project_id', $request->project_id)
                    ->where('employee_id', $user->id)->first();
        $assigned->is_completed = 1;
        $assigned->completed_at = date('Y-m-d H:i:s');
        $assigned->save();

        $task_notification = TaskNotification::where('node_type', $request->node_type)
                    ->where('node_id', $request->node_id)
                    ->where('project_id', $request->project_id)
                    ->where('employee_id', $user->id)->first();
        $task_notification->is_completed = 1;
        $task_notification->completed_at = date('Y-m-d H:i:s');
        $task_notification->save();

        /* Alarm Notification */
        $alarm_notification = new AlarmNotification;
        $alarm_notification->is_completed = 1;
        $alarm_notification->assign_id = $task_notification->assign_id;
        $alarm_notification->assigned_by = $task_notification->assigned_by;
        $alarm_notification->name = $user->first_name . $user->last_name;
        $alarm_notification->project = $task_notification->project;
        $alarm_notification->title = $task_notification->title;
        $alarm_notification->employee_id = $user->id;
        $alarm_notification->save();

        if ($task_notification->assigned_by == 'MANAGER') {
            $manager = Manager::findOrFail($alarm_notification->assign_id);
            $manager->notify(new PushDemo($alarm_notification->name.' completed task', $alarm_notification->project.'_'.$alarm_notification->title));
        } else {
            $admin = Admin::findOrFail($alarm_notification->assign_id);
            $admin->notify(new PushDemo($alarm_notification->name.' completed task', $alarm_notification->project.'_'.$alarm_notification->title));
        }
    }

    private function newnoteById($request, $employee_id) {
        $note = new EmployeeChecklistNote;
        $note->node_type = $request->node_type;
        $note->node_id = $request->node_id;
        $note->notes = $request->notes;
        $note->employee_id = $employee_id;
        $note->save();
    }

    private function editnoteById($request, $employee_id) {
        $note = EmployeeChecklistNote::find($request->note_id);
        if ($note) {
            $note->notes = $request->notes;
            $note->employee_id = $employee_id;
            $note->manager_id = null;
            $note->save();
        }
    }

    private function setTriggerByEmployeeId($request, $employee_id) {
        $task_notification = TaskNotification::find($request->id);

        $reminder = new Reminder;
        $reminder->node_type = $task_notification->node_type;
        $reminder->node_id = $task_notification->node_id;
        $reminder->project_id = $task_notification->project_id;
        $reminder->employee_id = $employee_id;
        $reminder->assign_id = $task_notification->assign_id;
        $reminder->assigned_by = $task_notification->assigned_by;
        $reminder->project = $task_notification->project;
        $reminder->title = $task_notification->title;
        $reminder->reminder_date = $request->reminder_date;
        $reminder->is_trigger = 0;
        $reminder->notes = $request->notes;
        $reminder->save();

        if ($task_notification->assigned_by == 'MANAGER') {
            $manager = Manager::findOrFail($task_notification->assign_id);
            $manager->notify(new PushDemo($task_notification->name.' set trigger', $task_notification->project.'_'.$task_notification->title.'('.$request->reminder_date.')'));
        } else {
            $admin = Admin::findOrFail($task_notification->assign_id);
            $admin->notify(new PushDemo($task_notification->name.' set trigger', $task_notification->project.'_'.$task_notification->title.'('.$request->reminder_date.')'));
        }
        $employee = Employee::findOrFail($employee_id);
        $employee->notify(new PushDemo($task_notification->name.' set trigger', $task_notification->project.'_'.$task_notification->title.'('.$request->reminder_date.')'));
    }

    public function getProjectById($year, $start_week, $id, $employee_id, $type) {
        $tasks = TaskNotification::select(DB::raw('project_id, project as project_name'))
                ->where('employee_id', $employee_id)
                ->where('year', $year)
                ->where('week_number', $start_week)
                ->where('project_id', $id)
                ->groupBy('project_id')
                ->orderBy('project_order')
                ->get()->toArray();

        $newTasks = [];
        foreach($tasks as $key => $task){
            if ($type == '') $task_ids = Task::where('project_id', $task['project_id'])->whereNull('category')->pluck('id')->toArray();
            else $task_ids = Task::where('project_id', $task['project_id'])->where('category', $type)->pluck('id')->toArray();
            if ($type == 'Refresh') {
                $tasks[$key]['tasks'] = TaskNotification::with([
                        'employeeCheckListNote',
                        'employeeCheckListNote.employee_details',
                        'employeeCheckListNote.manager_details',
                        'reminders',
                    ])
                    ->where('employee_id', $employee_id)
                    ->where('year', $year)
                    ->where('week_number', $start_week)
                    ->where('project_id', $task['project_id'])
                    ->where(function ($query) use($task_ids) {
                        $query->where('node_type', 'checklist')
                            ->orWhere(function($query) use($task_ids) {
                                $query->where('node_type', 'task')
                                    ->whereIn('node_id', $task_ids);
                            });
                    })
                    ->orderBy('task_order')
                    ->get()->toArray();
            } else {
                $tasks[$key]['tasks'] = TaskNotification::with([
                        'employeeCheckListNote',
                        'employeeCheckListNote.employee_details',
                        'employeeCheckListNote.manager_details',
                        'reminders',
                    ])
                    ->where('employee_id', $employee_id)
                    ->where('year', $year)
                    ->where('week_number', $start_week)
                    ->where('node_type', 'task')
                    ->whereIn('node_id', $task_ids)
                    ->where('project_id', $task['project_id'])
                    ->orderBy('task_order')
                    ->get()->toArray();
            }
            if (count($tasks[$key]['tasks'])) {
                $newTask = [];
                $newTask['project_id'] = $task['project_id'];
                $newTask['project_name'] = $task['project_name'];
                $newTask['tasks'] = $tasks[$key]['tasks'];
                $newTasks[] = $newTask;
            }
            if ($key == 1) break;
        }
        return $newTasks;
    }

    public function getProjects($year, $start_week, $employee_id, $type) {
        $tasks = TaskNotification::select(DB::raw('project_id, project as project_name, project_order'))
            ->where('employee_id', $employee_id)
            ->where('year', $year)
            ->where('week_number', $start_week)
            ->groupBy('project_id')
            ->orderBy('project_order')
            ->get()->toArray();

        $newTasks = [];
        foreach($tasks as $key => $task){
            $tasks[$key]['tasks'] = TaskNotification::with([
                    'employeeCheckListNote',
                    'employeeCheckListNote.employee_details',
                    'employeeCheckListNote.manager_details',
                    'reminders',
                    'tasks'
                ])
                ->where('employee_id', $employee_id)
                ->where('year', $year)
                ->where('week_number', $start_week)
                ->where('node_type', 'task')
                ->where('project_id', $task['project_id'])
                ->orderBy('task_order')
                ->get()->toArray();

            if (count($tasks[$key]['tasks'])) {
                $newTask = [];
                $newTask['project_id'] = $task['project_id'];
                $newTask['project_name'] = $task['project_name'];
                $newTask['project_order'] = $task['project_order'];
                $newTask['tasks'] = $tasks[$key]['tasks'];
                $newTasks[] = $newTask;
            }
        }
        return $newTasks;
    }

    /**
     *  Get Preview Project Details
     */
    protected function getPreviewProjectDetails($project) {
        $menu = array();
        $parts = ProjectPart::with([
                    'employeeCheckListNote' => function($query) {
                        $query->where('node_type', 'project_part');
                    },
                    'employeeCheckListNote.employee_details',
                    'employeeCheckListNote.manager_details',
                    'projectEmployeeAssignment',
                    'projectEmployeeAssignment.employee_details',
                    'checkLists' => function($query) {
                        $query->orderBy('sort_number');
                    },
                    'checkLists.employeeCheckListNote' => function($query) {
                        $query->where('node_type', 'checklist');
                    },
                    'checkLists.employeeCheckListNote.employee_details',
                    'checkLists.employeeCheckListNote.manager_details',
                    'checkLists.projectEmployeeAssignment',
                    'checkLists.projectEmployeeAssignment.employee_details',
                    'checkLists.tasks' => function($query) {
                        $query->orderBy('sort_number');
                    },
                    'checkLists.tasks.employeeCheckListNote' => function($query) {
                        $query->where('node_type', 'task');
                    },
                    'checkLists.tasks.projectEmployeeAssignment',
                    'checkLists.tasks.employeeCheckListNote.employee_details',
                    'checkLists.tasks.employeeCheckListNote.manager_details',
                    'checkLists.tasks.projectEmployeeAssignment.employee_details',
                    'checkLists.tasks.projectEmployeeAssignment.manager_details',
                    ])
                    ->where('project_id', $project->id)
                    ->orderBy('sort_number')
                    ->get()->toArray();
        foreach($parts as $part_key => $part){
            $menu[$part_key]['key'] = "part_".$part['id'];
            $menu[$part_key]['label'] = $part['part_name'];
            $menu[$part_key]['set_outline'] = $part['is_outline'];
            $checklist_submenu = array();
            $checklists = $part['check_lists'];
            foreach($checklists as $key => $checklist){
                $tasks = $checklist['tasks'];
                unset($checklists[$key]['tasks']);
                $task = array(
                    'assigned_employees' => [],
                    'check_list_id' => -1,
                    'checklist_id_by_manager' => null,
                    'created_at' => "",
                    'created_by' => "",
                    'duplicated_from' => 1,
                    'employee_check_list_note' => [],
                    'id' => 0,
                    'parent' => -1,
                    'parent_by_manager' => null,
                    'project_employee_assignment' => [],
                    'project_id' => -1,
                    'sort_number' => 0,
                    'sort_number_by_manager' => null,
                    'task_name' => "",
                    'task_name_by_manager' => null,
                    'updated_at' => '',
                    'is_outline' => 0,
                );

                array_unshift($tasks, $task);
                $new = array();
                foreach ($tasks as $a){
                    $new[$a['parent']][] = $a;
                }
                $tree = $this->getTasks($new, array($tasks[0]));
                $nodes = $this->getMenuTasks($new, array($tasks[0]));
                $checklists[$key]['tasks'] = array_key_exists('children', $tree[0]) ? $tree[0]['children'] : [];
                $checklist_submenu[$key]['key'] = "checklist_".$checklist['id'];
                $checklist_submenu[$key]['label'] = $checklist['name'];
                $checklist_submenu[$key]['set_outline'] = $checklist['is_outline'];
                $checklist_submenu[$key]['nodes'] = array_key_exists('nodes', $nodes[0]) ? $nodes[0]['nodes'] : [];
                $checklists[$key]['notes'] = array_key_exists('employee_check_list_note', $checklist) ? $checklist['employee_check_list_note'] : [];
                $checklists[$key]['assigned_employees'] = [];
                foreach($checklist['project_employee_assignment'] as $project_employee_assignment) {
                    $checklists[$key]['assigned_employees'][] = $project_employee_assignment['employee_details'];
                }
            }
            $menu[$part_key]['nodes'] = $checklist_submenu;
            $parts[$part_key]['checklists'] = $checklists;
            $parts[$part_key]['assigned_employees'] = [];
            foreach($part['project_employee_assignment'] as $project_employee_assignment) {
                $parts[$part_key]['assigned_employees'][] = $project_employee_assignment['employee_details'];
            }
            $parts[$part_key]['notes'] = array_key_exists('employee_check_list_note', $part) ? $part['employee_check_list_note'] : [];
            unset($parts[$part_key]['check_lists']);
        }

        return array(
            'project_detail' => $project->toArray(),
            'parts' => $parts,
            'menu' => $menu,
        );
    }

    /* Get Assignment Details */
    public function getAssignmentDetails($project)
    {
        $orgJson = array();
        $parts = ProjectPart::with([
                            'projectEmployeeAssignment',
                            'projectEmployeeAssignment.employee_details',
                            'projectEmployeeAssignment.manager_details',
                            'checkLists',
                            'checkLists.projectEmployeeAssignment',
                            'checkLists.projectEmployeeAssignment.employee_details',
                            'checkLists.projectEmployeeAssignment.manager_details',
                            'checkLists.tasks',
                            'checkLists.tasks.projectEmployeeAssignment',
                            'checkLists.tasks.projectEmployeeAssignment.employee_details',
                            'checkLists.tasks.projectEmployeeAssignment.manager_details',
                            ])
                            ->where('project_id', $project->id)->orderBy('sort_number')->get()->toArray();
        foreach($parts as $part_key => $part){
            $orgJson[$part_key]['key'] = "part_".$part['id'];
            $orgJson[$part_key]['node_type'] = "project_part";
            $orgJson[$part_key]['node_id'] = $part['id'];
            $orgJson[$part_key]['assigned_employees'] = array_filter($part['project_employee_assignment'], function($value){return $value['node_type'] === 'project_part';});

            $orgJson[$part_key]['label'] = $part['part_name'];
            $checklist_submenu = array();
            $checklists = $part['check_lists'];
            foreach($checklists as $key => $checklist){
                $tasks = $checklist['tasks'];
                unset($checklists[$key]['tasks']);
                $task = array(
                    'assigned_employees' => [],
                    'check_list_id' => -1,
                    'checklist_id_by_manager' => null,
                    'created_at' => "",
                    'created_by' => "",
                    'duplicated_from' => 1,
                    'employee_check_list_note' => [],
                    'id' => 0,
                    'parent' => -1,
                    'parent_by_manager' => null,
                    'project_employee_assignment' => [],
                    'project_id' => -1,
                    'sort_number' => 0,
                    'sort_number_by_manager' => null,
                    'task_name' => "",
                    'task_name_by_manager' => null,
                    'updated_at' => '',
                );

                array_unshift($tasks, $task);
                $new = array();
                foreach ($tasks as $a){
                    $new[$a['parent']][] = $a;
                }
                $tree = $this->getTasks($new, array($tasks[0]));
                $checklists[$key]['tasks'] = $tree[0]['children'];
                $checklist_submenu[$key]['key'] = "checklist_".$checklist['id'];
                $checklist_submenu[$key]['label'] = $checklist['name'];
                $checklist_submenu[$key]['description'] = $checklist['description'];
                $checklist_submenu[$key]['node_type'] = "checklist";
                $checklist_submenu[$key]['node_id'] = $checklist['id'];
                $checklist_submenu[$key]['children'] = $tree[0]['children'];
                $checklist_submenu[$key]['assigned_employees'] = array_filter($checklist['project_employee_assignment'], function($value){return $value['node_type'] === 'checklist';});
            }
            $orgJson[$part_key]['children'] = $checklist_submenu;
        }
        return array(
            'project_detail' => $project->toArray(),
            'orgJson' => $parts,
        );
    }

    /**
     * Get all Tasks Data
     */
    protected function getTasks(&$list, $parent) {
        $tree = array();
        foreach ($parent as $k=>$l){
            $l['title'] = $l['task_name'];
            $l['node_type'] = "task";
            $l['node_id'] = $l['id'];
            $l['is_duplicate'] = false;
            if (array_key_exists('employee_check_list_note', $l)) {
                $l['notes'] = $l['employee_check_list_note'];
            } else {
                $l['notes'] = [];
            }
            if (array_key_exists('project_employee_assignment', $l)) {
                $l['assigned_details'] = array_filter($l['project_employee_assignment'], function($value){return $value['node_type'] === 'task';});
                $l['assigned_employees'] = [];
                foreach($l['project_employee_assignment'] as $project_employee_assignment) {
                    $l['assigned_employees'][] = $project_employee_assignment['employee_details'] ;
                }
            } else {
                $l['assigned_details'] = [];
            }
            if(isset($list[$l['id']])){
                $l['expanede'] = true;
                $l['children'] = $this->getTasks($list, $list[$l['id']]);
            }
            $tree[] = $l;
        }
        return $tree;
    }

    /**
     * Get Task Menu Data
     */
    protected function getMenuTasks(&$list, $parent) {
        $tree = array();
        foreach ($parent as $k=>$l){
            $l['key'] = "task_".$l['id'];
            $l['label'] = $l['task_name'];
            $l['set_outline'] = $l['is_outline'];
            if(isset($list[$l['id']])){
                $l['nodes'] = $this->getMenuTasks($list, $list[$l['id']]);
            }
            $tree[] = $l;
        }
        return $tree;
    }

    public function getWeekLists($lastYear, $lastWeekNumber, $lastKey, $tasks, $weekLists, $currentMonthTasks, $currentWeekLists) {
        $date = Carbon::now();
        $currentWeekNumber = Carbon::now()->format('W');
        $currentYear = Carbon::now()->format('Y');
        $currentLists = $this->getWeeks();
        if ($currentYear > $lastYear && $lastYear !== 0) {
            for ($i = $lastWeekNumber+1; $i < 54; $i++) {
                $lastKey += 1;
                $task = array(); $weekList = array();
                $date->setISODate($lastYear, $i);
                $weekLists[$lastKey]['year'] = $lastYear;
                $weekLists[$lastKey]['week_number'] = $i;
                $weekLists[$lastKey]['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
                $tasks[$lastKey]['week_number'] = $i;
                $tasks[$lastKey]['year'] = (int)$lastYear;
                $tasks[$lastKey]['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
                $tasks[$lastKey]['category'] = [];
                $tasks[$lastKey]['category']['Setup'] = [];$tasks[$lastKey]['category']['Launch'] = [];$tasks[$lastKey]['category']['Refresh'] = [];$tasks[$lastKey]['category']['Optimize'] = [];$tasks[$lastKey]['category']['Miscellaneous'] = [];
            }

            for ($i = 1; $i <= $currentWeekNumber; $i++) {
                $lastKey += 1;
                $date->setISODate($currentYear, $i);
                $weekLists[$lastKey]['year'] = (int)$currentYear;
                $weekLists[$lastKey]['week_number'] = $i;
                $weekLists[$lastKey]['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
                $tasks[$lastKey]['week_number'] = $i;
                $tasks[$lastKey]['year'] = (int)$currentYear;
                $tasks[$lastKey]['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
                $tasks[$lastKey]['category'] = [];
                $tasks[$lastKey]['category']['Setup'] = [];$tasks[$lastKey]['category']['Launch'] = [];$tasks[$lastKey]['category']['Refresh'] = [];$tasks[$lastKey]['category']['Optimize'] = [];$tasks[$lastKey]['category']['Miscellaneous'] = [];
            }
        } else if ($currentYear = $lastYear) {
            for ($i = $lastWeekNumber+1; $i <= $currentWeekNumber; $i++) {
                $lastKey += 1;
                $date->setISODate($currentYear, $i);
                $weekLists[$lastKey]['year'] = (int)$currentYear;
                $weekLists[$lastKey]['week_number'] = $i;
                $weekLists[$lastKey]['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
                $tasks[$lastKey]['week_number'] = $i;
                $tasks[$lastKey]['year'] = (int)$currentYear;
                $tasks[$lastKey]['week_assigned'] = $date->startOfWeek()->format('m/d/Y') . '-' . $date->endOfWeek()->format('m/d/Y');
                $tasks[$lastKey]['category'] = [];
                $tasks[$lastKey]['category']['Setup'] = [];$tasks[$lastKey]['category']['Launch'] = [];$tasks[$lastKey]['category']['Refresh'] = [];$tasks[$lastKey]['category']['Optimize'] = [];$tasks[$lastKey]['category']['Miscellaneous'] = [];

                if ($i >= $currentLists[0] && $i <= $currentLists[count($currentLists)-1]) {
                    $currentMonthTask = array();
                    $currentMonthTask['year'] = $lastYear;
                    $currentMonthTask['week_number'] = $i;
                    $currentMonthTask['week_assigned'] = $weekLists[$lastKey]['week_assigned'];
                    $currentMonthTask['category'] = [];
                    $currentMonthTask['category']['Setup'] = [];$currentMonthTask['category']['Launch'] = [];$currentMonthTask['category']['Refresh'] = [];$currentMonthTask['category']['Optimize'] = [];$currentMonthTask['category']['Miscellaneous'] = [];

                    $currentMonthTasks[] = $currentMonthTask;

                    $currentWeekList = array();
                    $currentWeekList['year'] = $lastYear;
                    $currentWeekList['week_number'] = $i;
                    $currentWeekList['week_assigned'] = $weekLists[$lastKey]['week_assigned'];
                    $currentWeekLists[] = $currentWeekList;
                }
            }
        }

        // Tasks sort by year and week_number
        $weekSorts = array(); $yearSorts = array();
        foreach($tasks as $key => $task) {
            $weekSorts[$key] = $task['week_number'];
            $yearSorts[$key] = $task['year'];
        }
        array_multisort($yearSorts, SORT_ASC, $weekSorts, SORT_ASC, $tasks);
        // WeekNumbers sort by year and week_number
        $weekSorts = array(); $yearSorts = array();
        foreach($weekLists as $key => $weekList) {
            $weekSorts[$key] = $weekList['week_number'];
            $yearSorts[$key] = $weekList['year'];
        }
        array_multisort($yearSorts, SORT_ASC, $weekSorts, SORT_ASC, $weekLists);
        // CurrentMonthTasks sort by year and week_number
        $weekSorts = array(); $yearSorts = array();
        foreach($currentMonthTasks as $key => $currentMonthTask) {
            $weekSorts[$key] = $currentMonthTask['week_number'];
            $yearSorts[$key] = $currentMonthTask['year'];
        }
        array_multisort($yearSorts, SORT_ASC, $weekSorts, SORT_ASC, $currentMonthTasks);
        // CurrentWeekLists sort by year and week_number
        $weekSorts = array(); $yearSorts = array();
        foreach($currentWeekLists as $key => $currentWeekList) {
            $weekSorts[$key] = $currentWeekList['week_number'];
            $yearSorts[$key] = $currentWeekList['year'];
        }
        array_multisort($yearSorts, SORT_ASC, $weekSorts, SORT_ASC, $currentWeekLists);
        return array(
            'tasks' => $tasks,
            'weekLists' => $weekLists,
            'currentMonthTasks' => $currentMonthTasks,
            'currentWeekLists' => $currentWeekLists,
        );
    }

    private function sortByWeekNumber($a, $b) {
        return $a['week_number'] <=> $b['week_number'];
    }

    private function sortByYear($a, $b) {
        return $a['year'] <=> $b['year'];
    }

    public function setOutLineofTask($node_type, $id) {
        switch ($node_type) {
        case 'project_part':
            $object = ProjectPart::find($id);
            break;
        case 'checklist':
            $object = CheckList::find($id);
            break;
        case 'task':
            $object = Task::find($id);
            break;
        default:
            break;
        }
        if ($object) {
            $object->is_outline = 1;
            $object->refresh = 0;
            $object->save();
        }
    }

    public function setRefreshfTask($node_type, $id) {
        switch ($node_type) {
        case 'project_part':
            $object = ProjectPart::find($id);
            break;
        case 'checklist':
            $object = CheckList::find($id);
            break;
        case 'task':
            $object = Task::find($id);
            break;
        default:
            break;
        }
        if ($object) {
            $object->is_outline = 0;
            $object->refresh = 1;
            $object->save();
        }
    }

    private function getWeeks() {
        $today = Carbon::now();

        $startDate = Carbon::instance($today)->startOfMonth()->startOfWeek();
        $endDate = Carbon::instance($today)->endOfMonth();
        $endDate->addDays(6 - $endDate->dayOfWeek);

        $epoch = Carbon::createFromTimestamp(0);
        $firstDay = $epoch->diffInDays($startDate);
        $lastDay = $epoch->diffInDays($endDate);

        $theDay = $startDate;

        $data = array();

        while ($firstDay < $lastDay) {
            if (!in_array($theDay->format('W') ,$data)) $data[] = (int)$theDay->format('W');

            $firstDay++;
            $theDay = $theDay->copy()->addDay();
        }

        if ($data[count($data)-1] == 1) unset($data[count($data)-1]);

        return $data;
    }

    public function getAjaxCategoryTasks($type) {
        ini_set("memory_limit", "-1");
        $task_ids = ProjectEmployeeAssignment::where('node_type', 'task')->pluck('node_id')->toArray();
        if ($type == 'Optimize') {
            $projects = Project::with([
                'projectParts',
                'projectParts.checkLists',
                'projectParts.checkLists.tasks',
                ])
                ->where('category', '!=', 'Retired')
                ->orderBy('sell_rate', 'DESC')
                ->get()->toArray();
        } else {
            $projects = Project::with([
                'projectParts',
                'projectParts.checkLists',
                'projectParts.checkLists.tasks',
                ])
                ->where('category', '!=', 'Retired')
                ->orderBy('inventory', 'DESC')->get()->toArray();
        }

        $tmp_projects = array();
        foreach($projects as $project) {
            $res_tasks = array();
            $parts = $project['project_parts'];
            foreach($parts as $part){
                foreach($part['check_lists'] as $checklist){
                    $tmp_tasks = array();
                    $tasks = $checklist['tasks'];
                    $task = array(
                        'assigned_employees' => [],
                        'check_list_id' => -1,
                        'checklist_id_by_manager' => null,
                        'created_at' => "",
                        'created_by' => "",
                        'duplicated_from' => 1,
                        'employee_check_list_note' => [],
                        'id' => 0,
                        'parent' => -1,
                        'parent_by_manager' => null,
                        'project_employee_assignment' => [],
                        'project_id' => -1,
                        'sort_number' => 0,
                        'sort_number_by_manager' => null,
                        'task_name' => "",
                        'task_name_by_manager' => null,
                        'updated_at' => '',
                        'is_outline' => 0,
                    );

                    array_unshift($tasks, $task);
                    $new = array();
                    foreach ($tasks as $a){
                        $new[$a['parent']][] = $a;
                    }
                    $tree = $this->getTasks($new, array($tasks[0]));
                    $prefix = $checklist['name'];
                    if (array_key_exists('children', $tree[0])) {
                        $tmp_tasks = $this->getFullTasks($tree[0]['children'], $task_ids, $type, $prefix);
                        $res_tasks = array_merge($res_tasks, $tmp_tasks);
                    }
                }
            }
            if (count($res_tasks))
            {
                $tmp_project = array();
                $tmp_project['project_id'] = $project['id'];
                $tmp_project['project_name'] = $project['project_name'];
                if ($type == 'Optimize') $tmp_project['sell_rate'] = $project['sell_rate'];
                else $tmp_project['inventory'] = $project['inventory'];

                $tmp_project['project_tasks'] = $res_tasks;
                $tmp_projects[] = $tmp_project;
            }
        }

        return [
            'tasks' => $tmp_projects
        ];
    }

    public function getAjaxRefreshTasks() {
        ini_set("memory_limit", "-1");
        $projects = Project::with([
            'projectParts',
            'projectParts.checkLists',
            'projectParts.checkLists.tasks' => function($query) {
                $query->where('refresh', 1);
            },
            ])
            ->where('category', '!=', 'Retired')
            ->get()->toArray();

        $tmp_projects = array();
        foreach($projects as $project) {
            $res_tasks = array();
            $parts = $project['project_parts'];
            foreach($parts as $part){
                foreach($part['check_lists'] as $checklist){
                    $tmp_tasks = array();
                    $tasks = $checklist['tasks'];
                    $task = array(
                        'assigned_employees' => [],
                        'check_list_id' => -1,
                        'checklist_id_by_manager' => null,
                        'created_at' => "",
                        'created_by' => "",
                        'duplicated_from' => 1,
                        'employee_check_list_note' => [],
                        'id' => 0,
                        'parent' => -1,
                        'parent_by_manager' => null,
                        'project_employee_assignment' => [],
                        'project_id' => -1,
                        'sort_number' => 0,
                        'sort_number_by_manager' => null,
                        'task_name' => "",
                        'task_name_by_manager' => null,
                        'updated_at' => '',
                        'is_outline' => 0,
                    );

                    array_unshift($tasks, $task);
                    $new = array();
                    foreach ($tasks as $a){
                        $new[$a['parent']][] = $a;
                    }
                    $tree = $this->getTasks($new, array($tasks[0]));
                    $prefix = $checklist['name'];
                    if (array_key_exists('children', $tree[0])) {
                        $tmp_tasks = $this->getFullTasksRefresh($tree[0]['children'], $prefix);
                        $res_tasks = array_merge($res_tasks, $tmp_tasks);
                    }
                }
            }
            if (count($res_tasks))
            {
                $tmp_project = array();
                $tmp_project['project_id'] = $project['id'];
                $tmp_project['project_name'] = $project['project_name'];
                $tmp_project['project_tasks'] = $res_tasks;
                $tmp_projects[] = $tmp_project;
            }
        }
        return [
            'tasks' => $tmp_projects
        ];
    }

    public function getTemplateTasks ($projects) {
        $tmp_projects = array();
        foreach($projects as $project){
            $tmp_project = array();
            $tmp_project['project_id'] = $project['id'];
            $tmp_project['project_name'] = $project['project_name'];
            $project_tasks = array();
            foreach($project['project_tasks'] as $project_task) {
                $tmp_task = array();
                $tmp_task['node_id'] = $project_task['id'];
                $project_tasks[] = $tmp_task;
            }
            if (count($project_tasks))
                $tmp_projects[] = $tmp_project;
        }

        return $tmp_projects;
    }

    public function getFullTasks($tasks, $task_ids, $type, $prefix) {
        $res_tasks = array();
        foreach($tasks as $task) {
            if ($task['category'] == $type && !in_array($task['id'], $task_ids)) {
                $tmp_task = array();
                $tmp_task['node_id'] = $task['id'];
                $tmp_task['node_type'] = 'task';
                $tmp_task['need_relation'] = $task['need_relation'];
                $tmp_task['task_name'] = $prefix.'_'.$task['task_name'];
                $res_tasks[] = $tmp_task;
            }
            if (array_key_exists('children', $task)) {
                $tmp_tasks = $this->getFullTasks($task['children'], $task_ids, $type, $prefix.'_'.$task['task_name']);
                $res_tasks = array_merge($res_tasks, $tmp_tasks);
            }
        }
        return $res_tasks;
    }

    public function getFullTasksRefresh($tasks, $prefix) {
        $res_tasks = array();
        foreach($tasks as $task) {
            $tmp_task = array();
            $tmp_task['node_id'] = $task['id'];
            $tmp_task['node_type'] = 'task';
            $tmp_task['need_relation'] = $task['need_relation'];
            $tmp_task['task_name'] = $prefix.'_'.$task['task_name'];
            $res_tasks[] = $tmp_task;
            if (array_key_exists('children', $task)) {
                $tmp_tasks = $this->getFullTasksRefresh($task['children'], $prefix.'_'.$task['task_name']);
                $res_tasks = array_merge($res_tasks, $tmp_tasks);
            }
        }
        return $res_tasks;
    }

    public function getFullTasksName($tasks, $task_ids, $prefix) {
        $res_tasks = array();
        foreach($tasks as $task) {
            if (!in_array($task['id'], $task_ids)) {
                $tmp_task = array();
                $tmp_task['id'] = $task['id'];
                $tmp_task['need_relation'] = $task['need_relation'];
                $tmp_task['task_name'] = $prefix.'_'.$task['task_name'];
                $res_tasks[] = $tmp_task;
            }
            if (array_key_exists('children', $task)) {
                $tmp_tasks = $this->getFullTasksName($task['children'], $task_ids, $prefix.'_'.$task['task_name']);
                $res_tasks = array_merge($res_tasks, $tmp_tasks);
            }
        }
        return $res_tasks;
    }
}
