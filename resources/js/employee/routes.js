import React from 'react';
// const DashboardIndex = React.lazy(() => import('./pages/Dashboard/Index/Index'));
import  DashboardIndex from './pages/Dashboard/Index/Index'
import { Logout } from './pages/Logout/Logout';
import AllProjects from './pages/Project/AllProjects';
import ProjectChecklist from './pages/Project/ProjectChecklist';
import connectedProjectList from './pages/Project/ProjectList';
import connectedProjectPreview from './pages/Project/ProjectPreview';
// import connectedAssignmentProject from './pages/Project/ProjectAssignment';

const routes = [
  { path: '/', exact: true, name: 'Employee' },
  { path: '/dashboard', name: 'Dashboard', component: DashboardIndex },
  { path: '/allProjects', name: 'All Projects', component: connectedProjectList },
  { path: '/projects', exact: true, name: 'All Projects', component: AllProjects },
  { path: '/projects/view/:id', name: 'Edit Project',exact: true, component: connectedProjectPreview },
  { path: '/project/:id', name: 'Project', component: ProjectChecklist },
  { path: '/logout', name: 'Logout', component: Logout },
];
export default routes;
