import React, { Fragment } from 'react'
import { connect } from 'react-redux';

import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Label,
    Row,
    Modal, ModalBody, ModalFooter, ModalHeader,
} from 'reactstrap';
import axios from 'axios'
import ReactDatatable from '@ashvin27/react-datatable';

import { NotificationManager } from 'react-notifications';
import { Dropdown, Image } from 'semantic-ui-react'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
class AssignmentProject extends React.Component {
    constructor(props) {
        super(props);
        this.columns = [
            {
                key: "number",
                text: "No",
                className: "number",
                align: "left",
                sortable: true
            },
            {
                key: "manager_name",
                text: "Manager Name",
                className: "manager_name",
                align: "left",
                // sortable: true,
                cell: record =>{
                    return <Fragment>
                         <div>
                            <Image src={record.manager_profile_pic} avatar />
                            <span>{record.manager_name}</span>
                        </div>
                    </Fragment>
                }
            },
            {
                key: "action",
                text: "Action",
                className: "action",
                width: 200,
                align: "left",
                sortable: false,
                cell: record => {
                    return (
                        <Fragment>
                             <button
                                className="btn btn-success btn-sm m-1"
                                // onClick={() => this.previewRecord(record.id)}
                                style={{marginRight: '5px'}}>
                                <i className="fa fa-eye"></i>
                            </button>

                            <button
                                className="btn btn-danger btn-sm m-1"
                                onClick={() => this.deleteRecord(record.id)}>
                                <i className="fa fa-trash"></i>
                            </button>
                        </Fragment>
                    );
                }
            }
        ];
        this.config = {
            page_size: 10,
            length_menu: [ 10, 20, 50 ],
        }
        this.state = {
            isOpenAddModal : false,
            project_detail : {},
            assignmet_details : [],
            managers : [],
            selected_members:[]
        }

        this.getPageData = this.getPageData.bind(this)
        this.handleSelectMembers = this.handleSelectMembers.bind(this)
        this.handleAddMembers = this.handleAddMembers.bind(this)
        this.handleRemokeManager = this.handleRemokeManager.bind(this)

    }

    componentDidMount(){
        this.getPageData()
    }
    getPageData(){
        axios.get('/manager/ajax/project/get_assignments/' +  this.props.match.params.id)
        .then(response =>{
            this.setState({
                project_detail : response.data.project_detail,
                assignmet_details : response.data.assignmet_details,
                managers : response.data.managers,
                isOpenAddModal : false
            })
        })
    }

    handleOpenMembersModal(){

    }
    handleAddMembers(){
        let post_data = {
            managers : this.state.selected_members,
            project_id : this.props.match.params.id
        }
        axios.post('/manager/ajax/project/assignmember', post_data)
        .then(response =>{
            if(response.data.status == 'success'){
                NotificationManager.success(response.data.message, "Success", 3000)

                this.getPageData()
            }
            else{
                NotificationManager.error(response.data.message, "Error", 3000)
            }
        })
    }
    handleSelectMembers(e, data){
        this.setState({
            selected_members : data.value
        })
    }
    handleRemokeManager(id){
        axios.post('/manager/ajax/project/removeassign/' + id )
        .then(response =>{
            if(response.data.status == 'success'){
                NotificationManager.success(response.data.message, "Success", 3000)

                this.getPageData()
            }
            else{
                NotificationManager.error(response.data.message, "Error", 3000)
            }
        })
    }
    deleteRecord(id){
        confirmAlert({
            title: 'Confirm to revoke',
            message: 'Are you sure to revoke this manager?',
            buttons: [
              {
                label: 'Yes',
                onClick: () => this.handleRemokeManager(id)
              },
              {
                label: 'No',
                onClick: () => {

                }
              }
            ]
          });
    }
    render() {
        let managerOptions = this.state.managers.map((manager) =>{
            return  {
                key: manager.id,
                text: manager.full_name,
                value: manager.id,
                image: { avatar: true, src: manager.profile_image_src },
              }
        })
        let tableData = this.state.assignmet_details.map((assign, index) =>{
            return {
                number : index + 1,
                id : assign.id,
                manager_name : assign.manager_details.full_name,
                manager_id : assign.manager_details.id,
                manager_profile_pic : assign.manager_details.profile_image_src
            }
        })
        return <div className="animated fadeIn">
            <Row>
                <Modal isOpen={this.state.isOpenAddModal} toggle={()=>this.setState({isOpenAddModal : false})} >
                  <ModalHeader toggle={()=>this.setState({isOpenAddModal : false})}>Choose Managers to add</ModalHeader>
                  <ModalBody>
                    <FormGroup>
                        <Label>Manager</Label>
                        <Dropdown placeholder='Managers' search fluid multiple selection options={managerOptions}  onChange={this.handleSelectMembers}/>
                    </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="primary" onClick={this.handleAddMembers}>Add Members</Button>{' '}
                    <Button color="secondary" onClick={()=>this.setState({isOpenAddModal : false})}>Cancel</Button>
                  </ModalFooter>
                </Modal>

                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="icon-user-follow"></i><strong>Assignment Project</strong>
                            <div className="card-header-actions">
                                <Button color="success" className="mr-1" onClick={()=>{this.setState({isOpenAddModal : true})}}>
                                    <i className="icon-plus"></i>&nbsp;Add Managers
                                </Button>
                                <a color="danger" className="btn-pill btn btn-danger text-white" onClick={() => this.props.history.goBack()}>
                                    <i className="icon-arrow-left"></i>&nbsp;Back
                            </a>
                            </div>
                        </CardHeader>
                        <CardBody>

                               <Row>
                                   <Col xl={12}>
                                        <h2 className="font-weight-bold text-center text-capitalize">{this.state.project_detail.project_name}</h2>
                                        <hr/>
                                        <ReactDatatable
                                            config={this.config}
                                            records={tableData}
                                            columns={this.columns}
                                        />
                                   </Col>
                               </Row>

                        </CardBody>
                        <CardFooter>
                            <Button type="submit" size="sm" className="mr-1" color="primary" onClick={this.handleSave}><i className="fa fa-save"></i> Save</Button>
                            <Button type="reset" size="sm" className="mr-1" color="success" onClick={this.handleNext}><i className="icon-arrow-right"></i> Next</Button>
                        </CardFooter>
                    </Card>
                </Col>
            </Row>
        </div>
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedAssignmentProject = connect(mapStateToProps)(AssignmentProject);
export default connectedAssignmentProject;
