import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';

import {
    Badge,
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupButtonDropdown,
    InputGroupText,
    Label,
    Row,
  } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import Axios from 'axios';

class EditMaster extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            master_detail : {}
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }
    componentDidMount(){
        let masterId = this.props.match.params.id
        Axios.get(`/admin/ajax/master/get/${masterId}`)
        .then( response => {
            return response.data
        })
        .then( master =>{
            this.setState({
                master_detail : master
            })
        })
    }
    handleChange(e){
        // e.preventDefault()
        let master_detail = this.state.master_detail
        master_detail[e.target.name] = e.target.value
        this.setState({
            master_detail : master_detail
        })
    }
    handleSubmit(e){
        e.preventDefault()

        axios.post('/admin/ajax/master/update', this.state.master_detail)
        .then(response =>{
            let data = response.data
            if(data.status == "success"){
                NotificationManager.success(data.message,'Success', 3000);
                this.props.history.goBack()
            }
            else{
                NotificationManager.warning(data.message,'Warning', 3000);
            }
        }).catch(error =>{
            // NotificationManager.error( 'Server connection error!','Error', 3000);
        })
    }
    render(){
        return <div className="animated fadeIn">
                <Row>
                    <Col xs="12">
                        <Card>
                        <CardHeader>
                            <i className="icon-user-follow"></i><strong>Edit Employee</strong>
                            <div className="card-header-actions">
                            <Link color="danger" className="btn-pill btn btn-danger" to="/admin/master">
                                <i className="icon-arrow-left"></i>&nbsp;Back
                            </Link>
                            </div>
                        </CardHeader>
                        <CardBody>
                        <Form  method="post" onSubmit={this.handleSubmit}>
                            {/* <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" id="first_name" name="first_name" placeholder="First Name" required onChange={this.handleChange} value = {this.state.master_detail ? this.state.master_detail.first_name : ""} />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" id="last_name" name="last_name" placeholder="Last Name" required onChange={this.handleChange} value = {this.state.master_detail ? this.state.master_detail.last_name : ""} />
                                </InputGroup>
                            </FormGroup> */}
                            <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" id="username" name="username" placeholder="Username" required onChange={this.handleChange} value = {this.state.master_detail ? this.state.master_detail.name : ""} />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-envelope"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="email" id="email" name="email" placeholder="Email"required onChange={this.handleChange} value = {this.state.master_detail ? this.state.master_detail.email : ""} />
                                </InputGroup>
                            </FormGroup>
                            {/* <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-asterisk"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="password" id="password" name="password" placeholder="Password" required autoComplete={false} />
                                </InputGroup>
                            </FormGroup> */}
                            <FormGroup className="form-actions">
                                <Button type="submit" size="sm" color="success">Submit</Button>
                            </FormGroup>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
              </div>
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedEditMaster = connect(mapStateToProps)(EditMaster);
export default connectedEditMaster;
