import React from 'react'
import { connect } from 'react-redux';
import { Col, Row, ListGroupItem, Modal, ModalBody, ModalFooter, ModalHeader, FormGroup, Label,  Button } from 'reactstrap';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import { Dropdown } from 'semantic-ui-react'
import Axios from 'axios';

class Optimize extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            projects: [],
            isOpenAddModal: false,
            selectable_employees: [],
            selected_members : [],
            active_node_type: null,
            active_node_id: null,
            active_title: null,
            active_project_id: null,
            active_project_name: null,
        }

        this.handleRightClick = this.handleRightClick.bind(this);
        this.handleSelectMembers = this.handleSelectMembers.bind(this);
        this.handleAddMembers = this.handleAddMembers.bind(this);
    }

    componentWillMount() {
        this.reload()
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.user !== nextProps.user) {
            this.reload();
        }
    }

    reload() {
        Axios.post('/ajax/getAjaxCategoryTasks', {type: 'Optimize'})
            .then(response => {
                this.setState({
                    projects: response.data.tasks
                })
            })
    }

    handleRightClick(node) {
        this.setState({
            active_node_type: node.node_type,
            active_node_id: node.node_id,
            active_title: node.title,
            active_project_id: node.project_id,
            active_project_name: node.project_name,
        }, () => {
            let post_data = {
                node_type: node.node_type,
                node_id: node.node_id,
                project_id: node.project_id,
            }
            Axios.post('/manager/ajax/getAssignableEmployees', post_data)
                .then(response => {
                    this.setState({
                        selectable_employees: response.data,
                    })
                })
        })
    }

    handleSelectMembers(e, data){
        this.setState({
            selected_members : data.value
        })
    }

    handleAddMembers(){
        if (this.state.selected_members.length === 0) {
            this.setState({isOpenAddModal: false})
            return;
        }
        let post_data = {
            project_id : this.state.active_project_id,
            node_type : this.state.active_node_type,
            node_id : this.state.active_node_id,
            members : this.state.selected_members,
            title: this.state.active_title,
            name: this.state.active_project_name,
            type: 'Optimize'
        }
        Axios.post('/manager/ajax/newAssign', post_data)
            .then((response) =>{
                // let projects = JSON.parse(JSON.stringify(this.state.projects));
                // const { active_project_id, active_node_id } = this.state;
                // let active_project = projects.filter(item => item.project_id === active_project_id);
                // let tasks = active_project[0].project_tasks;
                // let active_task = tasks.filter(item => item.node_id === active_node_id);
                // tasks.splice(tasks.indexOf(active_task[0]), 1);
                // projects.map((project, index) => {
                //     if (project.project_tasks.length === 0)
                //         projects.splice(index, 1);
                // });

                this.setState({
                    is_loading : false,
                    selectable_employees : [],
                    selected_members: [],
                    active_node_id : null,
                    active_node_type : null,
                    active_title: '',
                    active_project_id: null,
                    active_project_name: null,
                    isOpenAddModal : false,
                    // projects: projects,
                }, () => {
                    this.reload();
                });
            })
    }

    render(){
        const { projects } = this.state;
        const employeeOptions = this.state.selectable_employees.map((employee) => {
            return {
                key: employee.id,
                text: employee.full_name,
                value: employee.id,
                image: { avatar: true, src: employee.profile_image_src },
            }
        });

        return (
            <Row>
                <Modal isOpen={this.state.isOpenAddModal} toggle={() => this.setState({ isOpenAddModal: false })} >
                    <ModalHeader toggle={() => this.setState({ isOpenAddModal: false })}>Choose Employees to add</ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label>Employee</Label>
                            <Dropdown placeholder='Employees' search fluid multiple selection options={employeeOptions} onChange={this.handleSelectMembers} />
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.handleAddMembers} >Add Members</Button>{' '}
                        <Button color="secondary" onClick={() => this.setState({ isOpenAddModal: false })}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                {projects.length > 0 && projects.map((project, project_index) => (
                    <Col sm="12" md="4" className="mt-lg-3 mb-lg-3" key={project_index}>
                        <div className="dashboard-box">
                            <div className="box-title"><a href={`/manager/project/view/${project.project_id}`}>{project.project_name}</a> | Current Inventory {project.inventory}</div>
                            <div className="box-content" style={{height: '200px', overflowY: 'auto'}}>
                                {project.project_tasks.length > 0 && project.project_tasks.map((task, task_index) => {
                                    return (<div key={task_index} className="task-item">
                                        <ContextMenu id={"some_unique_identifier" + project_index + task_index}>
                                            <MenuItem onClick={() => this.setState({isOpenAddModal:true})}>
                                                <ListGroupItem style={{cursor: 'pointer'}}><a>Add
                                                    Members </a></ListGroupItem>
                                            </MenuItem>
                                        </ContextMenu>
                                        <ContextMenuTrigger id={"some_unique_identifier" + project_index + task_index} collect={() => {
                                            this.handleRightClick({
                                                node_type: task.node_type,
                                                node_id: task.node_id,
                                                title: task.task_name,
                                                project_id: project.project_id,
                                                project_name: project.project_name,
                                            })
                                        }}>
                                            <strong className={task.need_relation ? 'relation_task' : ''}>{task.task_name}</strong>
                                        </ContextMenuTrigger>
                                    </div>)
                                })}
                            </div>
                        </div>
                    </Col>
                ))}
            </Row>
        )
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    const { users, authentication } = state;
    const { user } = authentication;

    return {
        users,
        user,
        loggingIn
    };
}

const connectedOptimize = connect(mapStateToProps)(Optimize);
export default connectedOptimize;
