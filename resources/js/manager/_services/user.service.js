import { authHeader } from '../_helpers';
import axios from 'axios'
import {NotificationContainer, NotificationManager} from 'react-notifications';

export const userService = {
    login,
    logout,
    register,
    getAll,
    getById,
    update,
    delete: _delete,
    changeAvatar
};

function login(username, password) {
    return axios.post('/manager/login', {username : username, password : password})
    .then(response =>{
        return response.data
    })
    .then(data =>{
        if(data.status == "success"){
            localStorage.setItem('user', JSON.stringify(data.user_info));
            return data.user_info;
        }
        else{
            NotificationManager.warning( 'Please input correct Username and Password!','Error', 3000);
            return Promise.reject('Error');
        }
    })
}

function logout() {
    localStorage.removeItem('user');
    axios.post('/manager/logout');
}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`/users`, requestOptions).then(handleResponse);
}

function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`/users/${id}`, requestOptions).then(handleResponse);
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`/users/register`, requestOptions).then(handleResponse);
}

function update(user) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`/users/${user.id}`, requestOptions).then(handleResponse);;
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(`/users/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }



        return data;
    });
}

function changeAvatar(file) {
    const data = new FormData();
    data.append('avatar', file);
    return axios.post("/manager/profile", data)
        .then(response =>{
            let data = response.data;
            if (data.status === 'success') {
                localStorage.setItem('user', JSON.stringify(data.user_info));
                return data.user_info.profile_image_src;
            } else {
                return Promise.reject('Error');
            }
        });
}