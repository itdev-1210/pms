<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Revolution\Google\Sheets\Sheets;
use App\Models\Project;
use App\Models\Employee;
use App\Models\Manager;
use App\Notifications\PushDemo;

class GetGoogleSheetTimer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get-google-sheet-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get google sheet data and update database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \Google_Client();
        $path = storage_path().'/key.json';
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.$path);
        $client->useApplicationDefaultCredentials();
        $client->setScopes([\Google_Service_Sheets::DRIVE, \Google_Service_Sheets::SPREADSHEETS]);

        $service = new \Google_Service_Sheets($client);

        $sheets = new Sheets();
        $sheets->setService($service);
        $rows = $sheets->spreadsheet(env("GOOGLE_SHEET_ID"))->sheet('Current Inventory')->all();
        $header = array_shift($rows);
        $values = $sheets->collection($header, $rows);
        $values = $values->toArray();

        $projects = Project::where('created_by', 'MANAGER')->get();
        foreach($projects as $project) {
            $sum = array_sum(array_map(function($item) {
                return $item['Quantity'];
            }, array_filter($values, function ($value) use ($project) {
                return (strtolower(str_replace(' ', '', $value['Project Name'])) == strtolower(str_replace(' ', '', $project->project_name)));
            })));
            $project->inventory = $sum;
            $project->is_notify = $sum < $project->limit_inventory ? 1 : 0;
            $project->save();
            if ($sum < $project->limit_inventory) {
                $employees = Employee::all();
                foreach ($employees as $employee) {
                    $employee->notify(new PushDemo('Inventy Low Limit!', $project->project_name.'(Total Inventory:'.$sum.')'));
                }
                $managers = Manager::all();
                foreach ($managers as $manager) {
                    $manager->notify(new PushDemo('Inventy Low Limit!', $project->project_name.'(Total Inventory:'.$sum.')'));
                }
            }
        }
    }
}
