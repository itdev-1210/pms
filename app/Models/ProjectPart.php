<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectPart extends Model
{
    //
    protected $table = "project_parts";

    // protected $appends = ['assigned_employees'];

    public function getCheclists(){
        return $checklists = CheckList::where('project_id', $this->project_id)->where('project_part_id', $this->id)->orderBy('sort_number')->get();
    }

    // public function getAssignedEmployeesAttribute(){
    //     $result = array();
    //     $assigns = ProjectEmployeeAssignment::with('employee_details')->where('node_type', 'project_part')->where('node_id', $this->attributes['id'])->get();
    //     foreach($assigns as $assign){
    //         $result[] = $assign->employee_details;
    //     }
    //     return $result;
    // }

    public static function assigned($projectid, $employeeid, $date = null){

        $parts = self::where('project_id', $projectid)->get();
        $assignedIds = [];
        foreach($parts as $part){
            $project_part_ids_query = ProjectEmployeeAssignment::where('node_type', 'project_part')
                            ->where('project_id', $projectid)
                            ->where('employee_id', $employeeid)
                            ->where('node_id', $part->id);
            if($date){
                $project_part_ids_query->whereRaw("Date(`created_at`) = '$date'");
            }
            // return $project_part_ids_query->toSql();
            if(($project_part_ids_query->count())){
                $assignedIds[] = $part->id;
            }
            else{
                if(count(CheckList::getAssignedIds($projectid, $part->id, $employeeid, $date))){
                    $assignedIds[] = $part->id;
                }
            }
        }
        return $assignedIds;
    }

    /**
     * Get all the checkList of this Project Part
     */
    public function checkLists() {
        return $this->hasMany(CheckList::class);
    }

    /**
     * Get all the projectEmployeeAssignment of this Project Part
     */
    public function projectEmployeeAssignment() {
        return $this->hasMany(ProjectEmployeeAssignment::class,'node_id','id');
    }

    /**
     * Get all the employee check list note of this task
     */
    public function employeeCheckListNote() {
        return $this->hasMany(EmployeeChecklistNote::class,'node_id','id');
    }
}
