<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class EmployeeChecklistNote extends Model
{
    //
    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone('America/Los_Angeles')
            ->toDateTimeString()
        ;
    }
    public function employee_details(){

        return $this->hasOne('App\Models\Employee','id','employee_id');
    }

    public function manager_details(){

        return $this->hasOne('App\Models\Manager','id','manager_id');
    }
}
