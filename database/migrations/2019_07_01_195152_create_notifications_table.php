<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum("from_account_type", ['ADMIN', 'MANAGER', 'EMPLOYEE']);
            $table->enum("to_account_type", ['ADMIN', 'MANAGER', 'EMPLOYEE']);
            $table->integer('from_account_id');
            $table->integer('to_account_id');
            $table->string('title');
            $table->string('message');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
