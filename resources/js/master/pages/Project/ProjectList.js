import React, { Fragment } from 'react'
import { connect } from 'react-redux';
import { Input, Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import ReactDatatable from '@ashvin27/react-datatable';
import axios from 'axios'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'
import Axios from 'axios';
import { NotificationManager } from 'react-notifications';
class ProjectList extends React.Component{
    constructor(props) {
        super(props);
        this.columns = [
            {
                key: "number",
                text: "No",
                className: "number",
                align: "left",
                sortable: true,
            },
            {
                key: "project_name",
                text: "Project Name",
                className: "project_name",
                align: "left",
                sortable: true
            },
            {
                key: "category",
                text: "Project Category",
                className: "category",
                align: "left",
                sortable: true
            },


            {
                key: "created_at",
                text: "Created At",
                className: "created_at",
                align: "left",
                sortable: true
            },

            {
                key: "updated_at",
                text: "Updated At",
                className: "updated_at",
                align: "left",
                sortable: true
            },
            {
                key: "action",
                text: "Action",
                className: "action",
                width: 200,
                align: "left",
                sortable: false,
                cell: record => {
                    return (
                        <Fragment>
                             <button
                                className="btn btn-success btn-sm m-1"
                                onClick={() => this.previewRecord(record.id)}
                                style={{marginRight: '5px'}}>
                                <i className="fa fa-eye"></i>
                            </button>
                            <button
                                className="btn btn-primary btn-sm m-1"
                                onClick={() => this.editRecord(record.id)}
                                style={{marginRight: '5px'}}>
                                <i className="fa fa-edit"></i>
                            </button>

                            <button
                                className="btn btn-info btn-sm m-1 text-white"
                                onClick={() => this.assignTask(record.id)}>
                                <i className="fa fa-tasks "></i>
                            </button>
                            <button
                                className="btn btn-danger btn-sm m-1"
                                onClick={() => this.deleteRecord(record.id)}>
                                <i className="fa fa-trash"></i>
                            </button>
                        </Fragment>
                    );
                }
            }
        ];
        this.config = {
            page_size: 10,
            length_menu: [ 10, 20, 50 ],
        }

        this.state = {
            records: [],
            projects: [],
            filterProjects: [],
            is_deleting : false,
            project_category: "default"
        }
        this.handleApprove = this.handleApprove.bind(this)
        this.handleRemoveProject = this.handleRemoveProject.bind(this)
        this.loadprojects = this.loadprojects.bind(this)
        this.onChangeType = this.onChangeType.bind(this)
    }
    componentDidMount(){
        this.loadprojects()
    }
    loadprojects(){
        axios.get("/admin/ajax/projects")
        .then(response =>{
            this.setState({ projects: response.data }, () => {this.handleFilter()});
        })
    }

    editRecord(id){
        this.props.history.push(`/admin/project/edit/${id}`)
    }
    assignTask(id){
        this.props.history.push(`/admin/project/assign/${id}`)
    }
    previewRecord(id){
        this.props.history.push(`/admin/project/view/${id}`)
    }
    handleRemoveProject(id){
        this.setState({
            is_deleting : true
        }, ()=>{
            Axios.post(`/admin/ajax/project/remove/${id}`)
            .then(response =>{
                if(response.data.status == 'success'){
                    this.setState({
                        is_deleting : false
                    }, ()=>{
                        this.loadprojects()
                    })
                    NotificationManager.success("Removed project successfully!", "Success", 3000)
                }
                else{
                    this.setState({
                        is_deleting : false
                    })
                    NotificationManager.error(response.data.message, "Error", 3000)
                }

            }).catch(() =>{
                this.setState({
                    is_deleting : false
                })
                NotificationManager.error("Failed to remove project!", "Error", 3000)
            })
        })
        // alert(id)
    }
    deleteRecord(id){
        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this project?',
            buttons: [
              {
                label: 'Yes',
                onClick: () => this.handleRemoveProject(id)
              },
              {
                label: 'No',
                onClick: () => {

                }
              }
            ]
          });
    }
    handleApprove(e, id, index){
        e.preventDefault()
        e.target.checked = !e.target.checked
        let projects = this.state.projects
        projects[index].is_approved = e.target.checked ? 1 : 0
        this.setState({
            projects : projects
        })
    }

    onChangeType(type) {
        this.setState({project_category: type.target.value}, ()=>{this.handleFilter()})
    }

    handleFilter() {
        const { projects } = this.state;
        let temp = [];
        if (this.state.project_category === 'default') {
            temp = projects.filter(item => item.category != 'retired')
        } else {
            temp = projects.filter(item => item.category === this.state.project_category)
        }

        temp.forEach((item, index) => item.number = index + 1);

        this.setState({ filterProjects: temp });
    }

    render(){

        return <div className="animated fadeIn">
                <LoadingOverlay
                        active={this.state.is_deleting}
                        spinner={<BounceLoader />}
                    >

                </LoadingOverlay>
                <Row>
                    <Col xs="12">
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"></i><strong>projects</strong>
                                <div className="card-header-actions">
                                <Link color="primary" className="btn-pill btn btn-primary" to="/admin/project/create">
                                    <i className="icon-plus"></i>&nbsp;Create New Project
                                </Link>
                                </div>
                            </CardHeader>
                            <CardBody>
                                <div className="project-filter">
                                    <Input type="select" name="select" id="exampleSelect" onChange={this.onChangeType}>
                                        <option value="default">Default</option>
                                        <option value="winners">Winners</option>
                                        <option value="testing">Testing</option>
                                        <option value="retired">Retired</option>
                                    </Input>
                                </div>
                                <ReactDatatable
                                    config={this.config}
                                    records={this.state.filterProjects}
                                    columns={this.columns}
                                />
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
              </div>
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedProjectList = connect(mapStateToProps)(ProjectList);
export default connectedProjectList;
