import React from 'react';
// const DashboardIndex = React.lazy(() => import('./pages/Dashboard/Index/Index'));
import  DashboardIndex from './pages/Dashboard/Index/Index'
import { Logout } from './pages/Logout/Logout';
import connectedManagers from './pages/Managers/Managers';
import connectedNewManager from './pages/Managers/NewManager';
import connectedEditManager from './pages/Managers/EditManager';
import connectedCreateProject from './pages/Project/CreateProject';
import connectedEditProject from './pages/Project/EditProject';
import connectedProjectList from './pages/Project/ProjectList';
import connectedProjectPreview from './pages/Project/ProjectPreview';
import connectedAssignmentProject from './pages/Project/ProjectAssignment';
import MasterChecklist from './pages/MasterChecklist/MasterChecklist';
import connectedEditEmployee from './pages/Employees/EditEmployee';
import connectedEmployees from './pages/Employees/Employees';
import connectedNewEmployee from './pages/Employees/NewEmployee';
import connectedMasters from './pages/Masters/Masters';
import connectedNewMaster from './pages/Masters/NewMaster';
import connectedEditMaster from './pages/Masters/EditMaster';
import connectedSetup from "./pages/Task/Setup";
import connectedLaunch from "./pages/Task/Launch";
import connectedRefresh from "./pages/Task/Refresh";
import connectedOptimize from "./pages/Task/Optimize";

const routes = [
  { path: '/admin', exact: true, name: 'Administrator' },
  { path: '/admin/masterchecklist', exact: true, name: 'Master Checklist', component: MasterChecklist },
  { path: '/admin/dashboard', name: 'Dashboard', component: DashboardIndex },
  { path: '/admin/users', name: 'Dashboard', component: DashboardIndex },
  { path: '/admin/manager', name: 'Managers',exact: true, component: connectedManagers },
  { path: '/admin/manager/add', name: 'New Manager',exact: true, component: connectedNewManager },
  { path: '/admin/manager/edit/:id', name: 'Edit Manager',exact: true, component: connectedEditManager },
  { path: '/admin/employee', name: 'Managers',exact: true, component: connectedEmployees },
  { path: '/admin/employee/add', name: 'New Manager',exact: true, component: connectedNewEmployee },
  { path: '/admin/employee/edit/:id', name: 'Edit Manager',exact: true, component: connectedEditEmployee },
  { path: '/admin/master', name: 'Managers',exact: true, component: connectedMasters },
  { path: '/admin/master/add', name: 'New Manager',exact: true, component: connectedNewMaster },
  { path: '/admin/master/edit/:id', name: 'Edit Manager',exact: true, component: connectedEditMaster },
  { path: '/admin/project', name: 'All Projects',exact: true, component: connectedProjectList },
  { path: '/admin/project/create', name: 'Create New Project',exact: true, component: connectedCreateProject },
  { path: '/admin/project/edit/:id', name: 'Edit Project',exact: true, component: connectedEditProject },
  { path: '/admin/project/view/:id', name: 'Edit Project',exact: true, component: connectedProjectPreview },
  { path: '/admin/project/assign/:id', name: 'Assignment Project',exact: true, component: connectedAssignmentProject },
  { path: '/admin/task/setup', name: 'Setup', component: connectedSetup },
  { path: '/admin/task/launch', name: 'Launch', component: connectedLaunch },
  { path: '/admin/task/refresh', name: 'Refresh', component: connectedRefresh },
  { path: '/admin/task/optimize', name: 'Optimize', component: connectedOptimize },
  { path: '/admin/logout', name: 'Logout', component: Logout },
];
export default routes;
