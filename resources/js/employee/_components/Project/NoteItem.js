import React from 'react'
import {
    Form, Label, Col, Input, FormGroup,
    InputGroup,
    InputGroupAddon,
    Button
} from 'reactstrap'
import Axios from 'axios';
import { confirmAlert } from 'react-confirm-alert'
import moment from 'moment'
import renderHTML from 'react-render-html'

export class NoteItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            is_editing: null,
            new_notes: [],
            edit_notes: [],
        }
        this.addNewNote = this.addNewNote.bind(this)
        this.editNewNote = this.editNewNote.bind(this)
        this.saveNewNote = this.saveNewNote.bind(this)
        this.cancelNewNote = this.cancelNewNote.bind(this)

        this.removeNote = this.removeNote.bind(this)
        this.editNote = this.editNote.bind(this)
        this.cancelEditNote = this.cancelEditNote.bind(this)
        this.saveEditNote = this.saveEditNote.bind(this)
        this.chaneEditNote = this.chaneEditNote.bind(this)
        this.handleRemoveNote = this.handleRemoveNote.bind(this)
    }
    addNewNote() {
        let new_note = {
            node_type: this.props.type,
            node_id: this.props.task.node_id,
            notes: ''
        }
        let new_notes = this.state.new_notes
        new_notes.push(new_note)
        this.setState({
            new_notes: new_notes
        })
    }
    editNewNote(e, index) {
        let new_notes = this.state.new_notes
        new_notes[index].notes = e.target.value
        this.setState({
            new_notes: new_notes
        })
    }
    saveNewNote(index) {
        let url = '/ajax/newnote'
        let note = this.state.new_notes[index]
        Axios.post(url, note)
            .then(() => {
                this.props.reload()
                let notes = this.state.new_notes
                notes.splice(index, 1)
                this.setState({
                    notes: notes
                })
            })
    }

    cancelNewNote(index) {
        let notes = this.state.new_notes
        notes.splice(index, 1)
        this.setState({
            new_notes: notes
        })
    }

    removeNote(id) {
        confirmAlert({
            title: 'Are you sure?',
            message: 'Would you like to remove this item from the list?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.handleRemoveNote(id)
                },
                {
                    label: 'No',
                    onClick: () => {
                    }
                }
            ]
        });
    }

    handleRemoveNote(id) {
        let url = '/ajax/removenote'
        Axios.post(url, { note_id: id })
            .then(() => {
                this.props.reload()
            })
    }

    editNote(note) {
        let edit_note = {
            note_id: note.id,
            notes: note.notes,
        }
        let edit_notes = this.state.edit_notes
        edit_notes.push(edit_note)
        this.setState({
            edit_notes: edit_notes
        })
    }

    cancelEditNote(index) {
        let notes = this.state.edit_notes
        notes.splice(index, 1)
        this.setState({
            edit_notes: notes
        })
    }

    saveEditNote(index) {
        let url = '/ajax/editnote'
        let note = this.state.edit_notes[index]
        Axios.post(url, note)
            .then(response => {
                this.props.reload()
                let edit_notes = this.state.edit_notes
                edit_notes.splice(index, 1)
                this.setState({
                    edit_notes: edit_notes
                })
            })
    }

    chaneEditNote(e, index) {
        let edit_notes = this.state.edit_notes
        edit_notes[index].notes = e.target.value
        this.setState({
            edit_notes: edit_notes
        })
    }

    urlParser(notes) {
        var res = notes.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
        if (res !== null) {
            var unique = res.filter(function(elem, index, self) {
                return index === self.indexOf(elem);
            })
            unique.forEach(item => {
                let newItem = `<a href="${item.includes('https://') ? item : (item.includes('http://') ? item : 'https://' + item) }" target="_blank"><b>${item}</b></a>`;
                notes = notes.replace(item, newItem);
            });
        }

        notes = notes.replace(/\n/g, "<br>");
        return notes;
    }

    render() {
        let new_notes_input = this.state.new_notes.map((new_note, index) => {
            return <FormGroup row key={index}>
                <Col md="12">
                    <InputGroup>
                        <Input type="textarea" id="input2-group2" name="input2-group2" value={new_note.notes} placeholder="Add Notes" onChange={(event) => this.editNewNote(event, index)} />
                        <InputGroupAddon addonType="append">
                            <Button type="button" color="primary" onClick={() => this.saveNewNote(index)} >Save</Button>
                            <Button type="button" onClick={() => this.cancelNewNote(index)} >Cancel</Button>
                        </InputGroupAddon>
                    </InputGroup>
                </Col>
            </FormGroup>
        })


        return <Form>
            <div>{this.props.task.employee_check_list_note.map((note, index) => {
                let i = this.state.edit_notes.findIndex(edit_note => edit_note.note_id === note.id)
                if (i === -1) {
                    return <div key={index} className="alert alert-primary" role="alert">
                        {renderHTML(this.urlParser(note.notes))}
                        <div className="pull-right">
                            (by <strong>{note.employee_details ? note.employee_details.full_name : note.manager_details ? note.manager_details.full_name : ''}</strong> at {moment(note.created_at).format("YYYY-MM-DD HH:mm")})
                            <i style={{ padding: '0px 4px' }} onClick={() => { this.editNote(note) }} className="fa fa-pencil"></i>
                            <i style={{ padding: '0px 4px' }} onClick={() => { this.removeNote(note.id) }} className="fa fa-trash "></i>
                        </div>
                    </div>
                } else {
                    return <FormGroup key={index} row>
                        <Col md="12">
                            <InputGroup>
                                <Input type="textarea" id="input2-group2" name="input2-group2" value={this.state.edit_notes[i].notes} onChange={(event) => this.chaneEditNote(event, i)} />
                                <InputGroupAddon addonType="append">
                                    <Button type="button" color="primary" onClick={() => this.saveEditNote(i)} >Save</Button>
                                    <Button type="button" onClick={() => this.cancelEditNote(i)} >Cancel</Button>
                                </InputGroupAddon>
                            </InputGroup>
                        </Col>
                    </FormGroup>
                }
            })
            }</div>
            {new_notes_input}
        </Form>
    }
}
