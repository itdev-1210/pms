import React from 'react'
import 'react-sortable-tree/style.css'; // This only needs to be imported once in your app
import SortableTree, { getVisibleNodeCount , changeNodeAtPath,  addNodeUnderParent, removeNodeAtPath }  from "react-sortable-tree"

import {
    Button,
    Card,
    CardBody,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
} from 'reactstrap';
export default class ChecklistItem extends React.Component {
    constructor(props){
        super(props)
    }
    render() {
        const count = getVisibleNodeCount({treeData:this.props.checklist.tasks})
        const getNodeKey = ({ treeIndex }) => treeIndex;
        console.log(this.props.checklist.tasks)
        return (
                <Row id={"checklist_" + this.props.index}>
                    <Col xs="12">
                        <Card>
                            <CardHeader>
                                <i className="icon-user-follow"></i><strong>{this.props.checklist.name != "" ? (this.props.index + 1) + ". " + this.props.checklist.name : "New Checklist"}</strong>
                                <div className="card-header-actions">
                                {
                                    this.props.index != 0 && <Button color="primary" className="btn-pill" onClick={()=>this.props.handleUp(this.props.index)}>
                                    <i className="fa fa-arrow-up"></i>&nbsp;Up
                                </Button>
                                }
                                {
                                    this.props.length - 1 != this.props.index &&  <Button color="primary" className="btn-pill"  onClick={()=>this.props.handleDown(this.props.index)}>
                                    <i className="fa fa-arrow-down"></i>&nbsp;Down
                                </Button>
                                }
                                {/* <Button color="success" className="btn-pill"  onClick={()=>this.props.handleAddTask(this.props.index)}>
                                    <i className="fa fa-plus"></i>&nbsp;Add Task
                                </Button> */}
                                <Button color="danger" className="btn-pill" onClick={()=>this.props.handleRemove(this.props.index)}>
                                <i className="icon-trash"></i>&nbsp;Remove
                                </Button>
                                </div>
                            </CardHeader>
                            <CardBody>
                                <FormGroup row  >
                                    <Label sm="3" lg="2" htmlFor="checklist_name">Name</Label>
                                    <Col sm="9" lg="10">
                                        <Input type="text" placeholder="Name" name="name" value={this.props.checklist.name} onChange={(e) => this.props.handleChange(e, this.props.index)} />
                                    </Col>
                                </FormGroup>
                                <FormGroup row  >
                                    <Label sm="3" lg="2" htmlFor="checklist_description">Description</Label>
                                    <Col sm="9" lg="10">
                                        <Input type="textarea" placeholder="Description" name="description" value={this.props.checklist.description} onChange={(e) => this.props.handleChange(e, this.props.index)} />
                                    </Col>
                                </FormGroup>
                                <div style={{height: count * 65 + 50}}>
                                {
                                    count != 0 && <SortableTree
                                    treeData={this.props.checklist.tasks}
                                    onChange={(treeData)=>this.props.handleChangeTaskTree(treeData, this.props.index)}
                                    shouldCopyOnOutsideDrop = {false}
                                    dndType={"taskTree"}
                                    generateNodeProps={({ node, path }) => ({
                                        title: (
                                            <input
                                            placeholder="Please input task title"
                                                className="form-control"
                                              value={node.title}
                                              onChange={event => {
                                                const title = event.target.value;
                                                this.props.handleChangeTaskTree(changeNodeAtPath({
                                                    treeData: this.props.checklist.tasks,
                                                    path,
                                                    getNodeKey,
                                                    newNode: { ...node, title },
                                                  }), this.props.index)
                                              }}
                                            />
                                          ),
                                        buttons: [
                                            // <Button><i className="fa fa-pencil"></i></Button>,
                                            <Button
                                                onClick={() =>
                                                    this.props.handleChangeTaskTree(
                                                        addNodeUnderParent({
                                                            treeData: this.props.checklist.tasks,
                                                            parentKey: path[path.length - 1],
                                                            expandParent: true,
                                                            getNodeKey,
                                                            newNode: {
                                                            title: ``,
                                                            },
                                                            addAsFirstChild: false,
                                                        }).treeData, this.props.index
                                                    )
                                                }
                                              ><i className="fa fa-plus"></i></Button>,
                                            <Button
                                                onClick={()=>{
                                                    console.log(path, getNodeKey)
                                                    this.props.handleChangeTaskTree(
                                                        removeNodeAtPath({
                                                            treeData: this.props.checklist.tasks,
                                                            path,
                                                            getNodeKey,
                                                          }), this.props.index
                                                    )

                                                }}
                                            ><i className="fa fa-trash"></i></Button>
                                        ]
                                    })}
                                />
                                }
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

        );
    }
}
