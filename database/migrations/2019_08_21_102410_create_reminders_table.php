<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reminders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('employee_id');
            $table->integer('assign_id');
            $table->enum('assigned_by', ['ADMIN', 'MANAGER'])->default('MANAGER');
            $table->integer('node_id');
            $table->string('node_type');
            $table->integer('project_id');
            $table->text('notes');
            $table->string('project');
            $table->string('title');
            $table->integer('is_trigger')->default(0);
            $table->dateTimeTz('reminder_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reminders');
    }
}
