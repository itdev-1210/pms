<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin;
class MasterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest_admin');
    }

    /* Get Master Detail */
     public function get($id){
        return Admin::find($id)->toArray();
    }
    /* Load Masters */
    public function load(){
        $employees = Admin::all()->toArray();
        $i = 0;
        foreach ($employees as $key => $employee) {
            $employees[$key]['number'] = ++$i;
            # code...
        }
        return $employees;
    }
    /* Register new manager */
    public function register(Request $request){
        /* If there is manager who have same email */
        $isSameEmailMaster = Admin::where('email', $request->email)->first();
        if($isSameEmailMaster){
            return array('status' => 'error', 'message' => "Email already has been taken!");
        }
        /* If there is manager who have same username */
        $isSameNamelMaster = Admin::where('email', $request->name)->first();
        if($isSameNamelMaster){
            return array('status' => 'error', 'message' => "Username already has been taken!");
        }


        /* Register new manager */
        $employee = new Admin;
        // $employee->first_name = $request->first_name;
        // $employee->last_name = $request->last_name;
        $employee->name = $request->username;
        $employee->email = $request->email;
        $employee->password = bcrypt($request->password);
        $employee->save();
        return array(
            'status' => 'success',
            'data' => $employee
        );
    }

    /* Update Employee Detail */
    public function update(Request $request){
        $employee = Admin::find($request->id);
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->email = $request->email;
        $employee->name = $request->name;
        $employee->save();
        return array(
            'status' => 'success',
            'message' => 'Updated successfully!'
        );
    }

    /* Remove Employee */
    public function remove($id){
        $employee = Admin::find($id);
        $employee->delete();
        return array(
            'status' => 'success',
            'message' => 'Removed successfully!'
        );
    }
}
