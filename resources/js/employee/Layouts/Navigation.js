export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW',
      },
    },
    {
      name: 'Projects',
      icon: 'fa fa-user-secret',
      children: []
    },
    {
      name: 'Logout',
      url: '/logout',
      icon: 'cui-account-logout'
    },
  ]
}
