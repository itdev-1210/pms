import React from 'react'
import { connect } from 'react-redux';

import {
    Button,
    Card,
    CardBody,
    CardHeader,
    Col,
    Form,
    FormGroup,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Row,
  } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios'
import {NotificationManager} from 'react-notifications';

class NewEmployee extends React.Component{
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleSubmit(e){
        e.preventDefault()
        let post_data = {
            first_name : e.target.first_name.value,
            last_name : e.target.last_name.value,
            username : e.target.username.value,
            email : e.target.email.value,
            password : e.target.password.value
        }
        axios.post('/manager/ajax/employee/register', post_data)
        .then(response =>{
            let data = response.data
            if(data.status == "success"){
                NotificationManager.success("New employee saved!",'Success', 3000);
            }
            else{
                NotificationManager.warning(data.message,'Warning', 3000);
            }
        }).catch(error =>{
            // NotificationManager.error( 'Server connection error!','Error', 3000);
        })
    }
    render(){
        return <div className="animated fadeIn">
                <Row>
                    <Col xs="12">
                        <Card>
                        <CardHeader>
                            <i className="icon-user-follow"></i><strong>New Employee</strong>
                            <div className="card-header-actions">
                            <Link color="danger" className="btn-pill btn btn-danger" to="/manager/employee">
                                <i className="icon-arrow-left"></i>&nbsp;Back
                            </Link>
                            </div>
                        </CardHeader>
                        <CardBody>
                        <Form  method="post" onSubmit={this.handleSubmit}>



                            <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" id="first_name" name="first_name" placeholder="First Name" required />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" id="last_name" name="last_name" placeholder="Last Name" required />
                                </InputGroup>
                            </FormGroup>

                            <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" id="username" name="username" placeholder="Username" required />
                                </InputGroup>
                            </FormGroup>


                            <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-envelope"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="email" id="email" name="email" placeholder="Email"required />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText><i className="fa fa-asterisk"></i></InputGroupText>
                                </InputGroupAddon>
                                <Input type="password" id="password" name="password" placeholder="Password" required autoComplete={false}/>
                                </InputGroup>
                            </FormGroup>
                            <FormGroup className="form-actions">
                                <Button type="submit" size="sm" color="success">Submit</Button>
                            </FormGroup>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
              </div>
    }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedNewEmployee  = connect(mapStateToProps)(NewEmployee);
export default connectedNewEmployee;
