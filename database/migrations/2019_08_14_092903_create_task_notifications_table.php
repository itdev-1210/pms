<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('employee_id');
            $table->integer('assign_id');
            $table->enum('assigned_by', ['ADMIN', 'MANAGER'])->default('MANAGER');
            $table->integer('node_id');
            $table->string('node_type');
            $table->integer('project_id');
            $table->string('title');
            $table->string('project');
            $table->string('name');
            $table->integer('is_started')->default(0);
            $table->integer('is_completed')->default(0);
            $table->integer('is_show')->default(0);
            $table->integer('week_number');
            $table->integer('year');
            $table->dateTimeTz('started_at')->nullable();
            $table->dateTimeTz('completed_at')->nullable();
            $table->integer('task_order')->default(0);
            $table->integer('project_order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_notifications');
    }
}
