import React from 'react'
import {
    Col, ListGroupItem, Label} from 'reactstrap';
import { Checkbox } from 'semantic-ui-react'
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import Axios from 'axios';
import AddNoteItem from './AddNoteItem';
export class ProjectDetail extends React.Component {
    constructor(props) {
        super(props)
        this.startTask = this.startTask.bind(this)
        this.completeTask = this.completeTask.bind(this)
        this.handleRightClick = this.handleRightClick.bind(this)
    }
    startTask(task) {

        let post_data = {
            node_type: 'task',
            node_id: task.id,
            project_id: task.project_id,
        }
        Axios.post('/ajax/startTask', post_data)
            .then(response => {
                console.log(response)
                this.props.reload()
            })
    }
    completeTask(task) {
        let post_data = {
            node_type: 'task',
            node_id: task.id,
            project_id: task.project_id,
        }
        Axios.post('/ajax/completeTask', post_data)
            .then(response => {
                console.log(response)
                this.props.reload()
            })
    }
    renderTasks(tasks) {
        return <ul>
            {
                tasks && tasks.map((task, index) => {
                    return <li key={index}>

                        {
                            task.children.length == 0 ?
                                task.is_started ? <Checkbox label={task.task_name} checked={task.is_completed} onChange={() => this.completeTask(task)} disabled={task.is_completed} /> : <ContextMenuTrigger id={"some_unique_identifier" + this.props.part.id} collect={() => {
                                    this.handleRightClick({
                                        node_type: 'task',
                                        node_id: task.id
                                    })
                                }}><Label onClick={() => this.startTask(task)}><i className="fa fa-play text-primary"></i>&nbsp;{task.task_name}</Label></ContextMenuTrigger> : task.task_name
                        }


                        <div>
                            {
                                task.is_started ? <div><Label>Started At: <i className="fa fa-calendar"></i>{task.started_at.split(" ")[0]} <i className="fa fa-clock-o"></i>{task.started_at.split(" ")[1]}</Label> </div> : null
                            }
                            {
                                task.is_completed ? <div> <Label>Completed At: <i className="fa fa-calendar"></i>{task.completed_at.split(" ")[0]} <i className="fa fa-clock-o"></i>{task.completed_at.split(" ")[1]}</Label> </div> : null
                            }
                            <AddNoteItem callId="employee" type={"task"} note = {task} reload = {this.props.reload}/>
                        </div>
                        <ul>
                            {this.renderTasks(task.children)}
                        </ul>
                    </li>
                })
            }
        </ul>
    }
    handleRightClick(node) {
        this.setState({
            active_node_type: node.node_type,
            active_node_id: node.node_id,
        }, () => { })
    }
    render() {
        return <Col xs="12">
            <ContextMenu id={"some_unique_identifier" + this.props.part.id}>
                <MenuItem onClick={this.startTask}>
                    <ListGroupItem style={{ cursor: 'pointer' }}><a >Start Task</a></ListGroupItem>
                </MenuItem>
            </ContextMenu>
            <h4 className="font-weight-bold" id={"part_" + this.props.part.id}>Part {this.props.part.sort_number + 1}: {this.props.part.part_name}</h4>
            <AddNoteItem callId="employee" type="project_part" note={this.props.part} reload = {this.props.reload}/>
            <ul style={{ listStyle: 'decimal' }}>
                {this.props.part.checklists && this.props.part.checklists.map((checklist, index) => {
                    return <li key={index} value={checklist.duplicated_from}>

                        <h5 className="font-weight-bold" >{checklist.name}</h5>
                        <AddNoteItem callId="employee" type="checklist" note={checklist} reload = {this.props.reload}/>
                        {this.renderTasks(checklist.tasks)}

                    </li>
                })}
            </ul>
        </Col>
    }
}
